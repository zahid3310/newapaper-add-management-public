<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();
Route::get('/home', 'HomeController@home')->name('home');
Route::get('/edit-profile/{id}', 'HomeController@editProfile')->name('edit_profile');
Route::post('/update-profile/{id}', 'HomeController@updateProfile')->name('update_profile');
Route::get('/edit-agent-profile/{id}', 'HomeController@editAgentProfile')->name('edit_agent_profile');
Route::post('/update-agent-profile/{id}', 'HomeController@updateAgentProfile')->name('update_agent_profile');

Route::get('/done/{id}', 'HomeController@done')->name('home_reminder_done');
Route::get('/later/{id}', 'HomeController@later')->name('home_reminder_later');
Route::get('/reminder-delete/{id}', 'HomeController@reminderDelete')->name('home_reminder_delete');

//Routes For Notice Board Crude
Route::get('/notice-board', 'HomeController@indexNoticeBoard')->name('notice_board_index');
Route::post('/store-notice-board', 'HomeController@storeNoticeBoard')->name('notice_board_store');
Route::get('/edit-notice-board/{id}', 'HomeController@editNoticeBoard')->name('notice_board_edit');
Route::post('/update-notice-board', 'HomeController@updateNoticeBoard')->name('notice_board_update');
Route::get('/delete-notice-board/{id}', 'HomeController@deleteNoticeBoard')->name('notice_board_delete');
Route::get('/download-notice-board/{id}', 'HomeController@downloadNoticeBoard')->name('notice_board_download');

//Money Receipt
Route::get('/money-receipt', 'HomeController@moneyReceipt')->name('money_receipt');
Route::get('/money-receipt-show/{id}', 'HomeController@moneyReceiptShow')->name('money_receipt_show');