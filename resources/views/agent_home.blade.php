@extends('layouts.agent_app')

@section('title', 'Dashboard')

@section('content')

<section class="content-header">
	<div class="box-body">
	    @if(session('message'))
		<div class="alert callout callout-info text-center alert-dismissable fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<p>{{session('message')}}</p>
		</div>
		@endif

		@if(session('error_message'))
		<div class="alert callout callout-danger text-center alert-dismissable fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<p>{{session('error_message')}}</p>
		</div>
		@endif

		<div class="box box-default" style="border:none;">
			<div class="box-body text-center">
				<h3>Welcome !</h3>
				<hr>
				<p>Hello, <span style="color: blue">{{Auth::user()->name}}</span>. You are successfully logged in to <a style="color: green" href="http://24dhaka.com" target="_blank">dainikdhakareport.net</a></p>
			</div>
		</div>
	</div>

	<div class="box-body">
		<div class="col-md-12 col-sm-12">
			<h2 style="text-align: center">Notice Board</h2>
		</div>
	    <div style="padding: 0px" class="col-md-12 col-sm-12 customAlign table-responsive">
			<table class="table table-bordered table-striped dataTable">
				<thead>
					<tr style="background-color: #F9E79F">
						<th style="width: 5%">SL</th>
						<th style="width: 15%">Date</th>
						<th style="width: 70%">Title</th>
						<th style="width: 10%">Action</th>
					</tr>
				</thead>
				<tbody>
					@if(!empty($notices) && (count($notices)>0) )
					@foreach($notices as $key => $notice)
					<tr>
						<td>{{ $key + 1 }}</td>
						<td>{{ date('d-m-Y', strtotime($notice->created_at)) }}</td>
						<td>{{ $notice->title }}</td>
						<td>
							<a href="{{ 'assets/images/notice/'.$notice->file_url }}" class="btn btn-success btn-xs" target="_blank"><i title="Edit" class="fa fa-download" aria-hidden="true"></i></a>
						</td>
					</tr>
					@endforeach
					@endif
				</tbody>
			</table>
		</div>
	</div>
</section>

@endsection