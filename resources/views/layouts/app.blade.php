<!DOCTYPE html>
<html>

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  
  <!-- For Cache Clear jquery -->
  <meta http-equiv='cache-control' content='no-cache'>
  <meta http-equiv='expires' content='0'>
  <meta http-equiv='pragma' content='no-cache'>

  <title>@yield('title')</title>

  <link rel="stylesheet" href="{{ asset('assets/assets/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/assets/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/assets/bower_components/Ionicons/css/ionicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/assets/dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/assets/bower_components/select2/dist/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/assets/dist/css/skins/_all-skins.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link rel="stylesheet" href="{{ asset('assets/assets/dist/css/skins/_all-skins.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/assets/plugins/iCheck/square/blue.css') }}">
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular.js"></script>
  <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.css"/>
  <script src="{{ asset('assets/assets/js/app.js') }}"></script>
  <link rel="stylesheet" href="{{ asset('assets/assets/dataTable/dataTables.bootstrap.css') }}">
  <style>
        .datepicker{
            z-index: 1500 !important;
        }
  </style>
</head>

<body class="hold-transition skin-blue sidebar-mini">

  <div class="wrapper">
    <header class="main-header">
      <!-- Logo -->
      <a href="{{ url('/home') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>D</b>DR</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Dainik Dhaka Report</b></span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                @if(!empty(Auth::user()->photo))
                <img src="{{asset('assets/images/Users/'.Auth::user()->photo)}}" class="user-image" alt="User Image">
                @else
                <img src="{{asset('assets/images/default-customer-photo.png')}}" class="user-image" alt="User Image">
                @endif
                <span class="hidden-xs">@php $name = explode(' ', Auth::user()->name); echo $name[0]; @endphp</span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">

                  @if(!empty(Auth::user()->photo))
                  <img src="{{asset('assets/images/Users/'.Auth::user()->photo)}}" class="img-circle" alt="User Image">
                  @else
                  <img src="{{asset('assets/images/default-customer-photo.png')}}" class="img-circle" alt="User Image">
                  @endif

                  <p>
                    {{Auth::user()->name}}
                    <small>
                      {{Auth::user()->created_at->format("l, d M Y")}}
                    </small>
                  </p>
                </li>
                <!-- Menu Body -->

                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="{{url('edit-profile/'.Auth::user()->id)}}" class="btn btn-default btn-flat">Edit Profile</a>
                  </div>
                  <div class="pull-right">
                    <a class="btn btn-default btn-flat" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> Logout </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                  </div>
                </li>

              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </header>

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            @if(!empty(Auth::user()->photo))
            <img src="{{asset('assets/images/Users/'.Auth::user()->photo)}}" class="img-circle" alt="User Image">
            @else
            <img src="{{asset('assets/images/default-customer-photo.png')}}" class="img-circle" alt="User Image">
            @endif
          </div>
          <div class="pull-left info">
            <p>{{Auth::user()->name}}</p>
            <a style="text-decoration: none" href="#"><i class="fa fa-circle text-success"></i>Online</a>
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">MAIN NAVIGATION</li>

          <li class="{{ Route::currentRouteName() == 'home' ? 'active' : '' }}">
            <a style="text-decoration: none" href="{{url('/home')}}">
              <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            </a>
          </li>

          <li class="{{ Route::currentRouteName() == 'items_index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'items_edit' ? 'active' : '' }} || {{ Route::currentRouteName() == 'items_create' ? 'active' : '' }}"><a style="text-decoration: none" href="{{ route('items_index') }}"><i class="fa fa-bullhorn"></i> Advertisements</a></li>

          <li class="{{ Route::currentRouteName() == 'agents_index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'agents_create' ? 'active' : '' }} || {{ Route::currentRouteName() == 'agents_edit' ? 'active' : '' }} || {{ Route::currentRouteName() == 'reporter_area_index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'reporter_area_create' ? 'active' : '' }} || {{ Route::currentRouteName() == 'reporter_area_edit' ? 'active' : '' }} treeview">
            <a style="text-decoration: none" href="#">
              <i class="fa fa-users"></i><span> Agents</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="{{ Route::currentRouteName() == 'reporter_area_index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'reporter_area_edit' ? 'active' : '' }} || {{ Route::currentRouteName() == 'reporter_area_create' ? 'active' : '' }}"><a style="text-decoration: none" href="{{ route('reporter_area_index') }}"> <i class="fa fa-map-marker"></i> Agents Area</a></li>

              <li class="{{ Route::currentRouteName() == 'agents_index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'agents_edit' ? 'active' : '' }} || {{ Route::currentRouteName() == 'agents_create' ? 'active' : '' }}"><a style="text-decoration: none" href="{{ route('agents_index') }}"><i class="fa fa-users"></i> Manage Agents</a></li>
            </ul>
          </li>

          <li class="{{ Route::currentRouteName() == 'customers_index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'customers_edit' ? 'active' : '' }} || {{ Route::currentRouteName() == 'customers_create' ? 'active' : '' }}"><a style="text-decoration: none" href="{{ route('customers_index') }}"><i class="fa fa-plus"></i> Customers</a></li>

          <li class="{{ Route::currentRouteName() == 'invoices_index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'invoices_create' ? 'active' : '' }} || {{ Route::currentRouteName() == 'invoices_edit' ? 'active' : '' }} || {{ Route::currentRouteName() == 'invoices_show' ? 'active' : '' }} treeview">
            <a style="text-decoration: none" href="#">
              <i class="fa fa-paperclip"></i><span> Invoices</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="{{ Route::currentRouteName() == 'invoices_create' ? 'active' : '' }}"><a style="text-decoration: none" href="{{ route('invoices_create') }}"><i class="fa fa-plus"></i> Create Invoice</a></li>
              <li class="{{ Route::currentRouteName() == 'invoices_index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'invoices_edit' ? 'active' : '' }} || {{ Route::currentRouteName() == 'invoices_show' ? 'active' : '' }}"><a style="text-decoration: none" href="{{ route('invoices_index') }}"><i class="fa fa-list"></i> Invoices List</a></li>
            </ul>
          </li>

          <li class="{{ Route::currentRouteName() == 'payment_receive_create' ? 'active' : '' }}"><a style="text-decoration: none" href="{{ route('payment_receive_create') }}"><i class="fa fa-money"></i> Payments</a></li>
           
          <li class="{{ Route::currentRouteName() == 'money_receipt' ? 'active' : '' }}"><a style="text-decoration: none" href="{{ route('money_receipt') }}"><i class="fa fa-money"></i> Money Receipts</a></li>
           
          <li class="{{ Route::currentRouteName() == 'sales_comission_create' ? 'active' : '' }}"><a style="text-decoration: none" href="{{ route('sales_comission_create') }}"><i class="fa fa-percent"></i> Pay Comission</a></li>
              
          <!-- <li class="treeview">
            <a href="#">
              <i class="fa fa-percent"></i><span> Sales Comission</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class=""><a href=""><i class="fa fa-plus"></i> Sales Comission Payment</a></li>
              <li class=""><a href=""><i class="fa fa-list"></i> Payment History</a></li>
            </ul>
          </li> -->

          <li class="{{ Route::currentRouteName() == 'customer_report_index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'customer_wise_details_report_index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'sales_comission_report_index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'sales_comission_details_report_index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'paynent_report_index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'area_report_index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'area_wise_details_report_index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'paynents_report_index' ? 'active' : '' }} treeview">
            <a style="text-decoration: none" href="#">
              <i class="fa fa-file"></i><span> Reports</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <!-- <li class=""><a href=""><i class="fa fa-plus"></i> All Transaction Report</a></li> -->
              <!-- <li class=""><a href=""><i class="fa fa-list"></i> Personal Tranaction Report</a></li> -->
              <li class="{{ Route::currentRouteName() == 'customer_report_index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'customer_wise_details_report_index' ? 'active' : '' }}"><a style="text-decoration: none" href="{{ route('customer_report_index') }}"><i class="fa fa-file"></i> Customer Report</a></li>
              <!-- <li class=""><a href=""><i class="fa fa-list"></i> Agent Report</a></li> -->
              <li class="{{ Route::currentRouteName() == 'sales_comission_report_index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'sales_comission_details_report_index' ? 'active' : '' }}"><a style="text-decoration: none" href="{{ route('sales_comission_report_index') }}"><i class="fa fa-file"></i> Sales Comission Report</a></li>
              <!-- <li class="{{ Route::currentRouteName() == 'paynent_report_index' ? 'active' : '' }}"><a href="{{ route('paynent_report_index') }}"><i class="fa fa-file"></i> Payment Report</a></li> -->
              <!-- <li class="{{ Route::currentRouteName() == 'paynents_report_index' ? 'active' : '' }}"><a href="{{ route('paynents_report_index') }}"><i class="fa fa-file"></i> Payment Report</a></li> -->
              <li class="{{ Route::currentRouteName() == 'area_report_index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'area_wise_details_report_index' ? 'active' : '' }}"><a style="text-decoration: none" href="{{ route('area_report_index') }}"><i class="fa fa-file"></i> Area Wise Report</a></li>
            </ul>
          </li>  

          <li class="{{ Route::currentRouteName() == 'personal_account_index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'personal_account_edit' ? 'active' : '' }} || {{ Route::currentRouteName() == 'personal_account_category_index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'personal_account_category_create' ? 'active' : '' }} || {{ Route::currentRouteName() == 'personal_account_category_edit' ? 'active' : '' }} || {{ Route::currentRouteName() == 'personal_account_sub_category_index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'personal_account_sub_category_create' ? 'active' : '' }} || {{ Route::currentRouteName() == 'personal_account_sub_category_edit' ? 'active' : '' }}"><a style="text-decoration: none" href="{{ route('personal_account_index') }}"><i class="fa fa-list"></i> Wallet</a></li>

          <li class="{{ Route::currentRouteName() == 'reminder_index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'reminder_edit' ? 'active' : '' }}"><a style="text-decoration: none" href="{{ route('reminder_index') }}"><i class="fa fa-clock-o"></i> Reminder</a></li>

          <li class="{{ Route::currentRouteName() == 'users_index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'users_edit' ? 'active' : '' }} || {{ Route::currentRouteName() == 'users_create' ? 'active' : '' }} treeview">
            <a style="text-decoration: none" href="#">
              <i class="fa fa-cog"></i> <span>Settings</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="{{ Route::currentRouteName() == 'users_create' ? 'active' : '' }}"><a style="text-decoration: none" href="{{ route('users_create') }}"><i class="fa fa-user-o"></i>Create User</a></li>
              <li class="{{ Route::currentRouteName() == 'users_index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'users_edit' ? 'active' : '' }}"><a style="text-decoration: none" href="{{ route('users_index') }}"><i class="fa fa-list"></i>User List </a></li>
            </ul>
          </li>


        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>

    <div class="content-wrapper">
      <!-- Main content -->
      @yield('content')
      <!-- /.content -->
    </div>

    <footer class="main-footer">
      <strong>Copyright &copy; {{date('Y')}} <a href="http://rootsoftbd.com" target="_blank">Root Soft Bangladesh</a>.</strong> All rights reserved.
    </footer>
  </div>

  <!--scrits -->
      <script src="{{ asset('assets/assets/bower_components/jquery/dist/jquery.min.js') }}"></script>
      <script src="{{ asset('assets/assets/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
      <script src="{{ asset('assets/assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
      <script src="{{ asset('assets/assets/bower_components/fastclick/lib/fastclick.js') }}"></script>
      <script src="{{ asset('assets/assets/dist/js/adminlte.min.js') }}"></script>
      <script src="{{ asset('assets/assets/dist/js/demo.js') }}"></script>
      <script src="{{ asset('assets/assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
      <script src="{{ asset('assets/assets/bower_components/fastclick/lib/fastclick.js') }}"></script>
      <script src="{{ asset('assets/assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
      <script src="{{ asset('assets/assets/plugins/iCheck/icheck.min.js') }}"></script>


      <script src="{{ asset('assets/assets/dataTable/dataTables.min.js') }}"></script>
      <script src="{{ asset('assets/assets/dataTable/dataTables.bootstrap.min.js') }}"></script>

      <script>
        $(function () {
          $('#dataTable').DataTable({
            "pageLength": 25,
          });
        });
      </script>

      <script>
        $(function () {
          $('input').iCheck({
                  checkboxClass: 'icheckbox_square-blue',
                  radioClass: 'iradio_square-blue',
              increaseArea: '20%' // optional
          });
        });
      </script>

      <script>
          $(document).ready(function () {
            $('.sidebar-menu').tree()
          })
      </script>

      <script>
          $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
          })
      </script>

      <script>
          $('.datepicker').datepicker({
            autoclose: true,
            format:'yyyy-mm-dd',
          })
      </script>
  <!--scrits -->

  @stack('scripts')

  <input type="hidden" class="site_url" value="{{ url('/') }}" />
</body>

</html>
