<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- For Cache Clear jquery -->
  <meta http-equiv='cache-control' content='no-cache'>
  <meta http-equiv='expires' content='0'>
  <meta http-equiv='pragma' content='no-cache'>
  
  <title>@yield('title')</title>

  <link rel="stylesheet" href="{{ asset('assets/assets/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/assets/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/assets/bower_components/Ionicons/css/ionicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/assets/dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/assets/bower_components/select2/dist/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/assets/dist/css/skins/_all-skins.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link rel="stylesheet" href="{{ asset('assets/assets/dist/css/skins/_all-skins.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/assets/plugins/iCheck/square/blue.css') }}">
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular.js"></script>
  <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.css"/>
  <script src="{{ asset('assets/assets/js/apps.js') }}"></script>
  <link rel="stylesheet" href="{{ asset('assets/assets/dataTable/dataTables.bootstrap.css') }}">
</head>

<body class="hold-transition skin-blue sidebar-mini">

  <div class="wrapper">
    <header class="main-header">
      <!-- Logo -->
      <a href="{{ url('/home') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>D</b>DR</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Dainik</b> Dhaka Report</span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                @if(!empty(Auth::user()->photo))
                <img src="{{asset('assets/images/Users/'.Auth::user()->photo)}}" class="user-image" alt="User Image">
                @else
                <img src="{{asset('assets/images/default-customer-photo.png')}}" class="user-image" alt="User Image">
                @endif
                <span class="hidden-xs">@php $name = explode(' ', Auth::user()->name); echo $name[0]; @endphp</span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">

                  @if(!empty(Auth::user()->photo))
                  <img src="{{asset('assets/images/Users/'.Auth::user()->photo)}}" class="img-circle" alt="User Image">
                  @else
                  <img src="{{asset('assets/images/default-customer-photo.png')}}" class="img-circle" alt="User Image">
                  @endif

                  <p>
                    {{Auth::user()->name}}
                    <small>
                      {{Auth::user()->created_at->format("l, d M Y")}}
                    </small>
                  </p>
                </li>
                <!-- Menu Body -->

                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="{{url('edit-agent-profile/'.Auth::user()->id)}}" class="btn btn-default btn-flat">Edit Profile</a>
                  </div>
                  <div class="pull-right">
                    <a class="btn btn-default btn-flat" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> Logout </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                  </div>
                </li>

              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </header>

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            @if(!empty(Auth::user()->photo))
            <img src="{{asset('assets/images/Users/'.Auth::user()->photo)}}" class="img-circle" alt="User Image">
            @else
            <img src="{{asset('assets/images/default-customer-photo.png')}}" class="img-circle" alt="User Image">
            @endif
          </div>
          <div class="pull-left info">
            <p>{{Auth::user()->name}}</p>
            <a href="#"><i class="fa fa-circle text-success"></i>Online</a>
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">MAIN NAVIGATION</li>

          <li class="{{ Route::currentRouteName() == 'home' ? 'active' : '' }}">
            <a style="text-decoration: none" href="{{url('/home')}}">
              <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            </a>
          </li>

          <li class="{{ Route::currentRouteName() == 'agents_invoices_index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'agents_invoices_show' ? 'active' : '' }}">
            <a style="text-decoration: none" href="{{ route('agents_invoices_index') }}"><i class="fa fa-list"></i> Invoices </a>
          </li>

          <li class="{{ Route::currentRouteName() == 'agents_money_recept_index' ? 'active' : '' }}">
            <a style="text-decoration: none" href="{{ route('agents_money_recept_index') }}"><i class="fa fa-money"></i> Money Receipt </a>
          </li>
        
          <li class="{{ Route::currentRouteName() == 'agents_comission_index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'agents_comission_details' ? 'active' : '' }}">
            <a style="text-decoration: none" href="{{ route('agents_comission_index') }}"><i class="fa fa-percent"></i>Commission</a>
          </li>

        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>

    <div class="content-wrapper">
      <!-- Main content -->
      @yield('content')
      <!-- /.content -->
    </div>

    <footer class="main-footer">
      <strong>Copyright &copy; {{date('Y')}} <a href="http://rootsoftbd.com" target="_blank">Root Soft Bangladesh</a>.</strong> All rights reserved.
    </footer>
  </div>

  <!--scrits -->
      <script src="{{ asset('assets/assets/bower_components/jquery/dist/jquery.min.js') }}"></script>
      <script src="{{ asset('assets/assets/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
      <script src="{{ asset('assets/assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
      <script src="{{ asset('assets/assets/bower_components/fastclick/lib/fastclick.js') }}"></script>
      <script src="{{ asset('assets/assets/dist/js/adminlte.min.js') }}"></script>
      <script src="{{ asset('assets/assets/dist/js/demo.js') }}"></script>
      <script src="{{ asset('assets/assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
      <script src="{{ asset('assets/assets/bower_components/fastclick/lib/fastclick.js') }}"></script>
      <script src="{{ asset('assets/assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
      <script src="{{ asset('assets/assets/plugins/iCheck/icheck.min.js') }}"></script>


      <script src="{{ asset('assets/assets/dataTable/dataTables.min.js') }}"></script>
      <script src="{{ asset('assets/assets/dataTable/dataTables.bootstrap.min.js') }}"></script>

      <script>
        $(function () {
          $('#dataTable').DataTable({
            "pageLength": 25,
          });
        });
      </script>

      <script>
        $(function () {
          $('input').iCheck({
                  checkboxClass: 'icheckbox_square-blue',
                  radioClass: 'iradio_square-blue',
              increaseArea: '20%' // optional
          });
        });
      </script>

      <script>
          $(document).ready(function () {
            $('.sidebar-menu').tree()
          })
      </script>

      <script>
          $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
          })
      </script>

      <script>
          $('.datepicker').datepicker({
            autoclose: true,
            format:'yyyy-mm-dd',
          })
      </script>
  <!--scrits -->

  @stack('scripts')

  <input type="hidden" class="site_url" value="{{ url('/') }}" />
</body>

</html>
