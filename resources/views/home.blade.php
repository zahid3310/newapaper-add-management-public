@extends('layouts.app')

@section('title', 'Dashboard')
<style type="text/css">
	.pagination {
	    margin: 5px 0 !important;
	}

	.small-box h3 {
	    font-size: 25px !important;
	}

</style>
@section('content')
	<section class="content-header">
	    <div style="padding-bottom: 0px;margin-bottom: 0px;padding-left: 15px;padding-right: 15px" class="">
    	  	<div class="box">
    		  <div class="box-body">
    			<div style="margin-left: 15px;margin-right: 15px" class="small-box bg-yellow">
    		        <div style="padding: 2px" class="inner">
    		          <h4 style="text-align: center;padding-bottom: 0px">Today Reminder</h4>
    		        </div>
    		    </div>
    
    		    @if(count($reminders) >0)
    		    <div class="col-md-12 col-sm-12 customAlign table-responsive">
    				<table class="table table-bordered table-striped dataTable">
    					<thead>
    						<tr style="background-color: #F9E79F">
    							<th style="width: 7%">SL</th>
    							<th style="width: 31%">Title</th>
    							<th style="width: 12%">Date</th>
    							<th style="width: 35%">Description</th>
    							<th style="width: 15%;" class="text-center">Action</th>
    						</tr>
    					</thead>
    					<tbody>
    						@if((!empty($reminders)) && (count($reminders) > 0))
    						@foreach($reminders as $key => $reminder)
    							<tr>
    								<td>{{ $key + 1 }}</td>
    								<td>{{ $reminder['name'] }}</td>
    								<td>{{ date('d-m-Y', strtotime($reminder['date'])) }}</td>
    								<td>{{ $reminder['description'] }}</td>
    								<td class="text-right">
    									<a href="{{ route('home_reminder_done', $reminder['id']) }}" class="btn btn-success btn-xs" @if($reminder['reminder_status'] == '1') disabled @endif>Done</a>
    									<a href="{{ route('home_reminder_later', $reminder['id']) }}" class="btn btn-danger btn-xs" @if($reminder['reminder_status'] == '2') disabled @endif>
    									   @if($reminder['reminder_status'] == '2') Pending @else Later @endif
    									</a>
    								    
    								    @if($reminder['file_url'] != null)
    									<a href="{{ asset('assets/images/reminders/'.$reminder['file_url']) }}" class="btn btn-info btn-xs" target="_blank"><i title="Download" class="fa fa-download" aria-hidden="true"></i></a>
    									@endif
    									
    									<a href="{{ route('home_reminder_delete', $reminder['id']) }}" class="btn btn-danger btn-xs confirm_box"><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></a>
    								</td>
    							</tr>
    						@endforeach
    					
    						@endif
    					</tbody>
    				</table>
    			</div>
    			@else
    			<div class="box-body">
    				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
    					<p style="text-align: center;font-size: 18px">No Reminder Set For Today.</p>
    				</div>
    			</div>
    			@endif
    		  </div>
    	  	</div>
    	 </div>
	  
		<div style="padding-bottom: 0px;padding-top: 0px" class="content">
			<div class="box">
				<div style="margin-bottom: 0px" class="box-body">
				    <div class="col-lg-3 col-xs-12">
				      <!-- small box -->
				      <div class="small-box bg-aqua">
				        <div class="inner">
				          <h3> 
				           {{ round($total_receivable, 2) }}
				          </h3>

				          <p>Total Receivable</p>
				        </div>
				        <div class="icon">
				          <i class="fa fa-money"></i>
				        </div>
				      </div>
				    </div>
				    <div class="col-lg-3 col-xs-12">
				      <!-- small box -->
				      <div class="small-box bg-green">
				        <div class="inner">
				          <h3>
				          {{ round($total_received, 2) }}
				         </h3>
				         <p>Total Received</p>
				        </div>
				        <div class="icon">
				          <i class="fa fa-money"></i>
				        </div>
				
				      </div>
				    </div>
				    <div class="col-lg-3 col-xs-12">
				      <!-- small box -->
				      <div class="small-box bg-yellow">
				        <div class="inner">
				          <h3>
				             {{ round($total_expense, 2) }}
				          </h3>
				          <p>Total other Expense</p>
				        </div>
				        <div class="icon">
				          <i class="fa fa-money"></i>
				        </div>
				
				      </div>
				    </div>
				    <div class="col-lg-3 col-xs-12">
				      <!-- small box -->
				      <div class="small-box bg-red">
				        <div class="inner">
				          <h3>
				            {{ round($total_dues, 2) }}
				          </h3>

				          <p>Total Dues</p>
				        </div>
				        <div class="icon">
				          <i class="fa fa-money"></i>
				        </div>
				
				      </div>
				    </div>
				</div>

				<div style="margin-bottom: 0px" class="box-body">
				    <div class="col-lg-3 col-xs-12">
				      <!-- small box -->
				      <div class="small-box bg-aqua">
				        <div class="inner">
				          <h3> 
				           {{ round($total_sales_comission_payable, 2) }}
				          </h3>

				          <p>Sales Comission Payable</p>
				        </div>
				        <div class="icon">
				          <i class="fa fa-money"></i>
				        </div>
				      </div>
				    </div>
				    <div class="col-lg-3 col-xs-12">
				      <!-- small box -->
				      <div class="small-box bg-green">
				        <div class="inner">
				          <h3>
				          {{ round($total_sales_comission_paid, 2) }}
				         </h3>
				         <p>Sales Comission Paid</p>
				        </div>
				        <div class="icon">
				          <i class="fa fa-money"></i>
				        </div>
				
				      </div>
				    </div>
				    <div class="col-lg-3 col-xs-12">
				      <!-- small box -->
				      <div class="small-box bg-yellow">
				        <div class="inner">
				          <h3>
				              {{ round($total_sales_comission_dues, 2) }}
				          </h3>
				          <p>Sales Comission Dues</p>
				        </div>
				        <div class="icon">
				          <i class="fa fa-money"></i>
				        </div>
				
				      </div>
				    </div>
				    <div class="col-lg-3 col-xs-12">
				      <!-- small box -->
				      <div class="small-box bg-red">
				        <div class="inner">
				          <h3>
				            {{ round($total_invoices, 2) }}
				          </h3>

				          <p>Total Invoice</p>
				        </div>
				        <div class="icon">
				          <i class="fa fa-money"></i>
				        </div>
				      </div>
				    </div>
				</div>
			</div>
		</div>
	  

	  <div style="padding-top: 0px" class="content">
	  	<div class="box">
		  <div class="box-body">
			<div style="margin-left: 15px;margin-right: 15px" class="small-box bg-aqua">
		        <div class="inner">
		          <h4 style="text-align: center;margin-top: 0px;margin-bottom: 0px">Notice Board</h4>
		        </div>
		    </div>

		    <div class="col-md-8 col-sm-12 customAlign table-responsive">
				<table class="table table-bordered table-striped dataTable">
					<thead>
						<tr style="background-color: #F9E79F">
							<th style="width: 10%">SL</th>
							<th style="width: 15%">Date</th>
							<th style="width: 65%">Title</th>
							<th style="width: 10%">Action</th>
						</tr>
					</thead>
					<tbody>
						@if((!empty($notices)) && ($notices->count() > 0))
						@foreach($notices as $key => $notice)
							<tr>
								<td>{{ $key + 1 }}</td>
								<td>{{ date('d-m-Y', strtotime($notice->created_at)) }}</td>
								<td>{{ $notice->title }}</td>
								<td>
									<a href="{{ 'assets/images/notice/'.$notice->file_url }}" class="btn btn-success btn-xs" target="_blank"><i title="Edit" class="fa fa-download" aria-hidden="true"></i></a>
									<a href="{{ route('notice_board_delete', $notice->id) }}" class="btn btn-danger btn-xs confirm_box"><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></a>
								</td>
							</tr>
						@endforeach
						<tr>
							<td style="padding: 0px" class="text-right" colspan="4">{{ $notices->links() }}</td>
						</tr>
						@endif
					</tbody>
				</table>
			</div>

			<div style="margin-top: 7px;" class="col-md-4 col-sm-12">
				<div style="padding: 0px" class="col-lg-12 col-xs-12">
					<form class="form-horizontal" method="POST" action="{{ route('notice_board_store') }}" enctype="multipart/form-data">
						{{ csrf_field() }}

				      <div class="small-box bg-aqua">
				        <div style="padding-bottom: 30px" class="inner">
							<h4 style="text-align: center;margin-top: 10px">Create New Notice</h4>
							<div style="padding-bottom: 15px" class="col-md-12 col-sm-12">
								<input id="title" type="text" class="form-control" name="title" placeholder="Title" required="required">
				
								@if ($errors->has('title'))
								<span class="help-block">
									<strong>{{ $errors->first('title') }}</strong>
								</span>
								@endif
							</div>
							<br>
							<br>
							<div style="padding-bottom: 15px" class="col-md-12 col-sm-12">
								<input id="file" type="file" class="form-control" name="file" required="required">
				
								@if ($errors->has('file'))
								<span class="help-block">
									<strong>{{ $errors->first('file') }}</strong>
								</span>
								@endif
							</div>
							<br>
							<br>
							<div class="col-md-12 col-sm-12">
								<button type="submit" class="btn btn-primary add">
									<i class="fa fa-plus-circle"></i>
									Save
								</button>
							</div>
							<br>
							<br>
				      	</div>
				      </div>
				    </form>
				</div>
		  	</div>
		  </div>
	  	</div>
	  </div>

	</section>
@endsection

@push('scripts')
<script type="text/javascript">
	$(".confirm_box").click(function()
	{
		if (!confirm("Do you want to delete"))
		{
			return false;
		}
	});
</script>
@endpush