@extends('layouts.agent_app')

@section('title', 'Edit Profile')

@section('content')
	<section class="content-header">
		<h1>
			Edit User
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i>
				Home 
			</a></li>
			<li><a href="#">
				Users
			</a></li>
			<li class="active">
				Edit User
			</li>
		</ol>
	</section>

	<section class="content">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
				Edit User
				</h3>
			</div>
			<div class="box-body">
				@if(Session::has('message'))
				<div style="padding: 0px" class="col-md-12">
					<div class="alert alert-success alert-dismissable text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{Session::get('message')}}
					</div>
				</div>
				@endif
				@if(Session::has('errors'))
				<div style="padding: 0px" class="col-md-12">
					<div class="alert alert-success alert-dismissable text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{Session::get('errors')}}
					</div>
				</div>
				@endif
				<form class="form-horizontal" method="POST" action="{{ route('update_agent_profile', $user->id) }}" enctype="multipart/form-data">
					{{ csrf_field() }}

					<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} name">
						<label for="name" class="col-md-4 control-label">
						Name *
						</label>

						<div class="col-md-6">
							<input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}" >

							@if ($errors->has('name'))
							<span class="help-block">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
						<label for="photo" class="col-md-4 control-label">
						Old Photo
						</label>

						<div class="col-md-6">
							@if($user->photo != null)
								<img src="{{ asset('assets/images/Users/'.$user->photo) }}" style="height: 250px; width: 250px">
							@else
								<img src="{{ asset('assets/images/Users/default-customer-photo.png') }}" style="height: 250px; width: 250px">
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
						<label for="photo" class="col-md-4 control-label">
						Photo
						</label>

						<div class="col-md-6">
							<input id="photo" type="file" class="form-control" name="photo">

							@if ($errors->has('photo'))
							<span class="help-block">
								<strong>{{ $errors->first('photo') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<hr>

					<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
			          <label for="password" class="col-md-4 control-label">Password *</label>

			          <div class="col-md-6">
			            <input id="password" type="password" class="form-control" name="password" value="">

			            @if ($errors->has('password'))
			            <span class="help-block">
			              <strong>{{ $errors->first('password') }}</strong>
			            </span>
			            @endif
			          </div>
			        </div>

			        <div class="form-group">
			          <label for="password-confirm" class="col-md-4 control-label">Confirm Password *</label>

			          <div class="col-md-6">
			            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" value="">
			            @if ($errors->has('password_confirmation'))
			            <span class="help-block">
			              <strong>{{ $errors->first('password_confirmation') }}</strong>
			            </span>
			            @endif
			          </div>
			        </div>

					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
							<button type="submit" class="btn btn-primary">
								<i class="fa fa-plus-circle"></i>
							Update
							</button>
							<a href="{{ route('customers_index') }}" class="btn btn-danger">
								<i class="fa fa-trash"></i>
							Cancel
							</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
@endsection
