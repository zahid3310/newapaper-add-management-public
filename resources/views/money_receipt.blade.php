@extends('layouts.app')

@section('title', 'Payment List')

@section('content')
	<style type="text/css">
		.customAlign table thead tr th{
			vertical-align: middle;
		}
		.table-tr-style tr:nth-child(even) {
			background-color: #dddddd;
		}
		.table-tr-style tr:nth-child(odd) {
			background-color: #F2F3F4;
		}
		.total th{
			font-size: 20px
		}
		.select2-container .select2-selection--single{
			height: auto
		}
		.select2-container--default .select2-selection--single .select2-selection__rendered{
			line-height: 20px
		}
		.select2-container .select2-selection--single .select2-selection__rendered{
			margin-top: 0px
		}
	</style>

	<section class="content-header">
		<h1>
			Manage Payment List
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i>
			 	Home
			 </a></li>
			<li><a href="#">
				Payments
			</a></li>
			<li class="active">
				Payment List
			</li>
		</ol>
	</section>

	<section class="content">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
						Payment List
				</h3><br><br>
				<div class="col-xs-12 col-sm-2 pull-right">
					
				</div>	
			</div>

			@if(count($payments) >0)
			<div class="box-body">
				<div class="row">
					@if(Session::has('message'))
					<div class="col-md-12">
						<div class="alert alert-success alert-dismissable text-center">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							{{Session::get('message')}}
						</div>
					</div>
					@endif
					@if(Session::has('errors'))
					<div class="col-md-12">
						<div class="alert alert-success alert-dismissable text-center">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							{{Session::get('errors')}}
						</div>
					</div>
					@endif
				</div>

				<div class="col-md-12 customAlign table-responsive">

					<table id="dataTable" class="table table-bordered text-center table-tr-style">
							<thead>
								<tr style="background-color: #F9E79F">
									<th style="text-align: left">SL</th>
									<th style="text-align: left">Payment Date</th>
									<th style="text-align: left">Invoice Number</th>
									<th style="text-align: left">Paid Through</th>
									<th style="text-align: right">Trans. Paid</th>
									<th style="text-align: right">Other Expense</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@php
								 	$total_paid 	= 0;
								 	$total_expense 	= 0;
								@endphp
								@foreach($payments as $key => $payment)
									@php
										$total_paid 	= $total_paid + $payment->total_amount;
										$total_expense 	= $total_expense + $payment->total_other_expense;
									@endphp
									<tr>
										<td style="text-align: left">{{ $key + 1 }}</td>
										<td style="text-align: left">{{ date('d-m-Y', strtotime($payment->date)) }}</td>
										<td style="text-align: left">{{ $payment->invoice_number }}</td>
										<td style="text-align: left">{{ $payment->payment_through }}</td>
										<td style="text-align: right">{{ number_format($payment->total_amount,2,'.',',') }}</td>
										<td style="text-align: right">{{ number_format($payment->total_other_expense,2,'.',',') }}</td>
										<td>
											<a href="{{ route('money_receipt_show', $payment->id) }}" class="btn btn-info btn-xs" target="_blank"><i title="Show" class="fa fa-eye" aria-hidden="true"></i></a>
										</td>
									</tr>
								@endforeach
								
							</tbody>
							<tr class="total" style="background-color: #F9E79F">
								<th style="text-align: right" colspan="4">Total Transaction</th>
								<th style="text-align: right">{{ number_format($total_paid,2,'.',',') }}</th>
								<th style="text-align: right">{{ number_format($total_expense,2,'.',',') }}</th>
								<th>
									<!-- <a class="btn btn-success btn-xs" href=""><i class="fa fa-download"></i> Download</a> -->
								</th>
							</tr>
						</table>
				</div>
		    </div>
		    @else
		    <div class="box-body">
		    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
		    	<p style="text-align: center;font-size: 18px">No Data Found.</p>
		    </div>
		    </div>
		    @endif
		</div>
	</section>
@endsection

@push('scripts')
	<script type="text/javascript">
		$(".confirm_box").click(function()
		{
			if (!confirm("Do you want to delete"))
			{
				return false;
			}
		});
	</script>
@endpush