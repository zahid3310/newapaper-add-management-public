<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoices extends Model
{
    protected $table = 'invoices';

    public function customer()
    {
        return $this->belongsTo('App\Models\Customers', 'customer_id');
    }

    public function agent()
    {
        return $this->belongsTo('App\Models\Agents', 'agent_id');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users', 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users', 'updated_by');
    }
}
