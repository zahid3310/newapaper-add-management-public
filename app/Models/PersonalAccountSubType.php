<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonalAccountSubType extends Model
{
    protected $table = 'personal_account_sub_type';
    
    public function personalAccountType()
    {
        return $this->belongsTo('App\Models\PersonalAccountType', 'personal_account_type_id');
    }
    
    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users', 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users', 'updated_by');
    }
}
