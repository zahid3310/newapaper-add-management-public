<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Areas extends Model
{
    protected $table = 'areas';

    public function district()
    {
        return $this->belongsTo('App\Models\Districts', 'district_id');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users', 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users', 'updated_by');
    }
}
