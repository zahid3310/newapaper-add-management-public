<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetails extends Model
{
    protected $table = 'invoices_details';

    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoices', 'invoice_id');
    }

    public function item()
    {
        return $this->belongsTo('App\Models\Items', 'item_id');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users', 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users', 'updated_by');
    }
}
