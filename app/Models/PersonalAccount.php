<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonalAccount extends Model
{
    protected $table = 'personal_account';

    public function accountName()
    {
        return $this->belongsTo('App\Models\PersonalAccountType', 'personal_account_type');
    }
    
    public function subAccountName()
    {
        return $this->belongsTo('App\Models\PersonalAccountSubType', 'personal_account_sub_type');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users', 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users', 'updated_by');
    }
}
