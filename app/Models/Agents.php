<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agents extends Model
{
    protected $table = 'agents';

    public function areaName()
    {
        return $this->belongsTo('App\Models\Areas', 'area');
    }

    public function districtName()
    {
        return $this->belongsTo('App\Models\Districts', 'district_id');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users', 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users', 'updated_by');
    }
}
