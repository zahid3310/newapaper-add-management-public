<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
    protected $table = 'transactions';

    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoices', 'invoice_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customers', 'customer_id');
    }

    public function agent()
    {
        return $this->belongsTo('App\Models\Agents', 'agent_id');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users', 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users', 'updated_by');
    }
}
