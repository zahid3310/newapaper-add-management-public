<?php

namespace App\Modules\Notice\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

//Models
use App\Models\Areas;
use App\Models\Districts;
use App\Models\Customers;
use App\Models\Agents;
use App\Models\Notice;

class NoticeBoardController extends Controller
{
    public function index()
    {
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('agents_index');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $agents = Agents::leftjoin('areas', 'areas.id', 'agents.area')
        ->where('agents.status', '=', 1)
        ->orderBy('areas.district_id', 'asc')
        ->selectRaw('agents.*, areas.district_id as district_id')
        ->get();

        return view('agents::index', compact('agents'));
    }

    public function create()
    {
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('agents_create');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $areas = Areas::where('status', '=', 1)->orderBy('district_id', 'asc')->get();
        return view('agents::create', compact('areas'));
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('agents_store');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $this->validate($request,[
           'name'  => 'required',
           ]);

        $agent                  = new Agents;

        if($request->hasFile('photo'))
        {
            $image              = $request->file('photo');
            $image_name         = time().'.'.$image->getClientOriginalExtension();
            $file_path          = 'assets/images/agents/';
            $rowImage           = $image;
            $savingPath         = $file_path;
            $imageName          = $image_name;
            $resizeImage        = $this->compressAndResize($rowImage, $savingPath, $imageName, 50, 250,250);
            $agent->photo       = $image_name;
        }

        $agent->area            = $request->area;
        $agent->name            = $request->name;
        $agent->intro           = $request->intro;
        $agent->code            = $request->code;
        $agent->address         = $request->address;
        $agent->email           = $request->email;
        $agent->mobile          = $request->mobile;
        $agent->email           = $request->email;
        $agent->status          = 1;
        $agent->billing_info    = $request->billing_info;
        $agent->created_by      = Auth::user()->id;

        if ($agent->save())
        {
            return redirect()->route('agents_index')->with('message', 'Successfully added !!');
        }else{
            return back()->with('message', 'Something went wrong !! Please try again.');
        }
    }

    public function edit($id)
    {
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('agents_edit');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $agent      = Agents::find($id);
        $areas      = Areas::where('status', '=', 1)->orderBy('district_id', 'asc')->get();

        return view('agents::edit', compact('agent', 'areas'));
    }

    public function update(Request $request, $id)
    {
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('agents_update');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $this->validate($request,[
           'name'  => 'required',
           ]);

        $agent              = Agents::find($id);

        if($request->hasFile('photo'))
        {   
            if($agent->photo != null)
            {
                unlink('assets/images/agents/'.$agent->photo);
            }

            $image          = $request->file('photo');
            $image_name     = time().'.'.$image->getClientOriginalExtension();
            $file_path      = 'assets/images/agents/';
            $rowImage       = $image;
            $savingPath     = $file_path;
            $imageName      = $image_name;
            $resizeImage    = $this->compressAndResize($rowImage, $savingPath, $imageName, 50, 250,250);
            $agent->photo   = $image_name;
        }

        $agent->area            = $request->area;
        $agent->name            = $request->name;
        $agent->intro           = $request->intro;
        $agent->code            = $request->code;
        $agent->address         = $request->address;
        $agent->email           = $request->email;
        $agent->mobile          = $request->mobile;
        $agent->email           = $request->email;
        $agent->billing_info    = $request->billing_info;
        $agent->updated_by      = Auth::user()->id;

        if ($agent->save())
        {
            return redirect()->route('agents_index')->with('message', 'Successfully added !!');
        }else{
            return back()->with('message', 'Something went wrong !! Please try again.');
        }
    }

    public function destroy($id)
    {
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('agents_delete');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $agent              = Agents::find($id);
        $agent->status      = 0;
        $agent->updated_by  = Auth::user()->id;

        if ($agent->save())
        {
            return redirect()->route('agents_index')->with('message', 'Successfully deleted !!');
        }else{
            return back()->with('message', 'Something went wrong !! Please try again.');
        }

        return view('areas::index');
    }

    public function compressAndResize($source_url, $destination_url, $imageName , $quality, $desired_width, $desired_height)
    {
        $info = getimagesize($source_url);
        if ($info['mime'] == 'image/jpeg')
        {
            list($width, $height) = getimagesize($source_url);
            $src = imagecreatefromjpeg($source_url);
            $dst = imagecreatetruecolor($desired_width, $desired_height);
            imagecopyresampled($dst, $src, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
            imagejpeg($dst, $destination_url.$imageName, $quality);
        }
        elseif ($info['mime'] == 'image/png'){
            list($width, $height) = getimagesize($source_url);
            $src = imagecreatefrompng($source_url);
            $dst = imagecreatetruecolor($desired_width, $desired_height);
            imagecopyresampled($dst, $src, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
            imagejpeg($dst, $destination_url.$imageName, $quality);
        }
        elseif ($info['mime'] == 'image/gif'){
            list($width, $height) = getimagesize($source_url);
            $src = imagecreatefromgif($source_url);
            $dst = imagecreatetruecolor($desired_width, $desired_height);
            imagecopyresampled($dst, $src, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
            imagejpeg($dst, $destination_url.$imageName, $quality);
        }
        return $destination_url.$imageName;
    }
}
