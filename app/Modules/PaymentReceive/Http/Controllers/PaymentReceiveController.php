<?php

namespace App\Modules\PaymentReceive\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Models\Areas;
use App\Models\Districts;
use App\Models\Customers;
use App\Models\Agents;
use App\Models\Items;
use App\Models\Invoices;
use App\Models\InvoiceDetails;
use App\Models\Transactions;
use App\Models\Payments;
use Response;
use DB;

class PaymentReceiveController extends Controller
{
    public function index()
    {
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('payment_receive_index');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $payments = Transactions::where('status', '=', 1)->where('type', '=', 'invoice')->orderBy('date', 'DESC')->get();

        return view('paymentReceive::index', compact('payments'));
    }

    public function show($id)
    {   
        $payments = Payments::join('transactions', 'transactions.payment_id', 'payments.id')
                            ->join('invoices', 'invoices.id', 'transactions.invoice_id')
                            ->where('payments.id', $id)
                            ->selectRaw('GROUP_CONCAT(DISTINCT payments.id) as id,
                                         GROUP_CONCAT(DISTINCT payments.customer_id) as customer_id,
                                         GROUP_CONCAT(DISTINCT transactions.payment_through) as payment_through,
                                         GROUP_CONCAT(DISTINCT payments.date) as date,
                                         GROUP_CONCAT(DISTINCT invoices.invoice_number) as invoice_number,
                                         GROUP_CONCAT(DISTINCT payments.total_amount) as total_amount,
                                         GROUP_CONCAT(DISTINCT payments.amount_bangla) as amount_bangla')
                            ->groupBy('payments.id')
                            ->first();

        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML(view('paymentReceive::show', compact('payments')));
        $mpdf->Output('Receipt', "I");
        
        //return view('paymentReceive::show');
    }
    
    public function testShow($id)
    {   
        $payments = Payments::join('transactions', 'transactions.payment_id', 'payments.id')
                            ->join('invoices', 'invoices.id', 'transactions.invoice_id')
                            ->where('payments.id', $id)
                            ->selectRaw('GROUP_CONCAT(DISTINCT payments.id) as id,
                                         GROUP_CONCAT(DISTINCT payments.customer_id) as customer_id,
                                         GROUP_CONCAT(DISTINCT transactions.payment_through) as payment_through,
                                         GROUP_CONCAT(DISTINCT payments.date) as date,
                                         GROUP_CONCAT(DISTINCT invoices.invoice_number) as invoice_number,
                                         GROUP_CONCAT(DISTINCT payments.total_amount) as total_amount,
                                         GROUP_CONCAT(DISTINCT payments.amount_bangla) as amount_bangla')
                            ->groupBy('payments.id')
                            ->first();

        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML(view('paymentReceive::test_show', compact('payments')));
        $mpdf->Output('Receipt', "I");
        
        //return view('paymentReceive::show');
    }

    public function create()
    {
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('payment_receive_create');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $customer_id    = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $customers      = Customers::where('status', '=', 1)->get();
        $find_customer  = Customers::where('id', $customer_id)->where('status', '=', 1)->first();
        $invoices       = Invoices::where('status', '=', 1)->where('customer_id', $customer_id)->orderBy('created_at', 'ASC')->get();

        $payments       = Payments::join('transactions', 'transactions.payment_id', 'payments.id')
                                    ->join('invoices', 'invoices.id', 'transactions.invoice_id')
                                    ->where('payments.status', '=', 1)
                                    ->where('invoices.status', '=', 1)
                                    ->where('payments.customer_id', $customer_id)
                                    ->selectRaw('GROUP_CONCAT(payments.id) as id,
                                                 GROUP_CONCAT(DISTINCT payments.date) as date,
                                                 GROUP_CONCAT(invoices.invoice_number) as invoice_number,
                                                 GROUP_CONCAT(DISTINCT transactions.payment_through) as payment_through,
                                                 GROUP_CONCAT(DISTINCT payments.total_amount) as total_amount,
                                                 GROUP_CONCAT(DISTINCT payments.total_other_expense) as total_other_expense')
                                    ->groupBy('payments.id')
                                    ->orderBy('payments.date', 'DESC')
                                    ->get();

        $transactions   = Transactions::where('customer_id', $customer_id)
                                        ->where('type', 'invoice')
                                        ->where('status', 1)
                                        ->orderBy('created_at', 'DESC')
                                        ->get();

                                        // dd($payments->toArray());

        $recevable      = round($invoices->sum('total_amount'), 2); 
        $other_expense  = round($invoices->sum('other_expense'), 2);
        $dues           = round($invoices->sum('due_amount'), 2);
        $paid           = round($transactions->sum('trans_paid'), 2);
        $invoice_count  = $invoices->count(); 

        // dd($other_expense); 

        return view('paymentReceive::create', compact('customers', 'find_customer', 'transactions', 'recevable', 'dues', 'paid', 'invoice_count', 'invoices', 'other_expense', 'payments'));
    }

    public function store(Request $request)
    {   
        // dd($request->toArray());
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('payment_receive_store');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $this->validate($request,[
         'invoice_id'         => 'required',
         'date'               => 'required',
         'payment_methode'    => 'required',
         'amount'             => 'required',
        ]);

        $total_invoice_paid     = 0;
        foreach($request->paid as $invoice_paid)
        {
            $total_invoice_paid = $total_invoice_paid + $invoice_paid;
        }

        $total_invoice_exp      = 0;
        foreach($request->expense as $invoice_expense)
        {
            $total_invoice_exp  = $total_invoice_exp + $invoice_expense;
        }

        if($total_invoice_paid > $request->amount)
        {
            return back()->with('message', 'Payment amount is grater than total payment amount !!');
        }

        if($total_invoice_exp > $request->other_expense)
        {
            return back()->with('message', 'Other expense is grater than total other expense !!');
        }
  
        try
        {   
            DB::beginTransaction();

            $payments                           = new Payments;
            $payments->date                     = $request->date;
            $payments->total_amount             = $request->amount;
            $payments->total_other_expense      = $request->other_expense;
            $payments->payment_methode          = $request->payment_methode;
            $payments->paid_through             = $request->payment_through;
            $payments->amount_bangla            = $request->amount_bangla;
            $payments->customer_id              = $request->customer_id_send;
            $payments->status                   = 1;
            $payments->created_by               = Auth::user()->id;

            if($request->hasFile('file_url'))
            {
                $image                  = $request->file('file_url');
                $image_name             = time().'.'.$image->getClientOriginalExtension();
                $file_path              = 'assets/images/payments/';
                $rowImage               = $image;
                $savingPath             = $file_path;
                $imageName              = $image_name;
                $resizeImage            = $this->compressAndResize($rowImage, $savingPath, $imageName, 50, 250,250);
                $payments->file_url     = $image_name;
            }

            if($payments->save())
            {
                foreach ($request->invoice_id as $key => $value)
                {
                    $trans_other_expense_check          = Transactions::where('invoice_id', $value)
                                                                ->whereNotNull('other_expense')
                                                                ->where('status', 1)
                                                                ->first();
                                                                
                    $transactions                       = new Transactions;
                    $transactions->payment_id           = $payments->id;
                    $transactions->date                 = $request->date;
                    $transactions->invoice_id           = $value;
                    $transactions->customer_id          = $request->customer_id_send;
                    $transactions->type                 = 'invoice';
                    $transactions->trans_receivable     = 0;
                    $transactions->trans_paid           = $request->paid[$key];
                    $transactions->payment_methode      = $request->payment_methode;
                    $transactions->payment_through      = $request->payment_through;
                    $transactions->amount_bangla        = $request->amount_bangla;
                    $transactions->status               = 1;
                    $transactions->created_by           = Auth::user()->id;

                    if ($request->expense[$key] != null)
                    {   
                        // if ($trans_other_expense_check != null)
                        // {
                        //     $other_expense_update                   = Transactions::where('id', $trans_other_expense_check->id)
                        //                                                             ->where('status', 1)
                        //                                                             ->first();
                        //     $other_expense_update->other_expense    = null;
                        //     $other_expense_update->updated_by       = Auth::user()->id;
                        //     $other_expense_update->save();

                        //     $transactions->other_expense            = $request->expense[$key];
                        // }
                        // else
                        // {
                            $transactions->other_expense            = $request->expense[$key];
                        // }          
                    }

                    if ($transactions->save())
                    {   
                        $invoices   = Invoices::find($value);
                        $paid       = Transactions::where('invoice_id', $value)
                                                    ->where('type', 'invoice')
                                                    ->where('status', 1)
                                                    ->get();

                        // if( ($request->amount + $request->other_expense) > $invoices->due_amount)
                        // {
                        //     return back()->with('message', 'Payment amount is grater than due amount !!');
                        // }

                        $invoices->due_amount        = $invoices->total_amount - $paid->sum('trans_paid') - $paid->sum('other_expense');
                        if($request->expense[$key] != null)
                        {
                            $invoices->other_expense = $paid->sum('other_expense');
                        }
                        
                        $invoices->save();
                    }
                }
            }

            DB::commit();

            return back()->with('message', 'Payment Successfully added !!');

        }
        catch(\Exception $e)
        {
            DB::rollback();
            $mesg = $e->getMessage();
            return back()->with('message', 'Something went wrong try again !!');
        }     
    }

    public function edit($id)
    {    
    }

    public function update(Request $request, $id)
    {     
    }

    public function destroy($id)
    {
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('payment_receive_delete');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        try
        {  
            DB::beginTransaction();

            $transactions            = Transactions::where('payment_id', $id)->get();
            $payments                = Payments::find($id);
            $payments->status        = 0;
           
            if ($payments->save())
            {

                foreach($transactions as $transaction)
                {   
                    $transaction             = Transactions::find($transaction->id);
                    $transaction->status     = 0;

                    if ($transaction->save())
                    {
                        $invoices       = Invoices::find($transaction->invoice_id);

                        if ($transaction->other_expense != null)
                        {
                            $invoices->due_amount       = $invoices->due_amount + $transaction->trans_paid + $transaction->other_expense;

                            $invoices->other_expense    = $invoices->other_expense - $transaction->other_expense;
                        }
                        else
                        {
                            $invoices->due_amount       = $invoices->due_amount + $transaction->trans_paid;
                        }

                        $invoices->save();
                    }
                }

                DB::commit();
                return back()->with('message', 'Successfully deleted !!');
                
            }
        }
        catch(\Exception $e)
        {
            DB::rollback();
            $mesg = $e->getMessage();
            dd($e);
            return back()->with('message', 'Something went wrong try again !!');
        }
    }

    public function getInvoice($id)
    {
        $data = Invoices::where('customer_id', $id)->get();

        return Response::json($data);
    }

    public function allInvoices($id)
    {    
    }

    public function downloadUplodedFile($id)
    {
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('payment_receive_download_uploaded_file');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $transaction    = Transactions::find($id);

        if($transaction->file_url == null)
        {
            return back()->with('message', 'No file uploaded for this payment.');
        }
        else
        {
            $file_path      = ('assets/images/payments/'.$transaction->file_url);
            $name           = 'payment-receive ';

            return response()->download($file_path, $name);
        }    
    }

    public function compressAndResize($source_url, $destination_url, $imageName , $quality, $desired_width, $desired_height)
    {
        $info = getimagesize($source_url);
        if ($info['mime'] == 'image/jpeg')
        {
            list($width, $height) = getimagesize($source_url);
            $src = imagecreatefromjpeg($source_url);
            $dst = imagecreatetruecolor($desired_width, $desired_height);
            imagecopyresampled($dst, $src, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
            imagejpeg($dst, $destination_url.$imageName, $quality);
        }
        elseif ($info['mime'] == 'image/png'){
            list($width, $height) = getimagesize($source_url);
            $src = imagecreatefrompng($source_url);
            $dst = imagecreatetruecolor($desired_width, $desired_height);
            imagecopyresampled($dst, $src, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
            imagejpeg($dst, $destination_url.$imageName, $quality);
        }
        elseif ($info['mime'] == 'image/gif'){
            list($width, $height) = getimagesize($source_url);
            $src = imagecreatefromgif($source_url);
            $dst = imagecreatetruecolor($desired_width, $desired_height);
            imagecopyresampled($dst, $src, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
            imagejpeg($dst, $destination_url.$imageName, $quality);
        }
        return $destination_url.$imageName;
    }
}



//Previous Code Start
    // <?php

    // namespace App\Modules\PaymentReceive\Http\Controllers;

    // use Illuminate\Http\Request;
    // use App\Http\Controllers\Controller;
    // use Auth;

    //Models
    // use App\Models\Areas;
    // use App\Models\Districts;
    // use App\Models\Customers;
    // use App\Models\Agents;
    // use App\Models\Items;
    // use App\Models\Invoices;
    // use App\Models\InvoiceDetails;
    // use App\Models\Transactions;
    // use Response;
    // use DB;

    // class PaymentReceiveController extends Controller
    // {
    //     public function index()
    //     {
    //         //Users Access Level Start
    //         $helper = new \App\Helpers\AccessLevel;
    //         $access = $helper->test('payment_receive_index');
    //         if($access != 'accepted')
    //         {
    //             return back()->with('message', 'You have not enough permission to do this operation !!');
    //         }
    //         //Users Access Level End

    //         $payments = Transactions::where('status', '=', 1)->where('type', '=', 'invoice')->orderBy('date', 'DESC')->get();

    //         return view('paymentReceive::index', compact('payments'));
    //     }

    //     public function show($id)
    //     {   
    //         $transaction = Transactions::find($id);

    //         $mpdf = new \Mpdf\Mpdf();
    //         $mpdf->WriteHTML(view('paymentReceive::show', compact('transaction')));
    //         $mpdf->Output('Receipt', "I");
            
    //         //return view('paymentReceive::show');
    //     }

    //     public function create()
    //     {
    //         //Users Access Level Start
    //         $helper = new \App\Helpers\AccessLevel;
    //         $access = $helper->test('payment_receive_create');
    //         if($access != 'accepted')
    //         {
    //             return back()->with('message', 'You have not enough permission to do this operation !!');
    //         }
    //         //Users Access Level End

    //         $customer_id    = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
    //         $customers      = Customers::where('status', '=', 1)->get();
    //         $find_customer  = Customers::where('id', $customer_id)->where('status', '=', 1)->first();
    //         $invoices       = Invoices::where('status', '=', 1)->where('customer_id', $customer_id)->get();

    //         $transactions   = Transactions::where('customer_id', $customer_id)
    //                                         ->where('type', 'invoice')
    //                                         ->where('status', 1)
    //                                         ->orderBy('created_at', 'DESC')
    //                                         ->get();

    //         $recevable      = $invoices->sum('total_amount'); 
    //         $other_expense  = $invoices->sum('other_expense');
    //         $dues           = $invoices->sum('due_amount');
    //         $paid           = $transactions->sum('trans_paid');
    //         $invoice_count  = $invoices->count(); 

    //         // dd($other_expense); 

    //         return view('paymentReceive::create', compact('customers', 'find_customer', 'transactions', 'recevable', 'dues', 'paid', 'invoice_count', 'invoices', 'other_expense'));
    //     }

    //     public function store(Request $request)
    //     {
    //         //Users Access Level Start
    //         $helper = new \App\Helpers\AccessLevel;
    //         $access = $helper->test('payment_receive_store');
    //         if($access != 'accepted')
    //         {
    //             return back()->with('message', 'You have not enough permission to do this operation !!');
    //         }
    //         //Users Access Level End

    //         $this->validate($request,[
    //          'invoice_id'         => 'required',
    //          'date'               => 'required',
    //          'payment_methode'    => 'required',
    //          'amount'             => 'required',
    //         ]);
      
    //         try
    //         {   
    //             DB::beginTransaction();

    //             $trans_other_expense_check          = Transactions::where('invoice_id', $request->invoice_id)
    //                                                                 ->whereNotNull('other_expense')
    //                                                                 ->where('status', 1)
    //                                                                 ->first();
                                                                    
    //             $transactions                       = new Transactions;
    //             $transactions->date                 = $request->date;
    //             $transactions->invoice_id           = $request->invoice_id;
    //             $transactions->customer_id          = $request->customer_id_send;
    //             $transactions->type                 = 'invoice';
    //             $transactions->trans_receivable     = 0;
    //             $transactions->trans_paid           = $request->amount;
    //             $transactions->payment_methode      = $request->payment_methode;
    //             $transactions->payment_through      = $request->payment_through;
    //             $transactions->amount_bangla        = $request->amount_bangla;
    //             $transactions->status               = 1;
    //             $transactions->created_by           = Auth::user()->id;

    //             if ($request->other_expense != null)
    //             {   
    //                 if ($trans_other_expense_check != null)
    //                 {
    //                     $other_expense_update                   = Transactions::where('id', $trans_other_expense_check->id)
    //                                                                         ->where('status', 1)
    //                                                                         ->first();
    //                     $other_expense_update->other_expense    = null;
    //                     $other_expense_update->updated_by       = Auth::user()->id;
    //                     $other_expense_update->save();

    //                     $transactions->other_expense            = $request->other_expense;
    //                 }
    //                 else
    //                 {
    //                     $transactions->other_expense            = $request->other_expense;
    //                 }          
    //             }


    //             if($request->hasFile('file_url'))
    //             {
    //                 $image                  = $request->file('file_url');
    //                 $image_name             = time().'.'.$image->getClientOriginalExtension();
    //                 $file_path              = 'assets/images/payments/';
    //                 $rowImage               = $image;
    //                 $savingPath             = $file_path;
    //                 $imageName              = $image_name;
    //                 $resizeImage            = $this->compressAndResize($rowImage, $savingPath, $imageName, 50, 250,250);
    //                 $transactions->file_url = $image_name;
    //             }

    //             if ($transactions->save())
    //             {   
    //                 $invoices               = Invoices::find($request->invoice_id);
    //                 $paid                   = Transactions::where('invoice_id', $request->invoice_id)
    //                                                         ->where('type', 'invoice')
    //                                                         ->where('status', 1)
    //                                                         ->get();

    //                 if( ($request->amount + $request->other_expense) > $invoices->due_amount)
    //                 {
    //                     return back()->with('message', 'Payment amount is grater than due amount !!');
    //                 }

    //                 $invoices->due_amount        = $invoices->total_amount - $paid->sum('trans_paid') - $paid->sum('other_expense');
    //                 if($request->other_expense != null)
    //                 {
    //                     $invoices->other_expense = $request->other_expense;
    //                 }
                    
    //                 $invoices->save();
                    
    //                 DB::commit();

    //                 return back()->with('message', 'Payment Successfully added !!');

    //             }
    //         }
    //         catch(\Exception $e)
    //         {dd($e);
    //             DB::rollback();
    //             $mesg = $e->getMessage();
    //             return back()->with('message', 'Something went wrong try again !!');
    //         }     
    //     }

    //     public function edit($id)
    //     {    
    //     }

    //     public function update(Request $request, $id)
    //     {     
    //     }

    //     public function destroy($id)
    //     {
    //         //Users Access Level Start
    //         $helper = new \App\Helpers\AccessLevel;
    //         $access = $helper->test('payment_receive_delete');
    //         if($access != 'accepted')
    //         {
    //             return back()->with('message', 'You have not enough permission to do this operation !!');
    //         }
    //         //Users Access Level End

    //         try
    //         {  
    //            DB::beginTransaction();

    //            $transactions            = Transactions::find($id);
    //            $transactions->status    = 0;

    //            if ($transactions->save())
    //             {   
    //                 $invoices           = Invoices::find($transactions->invoice_id);

    //                 if ($transactions->other_expense != null)
    //                 {
    //                     $invoices->due_amount       = $invoices->due_amount + $transactions->trans_paid + $transactions->other_expense;
    //                     $invoices->other_expense    = null;
    //                 }
    //                 else
    //                 {
    //                     $invoices->due_amount       = $invoices->due_amount + $transactions->trans_paid;
    //                 }

    //                 $invoices->save();

    //                 DB::commit();

    //                 return back()->with('message', 'Successfully deleted !!');
    //             }else{
    //                 return back()->with('message', 'Something went wrong !! Please try again.');
    //             }
    //         }
    //         catch(\Exception $e)
    //         {
    //             DB::rollback();
    //             $mesg = $e->getMessage();
    //             return back()->with('message', 'Something went wrong try again !!');
    //         }
    //     }

    //     public function getInvoice($id)
    //     {
    //         $data = Invoices::where('customer_id', $id)->get();

    //         return Response::json($data);
    //     }

    //     public function allInvoices($id)
    //     {    
    //     }

    //     public function downloadUplodedFile($id)
    //     {
    //         //Users Access Level Start
    //         $helper = new \App\Helpers\AccessLevel;
    //         $access = $helper->test('payment_receive_download_uploaded_file');
    //         if($access != 'accepted')
    //         {
    //             return back()->with('message', 'You have not enough permission to do this operation !!');
    //         }
    //         //Users Access Level End

    //         $transaction    = Transactions::find($id);

    //         if($transaction->file_url == null)
    //         {
    //             return back()->with('message', 'No file uploaded for this payment.');
    //         }
    //         else
    //         {
    //             $file_path      = ('assets/images/payments/'.$transaction->file_url);
    //             $name           = 'payment-receive ';

    //             return response()->download($file_path, $name);
    //         }    
    //     }

    //     public function compressAndResize($source_url, $destination_url, $imageName , $quality, $desired_width, $desired_height)
    //     {
    //         $info = getimagesize($source_url);
    //         if ($info['mime'] == 'image/jpeg')
    //         {
    //             list($width, $height) = getimagesize($source_url);
    //             $src = imagecreatefromjpeg($source_url);
    //             $dst = imagecreatetruecolor($desired_width, $desired_height);
    //             imagecopyresampled($dst, $src, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    //             imagejpeg($dst, $destination_url.$imageName, $quality);
    //         }
    //         elseif ($info['mime'] == 'image/png'){
    //             list($width, $height) = getimagesize($source_url);
    //             $src = imagecreatefrompng($source_url);
    //             $dst = imagecreatetruecolor($desired_width, $desired_height);
    //             imagecopyresampled($dst, $src, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    //             imagejpeg($dst, $destination_url.$imageName, $quality);
    //         }
    //         elseif ($info['mime'] == 'image/gif'){
    //             list($width, $height) = getimagesize($source_url);
    //             $src = imagecreatefromgif($source_url);
    //             $dst = imagecreatetruecolor($desired_width, $desired_height);
    //             imagecopyresampled($dst, $src, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    //             imagejpeg($dst, $destination_url.$imageName, $quality);
    //         }
    //         return $destination_url.$imageName;
    //     }
    // }
//Previous Code End