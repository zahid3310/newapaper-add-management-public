@extends('layouts.app')

@section('title', 'Create Payment Receive')

@section('content')

	<style type="text/css">
		.customAlign table thead tr th{
			vertical-align: middle;
		}
		.table-tr-style tr:nth-child(even) {
			background-color: #dddddd;
		}
		.table-tr-style tr:nth-child(odd) {
			background-color: #F2F3F4;
		}
		.total th{
			font-size: 20px
		}
		.select2-container .select2-selection--single{
			height: auto
		}
		.select2-container--default .select2-selection--single .select2-selection__rendered{
			line-height: 20px
		}
		.select2-container .select2-selection--single .select2-selection__rendered{
			margin-top: 0px
		}

		.loader {
		  border: 16px solid #f3f3f3;
		  border-radius: 50%;
		  border-top: 16px solid #3498db;
		  width: 120px;
		  height: 120px;
		  -webkit-animation: spin 2s linear infinite; /* Safari */
		  animation: spin 2s linear infinite;
		}

		/* Safari */
		@-webkit-keyframes spin {
		  0% { -webkit-transform: rotate(0deg); }
		  100% { -webkit-transform: rotate(360deg); }
		}

		@keyframes spin {
		  0% { transform: rotate(0deg); }
		  100% { transform: rotate(360deg); }
		}
	</style>

	<section class="content" style="min-height: auto">
		<div class="box" style="margin-bottom: 0px">
			<div class="box-body">
				@if(Session::has('message'))
				<div style="padding: 0px" class="col-md-12">
					<div class="alert alert-success alert-dismissable text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{Session::get('message')}}
					</div>
				</div>
				<hr>
				<hr>
				@endif
				@if(Session::has('errors'))
				<div style="padding: 0px" class="col-md-12">
					<div class="alert alert-danger alert-dismissable text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<b>Warning !</b> {{Session::get('errors')}}
					</div>
				</div>
				<hr>
				<hr>
				@endif
				<!-- SELECT2 EXAMPLE -->
				<!-- <div class="box box-default"> -->
				<div class="box-body">
					<div class="callout callout-info" style="margin-bottom: 0px">
						<div class="row">

							<form class="form-horizontal" method="GET" action="{{ route('payment_receive_create') }}" enctype="multipart/form-data">
								<!-- {{ csrf_field() }} -->

								<label for="name" class="col-md-3 col-xs-12 control-label text-right" style="padding-top: 6px">Search Customer :</label>
								<div class="col-md-7">
									<select id="customer_id" name="customer_id" class="form-control select2" style="width: 100%" onchange="selectEvent()">
										<option value="">--Select Customer--</option>
										@if(!empty($customers))
											@foreach($customers as $key => $customer)
											<option @if(isset($find_customer)) {{ $find_customer['id'] == $customer['id'] ? 'selected' : '' }} @endif value="{{ $customer->id }}">{{ $customer->name }}</option>
											@endforeach
										@endif
									</select>
								</div>

								<div style="display: none" class="col-md-2">
									<button id="searbButton" type="submit" class="btn btn-success btn-block"><i class="fa fa-search"></i> Search 
									</button>
								</div>
							</form>

						</div>
					</div>	
				</div>
			</div>
		</div>
	</section>

	<section class="content" style="padding-top: 8px">

		<div class="row">

			<div class="col-md-4">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title" style="width:100%">Payment Summary</h3>
					</div>
					<div class="box-body">
						<div class="table-responsive">
							<table class="table table-bordered text-center " style="background-color: #ecebeb;margin-bottom: 0px">
								<thead>
									<tr>
										<th style="text-align: left;width: 30%">Customer Name</th>
										<th style="text-align: left;width: 40%">{{ $find_customer['name'] }}</th>
									</tr>

									<tr>
										<th style="text-align: left;width: 30%">Total Receivable</th>
										<th style="text-align: left;width: 40%">{{ number_format($recevable,2,'.',',') }}</th>
									</tr>

									<tr>
										<th style="text-align: left;width: 30%">Total Paid</th>
										<th style="text-align: left;width: 40%">{{ number_format($paid,2,'.',',') }}</th>
									</tr>

									<tr>
										<th style="text-align: left;width: 30%">Other Expense</th>
										<th style="text-align: left;width: 40%">{{ number_format($other_expense,2,'.',',') }}</th>
									</tr>

									<tr>
										<th style="text-align: left;width: 30%">Total Dues</th>
										<th style="text-align: left;width: 40%">{{ number_format($recevable - $paid - $other_expense,2,'.',',') }}</th>
									</tr>

									<tr>
										<th style="text-align: left;width: 30%">Total Invoice</th>
										<th style="text-align: left;width: 40%">{{ $invoice_count }}</th>
									</tr>

								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-8">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title" style="width:100%">Add New Payment</h3>
					</div>
					<div class="box-body">
						<form method="POST" action="{{ route('payment_receive_store') }}" enctype="multipart/form-data">
							{{ csrf_field() }}

							<input type="hidden" class="form-control" name="customer_id_send" value="{{ isset($_GET['customer_id']) ? $_GET['customer_id'] : 0 }}">

							<div class="form-group">
								<div class="col-md-6">
									<label>Amount*</label>
									<input id="amount" class="form-control" type="number" name="amount" required="required" oninput="calculate()">
								</div>

								<div class="col-md-6">
									<label>Other Expense</label>
									<input id="other_expense" class="form-control" type="number" name="other_expense" oninput="calculateExpense()">
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-12 table-responsive">
									<br>
									<table class="table table-bordered text-center table-tr-style">
										<thead>
											<tr>
												<th style="text-align: left">Invoice#</th>
												<th style="text-align: left">Publication Date</th>
												<th style="text-align: left">Amount</th>
												<th style="text-align: left">Due</th>
												<th style="text-align: left">Paid</th>
												<th style="text-align: left">Other Expense</th>
											</tr>
										</thead>

										<tbody>
											@foreach($invoices as $key71 => $invoice_value)
												@if($invoice_value->due_amount > 0)
													<tr>
														<input class="form-control" type="hidden" name="invoice_id[]" value="{{ $invoice_value->id }}">
														<td style="text-align: left">
															{{ $invoice_value->invoice_number }}
														</td>
														<td style="text-align: left">
															{{ $invoice_value->due_date != null ? date('d-m-Y', strtotime($invoice_value->due_date)) : '' }}
														</td>
														<td style="text-align: left">
															{{ round($invoice_value->total_amount, 2) }}
														</td>
														<td style="text-align: left">
															{{ round($invoice_value->due_amount, 2) }}</td>
														<td style="text-align: left">
															<input style="width: 150px" id="paid_{{$key71}}" class="form-control" type="text" name="paid[]">
														</td>
														<td style="text-align: left">
															<input style="width: 150px" id="other_expense_{{$key71}}" class="form-control" type="text" name="expense[]">
														</td>
													</tr>
												@endif
											@endforeach
										</tbody>
									</table>
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-6">
									<br>
									<label>Payment Date*</label>
									<input type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control datepicker" name="date" required="required">
								</div>

								<div class="col-md-6">
									<br>
									<label>Payment Methode*</label>
									<select name="payment_methode" class="form-control" required="required">
										<option value="">--Select Payment Methode--</option>
										<option value="Cash">Cash</option>
										<option value="Bank">Bank</option>
										<option value="Mobile Banking/Others">Mobile Banking/Others</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-6">
									<br>
									<label>Paid Through</label>
									<input class="form-control" type="text" name="payment_through">
								</div>

								<div class="col-md-6">
									<br>
									<label>Amount Bangla</label>
									<input class="form-control" type="text" name="amount_bangla">
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-12">
									<br>
									<label>Upload File</label>
									<input id="file_url" type="file" class="form-control" name="file_url">
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-12">
									<br>
									<button type="submit" class="btn btn-success btn-block" style="border-radius: 0px">Save Payment</button>
									<br>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>

		</div>

			<div class="box-body">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<a style="color: white;border-radius: 0px" class="col-md-5 col-sm-3">
					</a>
					<a id="historyShow" style="color: white;border-radius: 0px" class="btn btn-info col-md-2 col-sm-6 col-xs-12">
					    Show Payment History
					</a>
				</div>

				<br>
				<br>
			</div>

		<div class="box HistoryIndex">
			<div class="box-body">
				<div class="col-md-12 customAlign">
					<hr>
					<div class="table-responsive">
						<table id="dataTable" class="table table-bordered text-center table-tr-style">
							<thead>
								<tr style="background-color: #F9E79F">
									<th style="text-align: left">SL</th>
									<th style="text-align: left">Payment Date</th>
									<th style="text-align: left">Invoice Number</th>
									<th style="text-align: left">Paid Through</th>
									<th style="text-align: right">Trans. Paid</th>
									<th style="text-align: right">Other Expense</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@php
								 	$total_paid 	= 0;
								 	$total_expense 	= 0;
								@endphp
								@foreach($payments as $key => $payment)
									@php
										$total_paid 	= $total_paid + $payment->total_amount;
										$total_expense 	= $total_expense + $payment->total_other_expense;
									@endphp
									<tr>
										<td style="text-align: left">{{ $key + 1 }}</td>
										<td style="text-align: left">{{ date('d-m-Y', strtotime($payment->date)) }}</td>
										<td style="text-align: left">{{ $payment->invoice_number }}</td>
										<td style="text-align: left">{{ $payment->payment_through }}</td>
										<td style="text-align: right">{{ number_format($payment->total_amount,2,'.',',') }}</td>
										<td style="text-align: right">{{ number_format($payment->total_other_expense,2,'.',',') }}</td>
										<td>
											<a href="{{ route('payment_receive_show', $payment->id) }}" class="btn btn-info btn-xs" target="_blank"><i title="Show" class="fa fa-eye" aria-hidden="true"></i></a>

											@if($payment->file_url != null)
												<a href="{{ route('payment_receive_download_uploaded_file',  $payment->id) }}" class="btn btn-success btn-xs"><i title="Download File" class="fa fa-download" aria-hidden="true"></i></a>
											@endif
											<a href="{{ route('payment_receive_delete', $payment->id) }}" class="btn btn-danger btn-xs confirm_box"><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></a>
										</td>
									</tr>
								@endforeach
								
							</tbody>
							<tr class="total" style="background-color: #F9E79F">
								<th style="text-align: right" colspan="4">Total Transaction</th>
								<th style="text-align: right">{{ number_format($total_paid,2,'.',',') }}</th>
								<th style="text-align: right">{{ number_format($total_expense,2,'.',',') }}</th>
								<th>
									<!-- <a class="btn btn-success btn-xs" href=""><i class="fa fa-download"></i> Download</a> -->
								</th>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="modal fade reloadModal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="text-center">
	      <div style="text-align: -webkit-center" class="modal-body">
	        <div class="loader">
	        	
	        </div>
	      </div>
	    </div>
	  </div>
	</div>

@endsection

@push('scripts')
	<script type="text/javascript">
		$(".confirm_box").click(function(){
			if (!confirm("Do you want to delete")){
				return false;
			}
		});
	</script>

	<script type="text/javascript">
		function calculate()
		{	
			var other_expense	= $('#other_expense').val(0); //Make this field 0 otherwise for previous other expense value will show
			var invoices        = <?php echo $invoices; ?>;
			var amount 			= $('#amount').val();
			var amount_left     = parseFloat(amount);
			var total_due       = 0;

			$.each(invoices, function(i, data )
			{	
				if(data.due_amount > 0)
				{
					$('#other_expense_'+i).val(0);  //Make this field 0 otherwise for previous other expense value will show
					//Calculation for setup amount start
					var due_amount  = data.due_amount;
					total_due      += parseFloat(data.due_amount);

					if (amount_left > due_amount)
					{
						$('#paid_'+i).val(due_amount.toFixed(2));

						amount_left     -= parseFloat(due_amount);
					}
					else
					{
						if (amount_left > 0)
						{
							$('#paid_'+i).val(amount_left.toFixed(2));
							amount_left     -= parseFloat(due_amount);
						}
						else
						{
							$('#paid_'+i).val(0);
						}
					}
				}
				//Calculation for setup amount end
			});

			if (amount > total_due)
			{
				$('#amount').val(total_due.toFixed(2));
			}
		}

		function calculateExpense()
		{
			var invoices        = <?php echo $invoices; ?>;
			var other_expense	= $('#other_expense').val();
			var total_ex_due    = 0;
			var expense_left    = parseFloat(other_expense);

			$.each(invoices, function(i, data )
			{	
				if(data.due_amount > 0)
				{
					//Calculation for setup other expense start
					var due_amount  = data.due_amount;
					var paid_amount = $('#paid_'+i).val();
					var dues        = parseFloat(due_amount) - parseFloat(paid_amount);
					total_ex_due    += parseFloat(dues);

					if (dues != 0)
					{
						if (expense_left > dues)
						{	
							$('#other_expense_'+i).val(dues.toFixed(2));
							expense_left     -= parseFloat(dues);
						}
						else
						{
							if (expense_left > 0)
							{
								$('#other_expense_'+i).val(expense_left.toFixed(2));
								expense_left     -= parseFloat(expense_left);
							}
							else
							{
								$('#other_expense_'+i).val(0);
							}
						}
					}
				}
			});

			if (other_expense > total_ex_due)
			{
				$('#other_expense').val(total_ex_due.toFixed(2));
			}
		}
	</script>

	<script type="text/javascript">
		$(document).ready(function()
		{
		   	$(".HistoryIndex").hide();
		});
	</script>

	<script type="text/javascript">
		$("#historyShow").click(function(){
		  $(".HistoryIndex").toggle();
		});
	</script>

	<script type="text/javascript">
		function selectEvent()
		{	
			$('.reloadModal').modal('show');
			$('#searbButton').click();
		}
	</script>
@endpush