<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Payment Receipt</title>
</head>

<style type="text/css">
	table, th, td {
	  border: 1px solid black;
	  border-collapse: collapse;
	  padding: 5px;
	  font-family: 'nikosh';
	}
	
	.colorWhite{
	    border-color: white;
	}
</style>

<body style="font-family: 'nikosh';">

	<div style="padding: 0px">
	    <table style="width: 100%;" class="colorWhite">
		    <tr>
		        <td style="width: 100%" class="colorWhite">
					<img style="width: 100%" src="http://ams.dainikdhakareport.net/assets/money-receipt-header.jpg">
				</td>
			</tr>
		</table>
		
		<table style="width: 100%;" class="colorWhite">
			<tr>
			    <td style="width: 100%;text-align: left;border-right: 6px solid #B2B3B7;padding-left: 20px" class="colorWhite">
					<p style="font-size: 60px;">রশিদ নং: @php $No = \App\Http\Controllers\HomeController::GetBangla($payments->id); echo $No; @endphp</p>
					<p style="font-size: 60px;">তারিখঃ @php $billDate = \App\Http\Controllers\HomeController::GetBangla(date('d-m-Y')); echo $billDate; @endphp</p>
				</td>
				
				<td style="width: 100%;text-align: left;border-right: 10px solid #B2B3B7;padding-left: 20px" class="colorWhite">
				    <p style="font-size: 60px;text-align: right">৬৮ যোগীনগর রোড ওয়ারী, <br>ঢাকা-১২০৩ | <br>ফোন : ৪৭১১৬৫৬৯, <br> মোবাইল : ০১৯১৫৫১১১৯২</p>
				</td>
				
				<td style="width: 100%;border-right: 22px solid #B2B3B7" class="colorWhite">
					<img style="width: 100%" src="http://dainikdhakareport.net/uploads/settings/logologo-1-1541316160.png">
				</td>
			</tr>
		</table>
		
		<!--<table style="width: 100%;border: 1px solid #323286;padding: 10px 10px 10px 10px;margin-top: 25px;margin-bottom: 25px">-->
		<!--    <tr>-->
		<!--        <td style="text-align: left;border-bottom: 1px solid black" colspan="4">-->
		<!--            <p style="font-size: 20px;margin-top: 20px;">জনাব/মেসার্স: <span style="color: #10497F">{{ $payments->customer->name }}</span></p>-->
		<!--        </td>-->
		<!--    </tr>-->
		    
		<!--    <tr>-->
		<!--        <td style="width: 100%;text-align: left;border-bottom: 1px solid black" colspan="4">-->
		<!--            <p style="font-size: 20px;margin-top: 20px;">টাকা (কথায়): <span style="color: #10497F">{{ $payments->amount_bangla }}</span></p>-->
		<!--        </td>-->
		<!--    </tr>-->
		    
		<!--    <tr>-->
		<!--        <td style="text-align: left;border-bottom: 1px solid black;width: 100%;border-right: 1px solid black" colspan="2">-->
		<!--            <p style="font-size: 20px;margin-top: 20px;padding-left: 10px">নগদ/ব্যাংক ড্রাফট/চেক নং-&nbsp;&nbsp;&nbsp;<span style="color: #10497F;border-left: 1px solid black;padding-left: 10px;padding-right: 10px">&nbsp;&nbsp;{{ $payments->payment_through }}</span> </p>-->
		<!--        </td>-->
		<!--        <td style="text-align: left;border-bottom: 1px solid black;width: 100%;padding-left: 10px" colspan="2">-->
		<!--            <p style="font-size: 20px;margin-top: 20px;padding-left: 15px">তারিখ - <span style="color: #10497F;border-left: 1px solid black;padding-right: 10px">&nbsp;&nbsp;@php $billDate = \App\Http\Controllers\HomeController::GetBangla(date('d-m-Y', strtotime($payments->date))); echo $billDate; @endphp</span> </p>-->
		<!--        </td>-->
		<!--    </tr>-->
		    
		    <!--<tr>-->
		    <!--    <td style="text-align: left;border-bottom: 1px solid black;width: 100%;" colspan="4">-->
		    <!--        <p style="font-size: 20px;margin-top: 20px;padding-left: 10px">ব্যাংক - &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="color: #10497F;border-left: 1px solid black;padding-left: 10px;padding-right: 10px">&nbsp;&nbsp;{{ $payments->paid_through }}</span> </p>-->
		    <!--    </td>-->
		    <!--</tr>-->
		    
		<!--    <tr>-->
		<!--        <td style="width: 100%;text-align: left;border-bottom: 1px solid black" colspan="4">-->
		<!--            <p style="font-size: 20px;margin-top: 20px;padding-left: 10px">অর্ডার নং-<span style="color: #10497Fk"> @php $billNo = \App\Http\Controllers\HomeController::GetBangla($payments->invoice_number); echo $billNo; @endphp</span></p>-->
		<!--        </td>-->
		<!--    </tr>-->
		    
		<!--    <tr>-->
		<!--        <td style="width: 100%;text-align: left;border-bottom: 1px solid black">-->
		<!--            <p style="font-size: 20px;margin-top: 20px;">মোট টাকার পরিমান: <span style="color: #10497F"> @php $amount = \App\Http\Controllers\HomeController::GetBangla($payments->total_amount); echo $amount; @endphp</span></p>-->
		<!--        </td>-->
		<!--    </tr>-->
		<!--</table>-->
		
		
		
		
		<table style="width: 100%;margin-top: 15px;margin-bottom: 15px">
		    <tr>
		        <td colspan="4">
		            <p style="font-size: 20px;margin-top: 20px;">জনাব/মেসার্স {{ $payments->customer->name }}</p>
		        </td>
		    </tr>
		    <tr>
		        <td colspan="4">
		            <p style="font-size: 20px;margin-top: 20px;">টাকা (কথায়):{{ $payments->amount_bangla }}</p>
		        </td>
		    </tr>
		    <tr>
		        <td style="width: 30%">
		             <p style="font-size: 20px;margin-top: 20px;padding-left: 10px">নগদ/ব্যাংক ড্রাফট/চেক নং-</p>
		        </td>
		        <td style="width: 25%">
		              <p style="font-size: 20px;margin-top: 20px;padding-left: 10px">{{ $payments->payment_through }} </p>
		        </td>
		        <td style="width: 15%">
                    <p style="font-size: 20px;margin-top: 20px;padding-left: 15px">তারিখ - </p>
		        </td>
		        <td style="width: 30%">
		            <p style="font-size: 20px;margin-top: 20px;padding-left: 15px">@php $billDate = \App\Http\Controllers\HomeController::GetBangla(date('d-m-Y', strtotime($payments->date))); echo $billDate; @endphp</p>
		        </td>
		    </tr>
		    <tr>
		        <td style="width: 30%;border-color: white;"></td>
		        <td style="width: 25%;border-color: white;border-right-color: black">
		        </td>
		        <td style="width: 15%">
		            <p style="font-size: 20px;margin-top: 20px;padding-left: 10px">অর্ডার নং-</p>
		        </td>
		        <td style="width: 30%">
		            <p style="font-size: 20px;margin-top: 20px;padding-left: 10px">@php $billNo = \App\Http\Controllers\HomeController::GetBangla($payments->invoice_number); echo $billNo; @endphp</p>
		        </td>
		    </tr>
		    <tr>
		        <td style="width: 30%;border-color: white;">
		        </td>
		        <td style="width: 25%;border-color: white;border-right-color: black;test-align: right;padding-left: 60px">
		            <span style="border-top: 1px solid black;test-align: right;font-size: 18px">
		                গ্রণকারীর স্বাক্ষর
		            </span>
		        </td>
		        <td style="width: 15%">
		            <p style="font-size: 20px;margin-top: 20px;">মোট টাকা-</p>
		        </td>
		        <td style="width: 30%">
		            <p style="font-size: 20px;margin-top: 20px;">@php $amount = \App\Http\Controllers\HomeController::GetBangla($payments->total_amount); echo $amount; @endphp</p>
		        </td>
		    </tr>
		</table>
		

		<table style="width: 100%;padding: 0px" class="colorWhite">
		    <tr>
		        <td style="width: 100%" class="colorWhite">
					<img style="width: 100%;padding: 0px" src="http://ams.dainikdhakareport.net/assets/money-receipt-footer.jpg">
				</td>
			</tr>
		</table>

	</div>

</body>
</html>