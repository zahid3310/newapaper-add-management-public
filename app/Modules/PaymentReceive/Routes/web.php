<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'paymentReceive', 'middleware' => 'auth'], function () {
    Route::get('/index', 'PaymentReceiveController@index')->name('payment_receive_index');
    Route::get('/show/{id}', 'PaymentReceiveController@show')->name('payment_receive_show');
    Route::get('/create', 'PaymentReceiveController@create')->name('payment_receive_create');
    Route::post('/store', 'PaymentReceiveController@store')->name('payment_receive_store');
    Route::get('/edit/{id}', 'PaymentReceiveController@edit')->name('payment_receive_edit');
    Route::post('/update/{id}', 'PaymentReceiveController@update')->name('payment_receive_update');
    Route::get('/delete/{id}', 'PaymentReceiveController@destroy')->name('payment_receive_delete');
    Route::get('/all-invoices/{id}', 'PaymentReceiveController@allInvoices')->name('payment_receive_all_invoices');
    Route::get('/get-invoice/{id}', 'PaymentReceiveController@getInvoice')->name('payment_receive_get_invoice');
    Route::get('/download-uploaded-file/{id}', 'PaymentReceiveController@downloadUplodedFile')->name('payment_receive_download_uploaded_file');
    
    
     Route::get('/test-show/{id}', 'PaymentReceiveController@testShow')->name('payment_receive_test_show');
});
