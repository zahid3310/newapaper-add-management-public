<?php

namespace App\Modules\Invoices\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Models\Areas;
use App\Models\Districts;
use App\Models\Customers;
use App\Models\Agents;
use App\Models\Items;
use App\Models\Invoices;
use App\Models\InvoiceDetails;
use Response;
use DB;
use App\Modules\Invoices\Http\Controllers\mPDF;

class InvoiceController extends Controller
{
    public function index()
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('invoices_index');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

    	$invoices = Invoices::where('status', '=', 1)->orderBy('created_at', 'DESC')->get();

    	return view('invoices::index', compact('invoices'));
    }

    public function create()
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('invoices_create');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

    	$customers 	= Customers::where('status', '=', 1)->get();
    	$agents 	= Agents::where('status', '=', 1)->get();
    	$items 		= Items::where('status', '=', 1)->get();

    	return view('invoices::create', compact('customers', 'agents', 'items'));
    }

    public function store(Request $request)
    {   
        // dd($request->toArray());
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('invoices_store');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $this->validate($request,[
         'customer_id'  	=> 'required',
         'invoice_date'     => 'required',
         'item_id'     		=> 'required',
        ]);

        $items 		        = $request->item_id;
        $rate 		        = $request->rate;
        $inch               = $request->inch;
        $colum 	            = $request->column;
        $discount 	        = $request->discount;
        $type               = $request->type;
        $color_print_type   = $request->color_print_type;
        $adjustment_type    = $request->adjustment_type;
        $amount 	        = $request->amount;
        $comission_type     = $request->commission_type;
        $invoice_type       = $request->invoice_type;
        $agent_commission   = $request->agent_commission != null ? $request->agent_commission : 0;
        $color_print        = $request->color_print_amount != null ? $request->color_print_amount : 0;

    	foreach ($items as $item)
    	{
    		if(is_null($item))
    		{
    			return redirect()->back()->with('message', 'Item can not be null !!');
    		}
    	}

        $invoices = Invoices::count();
    
	    if($invoices>0)
	    {
            $invoice 		= Invoices::orderBy('created_at', 'desc')->first();
            $invoice_number = $invoice['invoice_number'];
            $separate_year  = explode('/', $invoice_number);
            $invoice_number = $separate_year[0] + 1;
        }
        else
        {
            $invoice_number = 1;
        }      

        $invoice_number = str_pad($invoice_number, 6, '0', STR_PAD_LEFT).'/'.date('Y');

        try
        {   
            DB::beginTransaction();

            $invoices                       = new Invoices;
            $invoices->customer_id          = $request->customer_id;
            $invoices->note                 = $request->note;
            $invoices->nirdesok_number      = $request->nirdesok_number;
            $invoices->drb_number           = $request->drb_number;
            $invoices->adjustment_note      = $request->adjustment_note;
            $invoices->amount_bangla        = $request->amount_bangla;
            $invoices->invoice_date         = $request->invoice_date;
            $invoices->invoice_number       = $invoice_number;
            $invoices->due_date             = $request->due_date;
            $invoices->agent_id             = $request->agent_id;
            $invoices->vat_type             = $request->vat_type;
            $invoices->tax_type             = $request->tax_type;
            $invoices->color_print_type     = $request->color_print_type;
            $invoices->adjustment_type      = $request->adjustment_type;
            $invoices->email_status         = 0;

            if (($request->commission_amount != null))
            {
                $invoices->commission_amount  = $request->total_agent_commission;
            }

            if (($request->vat_amount != null))
            {
                $invoices->vat_amount  = $request->vat_type = 1 ? $request->vat_amount : (($request->vat_amount * $request->sub_total_amount)/100);
            }

            if (($request->tax_amount != null))
            {
                $invoices->tax_amount  = $request->tax_type = 1 ? $request->tax_amount : (($request->tax_amount * $request->sub_total_amount)/100);
            }

            if (($request->color_print_amount != null))
            {
                $invoices->color_print  = $color_print_type = 1 ? $color_print : (($color_print * $color_print_type)/100);
            }

            if (($request->adjustment_amount != null))
            {
                $invoices->adjustment   = $adjustment_type = 1 ? $request->adjustment_amount : (($request->adjustment_amount * $adjustment_type)/100);
            }

            if($request->hasFile('file_url'))
            {
               $photo               = $request->file('file_url');
               $photoName           = time().".".$photo->getClientOriginalExtension();
               $directory1          = 'assets/images/invoices/';
               $photo->move($directory1, $photoName);
               $photoUrl1           = $photoName;
               $invoices->file_url  = $photoUrl1;
            }

            $invoices->total_amount         = $request->total_amount;
            $invoices->due_amount           = $request->total_amount;
            $invoices->commission_amount    = $request->total_agent_commission;
            $invoices->status               = 1;
            $invoices->created_by           = Auth::user()->id;

            if ($invoices->save())
            {   
                foreach ($items as $key => $item_value)
                {   
                    if($type[$key] == 0) 
                    {
                        $dis_amount = $amount[$key]*$inch[$key]*$colum[$key]/100;
                    }
                    else
                    {
                       $dis_amount = $discount[$key]; 
                    } 

                    $invoice_details                    = new InvoiceDetails;
                    $invoice_details->invoice_id        = $invoices->id;
                    $invoice_details->item_id           = $item_value;
                    $invoice_details->inch              = $inch[$key];
                    $invoice_details->colum             = $colum[$key];
                    $invoice_details->rate              = $rate[$key];
                    $invoice_details->amount            = $amount[$key] - $dis_amount;
                    $invoice_details->discount_type     = $type[$key];
                    $invoice_details->discount_amount   = $discount[$key];
                    $invoice_details->commission_type   = $comission_type[$key];
                    $invoice_details->commission_amount = $agent_commission[$key];
                    $invoice_details->invoice_type      = $invoice_type[$key];
                    $invoice_details->status            = 1;
                    $invoice_details->created_by        = Auth::user()->id;
                    $invoice_details->save();
                }
                
                DB::commit();

                return redirect()->route('invoices_index')->with('message', 'Successfully added !!');

            }
        }
        catch(\Exception $e)
        {dd($e);
            DB::rollback();
            $mesg = $e->getMessage();
            return redirect()->route('invoices_index')->with('message', 'Something went wrong try again !!');
        }     
    }

    public function edit($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('invoices_edit');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $customers          = Customers::where('status', '=', 1)->get();
        $agents             = Agents::where('status', '=', 1)->get();
        $items              = Items::where('status', '=', 1)->get();
        $invoice            = Invoices::find($id);
        $invoice_details    = InvoiceDetails::where('invoice_id', $id)->where('status', '=', 1)->get();
        $apend_index_strat  = $invoice_details->count() - 1;

        return view('invoices::edit', compact('customers', 'agents', 'items', 'invoice', 'invoice_details', 'apend_index_strat'));
    }

    public function update(Request $request, $id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('invoices_update');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $this->validate($request,[
         'customer_id'      => 'required',
         'invoice_date'     => 'required',
         'item_id'          => 'required',
        ]);

        $items              = $request->item_id;
        $rate               = $request->rate;
        $inch               = $request->inch;
        $colum              = $request->column;
        $discount           = $request->discount;
        $type               = $request->type;
        $amount             = $request->amount;
        $color_print_type   = $request->color_print_type;
        $adjustment_type    = $request->adjustment_type;
        $comission_type     = $request->commission_type;
        $invoice_type       = $request->invoice_type;
        $agent_commission   = $request->agent_commission != null ? $request->agent_commission : 0;
        $color_print        = $request->color_print_amount != null ? $request->color_print_amount : 0;

        foreach ($items as $item)
        {
            if(is_null($item))
            {
                return redirect()->back()->with('message', 'Item can not be null !!');
            }
        }

        $invoices = Invoices::count();      

        try
        {   
            DB::beginTransaction();

            $invoices                       = Invoices::find($id);
            $invoices->customer_id          = $request->customer_id;
            $invoices->nirdesok_number      = $request->nirdesok_number;
            $invoices->drb_number           = $request->drb_number;
            $invoices->adjustment_note      = $request->adjustment_note;
            $invoices->amount_bangla        = $request->amount_bangla;
            $invoices->note                 = $request->note;
            $invoices->invoice_date         = $request->invoice_date;
            $invoices->invoice_number       = $invoices->invoice_number;
            $invoices->due_date             = $request->due_date;
            $invoices->agent_id             = $request->agent_id;
            $invoices->vat_type             = $request->vat_type;
            $invoices->tax_type             = $request->tax_type;
            $invoices->color_print_type     = $request->color_print_type;
            $invoices->adjustment_type      = $request->adjustment_type;

            if (($request->commission_amount != null))
            {
                $invoices->commission_amount  = $request->total_agent_commission;
            }

            if (($request->vat_amount != null))
            {
                $invoices->vat_amount  = $request->vat_type = 1 ? $request->vat_amount : (($request->vat_amount * $request->sub_total_amount)/100);
            }

            if (($request->tax_amount != null))
            {
                $invoices->tax_amount  = $request->tax_type = 1 ? $request->tax_amount : (($request->tax_amount * $request->sub_total_amount)/100);
            }

            if (($request->color_print_amount != null))
            {
                $invoices->color_print  = $color_print_type = 1 ? $color_print : (($color_print * $color_print_type)/100);
            }

            if (($request->adjustment_amount != null))
            {
                $invoices->adjustment   = $adjustment_type = 1 ? $request->adjustment_amount : (($request->adjustment_amount * $adjustment_type)/100);
            }

            if($request->hasFile('file_url'))
            {
                if($invoices->file_url != null)
                {   
                    unlink('assets/images/invoices/'.$invoices->file_url);
                }

                $photo               = $request->file('file_url');
                $photoName           = time().".".$photo->getClientOriginalExtension();
                $directory1          = 'assets/images/invoices/';
                $photo->move($directory1, $photoName);
                $photoUrl1           = $photoName;
                $invoices->file_url  = $photoUrl1;
            }

            $invoices->total_amount         = $request->total_amount;

            //Previous Due Calculations Start
            $invoices_pre_due               = Invoices::find($id);
            $previous_payment               = $invoices_pre_due->total_amount - $invoices_pre_due->due_amount;
            $invoices->due_amount           = $request->total_amount - $previous_payment;
            //Previous Due Calculations End
            
            $invoices->commission_amount    = $request->total_agent_commission;
            $invoices->status               = 1;
            $invoices->updated_by           = Auth::user()->id;

            if ($invoices->save())
            {
                $invoice_entry  = InvoiceDetails::where('invoice_id', $invoices->id)->delete();
            
                foreach ($items as $key => $item_value)
                {
                    $invoice_details                    = new InvoiceDetails;
                    $invoice_details->invoice_id        = $invoices->id;
                    $invoice_details->item_id           = $item_value;
                    $invoice_details->inch              = $inch[$key];
                    $invoice_details->colum             = $colum[$key];
                    $invoice_details->rate              = $rate[$key];
                    $invoice_details->amount            = $amount[$key];
                    $invoice_details->discount_type     = $type[$key];
                    $invoice_details->discount_amount   = $discount[$key];
                    $invoice_details->commission_type   = $comission_type[$key];
                    $invoice_details->commission_amount = $agent_commission[$key];
                    $invoice_details->invoice_type      = $invoice_type[$key];
                    $invoice_details->status            = 1;
                    $invoice_details->updated_by        = Auth::user()->id;
                    $invoice_details->save();
                }
                
                DB::commit();

                return redirect()->route('invoices_index')->with('message', 'Successfully updated !!');
            }
        }
        catch(\Exception $e)
        {dd($e);
            DB::rollback();
            $mesg = $e->getMessage();
            return redirect()->route('invoices_index')->with('message', 'Something went wrong try again !!');
        } 
    }

    public function show($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('invoices_show');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $invoice            = Invoices::find($id);
        $invoice_details    = InvoiceDetails::where('invoice_id', $id)
                                            ->selectRaw('invoices_details.*')
                                            ->get();
                                            
        $check_nirdharito   = $invoice_details->where('invoice_type', 2);

        $total_amount_sum   = $invoice_details->sum('amount') + $invoice->adjustment;

        return view('invoices::show', compact('invoice', 'invoice_details', 'total_amount_sum', 'check_nirdharito'));
    }

    public function download($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('invoices_download');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $invoice    = Invoices::find($id);
        $file_path  = ('assets/images/invoices/'.$invoice->file_url);
        $name       = 'INV - '.$invoice->invoice_number;

        return response()->download($file_path, $name);
    }

    public function downloadInvoice($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('customer_invoices_show');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $invoice            = Invoices::find($id);
        $invoice_details    = InvoiceDetails::where('invoice_id', $id)
                                            ->selectRaw('invoices_details.*')
                                            ->get();
                                            
        $check_nirdharito   = $invoice_details->where('invoice_type', 2);

        $total_amount_sum   = $invoice_details->sum('amount') + $invoice->adjustment;

        $name               = 'INV - '.$invoice->invoice_number;

        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML(view('invoices::download-invoice')->with('invoice', $invoice)->with('invoice_details', $invoice_details)->with('total_amount_sum', $total_amount_sum)->with('check_nirdharito', $check_nirdharito));
        $mpdf->Output('INV - '.$invoice->invoice_number, "I");
    }
    
    public function downloadInvoiceWithHeader($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('customer_invoices_show');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $invoice            = Invoices::find($id);
        $invoice_details    = InvoiceDetails::where('invoice_id', $id)
                                            ->selectRaw('invoices_details.*')
                                            ->get();
                                            
        $check_nirdharito   = $invoice_details->where('invoice_type', 2);

        $total_amount_sum   = $invoice_details->sum('amount') + $invoice->adjustment;

        $name               = 'INV - '.$invoice->invoice_number;

        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML(view('invoices::download-invoice-with-header')->with('invoice', $invoice)->with('invoice_details', $invoice_details)->with('total_amount_sum', $total_amount_sum)->with('check_nirdharito', $check_nirdharito));
        $mpdf->Output('INV - '.$invoice->invoice_number, "I");
    }
    
    public function destroy($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('invoices_delete');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

       try
        {
            DB::beginTransaction();

            $invoices         = Invoices::find($id);
            $invoices->status = 0;

            if ($invoices->save())
            {
                $invoice_entry  = InvoiceDetails::where('invoice_id', $invoices->id)->get();

                foreach ($invoice_entry as $key => $invoice_entry_value)
                {
                    $item       = Items::find($invoice_entry_value->item_id);
                    $item->sell = $item->sell - $invoice_entry_value->quantity;
                    $item->save();

                    $invoice_entry_delete           = InvoiceDetails::find($invoice_entry_value->id);
                    $invoice_entry_delete->status   = 0;
                    $invoice_entry_delete->save();
                }

                DB::table('transactions')->where('invoice_id', '=', $id)->update(['status' => 0]);
                
                DB::commit();

                return redirect()->route('invoices_index')->with('message', 'Successfully added !!');
            }
        }
        catch(\Exception $e)
        {
            DB::rollback();
            $mesg = $e->getMessage();
            return redirect()->route('invoices_index')->with('message', 'Something went wrong try again !!');
        } 
    }

    public function getItemRate($id)
    {   
        $data = Items::find($id);

        return Response::json($data);
    }

    public function sendInvoice(Request $request)
    {   
        // dd('This Module is under developement');
        $id                 = $request->invoice_id;
        $invoice            = Invoices::find($id);
        $invoice_details    = InvoiceDetails::where('invoice_id', $id)
                                            ->selectRaw('invoices_details.*')
                                            ->get();

        $total_amount_sum   = $invoice_details->sum('amount') + $invoice->adjustment;

        $explode_num        = explode('/', $invoice->invoice_number);
        $name               = 'INV-'.$explode_num[0].'.pdf';

        $mpdf               = new \Mpdf\Mpdf();

        $mpdf->WriteHTML(view('invoices::download-invoice')
                        ->with('invoice', $invoice)
                        ->with('invoice_details', $invoice_details)
                        ->with('total_amount_sum', $total_amount_sum));

        $location   = "assets/download-invoices/";

        $mpdf->Output($location . $name, \Mpdf\Output\Destination::FILE);

        //Send Mail Start Here
        $file_path = 'assets/download-invoices/'.$name;
        $data      = $request->all();

        \Mail::send('invoices::email', ['data' => $data], function($message) use ($data, $file_path) 
        {
            $message->to($data['to'], 'Dainik Dhaka Report')->subject($data['subject']);

            $message->attach($file_path);
        });

        if (\Mail::failures())
        {   
            unlink($location.$name);
            return back()->with('message', 'Email not sent try again !!');
        }

        if ($invoice->email_status == 0)
        {
            $invoice->email_status     = 1;
            $invoice->save();
        }

        unlink($location.$name);
        return back()->with('message', 'Email has been sent successfully !!');
    }
}