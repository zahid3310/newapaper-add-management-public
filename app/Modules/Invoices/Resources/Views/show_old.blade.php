@extends('layouts.app')

@section('title', 'Show Invoice')

<style type="text/css">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</style>

@section('content')
	<section class="content-header">
		<h1>
			Show Invoice
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i>
				Home 
			</a></li>
			<li><a href="#">
				Invoices
			</a></li>
			<li class="active">
				Show Invoice
			</li>
		</ol>
	</section>

	<section class="content">
		<div class="box">
			<div class="box-body">
				
				<body style="background-color: #525659">
				  	<br>
				  	<br>
				  	<div class="container" style="background-color: white;padding: 50px">
				    <div class="row">
				      	<div class="col-md-12">
				        	<table class="table" style="border-top: none;">
					          	<tr>
						            <td style="width: 30%;vertical-align: bottom;border-top: none;">
						              	<p style="text-align: left;margin-bottom: 10px">বিল নং- {{ $invoice->invoice_number }}</p>
						              	<p style="text-align: left;margin-bottom: 10px">পৃষ্ঠা নং- বি ০০১২৫</p>
						              	<p style="text-align: left;margin-bottom: 10px">তারিখঃ {{ date('d-m-Y', strtotime($invoice->invoice_date)) }}</p>
						            </td>
						            <td style="width: 40%;border-top: none;">
						              	<center>
						                	<img src="http://dainikdhakareport.net/uploads/settings/logologo-1-1541316160.png">
						              	</center>
						              	<p style="text-align: center;margin-bottom: 0px">৬৮ যোগীনগর রোড, ওয়ারী, ঢাকা-১২০৩</p>
						              	<p style="text-align: center;margin-bottom: 0px">Web: www.dainikdhakareport.net</p>
						              	<p style="text-align: center;margin-bottom: 0px">Email: dhakareportad@gmail.com</p>
						            </td>
						            <td style="width: 30%;vertical-align: bottom;border-top: none;">
						              	<p style="text-align: right;margin-bottom: 0px">ফোনঃ</p>
						              	<p style="text-align: right;margin-bottom: 0px"০২৭১১৬৫৬৯</p>
						                <p style="text-align: right;margin-bottom: 0px">০১৯১৫৫১১১৯২</p>
						                <p style="text-align: right;margin-bottom: 0px">০১৬১৫৫১১১৯২</p>
						                <p style="text-align: right;margin-bottom: 0px">০১৬১৯১৫৩১১৫</p>
						                <p style="text-align: right;margin-bottom: 0px">০১৮১৯১৫৩১১৫</p>
						            </td>
					            </tr>
					        </table>

				          	<table class="table table-bordered" style="border-top: 3px solid;border-bottom: 3px solid;height: 400px">
					            <tr style="height: 30px">
					              	<th style="font-style: italic;text-align: center;">নির্দেশক সূত্র</th>
					              	<th style="font-style: italic;text-align: center;">বিজ্ঞাপনদাতা</th>
					              	<th style="font-style: italic;text-align: center;">ছাপার তাং</th>
					              	<th style="font-style: italic;text-align: center;">আকার</th>
					              	<th style="font-style: italic;text-align: center;">মূল্য হার</th>
					              	<th style="font-style: italic;text-align: center;"></th>
					              	<th style="font-style: italic;text-align: center;">মোট</th>
					            </tr>
					            @foreach($invoice_details as $key=> $invoice_details_value)
						            <tr>
						              	<td></td>
						              	<td style="width: 200px;text-align: left;">
						              		@if($key == 0)
						              			{{ $invoice->customer->name }}
						              		@endif
						              	</td>
						              	<td style="text-align: center;">
						              		{{ date('d-m-Y', strtotime($invoice->due_date)) }}
						              	</td>
						              	<td style="text-align: center;">{{ $invoice_details_value->item->name }}</td>
						              	<td style="text-align: center;">{{ $invoice_details_value->rate }}</td>
						              	<td></td>
						              	<td style="text-align: right;">{{ $invoice_details_value->amount  }}</td>
						            </tr>
					            @endforeach
					            <tr>
					              	<td></td>
					              	<td style="width: 200px;text-align: left;"></td>
					              	<td style="text-align: center;"></td>
					              	<td style="text-align: center;"></td>
					              	<td style="text-align: center;"></td>
					              	<td>
					              		ভ্যাট {{ $invoice->vat_type == 0 ? $invoice->vat_amount.'%' : '' }}<br>
					              		স:বি:কর {{ $invoice->tax_type == 0 ? $invoice->tax_amount.'%' : '' }}
					              	</td>
					              	<td style="text-align: right;">
					              		{{ $invoice->vat_type == 0 ? ($invoice->vat_amount*$invoice->total_amount)/100 : $invoice->vat_amount }}<br>
					              		{{ $invoice->vat_type == 0 ? ($invoice->tax_amount*$invoice->total_amount)/100 : $invoice->tax_amount }}
					              	</td>
					            </tr>
					            <tr style="height: 30px">
					              	<th></th>
					              	<th colspan="3"></th>
					              	<th colspan="2" style="text-align: right;">সর্বমোটঃ</th>
					              	<th style="text-align: right;">{{ $invoice->total_amount }}/=</th>
					            </tr>

					            <tr>
					              	<th></th>
					              	<th colspan="6" style="height: 40px">কথায়ঃ আটত্রিশ হাজার টাকা মাত্র ।</th>
					            </tr>
				          	</table>

					        <table class="table">
					            <tr>
					              <td style="border-top: none;"><span class="btn btn-danger" style="border-radius: 0px;background-color: #231F20">দ্রস্টব্য</span> বিলের টাকা দৈনিক ঢাকা রিপোর্ট এর অনুকূলে একাউন্টপেয়ী চেক অথবা মনোনীত ব্যক্তির নিকট নগদ পরিশোধ যোগ্য</td>
					            </tr>
					        </table>

				          	<table class="table" style="margin-top: 50px">
				            	<tr>
				              		<th style="text-align: center;border-top: none;">হিসাব রক্ষক</th>
				              		<th style="text-align: center;border-top: none;">বিজ্ঞাপন কর্মকর্তা</th>
				            	</tr>
				          	</table>
				        </div>
				    </div>
				</body>
				
			</div>
		</div>
	</section>
@endsection
