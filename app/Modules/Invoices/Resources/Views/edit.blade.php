@extends('layouts.app')

@section('title', 'Edit Invoice')

@section('content')

	<section class="content">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
				Edit Invoice
				</h3>
			</div>
			<div class="box-body">
				@if(Session::has('message'))
				<div style="padding: 0px" class="col-md-12">
					<div class="alert alert-success alert-dismissable text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{Session::get('message')}}
					</div>
				</div>
				@endif

				@if(Session::has('errors'))
				<div style="padding: 0px" class="col-md-12">
					<div class="alert alert-success alert-dismissable text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{Session::get('errors')}}
					</div>
				</div>
				@endif
				<form class="form-horizontal" method="POST" action="{{ route('invoices_update', $invoice->id) }}" enctype="multipart/form-data">
					{{ csrf_field() }}

					<div class="form-group{{ $errors->has('nirdesok_number') ? ' has-error' : '' }}">
						<label for="name" class="col-md-4 control-label">
						Nirdesok Number
						</label>

						<div class="col-md-6">
							<input type="text" class="form-control" name="nirdesok_number" value="{{ $invoice->nirdesok_number }}">

							@if ($errors->has('nirdesok_number'))
							<span class="help-block">
								<strong>{{ $errors->first('nirdesok_number') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('drb_number') ? ' has-error' : '' }}">
						<label for="name" class="col-md-4 control-label">
						DRB Number
						</label>

						<div class="col-md-6">
							<input type="text" class="form-control" name="drb_number" value="{{ $invoice->drb_number }}">

							@if ($errors->has('drb_number'))
							<span class="help-block">
								<strong>{{ $errors->first('drb_number') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('customer_id') ? ' has-error' : '' }}">
						<label for="name" class="col-md-4 control-label">
						Customer *  
						</label>

						<div class="col-md-6">
							<select class="form-control select2 select2-hidden-accessible" style="width: 100%;padding: 15px" tabindex="-1" aria-hidden="true" name="customer_id">
								@if(!empty($customers) && (count($customers) > 0))
									@foreach($customers as $customer)
										<option @if($customer->id == $invoice->customer_id) selected @endif style="padding: 15px" value="{{ $customer->id }}">{{ $customer->name }}</option>
									@endforeach
								@endif
							</select>

							@if ($errors->has('customer_id'))
							<span class="help-block">
								<strong>{{ $errors->first('customer_id') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('invoice_date') ? ' has-error' : '' }}">
						<label for="name" class="col-md-4 control-label">
						Invoice Date *
						</label>

						<div class="col-md-6">
							<input type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control datepicker" name="invoice_date" value="{{ date('Y-m-d', strtotime($invoice->invoice_date)) }}">

							@if ($errors->has('invoice_date'))
							<span class="help-block">
								<strong>{{ $errors->first('invoice_date') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('due_date') ? ' has-error' : '' }}">
						<label for="name" class="col-md-4 control-label">
						Publication Date
						</label>

						<div class="col-md-6">
							<input type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control datepicker" name="due_date" value="{{ date('Y-m-d', strtotime($invoice->due_date)) }}">

							@if ($errors->has('due_date'))
							<span class="help-block">
								<strong>{{ $errors->first('due_date') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('agent_id') ? ' has-error' : '' }}">
						<label for="name" class="col-md-4 control-label">
						Agent  
						</label>

						<div class="col-md-6">
							<select class="form-control select2 select2-hidden-accessible" style="width: 100%;padding: 15px" tabindex="-1" aria-hidden="true" name="agent_id">
								@if(!empty($agents) && (count($agents) > 0))
								    @if($invoice->agent_id == null)
								        <option value="">--Select Agent--</option>
								    @endif
									@foreach($agents as $agent)
										<option  @if($agent->id == $invoice->agent_id) selected @endif style="padding: 15px" value="{{ $agent->id }}">{{ $agent->name }}</option>
									@endforeach
								@endif
							</select>

							@if ($errors->has('agent_id'))
							<span class="help-block">
								<strong>{{ $errors->first('agent_id') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
						<label for="note" class="col-md-4 control-label">
						Note
						</label>

						<div class="col-md-6">
							<textarea id="note" type="text" class="form-control" name="note" value="{{ old('note') }}">{{ $invoice->note }}</textarea>

							@if ($errors->has('note'))
							<span class="help-block">
								<strong>{{ $errors->first('note') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('file_url') ? ' has-error' : '' }}">
						<label for="name" class="col-md-4 control-label">
						Upload File
						</label>

						<div class="col-md-5">
							<input type="file" class="form-control" name="file_url">

							@if ($errors->has('file_url'))
							<span class="help-block">
								<strong>{{ $errors->first('file_url') }}</strong>
							</span>
							@endif
						</div>
						@if($invoice->file_url != null)
							<div class="col-md-1">
								<a href="{{ route('invoices_download', $invoice->id) }}">Download</a>
							</div>
						@endif
					</div>

					<hr>

					<div class="form-group">
						<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 customAlign table-responsive">
							<table class="table input_fields_wrap table-bordered table-striped dataTable">
								<thead>
									<tr style="background-color: #F9E79F">
										<th>Invoice Type</th>
										<th>Iteam *</th>
										<th>Comission</th>
										<th>Type</th>
										<th style="display: none">amo</th>
										<th>Rate</th>
										<th>Inch</th>
										<th>Column</th>
										<th>Discount</th>
										<th>Type</th>
										<th>Amount</th>
										<th>Action</th>
									</tr>
								</thead>

								<tbody class="getMultipleRow">
									@foreach($invoice_details as $key => $invoice_detail)
										@if($invoice_detail->commission_type == 1)
											<?php $commission_amount = $invoice_detail->commission_amount; ?>
										@else
											<?php $commission_amount = ($invoice_detail->commission_amount *$invoice_detail->rate)/100; ?>
										@endif
										<tr class="tr_{{$key}}">
											<th>
												<select class="form-control select2" style="width: 100%;padding: 15px" id="invoice_type_{{$key}}" name="invoice_type[]" onchange="invoiceType({{ $key }})">
													<option style="padding: 15px" value="1" @if($invoice_detail->invoice_type == 1) selected @endif>Regular Price</option>
													<option style="padding: 15px" value="2" @if($invoice_detail->invoice_type == 2) selected @endif>Fixed Price</option>

													@if ($errors->has('invoice_type'))
													<span class="help-block">
														<strong>{{ $errors->first('invoice_type') }}</strong>
													</span>
													@endif
												</select>
											</th>

											<th>
												<select class="form-control select2" style="width: 100%;padding: 15px" id="item_id_{{$key}}" name="item_id[]" onchange="getItemPrice({{ $key }})">
													@if(!empty($items) && (count($items) > 0))
														@foreach($items as $item)
															<option @if($item->id == $invoice_detail['item_id']) selected @endif style="padding: 15px" value="{{ $item->id }}">{{ $item->name }}</option>
														@endforeach
													@endif
													
													@if ($errors->has('item_id'))
													<span class="help-block">
														<strong>{{ $errors->first('item_id') }}</strong>
													</span>
													@endif
												</select>
											</th>
											<th>
												<input id="agent_commission_{{$key}}" type="text" class="form-control agentCommission" name="agent_commission[]" value="{{ $invoice_detail['commission_amount'] }}" oninput="calculateActualAmount({{ $key }})">
											</th>
											<th>
												<select style="padding: 6px;border-radius: 4px" class="commissionTupe" id="commission_type_{{$key}}" name="commission_type[]" onchange="calculateActualAmount({{ $key }})">
													<option @if($invoice_detail['commission_type'] == 1) selected @endif style="padding: 10px" value="1">BDT</option>
													<option @if($invoice_detail['commission_type'] == 0) selected @endif style="padding: 10px" value="0">%</option>
												</select>
											</th>
											<th style="display: none">
												<input id="amount_commission_{{$key}}" type="text" class="form-control amountCommission" name="amount_commission[]" value="{{ $commission_amount }}" oninput="calculateActualAmount({{ $key }})">
											</th>
											<th>
												<input id="rate_{{$key}}" type="text" class="form-control rate" name="rate[]" value="{{ $invoice_detail['rate'] }}" oninput="calculateActualAmount({{ $key }})">
											</th>
											<th>
												<input id="inch_{{$key}}" type="text" class="form-control inch" name="inch[]" value="{{ $invoice_detail['inch'] }}" oninput="calculateActualAmount({{$key}})">
											</th>
											<th>
												<input id="column_{{$key}}" type="text" class="form-control column" name="column[]" value="{{ $invoice_detail['colum'] }}" oninput="calculateActualAmount({{$key}})">
											</th>
											<th>
												<input id="discount_{{$key}}" type="text" class="form-control discount" name="discount[]" value="{{ $invoice_detail['discount_amount'] }}" oninput="calculateActualAmount({{ $key }})">
											</th>
											<th>
												<select style="padding: 6px;border-radius: 4px" class="type" id="type_{{$key}}" name="type[]" onchange="calculateActualAmount({{ $key }})">
													<option  @if($invoice_detail['discount_type'] == 1) selected @endif style="padding: 10px" value="1" selected>BDT</option>
													<option @if($invoice_detail['discount_type'] == 0) selected @endif style="padding: 10px" value="0">%</option>
												</select>
											</th>
											<th>
												<input id="amount_{{$key}}" type="text" class="form-control amount" name="amount[]" value="{{ $invoice_detail['amount'] }}" oninput="calculateActualAmount({{ $key }})">
											</th>
											<th style="text-align: center">
											@if($key == 0)
												<a href="#" class="add_field_button"><i style="font-size: 25px" class="fa fa-plus"></i></a>
											@else
												<a href="#" class="remove_field"><i style="font-size: 25px" class="fa fa-trash"></i></a>
											@endif
											</th>
										</tr>
									@endforeach
								</tbody>

								<tfoot style="line-height: 60px">
                                    <tr style="border-color: white">
	                                	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
	                                	<th style="text-align: right;border-color: white"><b>Sub Total</b></th>
	                                	<th style="text-align: right;border-color: white"></th>
	                                	<th style="border-color: white">
	                                		<a style="border: none;text-decoration: none;color: black" id="subTotalBdtShow"></a>
	                                	</th>
	                                	<th style="border-color: white"></th>
                                    </tr>

                                    <tr style="border-color: white">
	                                	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
	                                	<th style="text-align: right;border-color: white"><b>Color Print</b></th>
	                                	<th style="text-align: right;border-color: white">
	                                		<select style="padding: 6px;border-radius: 4px" class="colorPrintType" id="color_print_type_0" name="color_print_type" oninput="calculateActualAmount(0)">
												<option @if($invoice['color_print_type'] == 1) selected @endif style="padding: 10px" value="1" selected>BDT</option>
												<option @if($invoice['color_print_type'] == 0) selected @endif style="padding: 10px" value="0">%</option>
											</select>
	                                	</th>
	                                	<th style="border-color: white">
	                                		<input id="color_print_0" type="text" class="form-control colorPrint" name="color_print_amount" value="{{ $invoice['color_print'] }}" oninput="calculateActualAmount(0)">
	                                	</th>
	                                	<th style="border-color: white"></th>
                                    </tr>

                                    <tr style="border-color: white">
                                    	<th style="border-color: white"></th>
                                    	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
                                    	<th style="border-color: white"></th>
                                    	<th style="border-color: white"></th>
                                    	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
                                    	<th style="text-align: right;border-color: white"><b>Adjustment</b></th>
                                    	<th style="text-align: right;border-color: white">
                                    		<select style="padding: 6px;border-radius: 4px" class="adjustmentType" id="adjustment_type_0" name="adjustment_type" oninput="calculateActualAmount(0)">
												<option  @if($invoice['adjustment_type'] == 1) selected @endif style="padding: 10px" value="1" selected>BDT</option>
												<option  @if($invoice['adjustment_type'] == 0) selected @endif style="padding: 10px" value="0">%</option>
											</select>
                                    	</th>
                                    	<th style="border-color: white">
                                    		<input id="adjustment_0" type="text" class="form-control adjustment" name="adjustment_amount" value="{{ $invoice['adjustment'] }}" oninput="calculateActualAmount(0)">
                                    	</th>
                                    	<th style="border-color: white"></th>
                                    </tr>

                                    <tr style="border-color: white">
                                    	<th style="border-color: white"></th>
                                    	<th style="border-color: white"></th>
                                    	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
                                    	<th style="border-color: white"></th>
                                    	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
                                    	<th style="text-align: right;border-color: white"><b>Adjustment</b></th>
                                    	<th style="text-align: right;border-color: white"><b>Note</b></th>
                                    	<th style="border-color: white">
                                    		<textarea id="adjustment_note" type="text" class="form-control" name="adjustment_note" value="{{ $invoice['adjustment_note'] }}"></textarea>
                                    	</th>
                                    	<th style="border-color: white"></th>
                                    </tr>

                                    <tr style="border-color: white">
                                    	<th style="border-color: white"></th>
                                    	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
                                    	<th style="border-color: white"></th>
                                    	<th style="border-color: white"></th>
                                    	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
                                    	<th style="text-align: right;border-color: white"><b>Vat</b></th>
                                    	<th style="text-align: right;border-color: white">
                                    		<select style="padding: 6px;border-radius: 4px" class="vatType" id="vat_type_0" name="vat_type" oninput="calculateActualAmount(0)">
												<option @if($invoice['vat_type'] == 1) selected @endif style="padding: 10px" value="1" selected>BDT</option>
												<option @if($invoice['vat_type'] == 0) selected @endif style="padding: 10px" value="0">%</option>
											</select>
                                    	</th>
                                    	<th style="border-color: white">
                                    		<input id="vat_amount_0" type="text" class="form-control vatAmount" name="vat_amount" value="{{ $invoice['vat_amount'] }}" oninput="calculateActualAmount(0)">
                                    	</th>
                                    	<th style="border-color: white"></th>
                                    </tr>

                                    <tr style="border-color: white">
                                    	<th style="border-color: white"></th>
                                    	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
                                    	<th style="border-color: white"></th>
                                    	<th style="border-color: white"></th>
                                    	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
                                    	<th style="text-align: right;border-color: white"><b>Tax</b></th>
                                    	<th style="text-align: right;border-color: white">
                                    		<select style="padding: 6px;border-radius: 4px" class="taxType" id="tax_type_0" name="tax_type" oninput="calculateActualAmount(0)">
												<option @if($invoice['tax_type'] == 1) selected @endif style="padding: 10px" value="1" selected>BDT</option>
												<option @if($invoice['tax_type'] == 0) selected @endif style="padding: 10px" value="0">%</option>
											</select>
                                    	</th>
                                    	<th style="border-color: white">
                                    		<input id="tax_amount_0" type="text" class="form-control taxAmount" name="tax_amount" value="{{ $invoice['tax_amount'] }}" oninput="calculateActualAmount(0)">
                                    	</th>
                                    	<th style="border-color: white"></th>
                                    </tr>

                                    <tr style="border-color: white">
	                                	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
	                                	<th style="text-align: right;border-color: white"><b>Agent Comission</b></th>
	                                	<th style="text-align: right;border-color: white">(BDT)</th>
	                                	<th style="border-color: white">
	                                		<input id="total_agent_commission" type="text" class="form-control totalAgentCommission" name="total_agent_commission" value="0" oninput="calculateActualAmount(0)">
	                                	</th>
	                                	<th style="border-color: white"></th>
                                    </tr>
                                    
                                    <tr style="border-color: white">
                                    	<th style="border-color: white"></th>
                                    	<th style="border-color: white"></th>
                                    	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
                                    	<th style="border-color: white"></th>
                                    	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
                                    	<th style="text-align: right;border-color: white"><b>Total</b></th>
                                    	<th style="text-align: right;border-color: white">(BDT)</th>
                                    	<th style="border-color: white">
                                    		<a style="border: none;text-decoration: none;color: black" id="totalBdtShow"></a>
                                    	</th>
                                    	<th style="border-color: white">
                                    		<input style="display: none" type="text" id="totalBdt" name="total_amount">
                                    	</th>
                                    </tr>

                                    <tr style="border-color: white">
                                    	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
                                    	<th style="border-color: white"></th>
                                    	<th style="border-color: white"></th>
                                    	<th style="border-color: white"></th>
	                                	<th style="border-color: white"></th>
                                    	<th style="text-align: right;border-color: white"><b>Total Amount</b></th>
                                    	<th style="text-align: right;border-color: white"><b>(Bangla)</b></th>
                                    	<th style="border-color: white">
                                    		<textarea id="amount_bangla" type="text" class="form-control" name="amount_bangla">{{ $invoice['amount_bangla'] }}</textarea>
                                    	</th>
                                    	<th style="border-color: white"></th>
                                    </tr>

                                </tfoot>
							</table>
						</div>
					</div>

					<hr>

					<div class="form-group">
						<div class="col-md-3 col-md-offset-10">
							<button type="submit" class="btn btn-primary">
								<i class="fa fa-plus-circle"></i>
							Update
							</button>
							<a href="{{ route('invoices_index') }}" class="btn btn-danger">
								<i class="fa fa-trash"></i>
							Cancel
							</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
@endsection

@push('scripts')
	<script type="text/javascript">
		$(document).ready(function()
		{	
    		calculateActualAmount(0);

    		var get_val = $("#invoice_type_"+x).val();

    		if (get_val == '2')
    		{
    			$("#inch_"+x).attr("readonly", true);
    			$("#column_"+x).attr("readonly", true);
    		}
    		else
    		{
    			$("#inch_"+x).attr("readonly", false);
    			$("#column_"+x).attr("readonly", false);
    		}
		})
	</script>

	<script>
        var max_fields     = 50;                           //maximum input boxes allowed
        var wrapper        = $(".input_fields_wrap");      //Fields wrapper
        var add_button     = $(".add_field_button");       //Add button ID
        
        //For apending another rows start
        var x = 0;
        $(add_button).click(function(e)
        {
            e.preventDefault();

            var x = parseInt($('.getMultipleRow tr:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {
                x++;                                           

                $('.getMultipleRow').append( ' ' +'<tr class="tr_'+x+'">'+
                	'<td>\n'+'<select class="form-control select2" style="width: 100%;padding: 15px" id="invoice_type_'+x+'" name="invoice_type[]" onchange="invoiceType('+x+')">\n'+ '<option style="padding: 15px" value="1" selected>Regular Price</option>\n'+ '<option style="padding: 15px" value="2">Fixed Price</option>'+'</td>\n'+
                	'<td>\n'+'<select id="item_id_'+x+'" class="md-input select2" style="width: 100%;padding: 15px" name="item_id[]" onchange="getItemPrice('+x+')">\n'+ '<option value="">Select</option>\n'+ ' @foreach($items as $all) <option value="{{ $all->id }}">{{ $all->name }}</option> @endforeach</select>\n'+'</td>\n'+
                	'<td>\n'+'<input type="text" id="agent_commission_'+x+'" class="form-control agentCommission" name="agent_commission[]" value="0" oninput="calculateActualAmount('+x+')"/>\n'+'</td>\n'+
                	'<td>\n'+'<select style="padding: 6px;border-radius: 4px" id="commission_type_'+x+'" name="commission_type[]" value="0" class="commissionType" onchange="calculateActualAmount('+x+')">\n'+'<option value="1">BDT</option>\n'+'<option value="0">%</option>\n'+'</select>\n'+'</td>\n'+
                	'<td style="display: none">\n'+'<input id="amount_commission_'+x+'" type="text" class="form-control amountCommission" name="amount_commission[]" value="0" oninput="calculateActualAmount('+x+')">\n'+'</td>\n'+
                	'<td>\n'+'<input type="text" id="rate_'+x+'" class="form-control rate" name="rate[]" value="0" oninput="calculateActualAmount('+x+')"/>\n'+'</td>\n'+
                	'<td>\n'+'<input type="text" id="inch_'+x+'" class="form-control inch" name="inch[]" value="1" oninput="calculateActualAmount('+x+')"/>\n'+'</td>\n'+
                	'<td>\n'+'<input type="text" id="column_'+x+'" class="form-control column" name="column[]" value="1" oninput="calculateActualAmount('+x+')"/>\n'+'</td>\n'+
                	'<td>\n'+'<input type="text" id="discount_'+x+'" class="form-control discount" name="discount[]" value="0" oninput="calculateActualAmount('+x+')"/>\n'+'</td>\n'+
                	'<td>\n'+'<select style="padding: 6px;border-radius: 4px" id="type_'+x+'" name="type[]" value="0" class="type" onchange="calculateActualAmount('+x+')">\n'+'<option value="1" selected>BDT</option>\n'+'<option value="0">%</option>\n'+'</select>\n'+'</td>\n'+
                	'<td>\n'+'<input type="text" id="amount_'+x+'" class="form-control amount" name="amount[]" value="0" oninput="calculateActualAmount('+x+')"/>\n'+'</td>\n'+
                	'<td style="text-align: center">\n'+'<a href="#" class="remove_field">\n'+'<i style="font-size: 25px" class="fa fa-trash"></i>\n'+'</a>\n'+'</td>\n'+
                	'</tr>\n');

                $('.select2').select2();
            }	
        });
        //For apending another rows end

        $(wrapper).on("click",".remove_field", function(e)
        { 
            e.preventDefault(); 
            $(this).parent().parent().remove(); x--;

            calculateActualAmount(x);
        }); 
        
        function getItemPrice(x)
        {
        	//For getting item commission information from items table start
            var item_id  = $("#item_id_"+x).val();
            // console.log(item_id);
            var site_url = $(".site_url").val();
            if(item_id)
            {
                $.get(site_url+'/invoices/get-item-rate/'+ item_id, function(data){

                    $("#rate_"+x).val(data.price);
                    $("#agent_commission_"+x).val(data.commission);
                    $("#amount_"+x).val(data.price);
                    $("#amount_commission_"+x).val(data.commission);
                    //Code for making commission type selected start
                    if (data.commission_type == 1)
                    {
                    	$("#commission_type_"+x+" option[value=0]").removeAttr('selected');
                    	$("#commission_type_"+x+" option[value=1]").attr('selected', 'selected');
                    }
                    else
                    {
                    	$("#commission_type_"+x+" option[value=1]").removeAttr('selected');
                    	$("#commission_type_"+x+" option[value=0]").attr('selected', 'selected');
                    }
                    //Code for making commission type selected end 
                    calculateActualAmount(x);

                });
            }
            //For getting item commission information from items table end
        }

        function calculateActualAmount(x)
        {	
    		var rate                    = $("#rate_"+x).val();
            var inch                    = $("#inch_"+x).val();
            var column                  = $("#column_"+x).val();
            var discount                = $("#discount_"+x).val();
            var discountType            = $("#type_"+x).val();
            var vatType            		= $("#vat_type_0").val();
            var vatAmount         		= $("#vat_amount_0").val();
            var taxType            		= $("#tax_type_0").val();
            var taxAmount         		= $("#tax_amount_0").val();
            var adjustment         		= $("#adjustment_0").val();
            var adjustmentType      	= $("#adjustment_type_0").val();
            var colorPrint         		= $("#color_print_0").val();
            var colorPrintType     		= $("#color_print_type_0").val();

    		var comissionType      		= $("#commission_type_"+x).val();
    		var comissionAmon      		= $("#agent_commission_"+x).val();


    		if (colorPrint == 0)
            {
                var colorPrint       	= 0;
            }else{
                var colorPrint       	= $("#color_print_0").val();
            }

            if (rate == '')
            {
                var rateCal             = 1;
            }else{
                var rateCal             = $("#rate_"+x).val();
            }

            if (inch == '')
            {
                var inchCal         	= 1;
            }else{
                var inchCal       		= $("#inch_"+x).val();
            }

            if (column == '')
            {
                var columnCal         = 1;
            }else{
                var columnCal         = $("#column_"+x).val();
            }

            if (discount == '')
            {
                var discountCal         = 0;
            }else{
                var discountCal         = $("#discount_"+x).val();
            }

            if (discountType == 0)
            {
                var discountTypeCal     = (parseFloat(discountCal)*parseFloat(rateCal)*parseFloat(inchCal)*parseFloat(columnCal))/100;
            }else{
                var discountTypeCal     = $("#discount_"+x).val();
            } 

            if (adjustment == 0)
            {
                var adjustmentCal     = 0;
            }else{
                var adjustmentCal     = $("#adjustment_0").val();
            }
            
            if (comissionAmon == '')
            {	
            	$("#agent_commission_"+x).val(0);
                var comissionAmonCal     = 0;
            }else{
                var comissionAmonCal     = $("#agent_commission_"+x).val();
            }

            if (comissionType == 0)
            {	
            	var total_amount_cal     = (parseFloat(rateCal)*parseFloat(inchCal)*parseFloat(columnCal)) - parseFloat(discountTypeCal);
                var comissionTypeCal     = (parseFloat(comissionAmonCal)*total_amount_cal)/100;
            }else{
                var comissionTypeCal     = $("#agent_commission_"+x).val();
            } 

    		$("#amount_commission_"+x).val(comissionTypeCal);

    		var subTotalAmountIn   =  (parseFloat(rateCal)*parseFloat(inchCal)*parseFloat(columnCal)) - parseFloat(discountTypeCal);

    		$("#amount_"+x).val(subTotalAmountIn);

            //Calculating Subtotal Amount
            var total       = 0;

            $('.amount').each(function()
            {
                total       += parseFloat($(this).val());
            });

            //Calculating Agent Total Comission
            var total_commission       = 0;

            $('.amountCommission').each(function()
            {	
                total_commission       += parseFloat($(this).val());
            });

            var subTotalShow = total;

            if (colorPrintType == 0)
            {
                var colorPrintTypeCal     = (parseFloat(colorPrint)*parseFloat(total))/100;
            }else{
                var colorPrintTypeCal     = parseFloat(colorPrint);
            }

            if (adjustmentType == 0)
            {	
                var adjustmentTypeCal     = (parseFloat(adjustmentCal)*(parseFloat(total) + parseFloat(colorPrintTypeCal)))/100;
            }else{
                var adjustmentTypeCal     = parseFloat(adjustmentCal);
            }

            if (vatAmount == '')
            {	
            	$("#vat_amount_0").val(0);
                var vatCal         = 1;
            }else{
                var vatCal         = $("#vat_amount_0").val();
            }

            if (vatType == 0)
            {
                var vatTypeCal     = (parseFloat(vatCal)*(subTotalShow + parseFloat(adjustmentTypeCal) + parseFloat(colorPrintTypeCal)))/100;
            }else{
                var vatTypeCal     = $("#vat_amount_0").val();
            }

            if (taxAmount == '')
            {	
            	$("#tax_amount_0").val(0);
                var taxCal         = 1;
            }else{
                var taxCal         = $("#tax_amount_0").val();
            }

            if (taxType == 0)
            {
                var taxTypeCal     = (parseFloat(taxCal)*(subTotalShow + parseFloat(adjustmentTypeCal) + parseFloat(colorPrintTypeCal)))/100;
            }else{
                var taxTypeCal     = $("#tax_amount_0").val();
            }

            var totalShow = total + parseFloat(vatTypeCal) + parseFloat(taxTypeCal) + parseFloat(adjustmentTypeCal) + parseFloat(colorPrintTypeCal);

            $("#subTotalBdtShow").html(subTotalShow);
            $("#totalBdtShow").html(totalShow);
            $("#total_agent_commission").val(total_commission);
            console.log(total_commission);
    		if (isNaN(total_commission))
    		{
    			$("#total_agent_commission").val(0);
    		}
    		
            $("#totalBdt").val(totalShow);
            $("#subTotalBdt").val(subTotalShow);
            //Calculating Actual Amount Ends
        }
    </script>

    <script type="text/javascript">
    	function invoiceType(c)
    	{
    		var get_val = $("#invoice_type_"+c).val();

    		if (get_val == '2')
    		{
    			$("#inch_"+c).attr("readonly", true);
    			$("#column_"+c).attr("readonly", true);
    		}
    		else
    		{
    			$("#inch_"+c).attr("readonly", false);
    			$("#column_"+c).attr("readonly", false);
    		}

    	}
    </script>
@endpush