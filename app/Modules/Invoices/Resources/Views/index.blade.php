@extends('layouts.app')

@section('title', 'Invoice List')

@section('content')
	<style type="text/css">
		.customAlign table thead tr th{
			vertical-align: middle;
		}
		.table-tr-style tr:nth-child(even) {
			background-color: #dddddd;
		}
		.table-tr-style tr:nth-child(odd) {
			background-color: #F2F3F4;
		}
		.total th{
			font-size: 20px
		}
		.select2-container .select2-selection--single{
			height: auto
		}
		.select2-container--default .select2-selection--single .select2-selection__rendered{
			line-height: 20px
		}
		.select2-container .select2-selection--single .select2-selection__rendered{
			margin-top: 0px
		}
	</style>

	<section class="content">
		<div class="box">
			<div class="box-header with-border">
			    <div class="col-md-12">
				<h3 class="box-title" style="width:100%">
						Invoice List <a href="{{ route('invoices_create') }}" class="btn btn-success btn-xs pull-right">
					   <i class="fa fa-plus"></i> Add New
					</a>
				</h3>
				</div>
			</div>

			@if(count($invoices) >0)
			<div class="box-body">
				<div class="row">
					@if(Session::has('message'))
					<div class="col-md-12">
						<div class="alert alert-success alert-dismissable text-center">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							{{Session::get('message')}}
						</div>
					</div>
					@endif
					@if(Session::has('errors'))
					<div class="col-md-12">
						<div class="alert alert-success alert-dismissable text-center">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							{{Session::get('errors')}}
						</div>
					</div>
					@endif
				</div>

				<div class="col-md-12 customAlign table-responsive">

					<table id="dataTable" class="table table-bordered table-striped dataTable">
						<thead>
							<tr style="background-color: #F9E79F">
								<th>SL</th>
								<th>Invoice Number</th>
								<th>Customer Name</th>
								<th>Invoice Date</th>
								<th>Publication Date</th>
								<th>Total Amount</th>
								<!-- <th>Paid</th> -->
								<!-- <th>Other Expense</th> -->
								<th>Due</th>
								<th>Agent Name</th>
								<th style="width:90px">Action</th>
							</tr>
						</thead>
						<tbody>
						@if(!empty($invoices) && (count($invoices)>0) )
						@foreach($invoices as $key => $invoice)
							<tr>
								<td>{{ $key + 1 }}</td>
								<td>{{ $invoice->invoice_number }}</td>
								<td>{{ isset($invoice->customer->name) ? $invoice->customer->name : '' }}</td>
								<td>{{ date('d-m-Y', strtotime($invoice->invoice_date)) }}</td>
								<td>{{ date('d-m-Y', strtotime($invoice->due_date)) }}</td>
								<td>{{ number_format($invoice->total_amount,2,'.',',') }}</td>
								<!-- <td>{{ number_format($invoice->total_amount - $invoice->due_amount - $invoice->other_expense,2,'.',',') }}</td> -->
								<!-- <td>{{ number_format($invoice->other_expense,2,'.',',') }}</td> -->
								<td>{{ number_format($invoice->due_amount,2,'.',',') }}</td>
								<td>{{ isset($invoice->agent->name) ? $invoice->agent->name : '' }}</td>
								<td>
									<a href="{{ route('invoices_show', $invoice->id) }}" class="btn btn-info btn-xs"><i title="Show" class="fa fa-eye" aria-hidden="true"></i></a>
									<a href="{{ route('invoices_edit', $invoice->id) }}" class="btn btn-success btn-xs"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i></a>
									<a href="{{ route('invoices_delete', $invoice->id) }}" class="btn btn-danger btn-xs confirm_box"><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></a>
									<a @if( $invoice->email_status == 0) class="btn btn-success btn-xs" @else class="btn btn-danger btn-xs" @endif data-toggle="modal" data-target="#modal-default" onclick="getInvoiceId({{$key}})"><i title="Email" class="fa fa-envelope" aria-hidden="true"></i></a>
									<input id="get_invoice_id_{{$key}}" type="hidden" name="get_invoice_id" value="{{$invoice->id}}">
								</td>
							</tr>
						@endforeach
						@endif
						</tbody>
					</table>
				</div>
		    </div>
		    @else
		    <div class="box-body">
		    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
		    	<p style="text-align: center;font-size: 18px">No Data Found.</p>
		    </div>
		    </div>
		    @endif
		</div>
	</section>

	<div class="modal fade" id="modal-default">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title text-center">Send Invoice</h4>
				</div>

				<div class="modal-body">
					<div class="row">
						<form method="POST" action="{{ route('send_invoice') }}" enctype="multipart/form-data">
							{{ csrf_field() }}

							<input style="display: none" id="invoice_id" type="text" name="invoice_id">

							<div class="form-group">
								<div class="col-md-12">
									<label>To*</label>
									<input id="to" class="form-control" type="text" name="to" required="required">
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-12">
									<br>
									<label>Subject</label>
									<input id="subject" class="form-control" type="text" name="subject">
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-12">
									<br>
									<label>Message</label>
									<textarea class="form-control" type="text" name="message" rows="5"></textarea>
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-12">
									<br>
									<button type="submit" class="btn btn-success btn-block" style="border-radius: 0px">Send 
										Email
									<br>
								</div>
							</div>
						</form>

					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
@endsection

@push('scripts')
	<script type="text/javascript">
		$(".confirm_box").click(function()
		{
			if (!confirm("Do you want to delete"))
			{
				return false;
			}
		});
	</script>

	<script type="text/javascript">
		function getInvoiceId(id)
		{	
			var invoice_id = $('#get_invoice_id_'+id).val();
			$('#invoice_id').val(invoice_id);
		}
	</script>
@endpush