@extends('layouts.app')

@section('title', 'Show Invoice')

@section('content')

<section class="table-responsive content">
	<div class="box">
		<div class="box-header with-border">
		    <div class="col-md-12">
			<h3 class="box-title" style="width:100%">
				Invoice
				<a href="{{ route('customer_invoices_download', $invoice->id) }}" target="_blank">
				<button type="button" class="btn btn-success btn-xs pull-right">
					<i class="fa fa-download"></i> Download PDF
				</button>
			</a>
			</h3>
			</div>
		</div>


		<div style="padding: 20px">
		<table style="width: 100%">
			<tr>
				<td style="width: 50%">
					<img src="http://dainikdhakareport.net/uploads/settings/logologo-1-1541316160.png">
				</td>
				<td style="width: 50%;text-align: right">
					<p style="text-align: right;font-size: 20px;padding-bottom: 20px"><span style="border-bottom: 1px solid black;">বিল</span></p>
					<p style="font-size: 18px">বিল নং : <span style="font-size: 20px">@php $billNo = \App\Http\Controllers\HomeController::GetBangla($invoice->invoice_number); echo $billNo; @endphp</span> </p>
					<p style="font-size: 18px">নির্দেশক সূত্র : {{ $invoice->nirdesok_number }}</p>
					<p style="font-size: 18px">ছাপার তারিখ : <span style="font-size: 20px">@php $printDate = \App\Http\Controllers\HomeController::GetBangla(date('d-m-Y', strtotime($invoice->due_date))); echo $printDate; @endphp</span></p>
					<p style="font-size: 18px">বিল তৈরির তারিখ : <span style="font-size: 20px">@php $billDate = \App\Http\Controllers\HomeController::GetBangla(date('d-m-Y')); echo $billDate; @endphp</span></p>
				</td>
			</tr>
		</table>

		<p style="border-bottom: 2px solid #cacaca;"></p>

		<table style="width: 100%;">
			<tr>
				<td style="width: 50%;text-align: left">
					<p style="font-size: 18px">প্রেরকঃ</p>
					<p style="font-size: 18px">দৈনিক ঢাকা রিপোর্ট</p>
					<p style="font-size: 18px">৬৮ যোগীনগর রোড, ওয়ারী, ঢাকা-১২০৩</p>
					<p style="font-size: 18px">ফোন : ০২৭১১৬৫৬৯, ০২৭১১৬৫৬৯</p>
					<p style="font-size: 18px">ইমেইল : <span style="font-size: 15px">dhakareportad@gmail.com</span></p>
				</td>

				<td style="width: 50%;text-align: left">
					<p style="font-size: 18px">প্রাপকঃ</p>
					<p style="font-size: 18px">{{ $invoice->customer->name }}</p>
					<p style="font-size: 18px">{{ $invoice->customer->address }}</p>
					<p style="font-size: 18px">ফোন : {{ $invoice->customer->mobile }}</p>
					<p style="font-size: 18px">ইমেইল : <span style="font-size: 15px">{{ $invoice->customer->email }}</span></p>
				</td>
			</tr>
		</table>

		<p style="border-bottom: 2px solid #cacaca;"></p>



		<table style="width:100%;border-collapse: collapse;">
			<tr style="background-color: #FFFAC3;">
				<td style="padding: 10px 5px;font-size: 18px;border-top: 1px solid #cacaca;">নং</td>
				<td style="padding: 10px 5px;font-size: 18px;border-top: 1px solid #cacaca;">বিজ্ঞাপন</td>
				<td style="padding: 10px 5px;font-size: 18px;text-align: center;border-top: 1px solid #cacaca;">আকার</td>
				<td style="padding: 10px 5px;text-align: center;font-size: 18px;border-top: 1px solid #cacaca;">মূল্য হার (১ইঞ্চি X ১কলাম)</td>
				<td style="padding: 10px 5px;text-align: right;font-size: 18px;border-top: 1px solid #cacaca;">অন্যান্য</td>
				<td style="padding: 10px 5px;text-align: right;font-size: 18px;border-top: 1px solid #cacaca;">মোট</td>
			</tr>

			@foreach($invoice_details as $key => $invoice_detail)
			<tr style="background-color: #FBF9F9">
				<td style="padding: 8px 5px;font-size: 16px;border-top: 1px solid #cacaca;">@php $sl = \App\Http\Controllers\HomeController::GetBangla($key+1); echo $sl; @endphp</td>
				<td style="padding: 8px 5px;font-size: 16px;border-top: 1px solid #cacaca;">{{ $invoice_detail->item->name }}</td>
				<td style="padding: 8px 5px;font-size: 16px;text-align: center;border-top: 1px solid #cacaca;">
					@php $inch = \App\Http\Controllers\HomeController::GetBangla($invoice_detail->inch); @endphp
					@php $colum = \App\Http\Controllers\HomeController::GetBangla($invoice_detail->colum); @endphp
					{{ $inch.' ইঞ্চি'.' X '.$colum.' কলাম' }}</td>
					<td style="padding: 8px 5px;font-size: 20px;text-align: center;border-top: 1px solid #cacaca;">
					@php 
						$itemPrice = \App\Http\Controllers\HomeController::GetBangla( number_format($invoice_detail->item->price,2,'.',',')); 
					@endphp 
						{{ $itemPrice }}
					</td>
					<td style="padding: 8px 5px;font-size: 20px;text-align: right;border-top: 1px solid #cacaca;">
						@if($invoice_detail->discount_type == 0)
						@php 
							$discount = \App\Http\Controllers\HomeController::GetBangla( number_format((($invoice_detail->discount_amount*$invoice_detail->amount)/100),2,'.',',') );
						@endphp
						@else
							@php $discount = \App\Http\Controllers\HomeController::GetBangla(number_format($invoice_detail->discount_amount,2,'.',',')); @endphp
						@endif
							{{ $discount }}
					</td>
					<td style="padding: 8px 5px;font-size: 20px;text-align: right;border-top: 1px solid #cacaca;">
					@php 
						$amount = \App\Http\Controllers\HomeController::GetBangla(number_format($invoice_detail->amount,2,'.',',')); 
					@endphp 
						{{ $amount }}
					</td>
				</tr>
				@endforeach

				<tr>
					<td style="border-top: 1px solid #cacaca;"></td>
					<td style="border-top: 1px solid #cacaca;"></td>
					<td style="border-top: 1px solid #cacaca;"></td>
					<td style="border-top: 1px solid #cacaca;"></td>
					<td style="border-top: 1px solid #cacaca;text-align: right;font-size: 16px;padding: 8px 5px;background-color: #FBF9F9">অন্যান্য : </td>
					<td style="border-top: 1px solid #cacaca;text-align: right;padding: 8px 5px;background-color: #FBF9F9;font-size: 20px">@php $adjustment = \App\Http\Controllers\HomeController::GetBangla(number_format($invoice->adjustment,2,'.',',')); @endphp {{ $adjustment }}</td>
				</tr>

				<tr>
					<td style="border-top: 1px solid #cacaca;"></td>
					<td style="border-top: 1px solid #cacaca;"></td>
					<td style="border-top: 1px solid #cacaca;"></td>
					<td style="border-top: 1px solid #cacaca;"></td>
					<td style="border-top: 1px solid #cacaca;text-align: right;padding: 8px 5px;font-size: 16px;background-color: #FBF9F9">ভ্যাট {{ $invoice->vat_type == 0 ? '('.$invoice->vat_amount.'%'.')' : '' }} : </td>
					<td style="border-top: 1px solid #cacaca;text-align: right;padding: 8px 5px;font-size: 20px;background-color: #FBF9F9">

						@if($invoice->vat_type == 0)
						@php $vat_amount = \App\Http\Controllers\HomeController::GetBangla(number_format((($invoice->vat_amount*$total_amount_sum)/100),2,'.',',')); @endphp
						@else
						@php $vat_amount = \App\Http\Controllers\HomeController::GetBangla(number_format($invoice->vat_amount,2,'.',',')); @endphp
						@endif
						{{ $vat_amount }}
					</td>
				</tr>

				<tr>
					<td style="border-top: 1px solid #cacaca;"></td>
					<td style="border-top: 1px solid #cacaca;"></td>
					<td style="border-top: 1px solid #cacaca;"></td>
					<td style="border-top: 1px solid #cacaca;"></td>
					<td style="border-top: 1px solid #cacaca;text-align: right;padding: 8px 5px;font-size: 16px;background-color: #FBF9F9">স:বি:কর {{ $invoice->tax_type == 0 ? '('.$invoice->tax_amount.'%'.')ঃ' : '' }} : </td>
					<td style="border-top: 1px solid #cacaca;text-align: right;padding: 8px 5px;font-size: 20px;background-color: #FBF9F9">

						@if($invoice->tax_type == 0)
						@php $tax_amount = \App\Http\Controllers\HomeController::GetBangla(number_format((($invoice->tax_amount*$total_amount_sum)/100),2,'.',',')); @endphp
						@else
						@php $tax_amount = \App\Http\Controllers\HomeController::GetBangla(number_format($invoice->tax_amount,2,'.',',')); @endphp
						@endif

						{{$tax_amount }}
					</td>
				</tr>

				<tr>
					<td style="border-top: 1px solid #cacaca;border-bottom: 1px solid #cacaca;"></td>
					<td style="border-top: 1px solid #cacaca;border-bottom: 1px solid #cacaca;"></td>
					<td style="border-top: 1px solid #cacaca;border-bottom: 1px solid #cacaca;"></td>
					<td style="border-top: 1px solid #cacaca;border-bottom: 1px solid #cacaca;"></td>
					<td style="border-top: 1px solid #cacaca;text-align: right;padding: 8px 5px;font-size: 16px;background-color: #FBF9F9;border-bottom: 1px solid #cacaca;">সর্বমোটঃ : </td>
					<td style="border-top: 1px solid #cacaca;text-align: right;padding: 8px 5px;font-size: 20px;background-color: #FBF9F9;border-bottom: 1px solid #cacaca;">@php $total_amount = \App\Http\Controllers\HomeController::GetBangla(number_format($invoice->total_amount,2,'.',',')); @endphp {{ $total_amount }}</td>
				</tr>

				<tr>
					<td colspan="6" style="font-size: 18px;text-align: right;">{{ $invoice->amount_bangla ? 'কথায়ঃ '.$invoice->amount_bangla : '' }}</td>
				</tr>

			</table>

	<!-- <div class="row invoice-info">
		<div class="col-sm-4 invoice-col">
			প্রেরক
			<address>
				<strong>দৈনিক ঢাকা রিপোর্ট</strong><br>
				৬৮ যোগীনগর রোড, ওয়ারী, ঢাকা-১২০৩<br>
				ফোন : ০২৭১১৬৫৬৯<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;০২৭১১৬৫৬৯<br>
				ইমেইল : dhakareportad@gmail.com
			</address>
		</div>

		<div class="col-sm-4 invoice-col">
			প্রাপক
			<address>
				<strong>{{ $invoice->customer->name }}</strong><br>
				{{ $invoice->customer->address }}<br>
				ফোন : {{ $invoice->customer->mobile }}<br>
				ইমেইল : {{ $invoice->customer->email }}
			</address>
		</div>

		<div class="col-sm-4 invoice-col">
			<b>বিল নং :</b> {{ $invoice->invoice_number }}<br>
			<b>নির্দেশক সূত্র :</b> {{ $invoice->nirdesok_number }}<br>
			<b>ছাপার তারিখ :</b> {{ date('d-m-Y', strtotime($invoice->due_date)) }}<br>
			<b>বিল তৈরির তারিখ :</b> {{ date('d-m-Y') }}<br>
		</div>
	</div> -->

	<!-- <div class="row">
		<div class="col-xs-12 table-responsive">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>নং</th>
						<th>বিজ্ঞাপন</th>
						<th>আকার</th>
						<th style="text-align: right">মূল্য হার (১ইঞ্চি X ১কলাম)</th>
						<th style="text-align: right">ছাড়</th>
						<th style="text-align: right">মোট</th>
					</tr>
				</thead>

				<tbody>
					@foreach($invoice_details as $key => $invoice_detail)
					<tr>
						<td>{{ $key + 1 }}</td>
						<td>{{ $invoice_detail->item->name }}</td>
						<td>{{ $invoice_detail->inch.' ইঞ্চি'.' X '.$invoice_detail->colum.' কলাম' }}</td>
						<td style="text-align: right">{{ $invoice_detail->item->price }}</td>
						<td style="text-align: right">{{ $invoice_detail->discount_type == 0 ? ($invoice_detail->discount_amount*$invoice_detail->amount)/100 : $invoice_detail->discount_amount }}</td>
						<td style="text-align: right">{{ $invoice_detail->amount }}</td>
					</tr>
					@endforeach
				</tbody>

				<tfoot>
					<tr style="border-color: white">
						<th style="border-color: white"></th>
						<th style="border-color: white"></th>
						<th style="border-color: white"></th>
						<th style="border-color: white"></th>
						<th style="text-align: right;border-color: white;line-height: 7px"><b>ছাড় : </b></th>
						<th style="text-align: right;border-color: white;width: 150px;line-height: 7px">{{ $invoice->adjustment != null ? $invoice->adjustment : 0 }}</th>
					</tr>

					<tr style="border-color: white">
						<th style="border-color: white"></th>
						<th style="border-color: white"></th>
						<th style="border-color: white"></th>
						<th style="border-color: white"></th>
						<th style="text-align: right;border-color: white;line-height: 7px"><b>ভ্যাট {{ $invoice->vat_type == 0 ? '('.$invoice->vat_amount.'%'.')' : '' }} : </b></th>
						<th style="text-align: right;border-color: white;width: 150px;line-height: 7px">{{ $invoice->vat_type == 0 ? ($invoice->vat_amount*$total_amount_sum)/100 : $invoice->vat_amount }}</th>
					</tr>

					<tr style="border-color: white">
						<th style="border-color: white"></th>
						<th style="border-color: white"></th>
						<th style="border-color: white"></th>
						<th style="border-color: white"></th>
						<th style="text-align: right;border-color: white;line-height: 7px"><b>স:বি:কর {{ $invoice->tax_type == 0 ? '('.$invoice->tax_amount.'%'.')' : '' }} : </b></th>
						<th style="text-align: right;border-color: white;width: 150px;line-height: 7px">{{ $invoice->tax_type == 0 ? ($invoice->tax_amount*$total_amount_sum)/100 : $invoice->tax_amount }}</th>
					</tr>

					<tr style="border-color: white">
						<th style="border-color: white"></th>
						<th style="border-color: white"></th>
						<th style="border-color: white"></th>
						<th style="border-color: white"></th>
						<th style="text-align: right;border-color: white;line-height: 7px"><b>সর্বমোটঃ : </b></th>
						<th style="text-align: right;border-color: white;width: 150px;line-height: 7px">{{ $invoice->total_amount }}</th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div> -->

	<div class="row no-print" style="margin-top: 30px">
		<div class="col-xs-12">
			<a href="{{ route('customer_invoices_download', $invoice->id) }}" target="_blank">
				<button type="button" class="btn btn-success pull-right">
					<i class="fa fa-download"></i> Download PDF
				</button>
			</a>
		</div>
	</div>
	</div>

</div>
</section>
@endsection
