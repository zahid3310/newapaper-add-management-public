<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'invoices', 'middleware' => 'auth'], function () {
    Route::get('/index', 'InvoiceController@index')->name('invoices_index');
    Route::get('/create', 'InvoiceController@create')->name('invoices_create');
    Route::post('/store', 'InvoiceController@store')->name('invoices_store');
    Route::get('/edit/{id}', 'InvoiceController@edit')->name('invoices_edit');
    Route::post('/update/{id}', 'InvoiceController@update')->name('invoices_update');
    Route::get('/delete/{id}', 'InvoiceController@destroy')->name('invoices_delete');
    Route::get('/get-item-rate/{id}', 'InvoiceController@getItemRate')->name('invoices_item_rate');
    Route::get('/invoice-show/{id}', 'InvoiceController@show')->name('invoices_show');
    Route::get('/download/{id}', 'InvoiceController@downloadInvoice')->name('invoices_download');
    Route::get('/download-with-header/{id}', 'InvoiceController@downloadInvoiceWithHeader')->name('invoices_download_with_header');
    Route::post('/send-invoice', 'InvoiceController@sendInvoice')->name('send_invoice');
});
