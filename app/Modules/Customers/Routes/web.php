<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'customers', 'middleware' => 'auth'], function () {
    Route::get('/index', 'CustomersController@index')->name('customers_index');
    Route::get('/create', 'CustomersController@create')->name('customers_create');
    Route::post('/store', 'CustomersController@store')->name('customers_store');
    Route::get('/edit/{id}', 'CustomersController@edit')->name('customers_edit');
    Route::post('/update/{id}', 'CustomersController@update')->name('customers_update');
    Route::get('/delete/{id}', 'CustomersController@destroy')->name('customers_delete');
});
