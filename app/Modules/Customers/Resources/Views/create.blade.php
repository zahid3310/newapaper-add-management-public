@extends('layouts.app')

@section('title', 'Create Customer')

@section('content')

	<section class="content">
		<div class="box">
			<div class="box-header with-border">
			<div class="col-md-12">
				<h3 class="box-title">
				Create Customer
				</h3>
			</div>
			</div>
			<div class="box-body">
				@if(Session::has('message'))
				<div style="padding: 0px" class="col-md-12">
					<div class="alert alert-success alert-dismissable text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{Session::get('message')}}
					</div>
				</div>
				@endif
				@if(Session::has('errors'))
				<div style="padding: 0px" class="col-md-12">
					<div class="alert alert-success alert-dismissable text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{Session::get('errors')}}
					</div>
				</div>
				@endif
				<form class="form-horizontal" method="POST" action="{{ route('customers_store') }}" enctype="multipart/form-data">
					{{ csrf_field() }}

					<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
						<label for="name" class="col-md-4 control-label">
						Name *
						</label>

						<div class="col-md-6">
							<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

							@if ($errors->has('name'))
							<span class="help-block">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('intro') ? ' has-error' : '' }}">
						<label for="intro" class="col-md-4 control-label">
						Introduction
						</label>

						<div class="col-md-6">
							<input id="intro" type="text" class="form-control" name="intro" value="{{ old('intro') }}">

							@if ($errors->has('intro'))
							<span class="help-block">
								<strong>{{ $errors->first('intro') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
						<label for="code" class="col-md-4 control-label">
						Code
						</label>

						<div class="col-md-6">
							<input id="code" type="text" class="form-control" name="code" value="{{ old('code') }}">

							@if ($errors->has('code'))
							<span class="help-block">
								<strong>{{ $errors->first('code') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
						<label for="address" class="col-md-4 control-label">
						Address
						</label>

						<div class="col-md-6">
							<input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}">

							@if ($errors->has('address'))
							<span class="help-block">
								<strong>{{ $errors->first('address') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
						<label for="email" class="col-md-4 control-label">
						Email
						</label>

						<div class="col-md-6">
							<input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}">

							@if ($errors->has('email'))
							<span class="help-block">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
						<label for="mobile" class="col-md-4 control-label">
						Mobile
						</label>

						<div class="col-md-6">
							<input id="mobile" type="text" class="form-control" name="mobile" value="{{ old('mobile') }}">

							@if ($errors->has('mobile'))
							<span class="help-block">
								<strong>{{ $errors->first('mobile') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
						<label for="photo" class="col-md-4 control-label">
						Photo
						</label>

						<div class="col-md-6">
							<input id="photo" type="file" class="form-control" name="photo">

							@if ($errors->has('photo'))
							<span class="help-block">
								<strong>{{ $errors->first('photo') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
							<button type="submit" class="btn btn-primary">
								<i class="fa fa-plus-circle"></i>
							Save
							</button>
							<a href="{{ route('customers_index') }}" class="btn btn-danger">
								<i class="fa fa-trash"></i>
							Cancel
							</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
@endsection