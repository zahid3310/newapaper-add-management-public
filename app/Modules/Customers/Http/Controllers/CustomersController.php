<?php

namespace App\Modules\Customers\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Models\Areas;
use App\Models\Districts;
use App\Models\Customers;

class CustomersController extends Controller
{
    public function index()
    {
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('customers_index');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $customers = Customers::where('status', '=', 1)->orderBy('name', 'ASC')->get();

        return view('customers::index', compact('customers'));
    }

    public function create()
    {
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('customers_create');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('customers::create');
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('customers_store');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $this->validate($request,[
         'name'  => 'required',
        ]);

        $customer               = new Customers;

        if($request->hasFile('photo'))
        {
            $image              = $request->file('photo');
            $image_name         = time().'.'.$image->getClientOriginalExtension();
            $file_path          = 'assets/images/customers/';
            $rowImage           = $image;
            $savingPath         = $file_path;
            $imageName          = $image_name;
            $resizeImage        = $this->compressAndResize($rowImage, $savingPath, $imageName, 50, 250,250);
            $customer->photo    = $image_name;
        }

        $customer->name         = $request->name;
        $customer->intro        = $request->intro;
        $customer->code         = $request->code;
        $customer->address      = $request->address;
        $customer->email        = $request->email;
        $customer->mobile       = $request->mobile;
        $customer->email        = $request->email;
        $customer->status       = 1;
        $customer->created_by   = Auth::user()->id;

        if ($customer->save())
        {
            return redirect()->route('customers_index')->with('message', 'Successfully added !!');
        }else{
            return back()->with('message', 'Something went wrong !! Please try again.');
        }
    }

    public function edit($id)
    {
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('customers_edit');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $customer = Customers::find($id);

        return view('customers::edit', compact('customer'));
    }

    public function update(Request $request, $id)
    {
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('customers_update');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End


        $this->validate($request,[
         'name'  => 'required',
        ]);

        $customer               = Customers::find($id);

        if($request->hasFile('photo'))
        {   
            if($customer->photo != null)
            {
                unlink('assets/images/customers/'.$customer->photo);
            }

            $image              = $request->file('photo');
            $image_name         = time().'.'.$image->getClientOriginalExtension();
            $file_path          = 'assets/images/customers/';
            $rowImage           = $image;
            $savingPath         = $file_path;
            $imageName          = $image_name;
            $resizeImage        = $this->compressAndResize($rowImage, $savingPath, $imageName, 50, 250,250);
            $customer->photo    = $image_name;
        }

        $customer->name         = $request->name;
        $customer->intro        = $request->intro;
        $customer->code         = $request->code;
        $customer->address      = $request->address;
        $customer->email        = $request->email;
        $customer->mobile       = $request->mobile;
        $customer->email        = $request->email;
        $customer->updated_by   = Auth::user()->id;

        if ($customer->save())
        {
            return redirect()->route('customers_index')->with('message', 'Successfully updated !!');
        }else{
            return back()->with('message', 'Something went wrong !! Please try again.');
        }
    }

    public function destroy($id)
    {
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('customers_delete');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $customer               = Customers::find($id);
        $customer->status       = 0;
        $customer->updated_by   = Auth::user()->id;

        if ($customer->save())
        {
            return redirect()->route('customers_index')->with('message', 'Successfully deleted !!');
        }else{
            return back()->with('message', 'Something went wrong !! Please try again.');
        }
    }

    public function compressAndResize($source_url, $destination_url, $imageName , $quality, $desired_width, $desired_height)
    {
        $info = getimagesize($source_url);
        if ($info['mime'] == 'image/jpeg')
        {
            list($width, $height) = getimagesize($source_url);
            $src = imagecreatefromjpeg($source_url);
            $dst = imagecreatetruecolor($desired_width, $desired_height);
            imagecopyresampled($dst, $src, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
            imagejpeg($dst, $destination_url.$imageName, $quality);
        }
        elseif ($info['mime'] == 'image/png'){
            list($width, $height) = getimagesize($source_url);
            $src = imagecreatefrompng($source_url);
            $dst = imagecreatetruecolor($desired_width, $desired_height);
            imagecopyresampled($dst, $src, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
            imagejpeg($dst, $destination_url.$imageName, $quality);
        }
        elseif ($info['mime'] == 'image/gif'){
            list($width, $height) = getimagesize($source_url);
            $src = imagecreatefromgif($source_url);
            $dst = imagecreatetruecolor($desired_width, $desired_height);
            imagecopyresampled($dst, $src, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
            imagejpeg($dst, $destination_url.$imageName, $quality);
        }
        return $destination_url.$imageName;
    }
}
