<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'report', 'middleware' => 'auth'], function () {

	//Customer Report
    Route::get('/customer-wise-report-index', 'ReportController@customerReportIndex')->name('customer_report_index');
    Route::get('/customer-wise-details', 'ReportController@customerReportDetails')->name('customer_wise_details_report_index');

    Route::get('/customer-wise-report-index-download', 'ReportController@customerReportIndexDownload')->name('customer_report_index_download');
    Route::get('/customer-wise-details-download', 'ReportController@customerReportDetailsDownload')->name('customer_wise_details_report_index_download');

    //Sales Commission Report
    Route::get('/sales-comission-report-index', 'ReportController@salesComissionReportIndex')->name('sales_comission_report_index');
    Route::get('/sales-comission-details', 'ReportController@salesComissionReportDetails')->name('sales_comission_details_report_index');
    
    Route::get('/sales-comission-report-index-download', 'ReportController@salesComissionReportIndexDownload')->name('sales_comission_report_index_download');
    Route::get('/sales-comission-details-download', 'ReportController@salesComissionReportDetailsDownload')->name('sales_comission_details_report_index_download');

    //Payment Report
    Route::get('/payment-report-index', 'ReportController@paymentReportIndex')->name('paynent_report_index');
    Route::get('/payment-report-index-download', 'ReportController@paymentReportIndexDownload')->name('payment_report_index_download');
    
    //Area Wise Report
    Route::get('/area-wise-report-index', 'ReportController@areaReportIndex')->name('area_report_index');
    Route::get('/area-wise-details', 'ReportController@areaReportDetails')->name('area_wise_details_report_index');

    Route::get('/area-wise-report-index-download', 'ReportController@areaReportIndexDownload')->name('area_report_index_download');
    Route::get('/area-wise-details-download', 'ReportController@areaReportDetailsDownload')->name('area_wise_details_report_index_download');

    //Payments Report
    Route::get('/payments-report-index', 'ReportController@paymentsReportIndex')->name('paynents_report_index');
    Route::get('/payments-report-index-download', 'ReportController@paymentsReportIndexDownload')->name('payments_report_index_download');
});
