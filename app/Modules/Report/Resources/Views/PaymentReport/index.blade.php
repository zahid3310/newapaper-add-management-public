@extends('layouts.app')

@section('title', 'Payment Report')

@section('content')
	<style type="text/css">
		.customAlign table thead tr th{
			vertical-align: middle;
		}
		.table-tr-style tr:nth-child(even) {
			background-color: #dddddd;
		}
		.table-tr-style tr:nth-child(odd) {
			background-color: #F2F3F4;
		}
		.total th{
			font-size: 20px
		}
		.select2-container .select2-selection--single{
			height: auto
		}
		.select2-container--default .select2-selection--single .select2-selection__rendered{
			line-height: 20px
		}
		.select2-container .select2-selection--single .select2-selection__rendered{
			margin-top: 0px
		}
		table.dataTable thead > tr > th {
		     padding-right: 10px !important; 
		}
	</style>

	<section class="content">
		<div class="box">
		    <div class="box-header with-border">
			    <div class="col-md-12">
					<h3 class="box-title" style="width:100%">Payment Report 
						<span class="pull-right">
							<a style="text-decoration: none" data-toggle="modal" data-target="#modal-default-customer">
								<button class="btn btn-primary btn-xs"><i class="fa fa-search"></i> Search By Customer</button>
							</a>

							<a style="text-decoration: none" data-toggle="modal" data-target="#modal-default-agent">
								<button class="btn btn-info btn-xs"><i class="fa fa-search"></i> Search By Agent</button>
							</a>

							<a style="text-decoration: none" @if(isset($_GET['from_date']) && isset($_GET['to_date'])) href="{{ \Request::fullUrl().'&download=1' }}" @else href="{{ route('payment_report_index_download') }}" @endif target="_blank">
								<button class="btn btn-success btn-xs"><i class="fa fa-download"></i> Download</button>
							</a>
						</span>
					</h3>
				</div>
			</div>
			
			<div class="box-body">
				<div col-md-12 col-sm-12 col-xs-12 col-lg-12>
					<h3 style="text-align: center;margin-bottom: 0px">
					@if(isset($company_profile['name']))
						{{ $company_profile['name'] }}
					@else
						Dainik Dhaka Report
					@endif
					</h3>
					<p style="text-align: center;font-size: 14px;margin-bottom: 0px">
						@if(isset($company_profile['address']))
							{{ $company_profile['address'] }}
						@else
							68, Joginagar wari ,Dhaka-1203
						@endif
					</p>
					<h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px">
						Payment Report
					</h4>
					@if(isset($_GET['from_date']) && isset($_GET['to_date']))
						<p style="text-align: center;margin-bottom: 0px;margin-top: 0px;font-size: 15px;font-weight: bold">
							From {{ date('d-m-Y', strtotime($_GET['from_date'])) }} To {{ date('d-m-Y', strtotime($_GET['to_date'])) }}
						</p>
					@endif
					<p style="text-align: center;font-size: 20px">
						@if(isset($customer_name))
							{{ $customer_name['name'] }}
						@elseif(isset($agent_name))
							{{ $agent_name['name'] }}
						@else
							All Customers & Agents
						@endif
					</p>
				</div>
				<div class="col-md-12 customAlign table-responsive">

					<table class="table table-bordered table-striped dataTable">
						<thead>
							<tr style="background-color: #F9E79F">
								<th>SL</th>
								<th>Payment Date</th>
								<th style="text-align: left">Invoice Number</th>
								<th style="text-align: left">Customer Name</th>
								<th style="text-align: left">Agent Name</th>
								<th style="text-align: left">Payment Type</th>
								<th style="text-align: left">Payment Methode</th>
								<th style="text-align: left">Payment Through</th>
								<th style="text-align: right">Other Expense</th>
								<th style="text-align: right">Payment Receive</th>
								<th style="text-align: right">Paid</th>
							</tr>
						</thead>
						<tbody>
							@php
								$total_receive = 0;
								$total_paid    = 0;
								$total_expense = 0;
							@endphp

							@foreach($transactions as $key => $transaction)
								@if($transaction->type == 'invoice')
									<?php $receive 	= $transaction->trans_paid; ?>
									<?php $paid 	= 0; ?>
								@elseif($transaction->type == 'sales_comission')
									<?php $paid 	= $transaction->trans_paid; ?>
									<?php $receive 	= 0; ?>
								@endif
									
								@php
									$total_receive = $total_receive + $receive;
									$total_paid    = $total_paid + $paid;
									$total_expense = $total_expense + $transaction->other_expense;
								@endphp

								<tr>
									<td>{{ $key + 1 }}</td>
									<td style="text-align: left">{{ date('d-m-Y', strtotime($transaction->date)) }}</td>
									<td style="text-align: left">{{ $transaction->invoice->invoice_number }}</td>
									<td style="text-align: left">{{ isset($transaction->customer->name) ? $transaction->customer->name : '' }}</td>
									<td style="text-align: left">{{ isset($transaction->agent->name) ? $transaction->agent->name : '' }}</td>
									<td style="text-align: left">{{ $transaction->type }}</td>
									<td style="text-align: left">{{ $transaction->payment_methode }}</td>
									<td style="text-align: left">{{ $transaction->payment_through }}</td>
									<td style="text-align: right">
										{{ number_format($transaction->other_expense,2,'.',',') }}
									</td>
									<td style="text-align: right">
										@if($transaction->type == 'invoice')
											{{ number_format($transaction->trans_paid,2,'.',',') }}
										@endif
									</td>
									<td style="text-align: right">
										@if($transaction->type == 'sales_comission')
											{{ number_format($transaction->trans_paid,2,'.',',') }}
										@endif
									</td>
								</tr>
							@endforeach

							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td style="text-align: right"><b>Total</b></td>
								<td style="text-align: right"><b>{{ number_format($total_expense,2,'.',',') }}</b></td>
								<td style="text-align: right"><b>{{ number_format($total_receive,2,'.',',') }}</b></td>
								<td style="text-align: right"><b>{{ number_format($total_paid,2,'.',',') }}</b></td>
							</tr>
							
						</tbody>
					</table>
				</div>
		    </div>
		    
		</div>
	</section>

	<div class="modal fade" id="modal-default-customer">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title text-center">Select date range and customer</h4>
					</div>

					<div class="modal-body">
						<div class="row">
							<form method="get" action="{{ route('paynent_report_index') }}" enctype="multipart/form-data">
								<div class="form-group">
									<div class="col-md-8 col-md-offset-2">
										<br>
										<label>Customer*</label><br>
										<select style="width: 100%" name="customer_id" class="form-control select2" required="required">
												<option value="0">--All Customer--</option>
											@foreach($search_customers as $search_customer)
													<option {{ $customer_id == $search_customer->id ? 'selected' : ''}} value="{{ $search_customer->id }}">{{ $search_customer->name }}</option>
											@endforeach
										</select>
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-8 col-md-offset-2">
										<br>
										<label>From Date*</label>
										<input type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control datepicker" name="from_date" value="{{ isset($_GET['from_date']) ? $_GET['from_date'] : '' }}" required="required">
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-8 col-md-offset-2">
										<br>
										<label>To Date*</label>
										<input type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control datepicker" name="to_date" value="{{ isset($_GET['to_date']) ? $_GET['to_date'] : '' }}" required="required">
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-8 col-md-offset-2">
										<br>
										<button type="submit" class="btn btn-success btn-block" style="border-radius: 0px">Search</button>
										<br>
									</div>
								</div>
							</form>

						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
	</div>

	<div class="modal fade" id="modal-default-agent">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title text-center">Select date range and agent</h4>
					</div>

					<div class="modal-body">
						<div class="row">
							<form method="get" action="{{ route('paynent_report_index') }}" enctype="multipart/form-data">
								<div class="form-group">
									<div class="col-md-8 col-md-offset-2">
										<br>
										<label>Agent*</label><br>
										<select style="width: 100%" name="agent_id" class="form-control select2" required="required">
												<option value="0">--All Agent--</option>
											@foreach($search_agents as $search_agent)
													<option {{ $agent_id == $search_agent->id ? 'selected' : ''}} value="{{ $search_agent->id }}">{{ $search_agent->name }}</option>
											@endforeach
										</select>
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-8 col-md-offset-2">
										<br>
										<label>From Date*</label>
										<input type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control datepicker" name="from_date" value="{{ isset($_GET['from_date']) ? $_GET['from_date'] : '' }}" required="required">
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-8 col-md-offset-2">
										<br>
										<label>To Date*</label>
										<input type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control datepicker" name="to_date" value="{{ isset($_GET['to_date']) ? $_GET['to_date'] : '' }}" required="required">
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-8 col-md-offset-2">
										<br>
										<button type="submit" class="btn btn-success btn-block" style="border-radius: 0px">Search</button>
										<br>
									</div>
								</div>
							</form>

						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
	</div>
@endsection

@push('scripts')
	<script type="text/javascript">
		$(".confirm_box").click(function()
		{
			if (!confirm("Do you want to delete"))
			{
				return false;
			}
		});
	</script>
@endpush