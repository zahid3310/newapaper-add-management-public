<<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style type="text/css">
	table, th, td {
	  border: 1px solid black;
	  border-collapse: collapse;
	  padding: 5px;
	  font-family: 'nikosh';
	}
</style>

<body>
	<div col-md-12 col-sm-12 col-xs-12 col-lg-12>
		<h3 style="text-align: center;margin-bottom: 0px">
		@if(isset($company_profile['name']))
			{{ $company_profile['name'] }}
		@else
			Dainik Dhaka Report
		@endif
		</h3>
		<p style="text-align: center;font-size: 14px;margin-bottom: 0px">
			@if(isset($company_profile['address']))
				{{ $company_profile['address'] }}
			@else
				68, Joginagar wari ,Dhaka-1203
			@endif
		</p>
		<h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px;margin-top: 0px">
			Customer Wise Invoice Report
		</h4>
		<p style="text-align: center;font-size: 20px;margin-top: 0px">{{ $customer->name }}</p>
	</div>
	<table style="width: 100%">
		<tr>
			<th style="text-align: left">SL</th>
			<th style="text-align: left">Invoice Date</th>
			<th style="text-align: left">Invoice Number</th>
			<th style="text-align: right">Total Receivable</th>
			<th style="text-align: right">Total Paid</th>
			<th style="text-align: right">Total Other Expense</th>
			<th style="text-align: right">Total Dues</th>
		</tr>

		@php
			$total_recevable = 0;
			$total_paid      = 0;
			$total_dues      = 0;
			$total_expense   = 0;
		@endphp
		@foreach($invoices as $key => $invoice)
			@php
				$total_recevable = $total_recevable + $invoice->trans_receivable;
				$total_paid      = $total_paid + ($invoice->trans_receivable - $invoice->trans_due);
				$total_dues      = $total_dues + $invoice->trans_due;
				$total_expense   = $total_expense + $invoice->other_expense;
			@endphp
			<tr>
				<td style="text-align: left">{{ $key + 1 }}</td>
				<td style="text-align: left">
					{{ date('d-m-Y', strtotime($invoice->invoice_date)) }}
				</td>
				<td style="text-align: left">
					{{ $invoice->invoice_number }}
				</td>
				<td style="text-align: right">{{ $invoice->trans_receivable }}</td>
				<td style="text-align: right">{{ $invoice->trans_receivable - $invoice->trans_due }}</td>
				<td style="text-align: right">{{ $invoice->other_expense != 0 ? $invoice->other_expense : 0 }}</td>
				<td style="text-align: right">{{ $invoice->trans_due }}</td>
			</tr>
		@endforeach
		<tr>
			<td style="text-align: left"></td>
			<td></td>
			<td style="text-align: right"><b>Total</b></td>
			<td style="text-align: right"><b>{{ $total_recevable }}</b></td>
			<td style="text-align: right"><b>{{ $total_paid - $total_expense }}</b></td>
			<td style="text-align: right"><b>{{ $total_expense }}</b></td>
			<td style="text-align: right"><b>{{ $total_dues }}</b></td>
		</tr>
	</table>
</body>
</html>