@extends('layouts.app')

@section('title', 'Customers Wise Details')

@section('content')
	<style type="text/css">
		.customAlign table thead tr th{
			vertical-align: middle;
		}
		.table-tr-style tr:nth-child(even) {
			background-color: #dddddd;
		}
		.table-tr-style tr:nth-child(odd) {
			background-color: #F2F3F4;
		}
		.total th{
			font-size: 20px
		}
		.select2-container .select2-selection--single{
			height: auto
		}
		.select2-container--default .select2-selection--single .select2-selection__rendered{
			line-height: 20px
		}
		.select2-container .select2-selection--single .select2-selection__rendered{
			margin-top: 0px
		}
		table.dataTable thead > tr > th {
		     padding-right: 10px !important; 
		}
	</style>

	<section class="content-header">
		<h1>
			Customers Wise Report
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i>
			 	Home
			 </a></li>
			<li><a href="#">
				Report
			</a></li>
			<li class="active">
				Customers Wise Details
			</li>
		</ol>
	</section>

	<section class="content">
		<div class="box">
			
			<div class="box-body">
				<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
					<a href="{{ route('customer_wise_details_report_index_download', 'customer_id='.$_GET['customer_id']) }}" target="_blank">
						<button style="border-radius: 0px;padding: 5px" class="btn btn-success col-md-1 col-sm-1 col-xs-5 col-lg-1 pull-right">Download</button>
					</a>
				</div>
				<br>
				<div col-md-12 col-sm-12 col-xs-12 col-lg-12>
					<h3 style="text-align: center;margin-bottom: 0px">
					@if(isset($company_profile['name']))
						{{ $company_profile['name'] }}
					@else
						Dainik Dhaka Report
					@endif
					</h3>
					<p style="text-align: center;font-size: 14px;margin-bottom: 0px">
						@if(isset($company_profile['address']))
							{{ $company_profile['address'] }}
						@else
							68, Joginagar wari ,Dhaka-1203
						@endif
					</p>
					<h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px">
						Customer Wise Invoice
					</h4>
					<p style="text-align: center;font-size: 20px">{{ $customer->name }}</p>
				</div>
				<div class="col-md-12 customAlign table-responsive">

					<table class="table table-bordered table-striped dataTable">
						<thead>
							<tr style="background-color: #F9E79F">
								<th>SL</th>
								<th>Invoice Date</th>
								<th>Invoice Number</th>
								<th style="text-align: right">Total Receivable</th>
								<th style="text-align: right">Total Paid</th>
								<th style="text-align: right">Total Other Expense</th>
								<th style="text-align: right">Total Dues</th>
							</tr>
						</thead>
						<tbody>
							@php
								$total_recevable = 0;
								$total_paid      = 0;
								$total_dues      = 0;
								$total_expense   = 0;
							@endphp
							@foreach($invoices as $key => $invoice)
								@php
									$total_recevable = $total_recevable + $invoice->trans_receivable;
									$total_paid      = $total_paid + ($invoice->trans_receivable - $invoice->trans_due);
									$total_dues      = $total_dues + $invoice->trans_due;
									$total_expense   = $total_expense + $invoice->other_expense;
								@endphp
								<tr>
									<td>{{ $key + 1 }}</td>
									<td>{{ date('d-m-Y', strtotime($invoice->invoice_date)) }}</td>
									<td>{{ $invoice->invoice_number }}</td>
									<td style="text-align: right">{{ $invoice->trans_receivable }}</td>
									<td style="text-align: right">{{ $invoice->trans_receivable - $invoice->trans_due - $invoice->other_expense }}</td>
									<td style="text-align: right">{{ $invoice->other_expense != 0 ? $invoice->other_expense : 0 }}</td>
									<td style="text-align: right">{{ $invoice->trans_due }}</td>
								</tr>
							@endforeach
								<tr>
									<td></td>
									<td></td>
									<td style="text-align: right"><b>Total</b></td>
									<td style="text-align: right"><b>{{ $total_recevable }}</b></td>
									<td style="text-align: right"><b>{{ $total_paid - $total_expense }}</b></td>
									<td style="text-align: right"><b>{{ $total_expense }}</b></td>
									<td style="text-align: right"><b>{{ $total_dues }}</b></td>
								</tr>
						</tbody>
					</table>

				</div>
		    </div>
		    
		</div>
	</section>
@endsection

@push('scripts')
	<script type="text/javascript">
		$(".confirm_box").click(function()
		{
			if (!confirm("Do you want to delete"))
			{
				return false;
			}
		});
	</script>
@endpush