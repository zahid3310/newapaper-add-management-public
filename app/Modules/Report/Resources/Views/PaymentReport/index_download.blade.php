<<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style type="text/css">
	table, th, td {
	  border: 1px solid black;
	  border-collapse: collapse;
	  padding: 5px;
	  font-family: 'nikosh';
	}
</style>

<body>
	<div col-md-12 col-sm-12 col-xs-12 col-lg-12>
		<h3 style="text-align: center;margin-bottom: 0px">
		@if(isset($company_profile['name']))
			{{ $company_profile['name'] }}
		@else
			Dainik Dhaka Report
		@endif
		</h3>
		<p style="text-align: center;font-size: 14px;margin-bottom: 0px">
			@if(isset($company_profile['address']))
				{{ $company_profile['address'] }}
			@else
				68, Joginagar wari ,Dhaka-1203
			@endif
		</p>
		<h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px">
			Payment Report
		</h4>
		@if(isset($_GET['from_date']) && isset($_GET['to_date']))
			<p style="text-align: center;margin-bottom: 0px;margin-top: 0px;font-size: 15px;font-weight: bold">
				From {{ date('d-m-Y', strtotime($_GET['from_date'])) }} To {{ date('d-m-Y', strtotime($_GET['to_date'])) }}
			</p>
		@endif
		<p style="text-align: center;font-size: 20px;margin-top: 0px">
			@if(isset($customer_name))
				{{ $customer_name['name'] }}
			@elseif(isset($agent_name))
				{{ $agent_name['name'] }}
			@else
				All Customers & Agents
			@endif
		</p>
	</div>

	<table style="width: 100%">
		<thead>
			<tr style="background-color: #F9E79F">
				<th>SL</th>
				<th>Payment Date</th>
				<th style="text-align: left">Invoice Number</th>
				<th style="text-align: left">Customer Name</th>
				<th style="text-align: left">Agent Name</th>
				<th style="text-align: left">Payment Type</th>
				<th style="text-align: left">Payment Methode</th>
				<th style="text-align: left">Payment Through</th>
				<th style="text-align: right">Other Expense</th>
				<th style="text-align: right">Payment Receive</th>
				<th style="text-align: right">Paid</th>
			</tr>
		</thead>
		<tbody>
			@php
				$total_receive = 0;
				$total_paid    = 0;
				$total_expense = 0;
			@endphp

			@foreach($transactions as $key => $transaction)
				@if($transaction->type == 'invoice')
					<?php $receive 	= $transaction->trans_paid; ?>
					<?php $paid 	= 0; ?>
				@elseif($transaction->type == 'sales_comission')
					<?php $paid 	= $transaction->trans_paid; ?>
					<?php $receive 	= 0; ?>
				@endif
					
				@php
					$total_receive = $total_receive + $receive;
					$total_paid    = $total_paid + $paid;
					$total_expense = $total_expense + $transaction->other_expense;
				@endphp

				<tr>
					<td>{{ $key + 1 }}</td>
					<td style="text-align: left">{{ date('d-m-Y', strtotime($transaction->date)) }}</td>
					<td style="text-align: left">{{ $transaction->invoice->invoice_number }}</td>
					<td style="text-align: left">{{ isset($transaction->customer->name) ? $transaction->customer->name : '' }}</td>
					<td style="text-align: left">{{ isset($transaction->agent->name) ? $transaction->agent->name : '' }}</td>
					<td style="text-align: left">{{ $transaction->type }}</td>
					<td style="text-align: left">{{ $transaction->payment_methode }}</td>
					<td style="text-align: left">{{ $transaction->payment_through }}</td>
					<td style="text-align: right">
						{{ number_format($transaction->other_expense,2,'.',',') }}
					</td>
					<td style="text-align: right">
						@if($transaction->type == 'invoice')
							{{ number_format($transaction->trans_paid,2,'.',',') }}
						@endif
					</td>
					<td style="text-align: right">
						@if($transaction->type == 'sales_comission')
							{{ number_format($transaction->trans_paid,2,'.',',') }}
						@endif
					</td>
				</tr>
			@endforeach

			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td style="text-align: right"><b>Total</b></td>
				<td style="text-align: right"><b>{{ number_format($total_expense,2,'.',',') }}</b></td>
				<td style="text-align: right"><b>{{ number_format($total_receive,2,'.',',') }}</b></td>
				<td style="text-align: right"><b>{{ number_format($total_paid,2,'.',',') }}</b></td>
			</tr>
	</table>
</body>
</html>