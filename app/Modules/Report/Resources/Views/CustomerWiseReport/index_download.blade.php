<<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style type="text/css">
	table, th, td {
	  border: 1px solid black;
	  border-collapse: collapse;
	  padding: 5px;
	  font-family: 'nikosh';
	}
</style>

<body style="font-family: 'nikosh';">
	<div col-md-12 col-sm-12 col-xs-12 col-lg-12>
		<h3 style="text-align: center;margin-bottom: 0px">
		@if(isset($company_profile['name']))
			{{ $company_profile['name'] }}
		@else
			Dainik Dhaka Report
		@endif
		</h3>
		<p style="text-align: center;font-size: 14px;margin-bottom: 0px;margin-top: 0px">
			@if(isset($company_profile['address']))
				{{ $company_profile['address'] }}
			@else
				68, Joginagar wari ,Dhaka-1203
			@endif
		</p>
		<h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px;margin-top: 0px">
			Customer Wise Report
		</h4>
		<!-- @if(isset($_GET['from_date']) && isset($_GET['to_date']))
			<p style="text-align: center;margin-bottom: 0px;margin-top: 0px;font-size: 15px;font-weight: bold">
				From {{ date('d-m-Y', strtotime($_GET['from_date'])) }} To {{ date('d-m-Y', strtotime($_GET['to_date'])) }}
			</p>
		@endif -->
		<p style="text-align: center;font-size: 20px;margin-bottom: 0px;margin-top: 0px">
			@if(!empty($customer_name) && ($customer_name->count() > 0))
				{{ $customer_name['name'] }}
			@else
				All Customer 
			@endif
		</p>
		<p style="text-align: center;font-size: 15px;margin-top: 0px">
			@if(isset($area_name)) {{ $area_name['name'] }} @endif
		</p>
	</div>
	<table style="width: 100%">
		<tr>
			<th style="text-align: left">SL</th>
			<th style="text-align: left">Customer Name</th>
			<th style="text-align: right">Total Receivable</th>
			<th style="text-align: right">Total Paid</th>
			<th style="text-align: right">Total Other Expense</th>
			<th style="text-align: right">Total Dues</th>
		</tr>

		@php
			$total_recevable = 0;
			$total_paid      = 0;
			$total_dues      = 0;
			$total_expense   = 0;
		@endphp
		@foreach($customers as $key => $customer)
			@php
				$total_recevable = $total_recevable + $customer->trans_receivable;
				$total_paid      = $total_paid + ($customer->trans_receivable - $customer->trans_due);
				$total_dues      = $total_dues + $customer->trans_due;
				$total_expense   = $total_expense + $customer->other_expense;
			@endphp
			<tr>
				<td style="text-align: left">{{ $key + 1 }}</td>
				<td style="text-align: left">
					{{ $customer->customer->name }}
				</td>
				<td style="text-align: right">{{ number_format($customer->trans_receivable,2,'.',',') }}</td>
				<td style="text-align: right">{{ number_format($customer->trans_receivable - $customer->trans_due - $customer->other_expense,2,'.',',') }}</td>
				<td style="text-align: right">{{ $customer->other_expense != 0 ? number_format($customer->other_expense,2,'.',',') : 0 }}</td>
				<td style="text-align: right">{{ number_format($customer->trans_due,2,'.',',') }}</td>
			</tr>
		@endforeach
		<tr>
			<td style="text-align: left"></td>
			<td style="text-align: right"><b>Total</b></td>
			<td style="text-align: right"><b>{{ number_format($total_recevable,2,'.',',') }}</b></td>
			<td style="text-align: right"><b>{{ number_format($total_paid - $total_expense,2,'.',',') }}</b></td>
			<td style="text-align: right"><b>{{ number_format($total_expense,2,'.',',') }}</b></td>
			<td style="text-align: right"><b>{{ number_format($total_dues,2,'.',',') }}</b></td>
		</tr>
	</table>
</body>
</html>