@extends('layouts.app')

@section('title', 'Area Wise Report')

@section('content')
	<style type="text/css">
		.customAlign table thead tr th{
			vertical-align: middle;
		}
		.table-tr-style tr:nth-child(even) {
			background-color: #dddddd;
		}
		.table-tr-style tr:nth-child(odd) {
			background-color: #F2F3F4;
		}
		.total th{
			font-size: 20px
		}
		.select2-container .select2-selection--single{
			height: auto
		}
		.select2-container--default .select2-selection--single .select2-selection__rendered{
			line-height: 20px
		}
		.select2-container .select2-selection--single .select2-selection__rendered{
			margin-top: 0px
		}
		table.dataTable thead > tr > th {
		     padding-right: 10px !important; 
		}
	</style>

	<section class="content">
		<div class="box">
			<div class="box-header with-border">
			    <div class="col-md-12">
					<h3 class="box-title" style="width:100%">Area Wise Report 
					<span class="pull-right">
						<!-- <a style="text-decoration: none" data-toggle="modal" data-target="#modal-default">
							<button class="btn btn-info btn-xs"><i class="fa fa-search"></i> Search</button>
						</a> -->
						<a style="text-decoration: none" @if(isset($_GET['from_date']) && isset($_GET['to_date'])) href="{{ \Request::fullUrl().'&download=1' }}" @else href="{{ route('area_report_index_download') }}" @endif target="_blank">
							<button class="btn btn-success btn-xs"><i class="fa fa-download"></i> Download</button>
						</a>
						</span>

					</h3>
				</div>
			</div>

			<div class="box-body">

				<div col-md-12 col-sm-12 col-xs-12 col-lg-12>
					<h3 style="text-align: center;margin-bottom: 0px">
					@if(isset($company_profile['name']))
						{{ $company_profile['name'] }}
					@else
						Dainik Dhaka Report
					@endif
					</h3>
					<p style="text-align: center;font-size: 14px;margin-bottom: 0px">
						@if(isset($company_profile['address']))
							{{ $company_profile['address'] }}
						@else
							68, Joginagar wari ,Dhaka-1203
						@endif
					</p>
					<h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px">
						Area Wise Report
					</h4>
					<!-- @if(isset($_GET['from_date']) && isset($_GET['to_date']))
						<p style="text-align: center;margin-bottom: 0px;margin-top: 0px;font-size: 15px;font-weight: bold">
							From {{ date('d-m-Y', strtotime($_GET['from_date'])) }} To {{ date('d-m-Y', strtotime($_GET['to_date'])) }}
						</p>
					@endif -->
					<p style="text-align: center;font-size: 20px;margin-bottom: 0px">
						@if(!empty($area_name) && ($area_name->count() > 0))
							{{ $area_name['name'] }}
						@else
							All Areas
						@endif
					</p>
				</div>

				<div style="margin-bottom: 15px" class="col-md-12">
					<div class="">
						<div class="row">
							<form method="get" action="{{ route('area_report_index') }}" enctype="multipart/form-data">
						
								<div class="form-group">

									<div style="padding: 0px" class="col-md-12">
										<div class="form-group">
											<div class="col-md-2">
												<select style="width: 100%" name="area_id" class="form-control select2" required="required">
													<option value="0">--All Areas--</option>
													@foreach($area_find as $search_area)
															<option {{ $area_id == $search_area->id ? 'selected' : ''}} value="{{ $search_area->id }}">{{ $search_area->name }}</option>
													@endforeach
												</select>											
											</div>

											<div class="col-md-2">
												<input type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control datepicker" name="from_date" value="{{ isset($_GET['from_date']) ? $_GET['from_date'] : '' }}" placeholder="From Date">
											</div>

											<div class="col-md-2">
												<input type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control datepicker" name="to_date" value="{{ isset($_GET['to_date']) ? $_GET['to_date'] : '' }}" placeholder="To Date">
											</div>

											<div class="col-md-2">
												<button type="submit" class="btn btn-default btn-block">Search
												</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>

				<div class="col-md-12 customAlign table-responsive">
					<table class="table table-bordered table-striped dataTable">
						<thead>
							<tr style="background-color: #F9E79F">
								<th style="text-align: left">SL</th>
								<th style="text-align: left">Area</th>
								<th style="text-align: right">Total Invoice</th>
								<th style="text-align: right">Total Receivable</th>
								<th style="text-align: right">Total Paid</th>
								<th style="text-align: right">Total Dues</th>
							</tr>
						</thead>
						<tbody>
							@php
								$total_recevable = 0;
								$total_paid      = 0;
								$total_dues      = 0;
								$total_expense   = 0;
								$total_invoices  = 0;
							@endphp
							@foreach($search_areas as $key => $search_areas_value)
								@php
									$total_recevable = $total_recevable + $trans_receivable[$key];
									$total_paid      = $total_paid + ($trans_receivable[$key] - $trans_due[$key] - $other_expense[$key]);
									$total_dues      = $total_dues + $trans_due[$key];
									$total_invoices  = $total_invoices + $total_invoice[$key];
									$from_date       = isset($_GET['from_date']) && !empty($_GET['from_date']) ? $_GET['from_date'] : '';
									$to_date         = isset($_GET['to_date']) && !empty($_GET['to_date']) ? $_GET['to_date'] : '';
								@endphp
								<tr>
									<td>{{ $key + 1 }}</td>
									<td>
										 <a href="{{ url('report/area-wise-details?area_id='.$search_areas_value->id.'&from_date='.$from_date.'&to_date='.$to_date) }}"> 
											{{ $search_areas_value->name }}
										 </a> 
									</td>
									<td style="text-align: right">{{ $total_invoice[$key] }}</td>
									<td style="text-align: right">{{ number_format($trans_receivable[$key],2,'.',',') }}</td>
									<td style="text-align: right">{{ number_format($trans_receivable[$key] - $trans_due[$key] - $other_expense[$key],2,'.',',') }}</td>
									<td style="text-align: right">{{ number_format($trans_due[$key],2,'.',',') }}</td>
								</tr>
							@endforeach
							<tr>
								<td></td>
								<td style="text-align: right"><b>Total</b></td>
								<td style="text-align: right"><b>{{ $total_invoices }}</b></td>
								<td style="text-align: right"><b>{{ number_format($total_recevable,2,'.',',') }}</b></td>
								<td style="text-align: right"><b>{{ number_format($total_paid,2,'.',',') }}</b></td>
								<td style="text-align: right"><b>{{ number_format($total_dues,2,'.',',') }}</b></td>
							</tr>
						</tbody>
					</table>
				</div>
		    </div>
		    
		</div>
	</section>

	<!-- <div class="modal fade" id="modal-default">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title text-center">Select date range and area</h4>
					</div>

					<div class="modal-body">
						<div class="row">
							<form method="get" action="{{ route('area_report_index') }}" enctype="multipart/form-data">
								<div class="form-group">
									<div class="col-md-8 col-md-offset-2">
										<br>
										<label>Areas*</label><br>
										<select style="width: 100%" name="area_id" class="form-control select2" required="required">
												<option value="0">--All Areas--</option>
											@foreach($search_areas as $search_area)
													<option {{ $area_id == $search_area->id ? 'selected' : ''}} value="{{ $search_area->id }}">{{ $search_area->name }}</option>
											@endforeach
										</select>
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-8 col-md-offset-2">
										<br>
										<label>From Date*</label>
										<input type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control datepicker" name="from_date" value="{{ isset($_GET['from_date']) ? $_GET['from_date'] : '' }}" required="required">
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-8 col-md-offset-2">
										<br>
										<label>To Date*</label>
										<input type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control datepicker" name="to_date" value="{{ isset($_GET['to_date']) ? $_GET['to_date'] : '' }}" required="required">
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-8 col-md-offset-2">
										<br>
										<button type="submit" class="btn btn-success btn-block" style="border-radius: 0px">Search</button>
										<br>
									</div>
								</div>
							</form>

						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
	</div> -->
@endsection

@push('scripts')
	<script type="text/javascript">
		$(".confirm_box").click(function()
		{
			if (!confirm("Do you want to delete"))
			{
				return false;
			}
		});
	</script>
@endpush