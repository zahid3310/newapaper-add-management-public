<<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style type="text/css">
	table, th, td {
	  border: 1px solid black;
	  border-collapse: collapse;
	  padding: 5px;
	  font-family: 'nikosh';
	}
</style>

<body style="font-family: 'nikosh';">
	<div col-md-12 col-sm-12 col-xs-12 col-lg-12>
		<h3 style="text-align: center;margin-bottom: 0px">
		@if(isset($company_profile['name']))
			{{ $company_profile['name'] }}
		@else
			Dainik Dhaka Report
		@endif
		</h3>
		<p style="text-align: center;font-size: 14px;margin-bottom: 0px;margin-top: 0px">
			@if(isset($company_profile['address']))
				{{ $company_profile['address'] }}
			@else
				68, Joginagar wari ,Dhaka-1203
			@endif
		</p>
		<h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px;margin-top: 0px">
			Area Wise Report
		</h4>
		<p style="text-align: center;font-size: 20px;margin-top: 0px">
			@if(!empty($area_name) && ($area_name->count() > 0))
				{{ $area_name['name'] }}
			@else
				All Areas
			@endif
		</p>
	</div>
	<table style="width: 100%" class="table table-bordered table-striped dataTable">
		<thead>
			<tr style="background-color: #F9E79F">
				<th style="text-align: left">SL</th>
				<th style="text-align: left">Area</th>
				<th style="text-align: right">Total Invoice</th>
				<th style="text-align: right">Total Receivable</th>
				<th style="text-align: right">Total Paid</th>
				<th style="text-align: right">Total Dues</th>
			</tr>
		</thead>
		<tbody>
			@php
				$total_recevable = 0;
				$total_paid      = 0;
				$total_dues      = 0;
				$total_expense   = 0;
				$total_invoices  = 0;
			@endphp
			@foreach($search_areas as $key => $search_areas_value)
				@php
					$total_recevable = $total_recevable + $trans_receivable[$key];
					$total_paid      = $total_paid + ($trans_receivable[$key] - $trans_due[$key] - $other_expense[$key]);
					$total_dues      = $total_dues + $trans_due[$key];
					$total_invoices  = $total_invoices + $total_invoice[$key];
				@endphp
				<tr>
					<td>{{ $key + 1 }}</td>
					<td>
						{{ $search_areas_value->name }}
					</td>
					<td style="text-align: right">{{ $total_invoice[$key] }}</td>
					<td style="text-align: right">{{ number_format($trans_receivable[$key],2,'.',',') }}</td>
					<td style="text-align: right">{{ number_format($trans_receivable[$key] - $trans_due[$key] - $other_expense[$key],2,'.',',') }}</td>
					<td style="text-align: right">{{ number_format($trans_due[$key],2,'.',',') }}</td>
				</tr>
			@endforeach
			<tr>
				<td></td>
				<td style="text-align: right"><b>Total</b></td>
				<td style="text-align: right"><b>{{ $total_invoices }}</b></td>
				<td style="text-align: right"><b>{{ number_format($total_recevable,2,'.',',') }}</b></td>
				<td style="text-align: right"><b>{{ number_format($total_paid,2,'.',',') }}</b></td>
				<td style="text-align: right"><b>{{ number_format($total_dues,2,'.',',') }}</b></td>
			</tr>
		</tbody>
	</table>
</body>
</html>