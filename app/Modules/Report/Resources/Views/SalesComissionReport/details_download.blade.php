<<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style type="text/css">
	table, th, td {
	  border: 1px solid black;
	  border-collapse: collapse;
	  padding: 5px;
	  font-family: 'nikosh';
	}
</style>

<body style="font-family: 'nikosh';">
	<div col-md-12 col-sm-12 col-xs-12 col-lg-12>
		<h3 style="text-align: center;margin-bottom: 0px">
		@if(isset($company_profile['name']))
			{{ $company_profile['name'] }}
		@else
			Dainik Dhaka Report
		@endif
		</h3>
		<p style="text-align: center;font-size: 14px;margin-bottom: 0px;margin-top: 0px">
			@if(isset($company_profile['address']))
				{{ $company_profile['address'] }}
			@else
				68, Joginagar wari ,Dhaka-1203
			@endif
		</p>
		<h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px;margin-top: 0px">
			Sales Comission Details
		</h4>
		<p style="text-align: center;font-size: 20px;margin-top: 0px">{{ $agent->name }}</p>
	</div>
	<table style="width: 100%">
		<thead>
			<tr style="background-color: #F9E79F">
				<th>SL</th>
				<th>Date</th>
				<th>Invoice Number</th>
				<th>Payment Methode</th>
				<th>Paid Through</th>
				<th style="text-align: right">Total Paid</th>
			</tr>
		</thead>
		<tbody>
			@php
				$total = 0;
			@endphp
			@if(!empty($trans_paid) && ($trans_paid->count() > 0))
				@foreach($trans_paid as $key => $value)
						<?php $total = $total + $value->trans_paid; ?>
						<tr>
							<td>{{ $key + 1 }}</td>
							<td>{{ date('d-m-Y', strtotime($value->date)) }}</td>
							<td>{{ $value->invoice->invoice_number }}</td>
							<td>{{ $value->payment_methode }}</td>
							<td>{{ $value->payment_through }}</td>
							<td style="text-align: right">{{ number_format($value->trans_paid,2,'.',',') }}</td>
						</tr>
				@endforeach
			@endif
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td style="text-align: right"><b>Total</b></td>
				<td style="text-align: right"><b>{{ number_format($total,2,'.',',') }}</b></td>
			</tr>
		</tbody>
	</table>
</body>
</html>