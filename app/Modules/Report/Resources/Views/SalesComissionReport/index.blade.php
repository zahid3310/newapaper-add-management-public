@extends('layouts.app')

@section('title', 'Sales Comission Report')

@section('content')
	<style type="text/css">
		.customAlign table thead tr th{
			vertical-align: middle;
		}
		.table-tr-style tr:nth-child(even) {
			background-color: #dddddd;
		}
		.table-tr-style tr:nth-child(odd) {
			background-color: #F2F3F4;
		}
		.total th{
			font-size: 20px
		}
		.select2-container .select2-selection--single{
			height: auto
		}
		.select2-container--default .select2-selection--single .select2-selection__rendered{
			line-height: 20px
		}
		.select2-container .select2-selection--single .select2-selection__rendered{
			margin-top: 0px
		}
		table.dataTable thead > tr > th {
		     padding-right: 10px !important; 
		}
	</style>

	<section class="content">
		<div class="box">
		    <div class="box-header with-border">
			    <div class="col-md-12">
				<h3 class="box-title" style="width:100%">Sales Comission Report 
				<span class="pull-right">
				    <!-- <a style="text-decoration: none" data-toggle="modal" data-target="#modal-default-agent">
						<button class="btn btn-info btn-xs"><i class="fa fa-search"></i> Search By Agent</button>
					</a> -->
					<a style="text-decoration: none" @if(isset($_GET['from_date']) && isset($_GET['to_date'])) href="{{ \Request::fullUrl().'&download=1' }}" @else href="{{ route('sales_comission_report_index_download') }}" @endif target="_blank">
						<button class="btn btn-success btn-xs"><i class="fa fa-download"></i> Download</button>
					</a>
				</span>

				</h3>
			</div>
			</div>
			
			<div class="box-body">

				<div col-md-12 col-sm-12 col-xs-12 col-lg-12>
					<h3 style="text-align: center;margin-bottom: 0px">
					@if(isset($company_profile['name']))
						{{ $company_profile['name'] }}
					@else
						Dainik Dhaka Report
					@endif
					</h3>
					<p style="text-align: center;font-size: 14px;margin-bottom: 0px">
						@if(isset($company_profile['address']))
							{{ $company_profile['address'] }}
						@else
							68, Joginagar wari ,Dhaka-1203
						@endif
					</p>
					<h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px">
						Agent Wise Sales Comission Report
					</h4>
					<!-- @if(isset($_GET['from_date']) && isset($_GET['to_date']))
						<p style="text-align: center;margin-bottom: 0px;margin-top: 0px;font-size: 15px;font-weight: bold">
							From {{ date('d-m-Y', strtotime($_GET['from_date'])) }} To {{ date('d-m-Y', strtotime($_GET['to_date'])) }}
						</p>
					@else
						<p style="text-align: center;margin-bottom: 0px;margin-top: 0px;font-size: 15px;font-weight: bold">
							From {{ date('d-m-Y', strtotime($from_date)) }} To {{ date('d-m-Y', strtotime($to_date)) }}
						</p>
					@endif -->
					<p style="text-align: center;font-size: 20px;margin-bottom: 0px">
						@if($agent_name != null)
							{{ $agent_name->name }}
						@else
							All Agents
						@endif
					</p>
					<p style="text-align: center;font-size: 15px;margin-top: 0px">
						@if(isset($area_name)) {{ $area_name['name'] }} @endif
					</p>
				</div>

				<div style="margin-bottom: 15px" class="col-md-12">
					<div class="">
						<div class="row">
							<form method="get" action="{{ route('sales_comission_report_index') }}" enctype="multipart/form-data">
						
								<div class="form-group">

									<div style="padding: 0px" class="col-md-12">
										<div class="form-group">
											<div class="col-md-2">
												<select style="width: 100%" name="agent_id" class="form-control select2">
														<option value="0">--All Agents--</option>
													@foreach($agents as $agent)
															<option {{ $agent_id == $agent->id ? 'selected' : ''}} value="{{ $agent->id }}">{{ $agent->name }}</option>
													@endforeach
												</select>
											</div>

											<div class="col-md-2">
												<select style="width: 100%" name="area_id" class="form-control select2">
														<option value="0">--All Area--</option>
													@foreach($areas as $area)
															<option {{ $area_id == $area->id ? 'selected' : ''}} value="{{ $area->id }}">{{ $area->name }}</option>
													@endforeach
												</select>											
											</div>

											<div class="col-md-2">
												<input type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control datepicker" name="from_date" value="{{ isset($_GET['from_date']) ? $_GET['from_date'] : '' }}" placeholder="From Date">
											</div>

											<div class="col-md-2">
												<input type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control datepicker" name="to_date" value="{{ isset($_GET['to_date']) ? $_GET['to_date'] : '' }}" placeholder="To Date">
											</div>

											<div class="col-md-2">
												<button type="submit" class="btn btn-default btn-block">Search
												</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>

				<div style="padding: 0px" class="col-md-12">
					<div class="col-md-12 customAlign table-responsive">
						
						<table class="table table-bordered table-striped dataTable">
							<thead>
								<tr style="background-color: #F9E79F">
									<th>SL</th>
									<th>Agent Name</th>
									<th style="text-align: right">Total Payable</th>
									<th style="text-align: right">Total Paid</th>
									<th style="text-align: right">Total Dues</th>
								</tr>
							</thead>
							<tbody>
								@php
									$total_payable   = 0;
									$total_paid      = 0;
									$total_dues      = 0;
									$i               = 1;
								@endphp
									@foreach($data as $key => $value)
										@if($value['payable'] != 0)
											@php
												$total_payable   = $total_payable + $value['payable'];
												$total_paid      = $total_paid + $value['paid'];
												$total_dues      = $total_dues + $value['dues'];
												$from_date       = isset($_GET['from_date']) && !empty($_GET['from_date']) ? $_GET['from_date'] : '';
									            $to_date         = isset($_GET['to_date']) && !empty($_GET['to_date']) ? $_GET['to_date'] : '';
											@endphp
											<tr>
												<td>{{ $i }}</td>
												<td>
													 <a href="{{ url('report/sales-comission-details?agent_id='.$value['id'].'&from_date='.$from_date.'&to_date='.$to_date) }}"> 
														{{ $value['name'] }}
													 </a> 
												</td>
												<td style="text-align: right">{{ number_format($value['payable'],2,'.',',') }}</td>
												<td style="text-align: right">{{ number_format($value['paid'],2,'.',',') }}</td>
												<td style="text-align: right">{{ number_format($value['dues'],2,'.',',') }}</td>
											</tr>
											<?php $i = $i + 1; ?>
										@endif
									@endforeach
								<tr>
									<td></td>
									<td style="text-align: right"><b>Total</b></td>
									<td style="text-align: right"><b>{{ number_format($total_payable,2,'.',',') }}</b></td>
									<td style="text-align: right"><b>{{ number_format($total_paid,2,'.',',') }}</b></td>
									<td style="text-align: right"><b>{{ number_format($total_dues,2,'.',',') }}</b></td>
								</tr>
							</tbody>
						</table>

					</div>
			    </div>
			</div>

		</div>
	</section>

	<!-- <div class="modal fade" id="modal-default-agent">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title text-center">Select date range and agent</h4>
					</div>

					<div class="modal-body">
						<div class="row">
							<form method="get" action="{{ route('sales_comission_report_index') }}" enctype="multipart/form-data">
								<div class="form-group">
									<div class="col-md-8 col-md-offset-2">
										<br>
										<label>Agent*</label><br>
										<select style="width: 100%" name="agent_id" class="form-control select2" required="required">
												<option value="0">--All Agent--</option>
											@foreach($agents as $search_agent)
													<option {{ $agent_id == $search_agent->id ? 'selected' : ''}} value="{{ $search_agent->id }}">{{ $search_agent->name }}</option>
											@endforeach
										</select>
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-8 col-md-offset-2">
										<br>
										<label>From Date*</label>
										<input type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control datepicker" name="from_date" value="{{ isset($_GET['from_date']) ? $_GET['from_date'] : '' }}" required="required">
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-8 col-md-offset-2">
										<br>
										<label>To Date*</label>
										<input type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control datepicker" name="to_date" value="{{ isset($_GET['to_date']) ? $_GET['to_date'] : '' }}" required="required">
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-8 col-md-offset-2">
										<br>
										<button type="submit" class="btn btn-success btn-block" style="border-radius: 0px">Search</button>
										<br>
									</div>
								</div>
							</form>

						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
	</div> -->

@endsection

@push('scripts')
	<script type="text/javascript">
		$(".confirm_box").click(function()
		{
			if (!confirm("Do you want to delete"))
			{
				return false;
			}
		});
	</script>
@endpush