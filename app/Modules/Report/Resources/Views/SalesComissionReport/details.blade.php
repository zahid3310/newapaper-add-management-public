@extends('layouts.app')

@section('title', 'Sales Comission Details')

@section('content')
	<style type="text/css">
		.customAlign table thead tr th{
			vertical-align: middle;
		}
		.table-tr-style tr:nth-child(even) {
			background-color: #dddddd;
		}
		.table-tr-style tr:nth-child(odd) {
			background-color: #F2F3F4;
		}
		.total th{
			font-size: 20px
		}
		.select2-container .select2-selection--single{
			height: auto
		}
		.select2-container--default .select2-selection--single .select2-selection__rendered{
			line-height: 20px
		}
		.select2-container .select2-selection--single .select2-selection__rendered{
			margin-top: 0px
		}
		table.dataTable thead > tr > th {
		    padding-right: 10px !important; 
		}
	</style>

	<section class="content">
		<div class="box">
		    <div class="box-header with-border">
			    <div class="col-md-12">
				<h3 class="box-title" style="width:100%">Sales Comission Details Report 
				<a href="{{ route('sales_comission_details_report_index_download', ['agent_id='.$_GET['agent_id'], 'from_date='.$_GET['from_date'], 'to_date='.$_GET['to_date']]) }}" target="_blank">
						<button class="btn btn-success btn-xs pull-right"><i class="fa fa-download"></i> Download</button>
					</a>

				</h3>
			</div>
			</div>
			
			<div class="box-body">
				<div col-md-12 col-sm-12 col-xs-12 col-lg-12>
					<h3 style="text-align: center;margin-bottom: 0px">
					@if(isset($company_profile['name']))
						{{ $company_profile['name'] }}
					@else
						Dainik Dhaka Report
					@endif
					</h3>
					<p style="text-align: center;font-size: 14px;margin-bottom: 0px">
						@if(isset($company_profile['address']))
							{{ $company_profile['address'] }}
						@else
							68, Joginagar wari ,Dhaka-1203
						@endif
					</p>
					<h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px">
						Sales Comission Details
					</h4>
					<p style="text-align: center;font-size: 20px">{{ $agent->name }}</p>
				</div>
				<div class="col-md-12 customAlign table-responsive">

					<table class="table table-bordered table-striped dataTable">
						<thead>
							<tr style="background-color: #F9E79F">
								<th>SL</th>
								<th>Date</th>
								<th>Invoice Number</th>
								<th>Payment Methode</th>
								<th>Paid Through</th>
								<th style="text-align: right">Total Paid</th>
							</tr>
						</thead>
						<tbody>
							@php
								$total = 0;
							@endphp
							@if(!empty($trans_paid) && ($trans_paid->count() > 0))
								@foreach($trans_paid as $key => $value)
										<?php $total = $total + $value->trans_paid; ?>
										<tr>
											<td>{{ $key + 1 }}</td>
											<td>{{ date('d-m-Y', strtotime($value->date)) }}</td>
											<td>{{ $value->invoice->invoice_number }}</td>
											<td>{{ $value->payment_methode }}</td>
											<td>{{ $value->payment_through }}</td>
											<td style="text-align: right">{{ number_format($value->trans_paid,2,'.',',') }}</td>
										</tr>
								@endforeach
							@endif
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td style="text-align: right"><b>Total</b></td>
								<td style="text-align: right"><b>{{ number_format($total,2,'.',',') }}</b></td>
							</tr>
						</tbody>
					</table>

				</div>
		    </div>
		    
		</div>
	</section>
@endsection

@push('scripts')
	<script type="text/javascript">
		$(".confirm_box").click(function()
		{
			if (!confirm("Do you want to delete"))
			{
				return false;
			}
		});
	</script>
@endpush