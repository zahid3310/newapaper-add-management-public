<<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style type="text/css">
	table, th, td {
	  border: 1px solid black;
	  border-collapse: collapse;
	  padding: 5px;
	  font-family: 'nikosh';
	}
</style>

<body style="font-family: 'nikosh';">
	<div col-md-12 col-sm-12 col-xs-12 col-lg-12>
		<h3 style="text-align: center;margin-bottom: 0px">
		@if(isset($company_profile['name']))
			{{ $company_profile['name'] }}
		@else
			Dainik Dhaka Report
		@endif
		</h3>
		<p style="text-align: center;font-size: 14px;margin-bottom: 0px;margin-top: 0px">
			@if(isset($company_profile['address']))
				{{ $company_profile['address'] }}
			@else
				68, Joginagar wari ,Dhaka-1203
			@endif
		</p>
		<h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px;margin-bottom: 0px">
			Agent Wise Sales Comission Report
		</h4>
		<!-- @if(isset($_GET['from_date']) && isset($_GET['to_date']))
			<p style="text-align: center;margin-bottom: 0px;margin-top: 0px;font-size: 15px;font-weight: bold;margin-top: 0px">
				From {{ date('d-m-Y', strtotime($_GET['from_date'])) }} To {{ date('d-m-Y', strtotime($_GET['to_date'])) }}
			</p>
		@else
			<p style="text-align: center;margin-bottom: 0px;margin-top: 0px;font-size: 15px;font-weight: bold;margin-top: 0px">
				From {{ date('d-m-Y', strtotime($from_date)) }} To {{ date('d-m-Y', strtotime($to_date)) }}
			</p>
		@endif -->
		<p style="text-align: center;font-size: 20px;margin-bottom: 0px;margin-top: 0px">
			@if($agent_name != null)
				{{ $agent_name->name }}
			@else
				All Agents
			@endif
		</p>
		<p style="text-align: center;font-size: 15px;margin-top: 0px">
			@if(isset($area_name)) {{ $area_name['name'] }} @endif
		</p>
	</div>
	<table style="width: 100%">
		<tr>
			<th style="text-align: left">SL</th>
			<th style="text-align: left">Agent Name</th>
			<th style="text-align: right">Total Payable</th>
			<th style="text-align: right">Total Paid</th>
			<th style="text-align: right">Total Dues</th>
		</tr>

		@php
			$total_payable   = 0;
			$total_paid      = 0;
			$total_dues      = 0;
			$i               = 1;
		@endphp
			@foreach($data as $key => $value)
				@if($value['payable'] != 0)
					@php
						$total_payable   = $total_payable + $value['payable'];
						$total_paid      = $total_paid + $value['paid'];
						$total_dues      = $total_dues + $value['dues'];
					@endphp
					<tr>
						<td>{{ $i }}</td>
						<td>
							{{ $value['name'] }}
						</td>
						<td style="text-align: right">{{ number_format($value['payable'],2,'.',',') }}</td>
						<td style="text-align: right">{{ number_format($value['paid'],2,'.',',') }}</td>
						<td style="text-align: right">{{ number_format($value['dues'],2,'.',',') }}</td>
					</tr>
					<?php $i = $i + 1; ?>
				@endif
			@endforeach
		<tr>
			<td style="text-align: left"></td>
			<td style="text-align: right"><b>Total</b></td>
			<td style="text-align: right"><b>{{ number_format($total_payable,2,'.',',') }}</b></td>
			<td style="text-align: right"><b>{{ number_format($total_paid,2,'.',',') }}</b></td>
			<td style="text-align: right"><b>{{ number_format($total_dues,2,'.',',') }}</b></td>
		</tr>
	</table>
</body>
</html>