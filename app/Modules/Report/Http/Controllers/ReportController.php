<?php

namespace App\Modules\Report\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Models\Areas;
use App\Models\Districts;
use App\Models\Customers;
use App\Models\Agents;
use App\Models\Items;
use App\Models\Invoices;
use App\Models\InvoiceDetails;
use App\Models\Transactions;
use App\Models\Payments;
use Response;
use DB;

class ReportController extends Controller
{
    public function customerReportIndex()
    {	
    	//Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('customer_report_index');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $customer_id        = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $area_id            = isset($_GET['area_id']) ? $_GET['area_id'] : 0;
        $date               = date('Y-m-d');
        $from_date          = isset($_GET['from_date']) ? $_GET['from_date'] : date('Y-m-d', strtotime($date. ' - 9999 days'));
        $to_date            = isset($_GET['to_date']) ? $_GET['to_date'] : $date;

    	$customers          = Invoices::when(!empty($_GET['from_date']) && !empty($_GET['to_date']), function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                        })
                                        ->when($customer_id != 0, function ($query) use ($customer_id) {
                                            return $query->where('invoices.customer_id', $customer_id);
                                        })
                                        ->when($area_id != 0, function ($query) use ($area_id) {
                                            return $query->join('agents', 'agents.id', 'invoices.agent_id')
                                                         ->join('areas', 'areas.id', 'agents.area')
                                                         ->where('areas.id', $area_id);
                                        })
                                        // ->where('invoices.due_amount', '!=', 0)
                                        ->where('invoices.status', '=', 1)
        		    					->groupBy('invoices.customer_id')
        		    					->selectRaw('GROUP_CONCAT(DISTINCT invoices.customer_id) as customer_id,
        		    								 sum(invoices.total_amount) as trans_receivable,
        		    								 sum(invoices.due_amount) as trans_due,
                                                     sum(invoices.other_expense) as other_expense')
        		    					->get();


        $search_customers   = Customers::where('status', 1)->get();
        $customer_name      = Customers::find($customer_id);
        $area_name          = Areas::find($area_id);
        $areas              = Areas::where('status', 1)->get();

        //Download With Search Start
        if (isset($_GET['download']) && ($_GET['download'] == 1))
        {
            $date       = date('d-m-Y');
            $mpdf       = new \Mpdf\Mpdf();
            $mpdf->WriteHTML(view('report::CustomerWiseReport.index_download', compact('customers', 'search_customers', 'customer_id', 'customer_name', 'from_date', 'to_date', 'areas', 'area_id', 'area_name')));
            $mpdf->Output('Customer-Report-'.$date, "I");
        }
        //Download With Search End

    	return view('report::CustomerWiseReport.index', compact('customers', 'search_customers', 'customer_id', 'customer_name', 'from_date', 'to_date', 'areas', 'area_id', 'area_name'));
    }

    public function customerReportDetails()
    {	
    	//Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('customer_wise_details_report_index');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End
        
        $date               = date('Y-m-d');
        $from_date          = isset($_GET['from_date']) ? $_GET['from_date'] : date('Y-m-d', strtotime($date. ' - 9999 days'));
        $to_date            = isset($_GET['to_date']) ? $_GET['to_date'] : $date;
        
    	$customer = Customers::where('id', $_GET['customer_id'])->first();
    	$invoices = Invoices::when(!empty($_GET['from_date']) && !empty($_GET['to_date']), function ($query) use ($from_date, $to_date) {
                                    return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                })
                                ->where('invoices.status', '=', 1)
			    				->where('invoices.customer_id', '=', $_GET['customer_id'])
		    					->selectRaw('invoices.id as id,
                                             invoices.invoice_number as invoice_number,
		    								 invoices.note as note,
                                             invoices.invoice_date as invoice_date,
		    								 invoices.due_date as due_date,
		    								 invoices.total_amount as trans_receivable,
		    								 invoices.due_amount as trans_due,
                                             invoices.other_expense as other_expense')
		    					->get();

    	return view('report::CustomerWiseReport.details', compact('invoices', 'customer'));
    }

    public function customerReportIndexDownload()
    {	
    	//Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('customer_report_index_download');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

    	$date 		= date('d-m-Y');
        $from_date  = date('Y-m-d', strtotime($date. ' - 9999 days'));
        $to_date    = date('Y-m-d', strtotime($date));
    	$customers 	= Invoices::whereBetween('invoices.invoice_date', [$from_date, $to_date])
                                // ->where('invoices.due_amount', '!=', 0)
                                ->where('invoices.status', '=', 1)
		    					->groupBy('invoices.customer_id')
		    					->selectRaw('GROUP_CONCAT(DISTINCT invoices.customer_id) as customer_id,
		    								 sum(DISTINCT invoices.total_amount) as trans_receivable,
		    								 sum(DISTINCT invoices.due_amount) as trans_due,
                                             sum(DISTINCT invoices.other_expense) as other_expense')
		    					->get();

		$mpdf 		= new \Mpdf\Mpdf();
		$mpdf->WriteHTML(view('report::CustomerWiseReport.index_download', compact('customers')));
		$mpdf->Output('Customer-Report-'.$date, "I");
    }

    public function customerReportDetailsDownload()
    {	
    	//Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('customer_wise_details_report_index_download');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

    	$date 		= date('d-m-Y');
    	$from_date  = isset($_GET['from_date']) ? $_GET['from_date'] : date('Y-m-d', strtotime($date. ' - 9999 days'));
        $to_date    = isset($_GET['to_date']) ? $_GET['to_date'] : $date;
        
    	$customer 	= Customers::where('id', $_GET['customer_id'])->first();
    	$invoices 	= Invoices::when(!empty($_GET['from_date']) && !empty($_GET['to_date']), function ($query) use ($from_date, $to_date) {
                                    return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                })
                                ->where('invoices.status', '=', 1)
			    				->where('invoices.customer_id', '=', $_GET['customer_id'])
		    					->selectRaw('invoices.id as id,
		    								 invoices.invoice_number as invoice_number,
                                             invoices.note as note,
		    								 invoices.invoice_date as invoice_date,
		    								 invoices.total_amount as trans_receivable,
		    								 invoices.due_amount as trans_due,
                                             invoices.due_date as due_date,
                                             invoices.other_expense as other_expense')
		    					->get();

		$mpdf 		= new \Mpdf\Mpdf();
		$mpdf->WriteHTML(view('report::CustomerWiseReport.details_download', compact('customer', 'invoices')));
		$mpdf->Output('Customer-Wise-Invoice-Report-'.$date, "I");
    }

    public function salesComissionReportIndex()
    {	
    	//Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('sales_comission_report_index');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $date               = date('Y-m-d');
        $agent_id           = isset($_GET['agent_id']) ? $_GET['agent_id'] : 0;
        $from_date          = isset($_GET['from_date']) ? $_GET['from_date'] : date('Y-m-d', strtotime($date. ' - 9999 days'));
        $to_date            = isset($_GET['to_date']) ? $_GET['to_date'] : $date;

        $agent_name         = Agents::find($agent_id);
        $area_id            = isset($_GET['area_id']) ? $_GET['area_id'] : 0;
        $area_name          = Areas::find($area_id);
        $areas              = Areas::where('status', 1)->get();

        $agents             = Agents::when($area_id != 0, function ($query) use ($area_id) {
                                            return $query->join('areas', 'areas.id', 'agents.area')
                                                         ->where('areas.id', $area_id);
                                        })
                                        ->where('agents.status', 1)
                                        ->selectRaw('agents.*')
                                        ->get();

    	$trans_paid 		= Transactions::leftjoin('invoices', 'invoices.id', 'transactions.invoice_id')
    	                                        ->when(!empty($_GET['from_date']) && !empty($_GET['to_date']), function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('transactions.date', [$from_date, $to_date]);
                                                })
                                                ->when($agent_id != 0, function ($query) use ($agent_id) {
                                                    return $query->where('transactions.agent_id', $agent_id);
                                                })
                                                ->where('transactions.type', '=', 'sales_comission')
						    					->where('transactions.status', '=', 1)
						    					->selectRaw('transactions.*')
						    					->get();

		$trans_payable  	= Invoices::join('transactions', 'transactions.invoice_id', 'invoices.id')
		                                        ->when(!empty($_GET['from_date']) && !empty($_GET['to_date']), function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                                })
                                                ->when($agent_id != 0, function ($query) use ($agent_id) {
                                                    return $query->where('invoices.agent_id', $agent_id);
                                                })
                                                ->where('invoices.status', '=', 1)
												->whereNotNull('invoices.agent_id')
        								        ->where('invoices.commission_amount', '>', 0)
						    					->selectRaw('invoices.*')
						    					->get();

        if(!empty($trans_payable) &&(count($trans_payable) > 0))
        {
            foreach($trans_payable as $key1 => $value)
            {   
                $invoice_details            = InvoiceDetails::join('transactions', 'transactions.invoice_id', 'invoices_details.invoice_id')
                                                        ->where('invoices_details.invoice_id', $value->id)
                                                        ->where('transactions.type', 'invoice')
                                                        ->selectRaw('(CASE WHEN invoices_details.commission_type = 1 THEN invoices_details.amount ELSE ((transactions.trans_paid*invoices_details.commission_amount)/100) END) AS com_amount')
                                                        ->get();
                                                        
                $comission_payable                 = $invoice_details->sum('com_amount');
                // $other_expense_payable          = $value->other_expense;
                // $comission_payable              = $comission_payables - (($comission_payables*$other_expense_payable)/($value->total_amount));
    
                $datas[$value->id]['commission_amount']  = $comission_payable;
                $datas[$value->id]['agent_id']           = $value->agent_id;
            }
        }
        else
        {
            $datas = [];
        }
        
        if(!empty($agents) &&(count($agents) > 0))
        {
            foreach ($agents as $key => $agent)
            {
                $data[$agent->id]['id']      = $agent->id;
                $data[$agent->id]['name']    = $agent->name;
                $data[$agent->id]['payable'] = collect($datas)->where('agent_id', $agent->id)->sum('commission_amount');
                $data[$agent->id]['paid']    = $trans_paid->where('agent_id', $agent->id)->sum('trans_paid');
                $data[$agent->id]['dues']    = collect($datas)->where('agent_id', $agent->id)->sum('commission_amount') - $trans_paid->where('agent_id', $agent->id)->sum('trans_paid');
            }
        }
        else
        {
            $data = [];
        }

        //Download With Search Start
        if (isset($_GET['download']) && ($_GET['download'] == 1))
        {
            $mpdf       = new \Mpdf\Mpdf();
            $mpdf->WriteHTML(view('report::SalesComissionReport.index_download', compact('agents', 'data', 'agent_wise_payable', 'agent_wise_paid', 'agent_wise_dues', 'agent_id', 'agent_name', 'area_id', 'area_name', 'areas')));
            $mpdf->Output('Sales-Comission-Report-'.$date, "I");
        }
        //Download With Search End
						    
    	return view('report::SalesComissionReport.index', compact('agents', 'data', 'agent_id', 'from_date', 'to_date', 'agent_name', 'area_id', 'area_name', 'areas'));
    }

    public function salesComissionReportDetails()
    {	
    	//Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('sales_comission_details_report_index');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End
        
        $from_date          = isset($_GET['from_date']) ? $_GET['from_date'] : date('Y-m-d', strtotime($date. ' - 9999 days'));
        $to_date            = isset($_GET['to_date']) ? $_GET['to_date'] : $date;
        
    	$agent_id       = $_GET['agent_id'];  

    	$agent 		    = Agents::where('id', $agent_id)->first();

    	$trans_paid 	= Transactions::where('transactions.type', '=', 'sales_comission')
    									->where('transactions.agent_id', $agent_id )
    									->when(!empty($_GET['from_date']) && !empty($_GET['to_date']), function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('transactions.date', [$from_date, $to_date]);
                                        })
				    					->where('transactions.status', '=', 1)
				    					->selectRaw('transactions.*')
				    					->get();
		
    	return view('report::SalesComissionReport.details', compact('agent_id', 'agent', 'trans_paid'));
    }

    public function salesComissionReportIndexDownload()
    {	
    	//Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('sales_comission_report_index_download');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

    	$date               = date('Y-m-d');
        $agent_id           = isset($_GET['agent_id']) ? $_GET['agent_id'] : 0;
        $from_date          = isset($_GET['from_date']) ? $_GET['from_date'] : date('Y-m-d', strtotime($date. ' - 9999 days'));
        $to_date            = isset($_GET['to_date']) ? $_GET['to_date'] : $date;

        $agents             = Agents::where('agents.status', 1)->get();
        $agent_name         = Agents::find($agent_id);

        $trans_paid         = Transactions::whereBetween('transactions.date', [$from_date, $to_date])
                                                ->when($agent_id != 0, function ($query) use ($agent_id) {
                                                    return $query->where('transactions.agent_id', $agent_id);
                                                })
                                                ->where('transactions.type', '=', 'sales_comission')
                                                ->where('transactions.status', '=', 1)
                                                ->selectRaw('transactions.*')
                                                ->get();

        $trans_payable      = Invoices::join('transactions', 'transactions.invoice_id', 'invoices.id')
                                        ->whereBetween('invoices.invoice_date', [$from_date, $to_date])
                                                ->when($agent_id != 0, function ($query) use ($agent_id) {
                                                    return $query->where('invoices.agent_id', $agent_id);
                                                })
                                                ->where('invoices.status', '=', 1)
                                                // ->where('invoices.commission_amount', '!=', 0)
                                                ->selectRaw('invoices.*')
                                                ->get();

        foreach($trans_payable as $key1 => $value)
        {
            $comission_payables             = $value->commission_amount;
            $other_expense_payable          = $value->other_expense;
            $comission_payable              = $comission_payables - (($comission_payables*$other_expense_payable)/($value->total_amount));

            $datas[$value->id]['commission_amount']  = $comission_payable;
            $datas[$value->id]['agent_id']           = $value->agent_id;
        }

        foreach ($agents as $key => $agent)
        {
            $data[$agent->id]['id']      = $agent->id;
            $data[$agent->id]['name']    = $agent->name;
            $data[$agent->id]['payable'] = collect($datas)->where('agent_id', $agent->id)->sum('commission_amount');
            $data[$agent->id]['paid']    = $trans_paid->where('agent_id', $agent->id)->sum('trans_paid');
            $data[$agent->id]['dues']    = collect($datas)->where('agent_id', $agent->id)->sum('commission_amount') - $trans_paid->where('agent_id', $agent->id)->sum('trans_paid');
        }

		$mpdf 		= new \Mpdf\Mpdf();
		$mpdf->WriteHTML(view('report::SalesComissionReport.index_download', compact('agents', 'data' , 'agent_id', 'agent_name', 'from_date', 'to_date')));
		$mpdf->Output('Sales-Comission-Report-'.$date, "I");		    	
    }

    public function salesComissionReportDetailsDownload()
    {	
    	//Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('sales_comission_details_report_index_download');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End
        
    	$date 			= date('d-m-Y');
    	$from_date      = isset($_GET['from_date']) ? $_GET['from_date'] : date('Y-m-d', strtotime($date. ' - 9999 days'));
        $to_date        = isset($_GET['to_date']) ? $_GET['to_date'] : $date;
    	$agent_id       = $_GET['agent_id'];  

    	$agent 			= Agents::where('id', $agent_id)->first();

    	$trans_paid 	= Transactions::where('transactions.type', '=', 'sales_comission')
    									->where('transactions.agent_id', $agent_id )
    									->when(!empty($_GET['from_date']) && !empty($_GET['to_date']), function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('transactions.date', [$from_date, $to_date]);
                                        })
				    					->where('transactions.status', '=', 1)
				    					->selectRaw('transactions.*')
				    					->get();

		$mpdf 		= new \Mpdf\Mpdf();
		$mpdf->WriteHTML(view('report::SalesComissionReport.details_download', compact('agent_id', 'agent', 'trans_paid')));
		$mpdf->Output('Sales-Comission-details-Report-'.$date, "I");
    }

    public function paymentReportIndex()
    {  
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('paynent_report_index');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $customer_id        = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $agent_id           = isset($_GET['agent_id']) ? $_GET['agent_id'] : 0;
        $date               = date('Y-m-d');
        $from_date          = isset($_GET['from_date']) ? $_GET['from_date'] : date('Y-m-d', strtotime($date. ' - 30 days'));
        $to_date            = isset($_GET['to_date']) ? $_GET['to_date'] : $date;



        $transactions       = Transactions::whereBetween('transactions.date', [$from_date, $to_date])
                                        ->when($customer_id != 0, function ($query) use ($customer_id) {
                                            return $query->where('transactions.customer_id', $customer_id)->where('transactions.type', 'invoice');
                                        })
                                        ->when($agent_id != 0, function ($query) use ($agent_id) {
                                            return $query->where('transactions.agent_id', $agent_id)->where('transactions.type', 'sales_comission');
                                        })
                                        ->where('transactions.status', '=', 1)
                                        ->selectRaw('transactions.*')
                                        ->get();

                                        // dd($transactions->toArray());

        $search_customers   = Customers::where('status', 1)->get();
        $customer_name      = Customers::find($customer_id);

        $search_agents      = Agents::where('status', 1)->get();
        $agent_name         = Agents::find($agent_id);

        if (isset($_GET['download']) && ($_GET['download'] == 1))
        {
            $mpdf       = new \Mpdf\Mpdf();
            $mpdf->WriteHTML(view('report::PaymentReport.index_download', compact('transactions', 'search_customers', 'customer_name', 'customer_id', 'search_agents', 'agent_name', 'agent_id')));
            $mpdf->Output('Payment-Report-'.$date, "I");
        }

        return view('report::PaymentReport.index', compact('transactions', 'search_customers', 'customer_name', 'customer_id', 'search_agents', 'agent_name', 'agent_id'));
    }

    public function paymentReportIndexDownload()
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('payment_report_index_download');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $customer_id        = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $agent_id           = isset($_GET['agent_id']) ? $_GET['agent_id'] : 0;
        $date               = date('Y-m-d');
        $from_date          = isset($_GET['from_date']) ? $_GET['from_date'] : date('Y-m-d', strtotime($date. ' - 30 days'));
        $to_date            = isset($_GET['to_date']) ? $_GET['to_date'] : $date;

        $transactions       = Transactions::whereBetween('transactions.date', [$from_date, $to_date])
                                        ->when($customer_id != 0, function ($query) use ($customer_id) {
                                            return $query->where('transactions.customer_id', $customer_id)->where('transactions.type', 'invoice');
                                        })
                                        ->when($agent_id != 0, function ($query) use ($agent_id) {
                                            return $query->where('transactions.agent_id', $agent_id)->where('transactions.type', 'sales_comission');
                                        })
                                        ->where('transactions.status', '=', 1)
                                        ->selectRaw('transactions.*')
                                        ->get();

                                        // dd($transactions->toArray());

        $search_customers   = Customers::where('status', 1)->get();
        $customer_name      = Customers::find($customer_id);

        $search_agents      = Agents::where('status', 1)->get();
        $agent_name         = Agents::find($agent_id);

        $mpdf       = new \Mpdf\Mpdf();
        $mpdf->WriteHTML(view('report::PaymentReport.index_download', compact('transactions', 'search_customers', 'customer_name', 'customer_id', 'search_agents', 'agent_name', 'agent_id')));
        $mpdf->Output('Payment-Report-'.$date, "I");
    }
    
    public function areaReportIndex()
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('area_report_index');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $area_id            = isset($_GET['area_id']) ? $_GET['area_id'] : 0;
        $date               = date('Y-m-d');
        $from_date          = isset($_GET['from_date']) ? $_GET['from_date'] : date('Y-m-d', strtotime($date. ' - 9999 days'));
        $to_date            = isset($_GET['to_date']) ? $_GET['to_date'] : $date;

        $area_id            = isset($_GET['area_id']) ? $_GET['area_id'] : 0;
        $area_name          = Areas::find($area_id);
        $area_find          = Areas::where('areas.status', '=', 1)->get();

        $areas              = Invoices::leftjoin('agents', 'agents.id', 'invoices.agent_id')
                                        ->leftjoin('areas', 'areas.id', 'agents.area')
                                        ->when(!empty($_GET['from_date']) && !empty($_GET['to_date']), function ($query) use ($from_date, $to_date) {
                                            return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                        })
                                        ->where('invoices.status', '=', 1)
                                        ->where('areas.status', '=', 1)
                                        ->when($area_id != 0, function ($query) use ($area_id)
                                          {
                                                return $query->where('areas.id', $area_id);
                                          })
                                        ->selectRaw('areas.id as area_id,
                                                     areas.name as areas_name,
                                                     invoices.total_amount as trans_receivable,
                                                     invoices.due_amount as trans_due,
                                                     invoices.other_expense as other_expense')
                                        ->get();


        $search_areas       = Areas::when($area_id != 0, function ($query) use ($area_id)
                                    {
                                        return $query->where('areas.id', $area_id);
                                    })
                                    ->where('areas.status', '=', 1)
                                    ->get();

        foreach ($search_areas as $key => $value)
        {
            $trans_receivable[]  = $areas->where('area_id', $value->id)->sum('trans_receivable');
            $trans_due[]         = $areas->where('area_id', $value->id)->sum('trans_due');
            $other_expense[]     = $areas->where('area_id', $value->id)->sum('other_expense');
            $total_invoice[]     = $areas->where('area_id', $value->id)->count();
        }

        //Download With Search Start
        if (isset($_GET['download']) && ($_GET['download'] == 1))
        {
            $date       = date('d-m-Y');
            $mpdf       = new \Mpdf\Mpdf();
            $mpdf->WriteHTML(view('report::AreaWiseReport.index_download', compact('areas', 'search_areas', 'area_id', 'area_name', 'from_date', 'to_date', 'trans_receivable', 'trans_due', 'other_expense', 'total_invoice', 'customers', 'area_find')));
            $mpdf->Output('Area-wise-Report-'.$date, "I");
        }
        //Download With Search End

        return view('report::AreaWiseReport.index', compact('areas', 'search_areas', 'area_id', 'area_name', 'from_date', 'to_date', 'trans_receivable', 'trans_due', 'other_expense', 'total_invoice', 'area_find'));
    }

    public function areaReportDetails()
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('area_wise_details_report_index');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End
        
        $date               = date('Y-m-d');
        $from_date          = isset($_GET['from_date']) ? $_GET['from_date'] : date('Y-m-d', strtotime($date. ' - 9999 days'));
        $to_date            = isset($_GET['to_date']) ? $_GET['to_date'] : $date;
        
        $area     = Areas::where('id', $_GET['area_id'])->first();
        // dd($area);
        $invoices = Invoices::join('agents', 'agents.id', 'invoices.agent_id')
                                ->when(!empty($_GET['from_date']) && !empty($_GET['to_date']), function ($query) use ($from_date, $to_date) {
                                    return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                })
                                ->where('invoices.status', '=', 1)
                                ->where('agents.status', '=', 1)
                                ->where('agents.area', '=', $_GET['area_id'])
                                ->selectRaw('invoices.id as id,
                                             invoices.invoice_number as invoice_number,
                                             invoices.note as note,
                                             invoices.invoice_date as invoice_date,
                                             invoices.due_date as due_date,
                                             invoices.total_amount as trans_receivable,
                                             invoices.due_amount as trans_due,
                                             invoices.other_expense as other_expense')
                                ->get();

        return view('report::AreaWiseReport.details', compact('invoices', 'area'));
    }

    public function areaReportIndexDownload()
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('area_report_index_download');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $area_id            = isset($_GET['area_id']) ? $_GET['area_id'] : 0;
        $date               = date('d-m-Y');
        $from_date          = date('Y-m-d', strtotime($date. ' - 9999 days'));
        $to_date            = date('Y-m-d', strtotime($date));
        $areas              = Invoices::leftjoin('agents', 'agents.id', 'invoices.agent_id')
                                        ->leftjoin('areas', 'areas.id', 'agents.area')
                                        ->whereBetween('invoices.invoice_date', [$from_date, $to_date])
                                        ->where('invoices.status', '=', 1)
                                        ->where('areas.status', '=', 1)
                                        ->when($area_id != 0, function ($query) use ($area_id)
                                          {
                                                return $query->where('areas.id', $area_id);
                                          })
                                        ->selectRaw('areas.id as area_id,
                                                     areas.name as areas_name,
                                                     invoices.total_amount as trans_receivable,
                                                     invoices.due_amount as trans_due,
                                                     invoices.other_expense as other_expense')
                                        ->get();

        $search_areas       = Areas::when($area_id != 0, function ($query) use ($area_id)
                                    {
                                        return $query->where('areas.id', $area_id);
                                    })
                                    ->where('areas.status', '=', 1)
                                    ->get();

        $area_name          = Areas::find($area_id);

        foreach ($search_areas as $key => $value)
        {
            $trans_receivable[]  = $areas->where('area_id', $value->id)->sum('trans_receivable');
            $trans_due[]         = $areas->where('area_id', $value->id)->sum('trans_due');
            $other_expense[]     = $areas->where('area_id', $value->id)->sum('other_expense');
            $total_invoice[]     = $areas->where('area_id', $value->id)->count();
        }

        $mpdf       = new \Mpdf\Mpdf();
        $mpdf->WriteHTML(view('report::AreaWiseReport.index_download', compact('areas', 'search_areas', 'area_id', 'area_name', 'from_date', 'to_date', 'trans_receivable', 'trans_due', 'other_expense', 'total_invoice', compact('customers'))));
        $mpdf->Output('Area-wise-Report-'.$date, "I");
    }

    public function areaReportDetailsDownload()
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('area_wise_details_report_index_download');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End
        $date       = date('Y-m-d');
        $from_date  = isset($_GET['from_date']) ? $_GET['from_date'] : date('Y-m-d', strtotime($date. ' - 9999 days'));
        $to_date    = isset($_GET['to_date']) ? $_GET['to_date'] : $date;
        
        $area       = Areas::where('id', $_GET['area_id'])->first();
        $invoices   = Invoices::join('agents', 'agents.id', 'invoices.agent_id')
                                ->when(!empty($_GET['from_date']) && !empty($_GET['to_date']), function ($query) use ($from_date, $to_date) {
                                    return $query->whereBetween('invoices.invoice_date', [$from_date, $to_date]);
                                })
                                ->where('invoices.status', '=', 1)
                                ->where('agents.status', '=', 1)
                                ->where('agents.area', '=', $_GET['area_id'])
                                ->selectRaw('invoices.id as id,
                                             invoices.invoice_number as invoice_number,
                                             invoices.note as note,
                                             invoices.invoice_date as invoice_date,
                                             invoices.due_date as due_date,
                                             invoices.total_amount as trans_receivable,
                                             invoices.due_amount as trans_due,
                                             invoices.other_expense as other_expense')
                                ->get();

        $mpdf       = new \Mpdf\Mpdf();
        $mpdf->WriteHTML(view('report::AreaWiseReport.details_download', compact('invoices', 'area')));
        $mpdf->Output('Area-wise-Report-'.$date, "I");
    }

    public function paymentsReportIndex()
    {  
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('paynents_report_index');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $customer_id        = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $date               = date('Y-m-d');
        $from_date          = isset($_GET['from_date']) ? $_GET['from_date'] : date('Y-m-d', strtotime($date. ' - 9999 days'));
        $to_date            = isset($_GET['to_date']) ? $_GET['to_date'] : $date;



        $payments           = Payments::join('transactions', 'transactions.payment_id', 'payments.id')
                                        ->join('invoices', 'invoices.id', 'transactions.invoice_id')
                                        ->whereBetween('payments.date', [$from_date, $to_date])
                                        ->when($customer_id != 0, function ($query) use ($customer_id) {
                                            return $query->where('payments.customer_id', $customer_id);
                                        })
                                        ->where('payments.status', '=', 1)
                                        ->where('transactions.status', '=', 1)
                                        ->selectRaw('GROUP_CONCAT(payments.id) as id,
                                                     GROUP_CONCAT(payments.customer_id) as customer_id,
                                                     GROUP_CONCAT(DISTINCT payments.date) as date,
                                                     GROUP_CONCAT(DISTINCT payments.payment_methode) as payment_methode,
                                                     GROUP_CONCAT(DISTINCT payments.paid_through) as paid_through,
                                                     GROUP_CONCAT(Distinct invoices.invoice_number) as invoice_number,
                                                     GROUP_CONCAT(Distinct transactions.type) as type,
                                                     GROUP_CONCAT(Distinct payments.total_amount) as total_amount,
                                                     GROUP_CONCAT(Distinct payments.total_other_expense) as total_other_expense')
                                        ->groupBy('payments.id')
                                        ->get();

                    // dd($payments->toArray());

        $search_customers   = Customers::where('status', 1)->get();
        $customer_name      = Customers::find($customer_id);

        if (isset($_GET['download']) && ($_GET['download'] == 1))
        {
            $mpdf       = new \Mpdf\Mpdf();
            $mpdf->WriteHTML(view('report::PaymentsReport.index_download', compact('payments', 'search_customers', 'customer_name', 'customer_id')));
            $mpdf->Output('Payments-Report-'.$date, "I");
        }

        return view('report::PaymentsReport.index', compact('payments', 'search_customers', 'customer_name', 'customer_id'));
    }

    public function paymentsReportIndexDownload()
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('payments_report_index_download');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $customer_id        = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $date               = date('Y-m-d');
        $from_date          = isset($_GET['from_date']) ? $_GET['from_date'] : date('Y-m-d', strtotime($date. ' - 9999 days'));
        $to_date            = isset($_GET['to_date']) ? $_GET['to_date'] : $date;


        $payments           = Payments::join('transactions', 'transactions.payment_id', 'payments.id')
                                        ->join('invoices', 'invoices.id', 'transactions.invoice_id')
                                        ->whereBetween('payments.date', [$from_date, $to_date])
                                        ->when($customer_id != 0, function ($query) use ($customer_id) {
                                            return $query->where('payments.customer_id', $customer_id);
                                        })
                                        ->where('payments.status', '=', 1)
                                        ->selectRaw('GROUP_CONCAT(payments.id) as id,
                                                     GROUP_CONCAT(payments.customer_id) as customer_id,
                                                     GROUP_CONCAT(DISTINCT payments.date) as date,
                                                     GROUP_CONCAT(DISTINCT payments.payment_methode) as payment_methode,
                                                     GROUP_CONCAT(DISTINCT payments.paid_through) as paid_through,
                                                     GROUP_CONCAT(Distinct invoices.invoice_number) as invoice_number,
                                                     GROUP_CONCAT(Distinct transactions.type) as type,
                                                     GROUP_CONCAT(Distinct payments.total_amount) as total_amount,
                                                     GROUP_CONCAT(Distinct payments.total_other_expense) as total_other_expense')
                                        ->groupBy('payments.id')
                                        ->get();

        $search_customers   = Customers::where('status', 1)->get();
        $customer_name      = Customers::find($customer_id);

        $mpdf       = new \Mpdf\Mpdf();
        $mpdf->WriteHTML(view('report::PaymentsReport.index_download', compact('payments', 'search_customers', 'customer_name', 'customer_id')));
        $mpdf->Output('Payments-Report-'.$date, "I");
    }

}
