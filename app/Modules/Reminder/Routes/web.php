<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'reminder', 'middleware' => 'auth'], function () {
    Route::get('/index', 'ReminderController@index')->name('reminder_index');
    Route::get('/create', 'ReminderController@create')->name('reminder_create');
    Route::post('/store', 'ReminderController@store')->name('reminder_store');
    Route::get('/edit/{id}', 'ReminderController@edit')->name('reminder_edit');
    Route::post('/update/{id}', 'ReminderController@update')->name('reminder_update');
    Route::get('/delete/{id}', 'ReminderController@destroy')->name('reminder_delete');
    Route::get('/done/{id}', 'ReminderController@done')->name('reminder_done');
    Route::get('/later/{id}', 'ReminderController@later')->name('reminder_later');
});

