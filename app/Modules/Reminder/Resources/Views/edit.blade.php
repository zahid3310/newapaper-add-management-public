@extends('layouts.app')

@section('title', 'Edit Reminder')

@section('content')

<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<div class="col-md-12">
				<h3 class="box-title">
					Edit Reminder
				</h3>
			</div>
		</div>
		<div class="box-body">
			@if(Session::has('message'))
			<div style="padding: 0px" class="col-md-12">
				<div class="alert alert-success alert-dismissable text-center">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					{{Session::get('message')}}
				</div>
			</div>
			@endif
			@if(Session::has('errors'))
			<div style="padding: 0px" class="col-md-12">
				<div class="alert alert-success alert-dismissable text-center">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					{{Session::get('errors')}}
				</div>
			</div>
			@endif
			<form class="form-horizontal" method="POST" action="{{ route('reminder_update', $reminder->id) }}" enctype="multipart/form-data">
				{{ csrf_field() }}

				<div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
					<label for="name" class="col-md-4 control-label">
						Date * 
					</label>
					<div class="col-md-6">
						<input type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control datepicker" name="date" value="{{ $reminder['date'] }}" required="required">

						@if ($errors->has('date'))
						<span class="help-block">
							<strong>{{ $errors->first('date') }}</strong>
						</span>
						@endif
					</div>
				</div>

				<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
					<label for="name" class="col-md-4 control-label">
						Title * 
					</label>

					<div class="col-md-6">
				
						<input id="name" type="text" class="form-control" name="name" value="{{$reminder['name'] }}" required autofocus>

						@if ($errors->has('name'))
						<span class="help-block">
							<strong>{{ $errors->first('name') }}</strong>
						</span>
						@endif
					</div>
				</div>

				<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
					<label for="description" class="col-md-4 control-label">
						Description
					</label>

					<div class="col-md-6">
						
						<textarea id="name" type="text" class="form-control" name="description">{{ $reminder['description'] }}</textarea>

						@if ($errors->has('description'))
						<span class="help-block">
							<strong>{{ $errors->first('description') }}</strong>
						</span>
						@endif
					</div>
				</div>

				<div class="form-group{{ $errors->has('file_url') ? ' has-error' : '' }}">
					<label for="file_url" class="col-md-4 control-label">
						Old File  
					</label>

					<div class="col-md-6">
						@if($reminder->file_url != null)
							<img src="{{ asset('assets/images/reminders/'.$reminder->file_url) }}" style="height: 80px; width: 80px;border-radius: 50%">
						@endif
					</div>
				</div>

				<div class="form-group{{ $errors->has('file_url') ? ' has-error' : '' }}">
					<label for="file_url" class="col-md-4 control-label">
						File  
					</label>

					<div class="col-md-6">

						<input id="file_url" type="file" class="form-control" name="file_url">

						@if ($errors->has('file_url'))
						<span class="help-block">
							<strong>{{ $errors->first('file_url') }}</strong>
						</span>
						@endif
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-6 col-md-offset-4">
						<button type="submit" class="btn btn-primary">
							<i class="fa fa-save"></i>
							Update
						</button>
						<a href="{{route('reminder_index')}}" class="btn btn-danger">
							<i class="fa fa-trash"></i>
							Cancel
						</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>
@endsection