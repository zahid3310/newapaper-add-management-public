@extends('layouts.app')

@section('title', 'Reminder List')

@section('content')
<style type="text/css">
	.customAlign table thead tr th{
		vertical-align: middle;
	}
	.table-tr-style tr:nth-child(even) {
		background-color: #dddddd;
	}
	.table-tr-style tr:nth-child(odd) {
		background-color: #F2F3F4;
	}
	.total th{
		font-size: 20px
	}
	.select2-container .select2-selection--single{
		height: auto
	}
	.select2-container--default .select2-selection--single .select2-selection__rendered{
		line-height: 20px
	}
	.select2-container .select2-selection--single .select2-selection__rendered{
		margin-top: 0px
	}
</style>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				@if(Session::has('message'))
				<div class="col-md-12">
					<div class="alert alert-success alert-dismissable text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{Session::get('message')}}
					</div>
				</div>
				@endif
				@if(Session::has('errors'))
				<div class="col-md-12">
					<div class="alert alert-success alert-dismissable text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{Session::get('errors')}}
					</div>
				</div>
				@endif
			</div>
		</div>

		<div class="col-md-4">	
			<div class="box">
				<div class="col-md-12">
					<h3 class="box-title" style="width: 100%">Add New Reminder</h3>
					<hr>
				</div>

				<div class="box-body">
					<form class="form-horizontal" method="POST" action="{{ route('reminder_store') }}" enctype="multipart/form-data">
						{{ csrf_field() }}

						<div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
							<div class="col-md-12">
								<label for="name" class="control-label">
									Date * 
								</label>

								<input type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control datepicker" name="date" value="{{ date('Y-m-d') }}" required="required">

								@if ($errors->has('date'))
								<span class="help-block">
									<strong>{{ $errors->first('date') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<div class="col-md-12">
								<label for="name" class="control-label">
									Title * 
								</label>
								
								<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

								@if ($errors->has('name'))
								<span class="help-block">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
							<div class="col-md-12">
								<label for="description" class="control-label">
									Description
								</label>
								<textarea id="name" type="text" class="form-control" name="description" value="{{ old('description') }}"></textarea>

								@if ($errors->has('description'))
								<span class="help-block">
									<strong>{{ $errors->first('description') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('file_url') ? ' has-error' : '' }}">
							<div class="col-md-12">
								<label for="file_url" class="control-label">
									File  
								</label>
								
								<input id="file_url" type="file" class="form-control" name="file_url">

								@if ($errors->has('file_url'))
								<span class="help-block">
									<strong>{{ $errors->first('file_url') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-plus-circle"></i>
									Save
								</button>
								<a href="{{route('reminder_index')}}" class="btn btn-danger">
									<i class="fa fa-trash"></i>
									Cancel
								</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<div class="col-md-8">	
			<div class="box">
				<div class="col-md-12">
					<h3 class="box-title" style="width: 100%">Reminder List</h3>
					<hr>
				</div>

				<div class="box-body">
					@if(count($reminders) >0)
						<div class="col-md-12 customAlign table-responsive">

							<table id="dataTable" class="table table-bordered table-striped dataTable">
								<thead>
									<tr style="background-color: #F9E79F">
										<th style="width: 6%">SL</th>
										<th style="width: 30%">Title</th>
										<th style="width: 14%">Date</th>
										<th style="width: 20%">Description</th>
										<th style="width: 30%;" class="text-center">Action</th>
									</tr>
								</thead>
								<tbody>
									@if(!empty($reminders) && (count($reminders)>0) )
									@foreach($reminders as $key => $reminder)
									<tr>
										<td>{{ $key + 1 }}</td>
										<td>{{ $reminder->name }}</td>
										<td>{{ date('d-m-Y', strtotime($reminder->date)) }}</td>
										<td>{{ $reminder->description }}</td>
										<td class="text-right">
											<a href="{{ route('reminder_done', $reminder->id) }}" class="btn btn-success btn-xs" @if($reminder->reminder_status == '1') disabled @endif>Done</a>
        									<a href="{{ route('reminder_later', $reminder->id) }}" class="btn btn-danger btn-xs" @if($reminder->reminder_status == '2') disabled @endif>
        									   @if($reminder->reminder_status == '2') Pending @else Later @endif
        									</a>
        								    
        								    @if($reminder->file_url)
        									<a href="{{ asset('assets/images/reminders/'.$reminder->file_url) }}" class="btn btn-info btn-xs" target="_blank"><i title="Download" class="fa fa-download" aria-hidden="true"></i></a>
        									@endif
        									
        									<a href="{{ route('reminder_edit', $reminder->id) }}" class="btn btn-success btn-xs"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i></a>
											<a href="{{ route('reminder_delete', $reminder->id) }}" class="btn btn-danger btn-xs confirm_box"><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></a>
										</td>
									</tr>
									@endforeach
									@endif
								</tbody>
							</table>
						</div>
					</div>
					@else
					<div class="box-body">
						<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
							<p style="text-align: center;font-size: 18px">No Data Found.<br>Please Insert Reminder.</p>
						</div>
					</div>
					@endif
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@push('scripts')
<script type="text/javascript">
	$(".confirm_box").click(function()
	{
		if (!confirm("Do you want to delete"))
		{
			return false;
		}
	});
</script>
@endpush