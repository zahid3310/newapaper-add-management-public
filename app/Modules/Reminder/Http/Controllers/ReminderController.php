<?php

namespace App\Modules\Reminder\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Models\Reminder;

class ReminderController extends Controller
{
    public function index()
    {	
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('reminder_index');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $reminders = Reminder::where('status', '=', 1)->orderBy('name', 'asc')->get();

    	return view('reminder::index', compact('reminders'));
    }

    public function create()
    {	
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('reminder_create');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End
    	
    	return view('reminders::create');
    }

    public function store(Request $request)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('reporter_area_store');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $this->validate($request,[
         'name'         => 'required',
         'date'         => 'required',
        ]);

        $reminder            	    = new Reminder;
        $reminder->name  		    = $request->name;
        $reminder->date  		    = $request->date;
        $reminder->description      = $request->description;
        $reminder->reminder_status  = 0;
        $reminder->status           = 1;
        $reminder->created_by       = Auth::user()->id;

        if($request->hasFile('file_url'))
        {
           $photo               = $request->file('file_url');
           $photoName           = time().".".$photo->getClientOriginalExtension();
           $directory1          = 'assets/images/reminders/';
           $photo->move($directory1, $photoName);
           $photoUrl1           = $photoName;
           $reminder->file_url  = $photoUrl1;
        }

        if ($reminder->save())
        {
            return redirect()->route('reminder_index')->with('message', 'Successfully added !!');
        }else{
            return back()->with('message', 'Something went wrong !! Please try again.');
        }
    }

    public function edit($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('reminder_edit');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $reminder       = Reminder::find($id);

    	return view('reminder::edit', compact('reminder'));
    }

    public function update(Request $request, $id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('reminder_update');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $this->validate($request,[
         'name'         => 'required',
         'date'         => 'required',
        ]);

        $reminder            	    = Reminder::find($id);
        $reminder->name  		    = $request->name;
        $reminder->date  		    = $request->date;
        $reminder->description      = $request->description;
        $reminder->updated_by       = Auth::user()->id;

        if($request->hasFile('file_url'))
        {	
        	if ($reminder->file_url != null)
        	{
        		unlink('assets/images/reminders/'.$reminder->file_url);
        	}

            $photo               = $request->file('file_url');
            $photoName           = time().".".$photo->getClientOriginalExtension();
            $directory1          = 'assets/images/reminders/';
            $photo->move($directory1, $photoName);
            $photoUrl1           = $photoName;
            $reminder->file_url  = $photoUrl1;
        }       

        if ($reminder->save())
        {
            return redirect()->route('reminder_index')->with('message', 'Successfully updated !!');
        }else{
            return back()->with('message', 'Something went wrong !! Please try again.');
        }
    }

    public function destroy($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('reminder_delete');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $reminder               = Reminder::find($id);
        $reminder->status       = 0;
        $reminder->updated_by   = Auth::user()->id;

        if ($reminder->save())
        {
            return redirect()->route('reminder_index')->with('message', 'Successfully deleted !!');
        }else{
            return back()->with('message', 'Something went wrong !! Please try again.');
        }
    }
    
    public function done($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('reminder_done');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End
        
        $reminder            	    = Reminder::find($id);
        $reminder->reminder_status  = 1;
        $reminder->updated_by       = Auth::user()->id;

        if ($reminder->save())
        {
            return redirect()->route('reminder_index')->with('message', 'Successfully done !!');
        }else{
            return back()->with('message', 'Something went wrong !! Please try again.');
        }
    }
    
    public function later($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('reminder_later');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End
        
        $reminder            	    = Reminder::find($id);
        $reminder->reminder_status  = 2;
        $reminder->updated_by       = Auth::user()->id;

        if ($reminder->save())
        {
            return redirect()->route('reminder_index')->with('message', 'Successfully send to pending !!');
        }else{
            return back()->with('message', 'Something went wrong !! Please try again.');
        }
    }
}
