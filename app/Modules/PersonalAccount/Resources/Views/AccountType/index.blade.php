@extends('layouts.app')

@section('title', 'Account Type')

@section('content')


	<section class="content">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title"> Account Type </h3>
			</div>

			<div class="box-body">
			<div class="col-md-6">
				<div class="box-body">
					@if(Session::has('message'))
					<div style="padding: 0px" class="col-md-12">
						<div class="alert alert-success alert-dismissable text-center">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							{{Session::get('message')}}
						</div>
					</div>
					@endif
					@if(Session::has('errors'))
					<div style="padding: 0px" class="col-md-12">
						<div class="alert alert-success alert-dismissable text-center">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							{{Session::get('errors')}}
						</div>
					</div>
					@endif
					<form class="form-horizontal" method="POST" action="{{ route('personal_account_category_store') }}" enctype="multipart/form-data">
						{{ csrf_field() }}

						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<label for="name" class="col-md-4 control-label">
							Name *
							</label>

							<div class="col-md-8">
								<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" />

								@if ($errors->has('name'))
								<span class="help-block">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
							<label for="description" class="col-md-4 control-label">
							Description
							</label>

							<div class="col-md-8">
								<textarea id="description" type="text" class="form-control" name="description" value="{{ old('description') }}"></textarea>

								@if ($errors->has('description'))
								<span class="help-block">
									<strong>{{ $errors->first('description') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<hr>

						<div class="form-group">
							<div class="col-md-8 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-plus-circle"></i>
								Create
								</button>
								<a href="{{ route('personal_account_index') }}" class="btn btn-danger">
									<i class="fa fa-trash"></i>
								Cancel
								</a>
							</div>
						</div>
					</form>
				</div>
			</div>

			<div class="col-md-6">
				@if(count($category) >0)
				<div class="box-body">
					<div class="col-md-12 customAlign table-responsive">
						<table id="dataTable" class="table table-bordered table-striped display1">
							<thead>
								<tr style="background-color: #F9E79F">
									<th>SL</th>
									<th>name</th>
									<th>Description</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							@if(!empty($category) && (count($category)>0) )
							@foreach($category as $key => $category_value)
								<tr>
									<td>{{ $key + 1 }}</td>
									<td>{{ $category_value->name }}</td>
									<td>{{ $category_value->description }}</td>
									<td>
										<a href="{{ route('personal_account_category_edit', $category_value->id) }}" class="btn btn-success btn-xs"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i></a>
										<a href="{{ route('personal_account_category_delete', $category_value->id) }}" class="btn btn-danger btn-xs confirm_box"><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></a>
									</td>
								</tr>
							@endforeach
							@endif
							</tbody>
						</table>
					</div>
			    </div>
			    @else
			    <div class="box-body">
				    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
				    	<p style="text-align: center;font-size: 18px">No Data Found.</p>
				    </div>
			    </div>
			    @endif
			</div>
			</div>
		</div>
	</section>
@endsection