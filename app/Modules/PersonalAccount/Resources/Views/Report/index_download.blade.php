<<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style type="text/css">
	table, th, td {
	  border: 1px solid black;
	  border-collapse: collapse;
	  padding: 5px;
	  font-family: 'nikosh';
	}
</style>

<body>
	<div col-md-12 col-sm-12 col-xs-12 col-lg-12>
		<h3 style="text-align: center;margin-bottom: 0px">
		@if(isset($company_profile[0]->name))
			{{ $company_profile[0]->name }}
		@else
			Dainik Dhaka Report
		@endif
		</h3>
		<p style="text-align: center;font-size: 14px;margin-bottom: 0px;margin-top: 0px">
			@if(isset($company_profile[0]->address))
				{{ $company_profile[0]->address }}
			@else
				68, Joginagar wari ,Dhaka-1203
			@endif
		</p>
		<h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px">
			Wallet Report
		</h4>
		@if(isset($_GET['from_date']) && isset($_GET['to_date']))
			<p style="text-align: center;margin-bottom: 0px;margin-top: 0px;font-size: 15px;font-weight: bold">
				From {{ date('d-m-Y', strtotime($_GET['from_date'])) }} To {{ date('d-m-Y', strtotime($_GET['to_date'])) }}
			</p>
		@else
			<p style="text-align: center;margin-bottom: 0px;margin-top: 0px;font-size: 15px;font-weight: bold">
				From {{ date('d-m-Y', strtotime($from_date)) }} To {{ date('d-m-Y', strtotime($to_date)) }}
			</p>
		@endif
		<p style="text-align: center;font-size: 20px;margin-top: 0px">
			@if(!empty($customer_name) && ($customer_name->count() > 0))
				{{ $customer_name['name'] }}
			@else
				{{ $account_type_name != null ? $account_type_name['name'] : 'All Account' }}
			@endif
		</p>
	</div>

	<table style="width: 100%" class="table table-bordered dataTable">
		<thead>
			<tr>
				<th>SL</th>
				<th>Account Name</th>
				<th style="text-align: right">Total Expense</th>
				<th style="text-align: right">Total Income</th>
			</tr>
		</thead>
		<tbody>
			@php
				$total_expense = 0;
				$total_income  = 0;
			@endphp
			@foreach($search_account_type as $key => $account)
				@if( ($account_wise_expense[$key] - $account_wise_income[$key] != 0) )
					@php
						$total_expense = $total_expense + $account_wise_expense[$key];
						$total_income  = $total_income + $account_wise_income[$key];
					@endphp
					<tr>
						<td>{{ $key + 1 }}</td>
						<td>
							{{ $account->name }}
						</td>
						<td style="text-align: right">{{ $account_wise_expense[$key] }}</td>
						<td style="text-align: right">{{ $account_wise_income[$key] }}</td>
					</tr>
				@endif
			@endforeach
			<tr>
				<td style="text-align: right"></td>
				<td style="text-align: right"><b>Total</b></td>
				<td style="text-align: right"><b>{{ $total_expense }}</b></td>
				<td style="text-align: right"><b>{{ $total_income }}</b>
			</tr>
		</tbody>
	</table>
</body>
</html>