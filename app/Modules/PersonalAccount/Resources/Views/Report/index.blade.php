@extends('layouts.app')

@section('title', 'Wallet Report')

@section('content')
	<style type="text/css">
		.customAlign table thead tr th{
			vertical-align: middle;
		}
		.table-tr-style tr:nth-child(even) {
			background-color: #dddddd;
		}
		.table-tr-style tr:nth-child(odd) {
			background-color: #F2F3F4;
		}
		.total th{
			font-size: 20px
		}
		.select2-container .select2-selection--single{
			height: auto
		}
		.select2-container--default .select2-selection--single .select2-selection__rendered{
			line-height: 20px
		}
		.select2-container .select2-selection--single .select2-selection__rendered{
			margin-top: 0px
		}
		table.dataTable thead > tr > th {
		     padding-right: 10px !important; 
		}
	</style>
	<section class="content">
		<div class="box">
		     <div class="box-header with-border">
			    <div class="col-md-12">
    				<h3 class="box-title" style="width:100%">Wallet Report 
    				<span class="pull-right">
    					<a data-toggle="modal" data-target="#modal-default">
    						<button class="btn btn-info btn-xs"><i class="fa fa-search"></i> Search</button>
    					</a>
    					<a @if(isset($_GET['from_date']) && isset($_GET['to_date'])) href="{{ \Request::fullUrl().'&download=1' }}" @else href="{{ route('personal_transaction_report_index_download') }}" @endif target="_blank">
    						<button class="btn btn-success btn-xs"><i class="fa fa-download"></i> Download</button>
    					</a>
    					</span>
    				</h3>
    			</div>
			</div>

			<div class="box-body">
				<div col-md-12 col-sm-12 col-xs-12 col-lg-12>
					<h3 style="text-align: center;margin-bottom: 0px">
					@if(isset($company_profile[0]->name))
						{{ $company_profile[0]->name }}
					@else
						Dainik Dhaka Report
					@endif
					</h3>
					<p style="text-align: center;font-size: 14px;margin-bottom: 0px">
						@if(isset($company_profile[0]->address))
							{{ $company_profile[0]->address }}
						@else
							68, Joginagar wari ,Dhaka-1203
						@endif
					</p>
					<h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px">
						Wallet Report
					</h4>
					@if(isset($_GET['from_date']) && isset($_GET['to_date']))
						<p style="text-align: center;margin-bottom: 0px;margin-top: 0px;font-size: 15px;font-weight: bold">
							From {{ date('d-m-Y', strtotime($_GET['from_date'])) }} To {{ date('d-m-Y', strtotime($_GET['to_date'])) }}
						</p>
					@else
						<p style="text-align: center;margin-bottom: 0px;margin-top: 0px;font-size: 15px;font-weight: bold">
							From {{ date('d-m-Y', strtotime($from_date)) }} To {{ date('d-m-Y', strtotime($to_date)) }}
						</p>
					@endif
					<p style="text-align: center;font-size: 20px">
						@if(!empty($customer_name) && ($customer_name->count() > 0))
							{{ $customer_name['name'] }}
						@else
							{{ $account_type_name != null ? $account_type_name['name'] : 'All Account' }}
						@endif
					</p>
				</div>
				<div class="col-md-12 customAlign table-responsive">

					<table class="table table-bordered table-striped dataTable">
						<thead>
							<tr style="background-color: #F9E79F">
								<th style="text-align: left">SL</th>
								<th style="text-align: left">Account Name</th>
								<th style="text-align: right">Total Expense</th>
								<th style="text-align: right">Total Income</th>
							</tr>
						</thead>
						<tbody>
							@php
								$total_expense = 0;
								$total_income  = 0;
							@endphp
							@foreach($search_account_type as $key => $account)
								@if( ($account_wise_expense[$key] - $account_wise_income[$key] != 0) )
									@php
										$total_expense = $total_expense + $account_wise_expense[$key];
										$total_income  = $total_income + $account_wise_income[$key];
									@endphp
									<tr>
										<td>{{ $key + 1 }}</td>
										<td>
											<a href="{{ url('personalAccountReport/report-details?account_type='.$account->id) }}">{{ $account->name }}</a>
										</td>
										<td style="text-align: right">{{ $account_wise_expense[$key] }}</td>
										<td style="text-align: right">{{ $account_wise_income[$key] }}</td>
									</tr>
								@endif
							@endforeach
							<tr>
								<td style="text-align: right"></td>
								<td style="text-align: right"><b>Total</b></td>
								<td style="text-align: right"><b>{{ $total_expense }}</b></td>
								<td style="text-align: right"><b>{{ $total_income }}</b>
							</tr>
						</tbody>
					</table>
				</div>
		    </div>
		    
		</div>
	</section>

	<div class="modal fade" id="modal-default">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title text-center">Select date range and Account</h4>
					</div>

					<div class="modal-body">
						<div class="row">
							<form method="get" action="{{ route('personal_transaction_report_index') }}" enctype="multipart/form-data">
								<div class="form-group">
									<div class="col-md-8 col-md-offset-2">
										<br>
										<label>Account</label><br>
										<select style="width: 100%" name="account_type" class="form-control select2">
												<option value="0">--All Account--</option>
											@foreach($search_account_type as $search_account_type)
													<option {{ $account_type == $search_account_type->id ? 'selected' : ''}} value="{{ $search_account_type->id }}">{{ $search_account_type->name }}</option>
											@endforeach
										</select>
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-8 col-md-offset-2">
										<br>
										<label>From Date</label>
										<input type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control datepicker" name="from_date" value="{{ isset($_GET['from_date']) ? $_GET['from_date'] : '' }}">
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-8 col-md-offset-2">
										<br>
										<label>To Date</label>
										<input type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control datepicker" name="to_date" value="{{ isset($_GET['to_date']) ? $_GET['to_date'] : '' }}">
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-8 col-md-offset-2">
										<br>
										<button type="submit" class="btn btn-success btn-block" style="border-radius: 0px">Search</button>
										<br>
									</div>
								</div>
							</form>

						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
	</div>
@endsection

@push('scripts')
	<script type="text/javascript">
		$(".confirm_box").click(function()
		{
			if (!confirm("Do you want to delete"))
			{
				return false;
			}
		});
	</script>
@endpush