@extends('layouts.app')

@section('title', 'Wallet Details')

@section('content')
	<style type="text/css">
		.customAlign table thead tr th{
			vertical-align: middle;
		}
		.table-tr-style tr:nth-child(even) {
			background-color: #dddddd;
		}
		.table-tr-style tr:nth-child(odd) {
			background-color: #F2F3F4;
		}
		.total th{
			font-size: 20px
		}
		.select2-container .select2-selection--single{
			height: auto
		}
		.select2-container--default .select2-selection--single .select2-selection__rendered{
			line-height: 20px
		}
		.select2-container .select2-selection--single .select2-selection__rendered{
			margin-top: 0px
		}
		table.dataTable thead > tr > th {
		     padding-right: 10px !important; 
		}
	</style>

	<section class="content">
		<div class="box">
		    <div class="box-header with-border">
			    <div class="col-md-12">
				<h3 class="box-title" style="width:100%">Wallet Report 
					<a href="{{ route('personal_transaction_report_report_details_download', 'account_type='.$_GET['account_type']) }}" target="_blank">
						<button class="btn btn-success btn-xs pull-right"><i class="fa fa-download"></i> Download</button>
					</a>
				</h3>
			</div>
			</div>

			<div class="box-body">
				<div col-md-12 col-sm-12 col-xs-12 col-lg-12>
					<h3 style="text-align: center;margin-bottom: 0px">
					@if(isset($company_profile[0]->name))
						{{ $company_profile[0]->name }}
					@else
						Dainik Dhaka Report
					@endif
					</h3>
					<p style="text-align: center;font-size: 14px;margin-bottom: 0px">
						@if(isset($company_profile[0]->address))
							{{ $company_profile[0]->address }}
						@else
							68, Joginagar wari ,Dhaka-1203
						@endif
					</p>
					<h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px">
						Wallet Report
					</h4>
					
					<p style="text-align: center;font-size: 20px">
						@if(!empty($customer_name) && ($customer_name->count() > 0))
							{{ $customer_name['name'] }}
						@else
							{{ $account_type_name != null ? $account_type_name['name'] : 'All Account' }}
						@endif
					</p>
				</div>
				<div class="col-md-12 customAlign table-responsive">

					<table class="table table-bordered table-striped dataTable">
						<thead>
							<tr style="background-color: #F9E79F">
								<th style="text-align: left">SL</th>
								<th style="text-align: left">Date</th>
								<th style="text-align: left">Account</th>
								<th style="text-align: left">Transaction Type</th>
								<th style="text-align: left">Note</th>
								<th style="text-align: right">Amount</th>
							</tr>
						</thead>
						<tbody>
							@php
								$total = 0;
							@endphp
							@foreach($accounts as $key => $account)
								@php
									$total = $total + $account->amount;
								@endphp
								<tr>
									<td style="text-align: left">{{ $key + 1 }}</td>
									<td style="text-align: left">{{ date('d-m-Y', strtotime($account->date)) }}</td>
									<td style="text-align: left">{{ $account->accountName->name }}</td>

									<td style="text-align: left">
										@if($account->trans_type == 0)
											Money Out
										@else
											Money In
										@endif
									</td>
									<td style="text-align: left">{{ $account->note }}</td>
									<td style="text-align: right">{{ $account->amount }}</td>
								</tr>
							@endforeach
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td style="text-align: right"><b>Total</b></td>
									<td style="text-align: right"><b>{{ $total }}</b></td>
								</tr>
						</tbody>
					</table>

				</div>
		    </div>
		    
		</div>
	</section>
@endsection

@push('scripts')
	<script type="text/javascript">
		$(".confirm_box").click(function()
		{
			if (!confirm("Do you want to delete"))
			{
				return false;
			}
		});
	</script>
@endpush