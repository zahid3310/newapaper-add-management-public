<<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style type="text/css">
	table, th, td {
	  border: 1px solid black;
	  border-collapse: collapse;
	  padding: 5px;
	  font-family: 'nikosh';
	}
</style>

<body>
	<div col-md-12 col-sm-12 col-xs-12 col-lg-12>
		<h3 style="text-align: center;margin-bottom: 0px">
		@if(isset($company_profile[0]->name))
			{{ $company_profile[0]->name }}
		@else
			Dainik Dhaka Report
		@endif
		</h3>
		<p style="text-align: center;font-size: 14px;margin-bottom: 0px;margin-top: 0px">
			@if(isset($company_profile[0]->address))
				{{ $company_profile[0]->address }}
			@else
				68, Joginagar wari ,Dhaka-1203
			@endif
		</p>
		<h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px">
			Wallet Details Report
		</h4>
		
		<p style="text-align: center;font-size: 20px;margin-top: 0px">
			@if(!empty($customer_name) && ($customer_name->count() > 0))
				{{ $customer_name['name'] }}
			@else
				{{ $account_type_name != null ? $account_type_name['name'] : 'All Account' }}
			@endif
		</p>
	</div>

	<table width="100%" class="table table-bordered table-striped dataTable">
		<thead>
			<tr style="background-color: #F9E79F">
				<th style="text-align: left">SL</th>
				<th style="text-align: left">Date</th>
				<th style="text-align: left">Account</th>
				<th style="text-align: left">Transaction Type</th>
				<th style="text-align: left">Note</th>
				<th style="text-align: right">Amount</th>
			</tr>
		</thead>
		<tbody>
			@php
				$total = 0;
			@endphp
			@foreach($accounts as $key => $account)
				@php
					$total = $total + $account->amount;
				@endphp
				<tr>
					<td style="text-align: left">{{ $key + 1 }}</td>
					<td style="text-align: left">{{ date('d-m-Y', strtotime($account->date)) }}</td>
					<td style="text-align: left">{{ $account->accountName->name }}</td>

					<td style="text-align: left">
						@if($account->trans_type == 0)
							Money Out
						@else
							Money In
						@endif
					</td>
					<td style="text-align: left">{{ $account->note }}</td>
					<td style="text-align: right">{{ $account->amount }}</td>
				</tr>
			@endforeach
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td style="text-align: right"><b>Total</b></td>
					<td style="text-align: right"><b>{{ $total }}</b></td>
				</tr>
		</tbody>
	</table>
</body>
</html>