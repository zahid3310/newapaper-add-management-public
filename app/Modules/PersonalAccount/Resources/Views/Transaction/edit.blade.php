@extends('layouts.app')

@section('title', 'Edit Wallet')

@section('content')

	<section class="content">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
				Edit Wallet
				</h3>
			</div>
			<div class="box-body">
				@if(Session::has('message'))
				<div style="padding: 0px" class="col-md-12">
					<div class="alert alert-success alert-dismissable text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{Session::get('message')}}
					</div>
				</div>
				@endif
				@if(Session::has('errors'))
				<div style="padding: 0px" class="col-md-12">
					<div class="alert alert-success alert-dismissable text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{Session::get('errors')}}
					</div>
				</div>
				@endif
				<form class="form-horizontal" method="POST" action="{{ route('personal_account_update', $transaction->id) }}" enctype="multipart/form-data">
					{{ csrf_field() }}

					<div class="form-group">
						<div class="col-md-2">
						</div>

						<div class="col-md-4">
							<label style="padding-top: 16px" for="amount" class="control-label">
								Amount *
							</label>
							<input id="amount" type="text" class="form-control" name="amount" @if($transaction->trans_type == 0) value="{{ '-'.$transaction->amount }}" @else value="{{ $transaction->amount }}" @endif />

							@if ($errors->has('amount'))
							<span class="help-block">
								<strong>{{ $errors->first('amount') }}</strong>
							</span>
							@endif
						</div>

						<div class="col-md-4">
							<label style="padding-top: 16px" for="name" class="control-label">
								Date *
							</label>
							<input type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control datepicker" name="date" value="{{ date('Y-m-d', strtotime($transaction->date)) }}">

							@if ($errors->has('date'))
							<span class="help-block">
								<strong>{{ $errors->first('date') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-2">
						</div>

						<div class="col-md-4">
							<label for="name" class="box-title control-label" style="width:100%">
								<a style="text-decoration: none;color: black" class="pull-left">Type * </a>
								<a style="color: white;border-radius: 0px" href="{{ route('personal_account_category_create') }}" class="btn btn-success btn-xs pull-right">
								   <i class="fa fa-plus"></i> Add New
								</a>
							</label>
							<select class="form-control select2 select2-hidden-accessible" style="width: 100%;padding: 15px" tabindex="-1" aria-hidden="true" name="personal_account_type">
								<option style="padding: 15px" value="">Select</option>
								@foreach($accounts as $account)
									<option {{ $transaction->personal_account_type == $account->id ? 'selected' : '' }} style="padding: 15px" value="{{ $account->id }}">{{ $account->name }}</option>
								@endforeach
							</select>

							@if ($errors->has('personal_account_type'))
							<span class="help-block">
								<strong>{{ $errors->first('personal_account_type') }}</strong>
							</span>
							@endif
						</div>
						
						<div class="col-md-4">
							<label for="name" class="box-title control-label" style="width:100%">
								<a style="text-decoration: none;color: black" class="pull-left">Sub Type </a>
								<a style="color: white;border-radius: 0px" href="{{ route('personal_account_sub_category_index') }}" class="btn btn-success btn-xs pull-right">
								   <i class="fa fa-plus"></i> Add New
								</a>
							</label>
							<select class="form-control select2 select2-hidden-accessible" style="width: 100%;padding: 15px" tabindex="-1" aria-hidden="true" name="personal_account_sub_type_id">
								<option style="padding: 15px" value="">Select</option>
								@foreach($sub_accounts as $sub_account)
									<option {{ $transaction->personal_account_sub_type == $sub_account->id ? 'selected' : '' }} style="padding: 15px" value="{{ $sub_account->id }}">{{ $sub_account->name }}</option>
								@endforeach
							</select>

							@if ($errors->has('personal_account_sub_type_id'))
							<span class="help-block">
								<strong>{{ $errors->first('personal_account_sub_type_id') }}</strong>
							</span>
							@endif
						</div>
					</div>
                    
                    <div class="form-group">
                        <div class="col-md-2">
						</div>
						
                        <div class="col-md-8">
							<label style="padding-top: 16px" for="note" class="control-label">
								Note
							</label>
							<input id="note" type="text" class="form-control" name="note" value="{{ $transaction->note }}">

							@if ($errors->has('note'))
							<span class="help-block">
								<strong>{{ $errors->first('note') }}</strong>
							</span>
							@endif
						</div>    
                    </div>
                    
					<div class="form-group">
						<div class="col-md-2">
						</div>

						<div class="col-md-8">
							<button type="submit" class="btn btn-primary">
								<i class="fa fa-plus-circle"></i>
							Update
							</button>
							<a href="{{ route('personal_account_index') }}" class="btn btn-danger">
								<i class="fa fa-trash"></i>
							Cancel
							</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
@endsection