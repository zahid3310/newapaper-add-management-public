<<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style type="text/css">
	table, th, td {
	  border: 1px solid black;
	  border-collapse: collapse;
	  padding: 5px;
	  font-family: 'nikosh';
	}
</style>

<body style="font-family: 'nikosh';">
	<div col-md-12 col-sm-12 col-xs-12 col-lg-12>
		<h3 style="text-align: center;margin-bottom: 0px">
		@if(isset($company_profile['name']))
			{{ $company_profile['name'] }}
		@else
			Dainik Dhaka Report
		@endif
		</h3>
		<p style="text-align: center;font-size: 14px;margin-bottom: 0px;margin-top: 0px">
			@if(isset($company_profile['address']))
				{{ $company_profile['address'] }}
			@else
				68, Joginagar wari ,Dhaka-1203
			@endif
		</p>
		<h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px;margin-top: 0px">
			Wallet Report
		</h4>
		@if($from != '1970-01-01' && $to != '1970-01-01')
			<p style="text-align: center;margin-bottom: 0px;margin-top: 0px;font-size: 15px">
				From {{ date('d-m-Y', strtotime($from)) }} To {{ date('d-m-Y', strtotime($to)) }}
			</p>
		@endif
	</div>
	<table style="width: 100%;margin-top: 10px">
		<tr style="background-color: #F9E79F">
			<th style="text-align: left">SL</th>
			<th style="text-align: left">Date</th>
			<th style="text-align: left">Type</th>
			<th style="text-align: left">Note</th>
			<th style="text-align: left">Money In</th>
			<th style="text-align: left">Money Out</th>
		</tr>
		<?php
			$total_in   = 0; 
			$total_out  = 0; 
		?>
		@if(!empty($transactions) && (count($transactions)>0) )
		@foreach($transactions as $key => $transaction)
			<?php
				$in_amount  = $transaction->trans_type == 1 ? $transaction->amount : 0;
				$out_amount = $transaction->trans_type == 0 ? $transaction->amount : 0;
				$total_in   = $total_in + $in_amount; 
				$total_out  = $total_out + $out_amount; 
			?>
			<tr>
				<td style="text-align: left">{{ $key + 1 }}</td>
				<td style="text-align: left">{{ date('d-m-Y', strtotime($transaction->date)) }}</td>
				<td style="text-align: left">{{ $transaction->accountName->name }}</td>
				<td style="text-align: left">{{ $transaction->note }}</td>
				<td style="text-align: left">{{ $transaction->trans_type == 1 ? number_format($transaction->amount,2,'.',',') : '' }}</td>
				<td style="text-align: left">{{ $transaction->trans_type == 0 ? number_format($transaction->amount,2,'.',',') : '' }}</td>
			</tr>
		@endforeach
		@endif

			<tr>
				<td style="text-align: right" colspan="4"><b>Total</b></td>
				<td style="text-align: left"><b>{{ number_format($total_in,2,'.',',') }}</b></td>
				<td style="text-align: left"><b>{{ number_format($total_out,2,'.',',') }}</b></td>
			</tr>
	</table>
</body>
</html>