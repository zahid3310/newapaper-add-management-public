@extends('layouts.app')

@section('title', 'Wallet')

@section('content')
	<style type="text/css">
		.customAlign table thead tr th{
			vertical-align: middle;
		}
		.table-tr-style tr:nth-child(even) {
			background-color: #dddddd;
		}
		.table-tr-style tr:nth-child(odd) {
			background-color: #F2F3F4;
		}
		.total th{
			font-size: 20px
		}
		.select2-container .select2-selection--single{
			height: auto
		}
		.select2-container--default .select2-selection--single .select2-selection__rendered{
			line-height: 20px
		}
		.select2-container .select2-selection--single .select2-selection__rendered{
			margin-top: 0px
		}
	</style>

	<section class="content">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
					Wallet
				</h3>
			</div>

			<div class="box-body">
				<div class="col-md-12">
					@if(Session::has('message'))
					<div style="padding: 0px" class="col-md-12">
						<div class="alert alert-success alert-dismissable text-center">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							{{Session::get('message')}}
						</div>
					</div>
					@endif
					@if(Session::has('errors'))
					<div style="padding: 0px" class="col-md-12">
						<div class="alert alert-success alert-dismissable text-center">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							{{Session::get('errors')}}
						</div>
					</div>
					@endif
					<form class="form-horizontal" method="POST" action="{{ route('personal_account_store') }}" enctype="multipart/form-data">
						{{ csrf_field() }}

						<div class="form-group">
							<div class="col-md-2">
							</div>

							<div class="col-md-4">
								<label style="padding-top: 16px" for="amount" class="control-label">
									Amount *
								</label>
								<input id="amount" type="text" class="form-control" name="amount" value="{{ old('amount') }}" />

								@if ($errors->has('amount'))
								<span class="help-block">
									<strong>{{ $errors->first('amount') }}</strong>
								</span>
								@endif
							</div>

							<div class="col-md-4">
								<label style="padding-top: 16px" for="name" class="control-label">
									Date *
								</label>
								<input type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control datepicker" name="date" value="{{ date('Y-m-d') }}">

								@if ($errors->has('date'))
								<span class="help-block">
									<strong>{{ $errors->first('date') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-2">
							</div>

							<div class="col-md-4">
								<label for="name" class="box-title control-label" style="width:100%">
									<a style="text-decoration: none;color: black" class="pull-left">Type * </a>
									<a style="color: white;border-radius: 0px" href="{{ route('personal_account_category_index') }}" class="btn btn-success btn-xs pull-right">
									   <i class="fa fa-plus"></i> Add New
									</a>
								</label>
								<select class="form-control select2 select2-hidden-accessible" style="width: 100%;padding: 15px" tabindex="-1" aria-hidden="true" name="personal_account_type">
									<option style="padding: 15px" value="">Select</option>
									@foreach($accounts as $account)
										<option style="padding: 15px" value="{{ $account->id }}">{{ $account->name }}</option>
									@endforeach
								</select>

								@if ($errors->has('personal_account_type'))
								<span class="help-block">
									<strong>{{ $errors->first('personal_account_type') }}</strong>
								</span>
								@endif
							</div>
							
							<div class="col-md-4">
								<label for="name" class="box-title control-label" style="width:100%">
									<a style="text-decoration: none;color: black" class="pull-left">Sub Type </a>
									<a style="color: white;border-radius: 0px" href="{{ route('personal_account_sub_category_index') }}" class="btn btn-success btn-xs pull-right">
									   <i class="fa fa-plus"></i> Add New
									</a>
								</label>
								<select class="form-control select2 select2-hidden-accessible" style="width: 100%;padding: 15px" tabindex="-1" aria-hidden="true" name="personal_account_sub_type_id">
									<option style="padding: 15px" value="">Select</option>
									@foreach($sub_accounts as $sub_account)
										<option style="padding: 15px" value="{{ $sub_account->id }}">{{ $sub_account->name }}</option>
									@endforeach
								</select>

								@if ($errors->has('personal_account_sub_type_id'))
								<span class="help-block">
									<strong>{{ $errors->first('personal_account_sub_type_id') }}</strong>
								</span>
								@endif
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-2">
							</div>

							<div class="col-md-8">
								<label style="padding-top: 16px" for="note" class="control-label">
									Note
								</label>
								<input id="note" type="text" class="form-control" name="note" value="{{ old('note') }}">

								@if ($errors->has('note'))
								<span class="help-block">
									<strong>{{ $errors->first('note') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-2">
							</div>

							<div class="col-md-8">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-plus-circle"></i>
								Create
								</button>
								<a href="{{ route('personal_account_index') }}" class="btn btn-danger">
									<i class="fa fa-trash"></i>
								Cancel
								</a>
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>

		<div class="col-md-12 col-sm-12 col-xs-12">
			<a style="color: white;border-radius: 0px" class="col-md-5 col-sm-3">
			</a>
			<a id="historyShow" style="color: white;border-radius: 0px" class="btn btn-success col-md-2 col-sm-6 col-xs-12">
			    Show Wallet History
			</a>
		</div>

		<br>
		<br>
		<br>

		<div class="box">
			<div style="padding: 0px" class="box-body HistoryIndex">

				<br>

				<div style="margin-bottom: 15px" class="col-md-12">
					<div class="">
						<div class="row">
							<form method="get">
								<input type="hidden" name="search" value="yes">
								<div class="form-group">

									<div class="col-md-12">
										<div class="form-group">
											<div class="col-md-2">
												<select class="form-control select2 select2-hidden-accessible" style="width: 100%;padding: 15px" tabindex="-1" aria-hidden="true" name="in_out">
													<option style="padding: 15px" value="">Wallet Balance</option>
													<option style="padding: 15px" value="1" @if(isset($_GET['in_out']) && $_GET['in_out'] == 1) selected @endif>Money In</option>
													<option style="padding: 15px" value="2" @if(isset($_GET['in_out']) && $_GET['in_out'] == 2) selected @endif>Money Out</option>
												
												</select>
											</div>

											<div class="col-md-2">
												<select class="form-control select2 select2-hidden-accessible" style="width: 100%;padding: 15px" tabindex="-1" aria-hidden="true" name="type">
													<option style="padding: 15px" value="">All Types</option>
													@foreach($accounts as $account)
														<option style="padding: 15px" value="{{ $account->id }}" @if(isset($_GET['type']) && $_GET['type'] == $account->id) selected @endif>{{ $account->name }}</option>
													@endforeach
												</select>
											</div>
											
											<div class="col-md-2">
												<select class="form-control select2 select2-hidden-accessible" style="width: 100%;padding: 15px" tabindex="-1" aria-hidden="true" name="sub_type">
													<option style="padding: 15px" value="">All Sub Types</option>
													@foreach($sub_accounts as $sub_account)
														<option style="padding: 15px" value="{{ $sub_account->id }}" @if(isset($_GET['sub_type']) && $_GET['sub_type'] == $sub_account->id) selected @endif>{{ $sub_account->name }}</option>
													@endforeach
												</select>
											</div>
											
											<div class="col-md-2">
												<input type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control datepicker" name="from" value="{{isset($_GET['from']) ? $_GET['from'] : ''}}">
											</div>

											<div class="col-md-2">
												<input  type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control datepicker" name="to" value="{{isset($_GET['to']) ? $_GET['to'] : ''}}">
											</div>

											<div class="col-md-1">
												<button type="submit" class="btn btn-default btn-block">Search
												</button>
											</div>

											<div class="col-md-1">
												<a style="text-decoration: none" target="_blank">
												<button type="btn" name="download" class="btn btn-default btn-block">Download
												</button>
												</a>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>


				<div style="padding: 0px" class="col-md-12 col-sm-12 col-xs-12">
					@if(count($transactions) >0)
					<div class="box-body">

						<div class="col-md-12 customAlign table-responsive">
							<table id="" class="table table-bordered table-striped display1">
								<thead>
									<tr style="background-color: #F9E79F">
										<th>SL</th>
										<th>Date</th>
										<th>Type</th>
										<th>Sub Type</th>
										<th>Note</th>
										<th>Money In</th>
										<th>Money Out</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								<?php
									$total_in   = 0; 
									$total_out  = 0; 
								?>
								@if(!empty($transactions) && (count($transactions)>0) )
								@foreach($transactions as $key => $transaction)
									<?php
										$in_amount  = $transaction->trans_type == 1 ? $transaction->amount : 0;
										$out_amount = $transaction->trans_type == 0 ? $transaction->amount : 0;
										$total_in   = $total_in + $in_amount; 
										$total_out  = $total_out + $out_amount; 
									?>
									<tr>
										<td>{{ $key + 1 }}</td>
										<td>{{ date('d-m-Y', strtotime($transaction->date)) }}</td>
										<td>{{ $transaction->accountName->name }}</td>
										<td>{{ isset($transaction->subAccountName->name) ? $transaction->subAccountName->name : '' }}</td>
										<td>{{ $transaction->note }}</td>
										<td>{{ $transaction->trans_type == 1 ? number_format($transaction->amount,2,'.',',') : '' }}</td>
										<td>{{ $transaction->trans_type == 0 ? number_format($transaction->amount,2,'.',',') : '' }}</td>
										<td>
											<a href="{{ route('personal_account_edit', $transaction->id) }}" class="btn btn-success btn-xs"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i></a>
											<a href="{{ route('personal_account_delete', $transaction->id) }}" class="btn btn-danger btn-xs confirm_box"><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></a>
										</td>
									</tr>
								@endforeach
								@endif
								</tbody>
								<tr>
									<td class="text-right" colspan="5"><b>Total</b></td>
									<td class="text-left"><b>{{ number_format($total_in,2,'.',',') }}</b></td>
									<td class="text-left"><b>{{ number_format($total_out,2,'.',',') }}</b></td>
									<td class="text-left"><b></b></td>
								</tr>
							</table>
						</div>
				    </div>
				    @else
				    <div class="box-body">
					    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					    	<p style="text-align: center;font-size: 18px">No Data Found.</p>
					    </div>
				    </div>
				    @endif
				</div>
			</div>
		</div>
	</section>
@endsection

@push('scripts')
	<script type="text/javascript">
		$(document).ready(function()
		{
		  $('table.display2').dataTable();

		  var search = {{ isset($_GET['search']) ? 1 : '0' }};
		  
		  if (search == 0)
		  {
		  	$(".HistoryIndex").hide();	
		  }
		  else
		  {
		  	$(".HistoryIndex").show();
		  }
		  
		});
	</script>

	<script type="text/javascript">
		$(".confirm_box").click(function()
		{
			if (!confirm("Do you want to delete"))
			{
				return false;
			}
		});
	</script>

	<script type="text/javascript">
		$("#historyShow").click(function(){
		  $(".HistoryIndex").toggle();
		});
	</script>
@endpush