@extends('layouts.app')

@section('title', 'Create Personal Transaction')

@section('content')
	<section class="content">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
				Create Wallet Transaction
				</h3>
			</div>
			<div class="box-body">
				@if(Session::has('message'))
				<div style="padding: 0px" class="col-md-12">
					<div class="alert alert-success alert-dismissable text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{Session::get('message')}}
					</div>
				</div>
				@endif
				@if(Session::has('errors'))
				<div style="padding: 0px" class="col-md-12">
					<div class="alert alert-success alert-dismissable text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{Session::get('errors')}}
					</div>
				</div>
				@endif
				<form class="form-horizontal" method="POST" action="{{ route('personal_account_store') }}" enctype="multipart/form-data">
					{{ csrf_field() }}

					<div class="form-group{{ $errors->has('personal_account_type') ? ' has-error' : '' }}">
						<label for="name" class="col-md-4 control-label">
						Account Name *  
						</label>

						<div class="col-md-6">
							<select class="form-control select2 select2-hidden-accessible" style="width: 100%;padding: 15px" tabindex="-1" aria-hidden="true" name="personal_account_type">
								<option style="padding: 15px" value="">Select Transaction Type</option>
								@foreach($accounts as $account)
									<option style="padding: 15px" value="{{ $account->id }}">{{ $account->name }}</option>
								@endforeach
							</select>

							@if ($errors->has('personal_account_type'))
							<span class="help-block">
								<strong>{{ $errors->first('personal_account_type') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
						<label for="name" class="col-md-4 control-label">
						Transaction Date *
						</label>

						<div class="col-md-6">
							<input type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control datepicker" name="date" value="{{ date('Y-m-d') }}">

							@if ($errors->has('date'))
							<span class="help-block">
								<strong>{{ $errors->first('date') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('trans_type') ? ' has-error' : '' }}">
						<label for="name" class="col-md-4 control-label">
						Transaction Type *  
						</label>

						<div class="col-md-6">
							<select class="form-control select2 select2-hidden-accessible" style="width: 100%;padding: 15px" tabindex="-1" aria-hidden="true" name="trans_type">
									<option style="padding: 15px" value="1">Money In</option>
									<option style="padding: 15px" value="0">Money Out</option>
							</select>

							@if ($errors->has('trans_type'))
							<span class="help-block">
								<strong>{{ $errors->first('trans_type') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
						<label for="amount" class="col-md-4 control-label">
						Amount *
						</label>

						<div class="col-md-6">
							<input id="amount" type="text" class="form-control" name="amount" value="{{ old('amount') }}" />

							@if ($errors->has('amount'))
							<span class="help-block">
								<strong>{{ $errors->first('amount') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
						<label for="note" class="col-md-4 control-label">
						Note
						</label>

						<div class="col-md-6">
							<textarea id="note" type="text" class="form-control" name="note" value="{{ old('note') }}"></textarea>

							@if ($errors->has('note'))
							<span class="help-block">
								<strong>{{ $errors->first('note') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<hr>

					<div class="form-group">
						<div class="col-md-3 col-md-offset-4">
							<button type="submit" class="btn btn-primary">
								<i class="fa fa-plus-circle"></i>
							Create
							</button>
							<a href="{{ route('personal_account_index') }}" class="btn btn-danger">
								<i class="fa fa-trash"></i>
							Cancel
							</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
@endsection