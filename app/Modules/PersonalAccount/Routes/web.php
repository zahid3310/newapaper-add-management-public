<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'personalAccount', 'middleware' => 'auth'], function () {
	//Transaction Start
    Route::get('/index', 'PersonalAccountController@index')->name('personal_account_index');
    Route::get('/create', 'PersonalAccountController@create')->name('personal_account_create');
    Route::post('/store', 'PersonalAccountController@store')->name('personal_account_store');
    Route::get('/edit/{id}', 'PersonalAccountController@edit')->name('personal_account_edit');
    Route::post('/update/{id}', 'PersonalAccountController@update')->name('personal_account_update');
    Route::get('/delete/{id}', 'PersonalAccountController@destroy')->name('personal_account_delete');
	//Transaction End
});

Route::group(['prefix' => 'personalAccountCategory', 'middleware' => 'auth'], function () {
	//Account Type Start
    Route::get('/index', 'PersonalAccountController@categoryIndex')->name('personal_account_category_index');
    Route::get('/create', 'PersonalAccountController@categoryCreate')->name('personal_account_category_create');
    Route::post('/store', 'PersonalAccountController@categoryStore')->name('personal_account_category_store');
    Route::get('/edit/{id}', 'PersonalAccountController@categoryEdit')->name('personal_account_category_edit');
    Route::post('/update/{id}', 'PersonalAccountController@categoryUpdate')->name('personal_account_category_update');
    Route::get('/delete/{id}', 'PersonalAccountController@categoryDestroy')->name('personal_account_category_delete');
	//Account Type End
});


Route::group(['prefix' => 'personalAccountSubCategory', 'middleware' => 'auth'], function () {
	//Account Sub Type Start
    Route::get('/index', 'PersonalAccountController@subCategoryIndex')->name('personal_account_sub_category_index');
    Route::get('/create', 'PersonalAccountController@subCategoryCreate')->name('personal_account_sub_category_create');
    Route::post('/store', 'PersonalAccountController@subCategoryStore')->name('personal_account_sub_category_store');
    Route::get('/edit/{id}', 'PersonalAccountController@subCategoryEdit')->name('personal_account_sub_category_edit');
    Route::post('/update/{id}', 'PersonalAccountController@subCategoryUpdate')->name('personal_account_sub_category_update');
    Route::get('/delete/{id}', 'PersonalAccountController@subCategoryDestroy')->name('personal_account_sub_category_delete');
	//Account Sub Type End
});

Route::group(['prefix' => 'personalAccountReport', 'middleware' => 'auth'], function () {
    Route::get('/report', 'PersonalAccountController@report')->name('personal_transaction_report_index');
    Route::get('/report-download', 'PersonalAccountController@download')->name('personal_transaction_report_index_download');
    Route::get('/report-details', 'PersonalAccountController@reportDetails')->name('personal_transaction_report_report_details');
    Route::get('/report-details-download', 'PersonalAccountController@reportDetailsDownload')->name('personal_transaction_report_report_details_download');
});