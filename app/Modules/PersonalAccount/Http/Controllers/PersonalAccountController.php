<?php

namespace App\Modules\PersonalAccount\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Models\PersonalAccount;
use App\Models\PersonalAccountType;
use App\Models\PersonalAccountSubType;
use Response;
use DB;

class PersonalAccountController extends Controller
{
    public function index()
    {  
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('personal_account_index');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End
        $date       = date('Y-m-d');
        $type       = isset($_GET['type']) ? $_GET['type'] : '0';
        $sub_type   = isset($_GET['sub_type']) ? $_GET['sub_type'] : '0';
        $from       = isset($_GET['from']) ? date('Y-m-d', strtotime($_GET['from'])) : date('Y-m-d', strtotime($date. ' - 0 days'));
        $to         = isset($_GET['to']) ? date('Y-m-d', strtotime($_GET['to'])) : $date;
        $in_out     = isset($_GET['in_out']) ? $_GET['in_out'] : '0';

    	$transactions   = PersonalAccount::when($from != '1970-01-01' , function ($query) use ($from, $to)
                                        {
                                            return $query->whereBetween('date', [$from, $to]);
                                        })
                                        ->when($type != 0 , function ($query) use ($type)
                                        {
                                            return $query->where('personal_account_type', $type);
                                        })
                                        ->when($sub_type != 0 , function ($query) use ($sub_type)
                                        {
                                            return $query->where('personal_account_sub_type', $sub_type);
                                        })
                                        ->when($in_out == 1 , function ($query) use ($in_out)
                                        {
                                            return $query->where('trans_type', 1);
                                        })
                                        ->when($in_out == 2 , function ($query) use ($in_out)
                                        {
                                            return $query->where('trans_type', 0);
                                        })
                                        ->where('status', '=', 1)
                                        ->orderBy('date', 'DESC')
                                        ->get();

        $accounts       = PersonalAccountType::where('status', 1)->orderBy('name', 'asc')->get();
        $sub_accounts   = PersonalAccountSubType::where('status', 1)->orderBy('name', 'asc')->get();

        if (isset($_GET['download']))
        {
            $name               = 'Wallet';

            $mpdf = new \Mpdf\Mpdf();
            $mpdf->WriteHTML(view('personalAccount::Transaction.download-wallet')->with('accounts', $accounts)->with('sub_accounts', $sub_accounts)->with('transactions', $transactions)->with('type', $type)->with('from', $from)->with('to', $to));
            $mpdf->Output('Wallet', "I");
        }

    	return view('personalAccount::Transaction.index', compact('transactions', 'accounts', 'sub_accounts', 'type', 'from', 'to'));
    }

    public function create()
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('personal_account_create');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $accounts  = PersonalAccountType::where('status', 1)->orderBy('name', 'asc')->get();

    	return view('personalAccount::Transaction.create', compact('accounts'));
    }

    public function store(Request $request)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('personal_account_store');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $this->validate($request,[
         'date'                     => 'required',
         'personal_account_type'    => 'required',
         // 'trans_type'               => 'required',
         'amount'     	            => 'required',
        ]);

        try
        {   
            DB::beginTransaction();

            $personal_account                               = new PersonalAccount;
            $personal_account->date                         = $request->date;
            $personal_account->personal_account_type        = $request->personal_account_type;
            $personal_account->personal_account_sub_type    = $request->personal_account_sub_type_id;
            $personal_account->trans_type                   = $request->amount < 0 ? 0 : '1';
            $personal_account->amount                       = abs($request->amount);
            $personal_account->note                         = $request->note;
            $personal_account->status                       = 1;
            $personal_account->created_by                   = Auth::user()->id;

            if ($personal_account->save())
            {   
                DB::commit();
                return redirect()->route('personal_account_index')->with('message', 'Successfully added !!');
            }     
        }
        catch(\Exception $e)
        {	
            DB::rollback();
            $mesg = $e->getMessage();
            dd($mesg);
            return redirect()->route('personal_account_index')->with('message', 'Something went wrong try again !!');
        }     
    }

    public function edit($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('personal_account_edit');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $accounts           = PersonalAccountType::where('status', 1)->orderBy('name', 'asc')->get();
        $sub_accounts       = PersonalAccountSubType::where('status', 1)->orderBy('name', 'asc')->get();
        $transaction        = PersonalAccount::find($id);

    	return view('personalAccount::Transaction.edit', compact('transaction', 'accounts', 'sub_accounts'));
    }

    public function update(Request $request, $id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('personal_account_update');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $this->validate($request,[
         'date'  		            => 'required',
         'personal_account_type'    => 'required',
         // 'trans_type'               => 'required',
         'amount'     	            => 'required',
        ]);

        try
        {   
            DB::beginTransaction();

            $personal_account                               = PersonalAccount::find($id);
            $personal_account->date                         = $request->date;
            $personal_account->personal_account_type        = $request->personal_account_type;
            $personal_account->personal_account_sub_type    = $request->personal_account_sub_type_id;
            $personal_account->trans_type                   = $request->amount < 0 ? 0 : '1';
            $personal_account->amount                       = abs($request->amount);
            $personal_account->note                         = $request->note;
            $personal_account->status                       = 1;
            $personal_account->updated_by                   = Auth::user()->id;

            if ($personal_account->save())
            {   
                DB::commit();
                return redirect()->route('personal_account_index')->with('message', 'Successfully updated !!');
            }     
        }
        catch(\Exception $e)
        {	dd($e);
            DB::rollback();
            $mesg = $e->getMessage();
            return redirect()->route('personal_account_index')->with('message', 'Something went wrong try again !!');
        }     
    }

    public function destroy($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('personal_account_delete');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        try
        {   
            DB::beginTransaction();

            $personal_account               = PersonalAccount::find($id);
            $personal_account->status       = 0;
            $personal_account->updated_by   = Auth::user()->id;

            if ($personal_account->save())
            {   
                DB::commit();
                return redirect()->route('personal_account_index')->with('message', 'Successfully added !!');
            }     
        }
        catch(\Exception $e)
        {	
            DB::rollback();
            $mesg = $e->getMessage();
            return redirect()->route('personal_account_index')->with('message', 'Something went wrong try again !!');
        }     
    }

    public function categoryIndex()
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('personal_account_category_index');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End
        $category  = PersonalAccountType::where('status', '=', 1)->orderBy('name', 'asc')->get();

        return view('personalAccount::AccountType.index', compact('category'));
    }

    public function categoryCreate()
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('personal_account_category_create');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $category  = PersonalAccountType::where('status', '=', 1)->orderBy('name', 'asc')->get();

        return view('personalAccount::AccountType.create', compact('category'));
    }

    public function categoryStore(Request $request)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('personal_account_category_store');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $this->validate($request,[
         'name'     => 'required',
        ]);

        try
        {   
            DB::beginTransaction();

            $personal_account               = new PersonalAccountType;
            $personal_account->name         = $request->name;
            $personal_account->description  = $request->description;
            $personal_account->status       = 1;
            $personal_account->created_by   = Auth::user()->id;

            if ($personal_account->save())
            {   
                DB::commit();
                return redirect()->route('personal_account_category_index')->with('message', 'Successfully added !!');
            }     
        }
        catch(\Exception $e)
        {   dd($e);
            DB::rollback();
            $mesg = $e->getMessage();
            return redirect()->route('personal_account_category_index')->with('message', 'Something went wrong try again !!');
        }     
    }

    public function categoryEdit($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('personal_account_category_edit');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $account = PersonalAccountType::find($id);

        return view('personalAccount::AccountType.edit', compact('account'));
    }

    public function categoryUpdate(Request $request, $id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('personal_account_category_update');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $this->validate($request,[
         'name'     => 'required',
        ]);

        try
        {   
            DB::beginTransaction();

            $personal_account               = PersonalAccountType::find($id);
            $personal_account->name         = $request->name;
            $personal_account->description  = $request->description;
            $personal_account->status       = 1;
            $personal_account->updated_by   = Auth::user()->id;

            if ($personal_account->save())
            {   
                DB::commit();
                return redirect()->route('personal_account_category_index')->with('message', 'Successfully updated !!');
            }     
        }
        catch(\Exception $e)
        {   dd($e);
            DB::rollback();
            $mesg = $e->getMessage();
            return redirect()->route('personal_account_category_index')->with('message', 'Something went wrong try again !!');
        }    
    }

    public function categoryDestroy($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('personal_account_category_delete');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        try
        {   
            DB::beginTransaction();

            $personal_account               = PersonalAccountType::find($id);
            $personal_account->status       = 0;
            $personal_account->updated_by   = Auth::user()->id;

            if ($personal_account->save())
            {   
                DB::commit();
                return redirect()->route('personal_account_category_index')->with('message', 'Successfully deleted !!');
            }     
        }
        catch(\Exception $e)
        {   
            DB::rollback();
            $mesg = $e->getMessage();
            return redirect()->route('personal_account_category_index')->with('message', 'Something went wrong try again !!');
        }     
    }
    
    public function subCategoryIndex()
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('personal_account_category_index');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End
        $sub_category   = PersonalAccountSubType::where('status', '=', 1)->orderBy('name', 'asc')->get();
        $category       = PersonalAccountType::where('status', '=', 1)->orderBy('name', 'asc')->get();

        return view('personalAccount::AccountSubType.index', compact('sub_category', 'category'));
    }

    public function subCategoryCreate()
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('personal_account_category_create');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $sub_category  = PersonalAccountSubType::where('status', '=', 1)->orderBy('name', 'asc')->get();

        return view('personalAccount::AccountSubType.create', compact('sub_category'));
    }

    public function subCategoryStore(Request $request)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('personal_account_category_store');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $this->validate($request,[
         'name'     => 'required',
        ]);

        try
        {   
            DB::beginTransaction();

            $personal_account                               = new PersonalAccountSubType;
            $personal_account->name                         = $request->name;
            $personal_account->personal_account_type_id     = $request->personal_account_sub_type_id;
            $personal_account->status                       = 1;
            $personal_account->created_by                   = Auth::user()->id;

            if ($personal_account->save())
            {   
                DB::commit();
                return redirect()->route('personal_account_sub_category_index')->with('message', 'Successfully added !!');
            }     
        }
        catch(\Exception $e)
        {   dd($e);
            DB::rollback();
            $mesg = $e->getMessage();
            return redirect()->route('personal_account_sub_category_index')->with('message', 'Something went wrong try again !!');
        }     
    }

    public function subCategoryEdit($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('personal_account_category_edit');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $account        = PersonalAccountSubType::find($id);
        $category       = PersonalAccountType::where('status', '=', 1)->orderBy('name', 'asc')->get();
        
        return view('personalAccount::AccountSubType.edit', compact('account', 'category'));
    }

    public function subCategoryUpdate(Request $request, $id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('personal_account_category_update');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $this->validate($request,[
         'name'     => 'required',
        ]);

        try
        {   
            DB::beginTransaction();

            $personal_account                               = PersonalAccountSubType::find($id);
            $personal_account->name                         = $request->name;
            $personal_account->personal_account_type_id     = $request->personal_account_sub_type_id;
            $personal_account->status                       = 1;
            $personal_account->updated_by                   = Auth::user()->id;

            if ($personal_account->save())
            {   
                DB::commit();
                return redirect()->route('personal_account_sub_category_index')->with('message', 'Successfully updated !!');
            }     
        }
        catch(\Exception $e)
        {   dd($e);
            DB::rollback();
            $mesg = $e->getMessage();
            return redirect()->route('personal_account_sub_category_index')->with('message', 'Something went wrong try again !!');
        }    
    }

    public function subCategoryDestroy($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('personal_account_category_delete');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        try
        {   
            DB::beginTransaction();

            $personal_account               = PersonalAccountSubType::find($id);
            $personal_account->status       = 0;
            $personal_account->updated_by   = Auth::user()->id;

            if ($personal_account->save())
            {   
                DB::commit();
                return redirect()->route('personal_account_sub_category_index')->with('message', 'Successfully deleted !!');
            }     
        }
        catch(\Exception $e)
        {   
            DB::rollback();
            $mesg = $e->getMessage();
            return redirect()->route('personal_account_category_index')->with('message', 'Something went wrong try again !!');
        }     
    }

    public function report()
    {
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('transaction_report_index');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $date               = date('Y-m-d');
        $account_type       = isset($_GET['account_type']) ? $_GET['account_type'] : 0;
        $from_date          = isset($_GET['from_date']) ? $_GET['from_date'] : date('Y-m-d', strtotime($date. ' - 30 days'));
        $to_date            = isset($_GET['to_date']) ? $_GET['to_date'] : $date;

        $accounts           = PersonalAccount::when($from_date != 0 && $to_date != 0, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('personal_account.date', [$from_date, $to_date]);
                                                })
                                                ->when($account_type != 0, function ($query) use ($account_type) {
                                                    return $query->where('personal_account.personal_account_type', $account_type);
                                                })
                                                ->where('personal_account.status', '=', 1)
                                                ->selectRaw('personal_account.*')
                                                ->get();

        $search_account_type    = PersonalAccountType::where('status', 1)->get();
        $account_type_name      = PersonalAccountType::find($account_type);

        foreach ($search_account_type as $key => $search_account_type_value)
        {
            $expense_sum        = $accounts->where('trans_type', 0)->where('personal_account_type', $search_account_type_value->id)->sum('amount');
            $income_sum        = $accounts->where('trans_type', 1)->where('personal_account_type', $search_account_type_value->id)->sum('amount');

            $account_wise_expense[]  = $expense_sum;
            $account_wise_income[]   = $income_sum;
        }

        // dd($account_wise_expense, $account_wise_income);

        //Download With Search Start
        if (isset($_GET['download']) && ($_GET['download'] == 1))
        {
            $date       = date('d-m-Y');
            $mpdf       = new \Mpdf\Mpdf();
            $mpdf->WriteHTML(view('personalAccount::Report.index_download', compact('accounts', 'search_account_type', 'account_type_name', 'account_type', 'account_wise_expense', 'account_wise_income', 'from_date', 'to_date')));
            $mpdf->Output('Customer-Report-'.$date, "I");
        }
        //Download With Search End

        return view('personalAccount::Report.index', compact('accounts', 'search_account_type', 'account_type_name', 'account_type', 'account_wise_expense', 'account_wise_income', 'from_date', 'to_date'));
    }

    public function download()
    {
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('personal_transaction_report_index_download');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $date               = date('Y-m-d');
        $account_type       = isset($_GET['account_type']) ? $_GET['account_type'] : 0;
        $from_date          = isset($_GET['from_date']) ? $_GET['from_date'] : date('Y-m-d', strtotime($date. ' - 30 days'));
        $to_date            = isset($_GET['to_date']) ? $_GET['to_date'] : $date;

        $accounts           = PersonalAccount::when($from_date != 0 && $to_date != 0, function ($query) use ($from_date, $to_date) {
                                                    return $query->whereBetween('personal_account.date', [$from_date, $to_date]);
                                                })
                                                ->when($account_type != 0, function ($query) use ($account_type) {
                                                    return $query->where('personal_account.personal_account_type', $account_type);
                                                })
                                                ->where('personal_account.status', '=', 1)
                                                ->selectRaw('personal_account.*')
                                                ->get();

        $search_account_type    = PersonalAccountType::where('status', 1)->get();
        $account_type_name      = PersonalAccountType::find($account_type);

        foreach ($search_account_type as $key => $search_account_type_value)
        {
            $expense_sum        = $accounts->where('trans_type', 0)->where('personal_account_type', $search_account_type_value->id)->sum('amount');
            $income_sum        = $accounts->where('trans_type', 1)->where('personal_account_type', $search_account_type_value->id)->sum('amount');

            $account_wise_expense[]  = $expense_sum;
            $account_wise_income[]   = $income_sum;
        }

   
        $date       = date('d-m-Y');
        $mpdf       = new \Mpdf\Mpdf();
        $mpdf->WriteHTML(view('personalAccount::Report.index_download', compact('accounts', 'search_account_type', 'account_type_name', 'account_type', 'account_wise_expense', 'account_wise_income', 'from_date', 'to_date')));
        $mpdf->Output('Personal-Account-Report-'.$date, "I");   
    }

    public function reportDetails()
    {
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('personal_transaction_report_report_details');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $date               = date('Y-m-d');
        $account_type       = isset($_GET['account_type']) ? $_GET['account_type'] : 0;

        $accounts           = PersonalAccount::where('personal_account.personal_account_type', $account_type)
                                                ->where('personal_account.status', '=', 1)
                                                ->selectRaw('personal_account.*')
                                                ->get();

        $account_type_name  = PersonalAccountType::find($account_type);

        return view('personalAccount::Report.details', compact('accounts', 'account_type_name', 'account_type'));
    }

    public function reportDetailsDownload()
    {
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('personal_transaction_report_report_details_download');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $date               = date('Y-m-d');
        $account_type       = isset($_GET['account_type']) ? $_GET['account_type'] : 0;

        $accounts           = PersonalAccount::where('personal_account.personal_account_type', $account_type)
                                                ->where('personal_account.status', '=', 1)
                                                ->selectRaw('personal_account.*')
                                                ->get();

        $account_type_name  = PersonalAccountType::find($account_type);

        $date       = date('d-m-Y');
        $mpdf       = new \Mpdf\Mpdf();
        $mpdf->WriteHTML(view('personalAccount::Report.details_download', compact('accounts', 'account_type_name', 'account_type')));
        $mpdf->Output('Personal-Account-Report-'.$date, "I");
    }
}
