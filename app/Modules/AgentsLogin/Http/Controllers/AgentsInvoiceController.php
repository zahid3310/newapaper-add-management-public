<?php

namespace App\Modules\AgentsLogin\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Models\Areas;
use App\Models\Districts;
use App\Models\Customers;
use App\Models\Agents;
use App\Models\Items;
use App\Models\Invoices;
use App\Models\InvoiceDetails;
use App\Models\Transactions;
use App\Models\Payments;
use Response;
use DB;

class AgentsInvoiceController extends Controller
{
    public function index()
    {  
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('agents_invoices_index');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $agent_id = Auth::user()->agent_id;
    	$invoices = Invoices::where('status', '=', 1)->where('agent_id', $agent_id)->whereNotNull('agent_id')->get();

    	return view('agentsLogin::Invoices.index', compact('invoices'));
    }

    public function moneyRecept()
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('agents_money_recept_index');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $agent_id       = Auth::user()->agent_id;
        $customer_id    = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $customers      = Invoices::where('status', '=', 1)
                                    ->where('agent_id', $agent_id)
                                    ->whereNotNull('agent_id')
                                    ->groupBy('customer_id')
                                    ->selectRaw('customer_id as customer_id')
                                    ->get();

        if (isset($_GET['customer_id']) && $customers->count() == 0)
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }

        $find_customer  = Customers::where('id', $customer_id)->where('status', '=', 1)->first();
        $invoices       = Invoices::where('status', '=', 1)->where('customer_id', $customer_id)->where('agent_id', $agent_id)->whereNotNull('agent_id')->get();

        $payments       = Payments::join('transactions', 'transactions.payment_id', 'payments.id')
                                    ->join('invoices', 'invoices.id', 'transactions.invoice_id')
                                    ->where('payments.status', '=', 1)
                                    ->where('invoices.status', '=', 1)
                                    ->whereNotNull('invoices.agent_id')
                                    ->where('payments.customer_id', $customer_id)
                                    ->where('invoices.agent_id', $agent_id)
                                    ->selectRaw('GROUP_CONCAT(payments.id) as id,
                                                 GROUP_CONCAT(DISTINCT payments.date) as date,
                                                 GROUP_CONCAT(invoices.invoice_number) as invoice_number,
                                                 GROUP_CONCAT(DISTINCT transactions.payment_through) as payment_through,
                                                 GROUP_CONCAT(DISTINCT payments.total_amount) as total_amount,
                                                 GROUP_CONCAT(DISTINCT payments.total_other_expense) as total_other_expense')
                                    ->groupBy('payments.id')
                                    ->get();

                                    // dd($payments->toArray());

        $transactions   = Transactions::join('invoices', 'invoices.id', 'transactions.agent_id')
                                        ->where('transactions.customer_id', $customer_id)
                                        ->where('invoices.agent_id', $agent_id)
                                        ->whereNotNull('invoices.agent_id')
                                        ->where('transactions.type', 'invoice')
                                        ->where('transactions.status', 1)
                                        ->where('invoices.status', 1)
                                        ->orderBy('transactions.created_at', 'DESC')
                                        ->get();

                                        // dd($payments->toArray());

        $recevable      = $invoices->sum('total_amount'); 
        $other_expense  = $invoices->sum('other_expense');
        $dues           = $invoices->sum('due_amount');
        $paid           = $transactions->sum('trans_paid');
        $invoice_count  = $invoices->count();    

        return view('agentsLogin::Invoices.money-recept', compact('customers', 'find_customer', 'transactions', 'recevable', 'dues', 'paid', 'invoice_count', 'invoices', 'other_expense', 'id', 'payments'));
    }

    public function show($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('agents_invoices_show');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $agent_id           = Auth::user()->agent_id;
        $invoice            = Invoices::find($id);

        if ($invoice->agent_id != $agent_id)
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }

        $invoice_details    = InvoiceDetails::where('invoice_id', $id)
                                            ->selectRaw('invoices_details.*')
                                            ->get();
                                            
        $check_nirdharito   = $invoice_details->where('invoice_type', 2);

        $total_amount_sum   = $invoice_details->sum('amount') + $invoice->adjustment;

        return view('agentsLogin::Invoices.show', compact('invoice', 'invoice_details', 'total_amount_sum', 'check_nirdharito'));
    }

    public function download($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('agents_invoices_download');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $agent_id           = Auth::user()->agent_id;
        $invoice            = Invoices::find($id);

        if ($invoice->agent_id != $agent_id)
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }

        $invoice_details    = InvoiceDetails::where('invoice_id', $id)
                                            ->selectRaw('invoices_details.*')
                                            ->get();
                                            
        $check_nirdharito   = $invoice_details->where('invoice_type', 2);
        
        $total_amount_sum   = $invoice_details->sum('amount') + $invoice->adjustment;

        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML(view('agentsLogin::Invoices.download-invoice')->with('invoice', $invoice)->with('invoice_details', $invoice_details)->with('total_amount_sum', $total_amount_sum)->with('check_nirdharito', $check_nirdharito));
        $mpdf->Output('INV - '.$invoice->invoice_number, "I");
    }
    
    public function downloadWithHeader($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('agents_invoices_download');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $agent_id           = Auth::user()->agent_id;
        $invoice            = Invoices::find($id);

        if ($invoice->agent_id != $agent_id)
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }

        $invoice_details    = InvoiceDetails::where('invoice_id', $id)
                                            ->selectRaw('invoices_details.*')
                                            ->get();
                                            
        $check_nirdharito   = $invoice_details->where('invoice_type', 2);
        
        $total_amount_sum   = $invoice_details->sum('amount') + $invoice->adjustment;

        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML(view('agentsLogin::Invoices.download-invoice-with-header')->with('invoice', $invoice)->with('invoice_details', $invoice_details)->with('total_amount_sum', $total_amount_sum)->with('check_nirdharito', $check_nirdharito));
        $mpdf->Output('INV - '.$invoice->invoice_number, "I");
    }
    
    public function moneyReceiptDownload($id)
    {   
        $invoice    = Transactions::where('payment_id', $id)->first();
        $invoice_id = Invoices::find($invoice->invoice_id);
        $agent_id   = Auth::user()->agent_id;

        if ($invoice_id['agent_id'] != $agent_id)
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }

        $payments = Payments::join('transactions', 'transactions.payment_id', 'payments.id')
                            ->join('invoices', 'invoices.id', 'transactions.invoice_id')
                            ->where('payments.id', $id)
                            ->selectRaw('GROUP_CONCAT(DISTINCT payments.id) as id,
                                         GROUP_CONCAT(DISTINCT payments.customer_id) as customer_id,
                                         GROUP_CONCAT(DISTINCT transactions.payment_through) as payment_through,
                                         GROUP_CONCAT(DISTINCT invoices.invoice_number) as invoice_number,
                                         GROUP_CONCAT(DISTINCT payments.date) as date,
                                         GROUP_CONCAT(DISTINCT payments.total_amount) as total_amount,
                                         GROUP_CONCAT(DISTINCT payments.amount_bangla) as amount_bangla,
                                         GROUP_CONCAT(DISTINCT payments.date) as payment_date')
                            ->groupBy('payments.id')
                            ->first();

        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML(view('agentsLogin::Invoices.download-money-receipt', compact('payments')));
        $mpdf->Output('Receipt', "I");
        
        //return view('paymentReceive::show');
    }
}
