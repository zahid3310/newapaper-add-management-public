<?php

namespace App\Modules\AgentsLogin\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Models\Areas;
use App\Models\Districts;
use App\Models\Customers;
use App\Models\Agents;
use App\Models\Items;
use App\Models\Invoices;
use App\Models\InvoiceDetails;
use App\Models\Transactions;
use Response;
use DB;

class AgentsInvoiceComissionController extends Controller
{
    public function index()
    {	
    	//Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('agents_comission_index');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

    	$agent_id       = Auth::user()->agent_id;
    	$agent 		    = Agents::where('id', $agent_id)->first();
				    					
		$invoices       = Invoices::join('transactions', 'transactions.invoice_id', 'invoices.id')
    	                                ->where('invoices.agent_id', $agent_id)
        								->where('invoices.status', 1)
        								->where('transactions.status', 1)
        								->whereNotNull('invoices.agent_id')
        								->where('invoices.commission_amount', '>', 0)
        								->where('transactions.type', 'invoice')
        								->where('transactions.trans_paid', '>', 0)
                                        ->selectRaw('invoices.*')
                                        ->orderBy('created_at', 'ASC')
        								->get();
        								

        $transactions   = Transactions::leftjoin('invoices', 'invoices.id', 'transactions.invoice_id')
        								->where('transactions.agent_id', $agent_id)
                                        ->where('transactions.type', 'sales_comission')
                                        ->where('transactions.status', 1)
                                        ->where('invoices.status', 1)
                                        ->selectRaw('transactions.*, invoices.invoice_number as invoice_number')
                                        ->orderBy('created_at', 'DESC')
                                        ->get();
				    					
        foreach ($invoices as $key => $invoice)
        {   
            $invoice_details            = InvoiceDetails::join('transactions', 'transactions.invoice_id', 'invoices_details.invoice_id')
                                                        ->where('invoices_details.invoice_id', $invoice->id)
                                                        ->where('transactions.type', 'invoice')
                                                        ->where('transactions.status', 1)
                                                        ->where('transactions.trans_paid', '>', 0)
                                                        ->selectRaw('(CASE WHEN invoices_details.commission_type = 1 THEN invoices_details.amount ELSE ((transactions.trans_paid*invoices_details.commission_amount)/100) END) AS com_amount')
                                                        ->get();
                                                        
            $invoice_val                = $invoices->where('id', $invoice->id);
            $comission                  = $invoice_details->sum('com_amount');
            // $other_expense_deduction = ($comission*$invoice->other_expense)/$invoice->total_amount;
            $other_expense_deduction    = 0;
            $payable                    = $comission - $other_expense_deduction; 
            $paid                       = $transactions->where('invoice_id', $invoice->id)->where('type', 'sales_comission')->sum('trans_paid');

            $invoice_wise_payable[]     = round($payable, 2);
            $invoice_wise_paid[]        = round($paid, 2);
            $invoice_wise_dues[]        = round($payable - $paid, 2);
        }
        
    	return view('agentsLogin::Comissions.index', compact('invoices', 'invoice_wise_payable', 'invoice_wise_paid', 'invoice_wise_dues', 'agent_id', 'agent'));
    }

    public function details()
    {	
    	//Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('agents_comission_details');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        if (isset($_GET['agent_id']) && ($_GET['agent_id'] != Auth::user()->agent_id))
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }


        $agent_id       = $_GET['agent_id'];
    	$invoice_id 	= $_GET['invoice_id'];

    	$invoices       = Invoices::where('invoices.agent_id', $agent_id)
                                        ->where('invoices.id', $invoice_id)
        								->where('invoices.status', 1)
        								->whereNotNull('invoices.agent_id')
        								->selectRaw('invoices.*')
        								->get();

        $transactions   = Transactions::leftjoin('invoices', 'invoices.id', 'transactions.invoice_id')
        								->where('transactions.agent_id', $agent_id)
                                        ->where('invoices.id', $invoice_id)
                                        ->where('transactions.type', 'sales_comission')
                                        ->where('transactions.status', 1)
                                        ->where('invoices.status', 1)
                                        ->selectRaw('transactions.*, invoices.invoice_number as invoice_number')
                                        ->get();

        foreach ($invoices as $key => $invoice)
        {
            $payable                = $invoices->where('id', $invoice->id)->sum('commission_amount'); 
            $paid                   = $transactions->where('invoice_id', $invoice->id)->sum('trans_paid');

            $invoice_wise_payable[]   = $payable;
            $invoice_wise_paid[]      = $paid;
            $invoice_wise_dues[]      = $payable - $paid;
        }

    	return view('agentsLogin::Comissions.details', compact('invoices', 'transactions', 'invoice_wise_payable', 'invoice_wise_paid', 'invoice_wise_dues', 'agent_id'));
    }
}
