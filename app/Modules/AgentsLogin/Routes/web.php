<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'agentsLogin', 'middleware' => 'auth'], function () {
    Route::get('/index', 'AgentsInvoiceController@index')->name('agents_invoices_index');
    Route::get('/agent-money-recept', 'AgentsInvoiceController@moneyRecept')->name('agents_money_recept_index');
    Route::get('/download-money-recept/{id}', 'AgentsInvoiceController@moneyReceiptDownload')->name('agents_money_recept_download');
    Route::get('/agent-invoice-show/{id}', 'AgentsInvoiceController@show')->name('agents_invoices_show');
    Route::get('/download/{id}', 'AgentsInvoiceController@download')->name('agents_invoices_download');
    Route::get('/download-with-header/{id}', 'AgentsInvoiceController@downloadWithHeader')->name('agents_invoices_download_with_header');

    Route::get('/agents-sales-comission', 'AgentsInvoiceComissionController@index')->name('agents_comission_index');
    Route::get('/agents-sales-comission-details', 'AgentsInvoiceComissionController@details')->name('agents_comission_details');
});
