@extends('layouts.agent_app')

@section('content')

<section class="content" style="padding-top: 8px">
	<div class="box" style="margin-bottom: 0px">
		<div class="box-header with-border">
			<h3 class="box-title">Transaction History</h3>
			<div class="pull-right">
			</div>
		</div>

		<div class="row">
			@if(Session::has('message'))
			<div class="col-md-12">
				<div class="alert alert-success alert-dismissable text-center">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					{{Session::get('message')}}
				</div>
			</div>
			@endif
			@if(Session::has('errors'))
			<div class="col-md-12">
				<div class="alert alert-success alert-dismissable text-center">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					{{Session::get('errors')}}
				</div>
			</div>
			@endif
		</div>
				
		<div class="box-body">
			<div class="row">
				<div class="col-md-12 customAlign">
					<div class="table-responsive">
						<table id="dataTable" class="table table-bordered text-center table-tr-style">
							<thead>
								<tr style="background-color: #F9E79F">
									<th style="text-align: left">SL</th>
									<th style="text-align: left">Payment Date</th>
									<th style="text-align: left">Invoice Number</th>
									<th style="text-align: left">Paid Through</th>
									<th style="text-align: right">Trans. Paid</th>
								</tr>
							</thead>
							<tbody>
								<?php $total_paid = 0; ?>
								@foreach($transactions as $key => $transaction)
									
									<?php $total_paid = $total_paid + $transaction->trans_paid; ?>
									
									<tr>
										<td style="text-align: left">{{ $key + 1 }}</td>
										<td style="text-align: left">{{ date('d-m-Y', strtotime($transaction->date)) }}</td>
										<td style="text-align: left">{{ $transaction->invoice_number }}</td>
										<td style="text-align: left">
											@if($transaction->payment_methode == 'Cash')
												Cash
											@else
												{{ $transaction->payment_through }}
											@endif
										</td>
										<td style="text-align: right">{{ $transaction->trans_paid }}</td>
									</tr>
								@endforeach
								
							</tbody>
							<tr class="total" style="background-color: #F9E79F">
								<th style="text-align: right"></th>
								<th style="text-align: right"></th>
								<th style="text-align: right"></th>
								<th style="text-align: right">Total Transaction</th>
								<th style="text-align: right">{{ $total_paid }}</th>
							</tr>
						</table>
					</div>
				</div>
			
			</div>
		</div>
	</div>
</section>

@endsection