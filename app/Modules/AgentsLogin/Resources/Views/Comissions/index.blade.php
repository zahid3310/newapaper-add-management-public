@extends('layouts.agent_app')

@section('title', 'Sales Comission Details')

@section('content')
	<style type="text/css">
		.customAlign table thead tr th{
			vertical-align: middle;
		}
		.table-tr-style tr:nth-child(even) {
			background-color: #dddddd;
		}
		.table-tr-style tr:nth-child(odd) {
			background-color: #F2F3F4;
		}
		.total th{
			font-size: 20px
		}
		.select2-container .select2-selection--single{
			height: auto
		}
		.select2-container--default .select2-selection--single .select2-selection__rendered{
			line-height: 20px
		}
		.select2-container .select2-selection--single .select2-selection__rendered{
			margin-top: 0px
		}
		table.dataTable thead > tr > th {
		    padding-right: 10px !important; 
		}
	</style>

	<section class="content-header">
		<h1>
			Sales Comission Details 
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i>
			 	Home
			 </a></li>
			<li><a href="#">
				Comission
			</a></li>
			<li class="active">
				Sales Comission Details
			</li>
		</ol>
	</section>

	<section class="content">
		<div class="box">
			
			<div class="box-body">
				<div class="col-md-12 customAlign table-responsive">

					<table class="table table-bordered table-striped dataTable">
						<thead>
							<tr style="background-color: #F9E79F">
								<th>SL</th>
								<th>Invoice Number</th>
								<th>Invoice Date</th>
								<th>Publication Date</th>
								<th style="text-align: right">Total Receivable</th>
								<th style="text-align: right">Total Received</th>
								<th style="text-align: right">Total Dues</th>
							</tr>
						</thead>
						<tbody>
							@php
								$total_payable   = 0;
								$total_paid      = 0;
								$total_dues      = 0;
								$i               = 1;
							@endphp
							@if(!empty($invoices) && ($invoices->count() > 0))
								@foreach($invoices as $key => $invoice)
									@if($invoice_wise_payable[$key] != 0)
										@php
											$total_payable   = $total_payable + $invoice_wise_payable[$key];
											$total_paid      = $total_paid + $invoice_wise_paid[$key];
											$total_dues      = $total_dues + $invoice_wise_dues[$key];
										@endphp
										<tr>
											<td>{{ $key + 1 }}</td>
											<td>
												<a href="{{ url('agentsLogin/agents-sales-comission-details?agent_id='.$agent_id.'&invoice_id='.$invoice->id) }}">{{ $invoice->invoice_number }}</a>
											</td>
											<td>{{ date('d-m-Y', strtotime($invoice->invoice_date)) }}</td>
											<td>{{ date('d-m-Y', strtotime($invoice->due_date)) }}</td>
											<td style="text-align: right">{{ $invoice_wise_payable[$key] }}</td>
											<td style="text-align: right">{{ $invoice_wise_paid[$key] }}</td>
											<td style="text-align: right">{{ $invoice_wise_dues[$key] }}</td>
										</tr>
									@endif
								@endforeach
							@endif
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td style="text-align: right"><b>Total</b></td>
								<td style="text-align: right"><b>{{ $total_payable }}</b></td>
								<td style="text-align: right"><b>{{ $total_paid }}</b></td>
								<td style="text-align: right"><b>{{ $total_dues }}</b></td>
							</tr>
						</tbody>
					</table>

				</div>
		    </div>
		    
		</div>
	</section>
@endsection

@push('scripts')
	<script type="text/javascript">
		$(".confirm_box").click(function()
		{
			if (!confirm("Do you want to delete"))
			{
				return false;
			}
		});
	</script>
@endpush