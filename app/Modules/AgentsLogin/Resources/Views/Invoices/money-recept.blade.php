@extends('layouts.agent_app')

@section('title', 'Money Receipt')

@section('content')

	<style type="text/css">
		.customAlign table thead tr th{
			vertical-align: middle;
		}
		.table-tr-style tr:nth-child(even) {
			background-color: #dddddd;
		}
		.table-tr-style tr:nth-child(odd) {
			background-color: #F2F3F4;
		}
		.total th{
			font-size: 20px
		}
		.select2-container .select2-selection--single{
			height: auto
		}
		.select2-container--default .select2-selection--single .select2-selection__rendered{
			line-height: 20px
		}
		.select2-container .select2-selection--single .select2-selection__rendered{
			margin-top: 0px
		}
	</style>

	<section class="content" style="min-height: auto">
		<div class="box" style="margin-bottom: 0px">
			<div class="box-body">
				@if(Session::has('message'))
				<div style="padding: 0px" class="col-md-12">
					<div class="alert alert-success alert-dismissable text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{Session::get('message')}}
					</div>
				</div>
				<hr>
				<hr>
				@endif
				@if(Session::has('errors'))
				<div style="padding: 0px" class="col-md-12">
					<div class="alert alert-danger alert-dismissable text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<b>Warning !</b> {{Session::get('errors')}}
					</div>
				</div>
				<hr>
				<hr>
				@endif
				<!-- SELECT2 EXAMPLE -->
				<!-- <div class="box box-default"> -->
				<div class="box-body">
					<div class="callout callout-info" style="margin-bottom: 0px">
						<div class="row">

							<form class="form-horizontal" method="GET" action="{{ route('agents_money_recept_index') }}" enctype="multipart/form-data">
								<!-- {{ csrf_field() }} -->

								<label for="name" class="col-md-3 col-xs-12 control-label text-right" style="padding-top: 6px">Customer Transaction :</label>
								<div class="col-md-7">
									<select id="customer_id" name="customer_id" class="form-control select2" style="width: 100%">
										<option value="">--Select Customer--</option>
										@if(!empty($customers))
											@foreach($customers as $key => $customer)
											<option @if(isset($find_customer)) {{ $find_customer['id'] == $customer['customer_id'] ? 'selected' : '' }} @endif value="{{ $customer->customer_id }}">{{ $customer->customer->name }}</option>
											@endforeach
										@endif
									</select>
								</div>

								<div class="col-md-2">
									<button type="submit" class="btn btn-success btn-block"><i class="fa fa-search"></i> Search 
									</button>
								</div>
							</form>

						</div>
					</div>	
				</div>
			</div>
		</div>
	</section>

	@if(($invoices != null) && isset($find_customer))
		<section class="content" style="padding-top: 8px">
			<div class="box" style="margin-bottom: 0px">
				<div style="padding-bottom: 0px" class="box-header">
					<h3 class="box-title" style="width:100%">Transaction History
					</h3>
				</div>

				<div class="box-body">
					<div class="row">
						<!-- <div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-bordered text-center " style="background-color: #ecebeb;">
									<thead>
										<tr>
											<th style="text-align: center">Customer Name</th>
											<th style="text-align: center">Total Receivable</th>
											<th style="text-align: center">Total Paid</th>
											<th style="text-align: center">Other Expense</th>
											<th style="text-align: center">Total Dues</th>
											<th style="text-align: center">Total Invoice</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td style="text-align: center">{{ $find_customer['name'] }}</td>
											<td style="text-align: center">{{ number_format($recevable,2,'.',',') }}</td>
											<td style="text-align: center">{{ number_format($paid,2,'.',',') }}</td>
											<td style="text-align: center">{{ number_format($other_expense,2,'.',',') }}</td>
											<td style="text-align: center">{{ number_format($recevable - $paid - $other_expense,2,'.',',') }}</td>
											<td style="text-align: center">{{ $invoice_count }}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div> -->

						<div class="col-md-12 customAlign">
							<hr>
							<div class="table-responsive">
								<table id="dataTable" class="table table-bordered text-center table-tr-style">
									<thead>
										<tr style="background-color: #F9E79F">
											<th style="text-align: left">SL</th>
											<th style="text-align: left">Payment Date</th>
											<th style="text-align: left">Invoice Number</th>
											<th style="text-align: left">Paid Through</th>
											<th style="text-align: right">Trans. Paid</th>
											<th style="text-align: right">Other Expense</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										@php
										 	$total_paid 	= 0;
										 	$total_expense 	= 0;
										@endphp
										@foreach($payments as $key => $payment)
											@php
												$total_paid 	= $total_paid + $payment->total_amount;
												$total_expense 	= $total_expense + $payment->total_other_expense;
											@endphp
											<tr>
												<td style="text-align: left">{{ $key + 1 }}</td>
												<td style="text-align: left">{{ date('d-m-Y', strtotime($payment->date)) }}</td>
												<td style="text-align: left">{{ $payment->invoice_number }}</td>
												<td style="text-align: left">{{ $payment->payment_through }}</td>
												<td style="text-align: right">{{ number_format($payment->total_amount,2,'.',',') }}</td>
												<td style="text-align: right">{{ number_format($payment->total_other_expense,2,'.',',') }}</td>
												<td>
													<a href="{{ route('agents_money_recept_download', $payment->id) }}" class="btn btn-info btn-xs" target="_blank"><i title="Show" class="fa fa-eye" aria-hidden="true"></i></a>
												</td>
											</tr>
										@endforeach
										
									</tbody>
								</table>
							</div>
						</div>
					
					</div>
				</div>
			</div>
		</section>
	@endif
	
@endsection

@push('scripts')
	<script type="text/javascript">
		$(".confirm_box").click(function(){
			if (!confirm("Do you want to delete")){
				return false;
			}
		});
	</script>
@endpush