@extends('layouts.agent_app')

@section('title', 'Invoice List')

@section('content')
	<style type="text/css">
		.customAlign table thead tr th{
			vertical-align: middle;
		}
		.table-tr-style tr:nth-child(even) {
			background-color: #dddddd;
		}
		.table-tr-style tr:nth-child(odd) {
			background-color: #F2F3F4;
		}
		.total th{
			font-size: 20px
		}
		.select2-container .select2-selection--single{
			height: auto
		}
		.select2-container--default .select2-selection--single .select2-selection__rendered{
			line-height: 20px
		}
		.select2-container .select2-selection--single .select2-selection__rendered{
			margin-top: 0px
		}
	</style>

	<section class="content-header">
		<h1>
			Manage Invoice List
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i>
			 	Home
			 </a></li>
			<li><a href="#">
				Invoices
			</a></li>
			<li class="active">
				Invoice List
			</li>
		</ol>
	</section>

	<section class="content">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
						Invoice List
				</h3>
			</div>

			@if(count($invoices) >0)
			<div class="box-body">
				<div class="row">
					@if(Session::has('message'))
					<div class="col-md-12">
						<div class="alert alert-success alert-dismissable text-center">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							{{Session::get('message')}}
						</div>
					</div>
					@endif
					@if(Session::has('errors'))
					<div class="col-md-12">
						<div class="alert alert-success alert-dismissable text-center">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							{{Session::get('errors')}}
						</div>
					</div>
					@endif
				</div>

				<div class="col-md-12 customAlign table-responsive">

					<table id="dataTable" class="table table-bordered table-striped dataTable">
						<thead>
							<tr style="background-color: #F9E79F">
								<th>SL</th>
								<th>Invoice Number</th>
								<th>Customer Name</th>
								<th>Invoice Date</th>
								<th>Publication Date</th>
								<th>Total Amount</th>
								<th>Paid</th>
								<th>Due</th>
								<th>Agent Name</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						@if(!empty($invoices) && (count($invoices)>0) )
						@foreach($invoices as $key => $invoice)
							<tr>
								<td>{{ $key + 1 }}</td>
								<td>{{ $invoice->invoice_number }}</td>
								<td>{{ isset($invoice->customer->name) ? $invoice->customer->name : '' }}</td>
								<td>{{ date('d-m-Y', strtotime($invoice->invoice_date)) }}</td>
								<td>{{ date('d-m-Y', strtotime($invoice->due_date)) }}</td>
								<td>{{ number_format($invoice->total_amount,2,'.',',') }}</td>
								<td>{{ number_format($invoice->total_amount - $invoice->due_amount,2,'.',',') }}</td>
								<td>{{ number_format($invoice->due_amount,2,'.',',') }}</td>
								<td>{{ isset($invoice->agent->name) ? $invoice->agent->name : '' }}</td>
								<td>
									<a href="{{ route('agents_invoices_show', $invoice->id) }}" class="btn btn-info btn-xs" target="_blank"><i title="Show" class="fa fa-eye" aria-hidden="true"></i></a>
								</td>
							</tr>
						@endforeach
						@endif
						</tbody>
					</table>
				</div>
		    </div>
		    @else
		    <div class="box-body">
		    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
		    	<p style="text-align: center;font-size: 18px">No Data Found.</p>
		    </div>
		    </div>
		    @endif
		</div>
	</section>
@endsection

@push('scripts')
	<script type="text/javascript">
		$(".confirm_box").click(function()
		{
			if (!confirm("Do you want to delete"))
			{
				return false;
			}
		});
	</script>
@endpush