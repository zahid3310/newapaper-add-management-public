<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Download Invoice</title>
</head>
<body style="font-family: 'nikosh';">

	<table style="width: 100%">
		<tr>
			<td style="width: 100%;padding: 0px">
				<img src="{{asset('assets/images/invoice-header.png')}}" style="width: 100%">
			</td>
		</tr>
	</table>

	<table style="width: 100%;margin-top: 30px">
		<tr>
			<td style="width: 32%;vertical-align: text-top;border:1px solid black;padding: 10px;">
				<div>
					<p style="font-size: 18px;border-bottom: 1px solid black;width: 100%">বিজ্ঞাপনদাতা : </p>
					<p>&nbsp;</p>
					<p style="font-size: 18px;width: 100%">{{ $invoice->customer->name }}</p>
				</div>
			</td>
			<td style="width: 2%"></td>
			<td style="width: 32%;vertical-align: text-top;border:1px solid black;padding: 10px">
				<div>
					<p style="font-size: 18px;border-bottom: 1px solid black;width: 100%">বিল নং : @php $billNo = \App\Http\Controllers\HomeController::GetBangla($invoice->invoice_number); echo $billNo; @endphp</p>
					<p style="font-size: 18px;border-bottom: 1px solid black;width: 100%">তারিখ : @php $billDate = \App\Http\Controllers\HomeController::GetBangla(date('d-m-Y', strtotime($invoice->invoice_date))); echo $billDate; @endphp</p>
					<p style="font-size: 18px;border-bottom: 1px solid black;width: 100%">প্রকাশের তারিখ : @php $printDate = \App\Http\Controllers\HomeController::GetBangla(date('d-m-Y', strtotime($invoice->due_date))); echo $printDate; @endphp</p>
					<p style="font-size: 18px;border-bottom: 1px solid black;width: 100%">ডিআর (বি) নাম্বার : {{ $invoice ->drb_number }} </p>
				</div>
			</td>
			<td style="width: 2%"></td>
			<td style="width: 32%;vertical-align: text-top;border:1px solid black;padding: 10px">
				<div>
					<p style="font-size: 18px;border-bottom: 1px solid black;width: 100%">নির্দেশক সূত্র : {{ $invoice->nirdesok_number ? $invoice->nirdesok_number : '' }}</p>
					<p style="font-size: 18px;">&nbsp;</p>
					<p style="font-size: 18px;">&nbsp;</p>
					<p style="font-size: 18px;border-bottom: 1px solid black;border-top: 1px solid black;width: 100%">ভ্যাট রেজি নং  -২১২৮১০৩৭২৫৬</p>
				</div>
			</td>
		</tr>
	</table>


	<div style="width: 100%;margin-top: 40px;">
		<table style="width:100%;border-collapse: collapse;border: 1px solid black">
			<tr style="background-color: #999999;">
				<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;font-size: 20px;text-align: center;">নং</td>
				<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;font-size: 20px;text-align: center;">আকার</td>
				<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;text-align: center;font-size: 20px;">মূল্য হার (@if(count($check_nirdharito)>0) নির্ধারিত @else  প্রতি কলাম ইঞ্চি@endif)</td>
				<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;text-align: right;font-size: 20px;text-align: center;"></td>
				<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;text-align: center;font-size: 20px;width: 120px">মোট</td>
			</tr>

			@foreach($invoice_details as $key => $invoice_detail)
			<tr style="background-color: #E6E6E6">
				<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;font-size: 20px;text-align: center;">@php $sl = \App\Http\Controllers\HomeController::GetBangla($key+1); echo $sl; @endphp</td>
					@if($invoice_detail->invoice_type == 1)
					<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;font-size: 20px;text-align: center;">
						@php $inch = \App\Http\Controllers\HomeController::GetBangla($invoice_detail->inch); @endphp
						@php $colum = \App\Http\Controllers\HomeController::GetBangla($invoice_detail->colum); @endphp
						{{ $inch.' ইঞ্চি'.' X '.$colum.' কলাম' }}
					</td>
					@else
					<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;font-size: 20px;text-align: center;">
						নির্ধারিত
					</td>
					@endif
					<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;font-size: 20px;text-align: center;">@php $itemPrice = \App\Http\Controllers\HomeController::GetBangla(number_format($invoice_detail->item->price,2)); @endphp {{ $itemPrice }}</td>
					<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;font-size: 20px;text-align: center;">
						@if($invoice_detail->discount_type == 0)
						@php $discount = \App\Http\Controllers\HomeController::GetBangla(($invoice_detail->discount_amount*$invoice_detail->amount)/100); @endphp
						@else
						@php $discount = \App\Http\Controllers\HomeController::GetBangla($invoice_detail->discount_amount); @endphp
						@endif
						@if($discount != 0) {{ $discount }} @endif
					</td>
					<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;font-size: 20px;text-align: center;">@php $amount = \App\Http\Controllers\HomeController::GetBangla(number_format($invoice_detail->amount,2)); @endphp {{ $amount }}</td>
				</tr>
				@endforeach
				<tr style="background-color: #FFFAC3;">
					<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;font-size: 20px;text-align: center;"></td>
					<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;font-size: 20px;text-align: center;"></td>
					<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;text-align: center;font-size: 20px;"></td>
					<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;text-align: right;font-size: 20px;text-align: center;"></td>
					<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;text-align: center;font-size: 20px;"></td>
				</tr>

				<tr style="background-color: #D9D9D9">
					<td colspan="2" rowspan="4" style="padding:10px ;border: 1px solid black;height: 35px ;text-align: center;font-size: 20px;background-color: white;text-align: left;vertical-align: text-top;">
						<p style="font-size: 20px;border-bottom: 1px solid black;">কথায়ঃ</p>
						<p style="font-size: 20px;">{{ $invoice->amount_bangla ? $invoice->amount_bangla : '' }}</p>
					</td>
					<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;text-align: right;font-size: 20px;">রঙ্গিন অতিরিক্ত : </td>
					<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;text-align: center;font-size: 20px;">@if($invoice->color_print_type == 1) &#2547; @else % @endif</td>
					<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;text-align: center;font-size: 20px;">@php $color_print = \App\Http\Controllers\HomeController::GetBangla(number_format($invoice->color_print,2)); @endphp {{ $color_print }}</td>
				</tr>

				<tr style="background-color: #D9D9D9">
					<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;text-align: right;font-size: 20px;">অন্যান্য : @if(!empty($invoice->adjustment_note))<br> <span style="font-size:18px;line-height:22px"> ({{$invoice->adjustment_note}}) </span>@endif</td>
					<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;text-align: center;font-size: 20px;">@if($invoice->adjustment_type == 1) &#2547; @else % @endif</td>
					<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;text-align: center;font-size: 20px;">@php $adjustment = \App\Http\Controllers\HomeController::GetBangla(number_format($invoice->adjustment,2)); @endphp {{ $adjustment }}</td>
				</tr>

				<tr style="background-color: #D9D9D9">
					<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;text-align: right;font-size: 20px;">ভ্যাট : </td>
					<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;text-align: center;font-size: 20px;">
						@if($invoice->vat_type == 0)
						@php $vat_percent = \App\Http\Controllers\HomeController::GetBangla($invoice->vat_amount); @endphp
						{{ $vat_percent ? $vat_percent.'%' : '' }}
						@else
						&#2547;
						@endif
					</td>
					<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;text-align: center;font-size: 20px;">
						@if($invoice->vat_type == 0)
						@php $vat_amount = \App\Http\Controllers\HomeController::GetBangla(number_format(($invoice->vat_amount*$total_amount_sum)/100, 2)); @endphp
						@else
						@php $vat_amount = \App\Http\Controllers\HomeController::GetBangla(number_format($invoice->vat_amount,2)); @endphp
						@endif
						{{ $vat_amount }}
					</td>
				</tr>

				<tr style="background-color: #D9D9D9">
					<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;text-align: right;font-size: 20px;">স:বি:কর : </td>
					<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;text-align: center;font-size: 20px;">
						@if($invoice->tax_type == 0)
						@php $tax_percent = \App\Http\Controllers\HomeController::GetBangla($invoice->tax_amount); @endphp

						{{ $tax_percent ? $tax_percent.'%' : '' }}
						@else
						&#2547;
						@endif
						 </td>
						<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;text-align: center;font-size: 20px;">
							@if($invoice->tax_type == 0)
							@php $tax_amount = \App\Http\Controllers\HomeController::GetBangla(number_format(($invoice->tax_amount*$total_amount_sum)/100, 2)); @endphp
							@else
							@php $tax_amount = \App\Http\Controllers\HomeController::GetBangla(number_format($invoice->tax_amount,2)); @endphp
							@endif
							{{ $tax_amount }}
						</td>
					</tr>

					<tr style="background-color: #999999">
						<td colspan="4" style="padding:0px 5px ;border: 1px solid black;height: 35px ;text-align: right;font-size: 20px;;">সর্বমোট : </td>
						<td style="padding:0px 5px ;border: 1px solid black;height: 35px ;text-align: center;font-size: 20px;">@php $total_amount = \App\Http\Controllers\HomeController::GetBangla(number_format($invoice->total_amount, 2)); @endphp {{ $total_amount }}</td>
					</tr>
				</table>
				<p style="font-size: 20px"><span style="background-color: black;color: white;padding: 5px"> দ্রস্টব্য: </span>&nbsp; বিলের টাকা দৈনিক ঢাকা রিপোর্ট এর অনুকূলে একাউন্টপেয়ী চেক এর মাধ্যমে পরিশোধ যোগ্য ।</p>
			</div>


			<table style="width:100%;margin-top: 70px">
				<tr>
					<td style="width: 50%;text-align:center;font-size: 20px">
						<p style="border-top: 2px solid black">হিসাব রক্ষক</p>
					</td>
					<td style="width: 50%;text-align:center;font-size: 20px">
						<p style="border-top: 2px solid black">বিজ্ঞাপন কর্মকর্তা</p>
					</td>
				</tr>
			</table>


			<p style="font-size: 20px;text-align: center;border-top: 1px solid black;border-bottom: 1px solid black;margin-top: 60px">ধন্যবাদ, আমাদের সাথে থাকার জন্য | এটি একটি সফটওয়্যারাইজড বিল</p>


			<p style="font-size: 14px;text-align: left;vertical-align: text-bottom;bottom: 0px;position: absolute;color: black;padding: 0px 5px">Created by : {{ $invoice->createdBy->name  }}</p>

		</body>
		</html>
