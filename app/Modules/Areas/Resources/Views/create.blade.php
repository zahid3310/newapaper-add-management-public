@extends('layouts.app')

@section('title', 'Create Area')

@section('content')

<section class="content">
	<div class="box">
		<div class="box-header with-border">
		<div class="col-md-12">
			<h3 class="box-title">
				Create Agents Area
			</h3>
		</div>
		</div>
		<div class="box-body">
			@if(Session::has('message'))
			<div style="padding: 0px" class="col-md-12">
				<div class="alert alert-success alert-dismissable text-center">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					{{Session::get('message')}}
				</div>
			</div>
			@endif
			@if(Session::has('errors'))
			<div style="padding: 0px" class="col-md-12">
				<div class="alert alert-success alert-dismissable text-center">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					{{Session::get('errors')}}
				</div>
			</div>
			@endif
			<form class="form-horizontal" method="POST" action="{{ route('reporter_area_store') }}" enctype="multipart/form-data">
				{{ csrf_field() }}

				<div class="form-group{{ $errors->has('district_id') ? ' has-error' : '' }}">
					<label for="name" class="col-md-4 control-label">
						District Name * 
					</label>

					<div class="col-md-6">
						<select class="form-control select2 select2-hidden-accessible" style="width: 100%;padding: 15px" tabindex="-1" aria-hidden="true" name="district_id">
							<option style="padding: 15px" value="" selected>Select District</option>
							@foreach($districts as $district)
							<option style="padding: 15px" value="{{ $district->id }}">{{ $district->name }}</option>
							@endforeach
						</select>

						@if ($errors->has('district_id'))
						<span class="help-block">
							<strong>{{ $errors->first('district_id') }}</strong>
						</span>
						@endif
					</div>
				</div>

				<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
					<label for="name" class="col-md-4 control-label">
						Area Name *
					</label>

					<div class="col-md-6">
						<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

						@if ($errors->has('name'))
						<span class="help-block">
							<strong>{{ $errors->first('name') }}</strong>
						</span>
						@endif
					</div>
				</div>

				<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
					<label for="description" class="col-md-4 control-label">
						Description
					</label>

					<div class="col-md-6">
						<textarea id="name" type="text" class="form-control" name="description" value="{{ old('description') }}"></textarea>

						@if ($errors->has('description'))
						<span class="help-block">
							<strong>{{ $errors->first('description') }}</strong>
						</span>
						@endif
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-6 col-md-offset-4">
						<button type="submit" class="btn btn-primary">
							<i class="fa fa-plus-circle"></i>
							Save
						</button>
						<a href="{{route('reporter_area_index')}}" class="btn btn-danger">
							<i class="fa fa-trash"></i>
							Cancel
						</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>
@endsection