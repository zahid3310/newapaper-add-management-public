@extends('layouts.app')

@section('title', 'Area List')

@section('content')
<style type="text/css">
	.customAlign table thead tr th{
		vertical-align: middle;
	}
	.table-tr-style tr:nth-child(even) {
		background-color: #dddddd;
	}
	.table-tr-style tr:nth-child(odd) {
		background-color: #F2F3F4;
	}
	.total th{
		font-size: 20px
	}
	.select2-container .select2-selection--single{
		height: auto
	}
	.select2-container--default .select2-selection--single .select2-selection__rendered{
		line-height: 20px
	}
	.select2-container .select2-selection--single .select2-selection__rendered{
		margin-top: 0px
	}
</style>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				@if(Session::has('message'))
				<div class="col-md-12">
					<div class="alert alert-success alert-dismissable text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{Session::get('message')}}
					</div>
				</div>
				@endif
				@if(Session::has('errors'))
				<div class="col-md-12">
					<div class="alert alert-success alert-dismissable text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{Session::get('errors')}}
					</div>
				</div>
				@endif
			</div>
		</div>

		<div class="col-md-4">	
			<div class="box">
				<div class="col-md-12">
					<h3 class="box-title" style="width: 100%">Add New Area</h3>
					<hr>
				</div>

				<div class="box-body">
					<form class="form-horizontal" method="POST" action="{{ route('reporter_area_store') }}" enctype="multipart/form-data">
						{{ csrf_field() }}

						<div class="form-group{{ $errors->has('district_id') ? ' has-error' : '' }}">
							<div class="col-md-12">
								<label for="name" class="control-label">
									District Name * 
								</label>
								<select class="form-control select2 select2-hidden-accessible" style="width: 100%;padding: 15px" tabindex="-1" aria-hidden="true" name="district_id">
									<option style="padding: 15px" value="" selected>Select District</option>
									@foreach($districts as $district)
									<option style="padding: 15px" value="{{ $district->id }}">{{ $district->name }}</option>
									@endforeach
								</select>

								@if ($errors->has('district_id'))
								<span class="help-block">
									<strong>{{ $errors->first('district_id') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<div class="col-md-12">
								<label for="name" class="control-label">
									Area Name *
								</label>
								<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

								@if ($errors->has('name'))
								<span class="help-block">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
							<div class="col-md-12">
								<label for="description" class="control-label">
									Description
								</label>
								<textarea id="name" type="text" class="form-control" name="description" value="{{ old('description') }}"></textarea>

								@if ($errors->has('description'))
								<span class="help-block">
									<strong>{{ $errors->first('description') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-plus-circle"></i>
									Save
								</button>
								<a href="{{route('reporter_area_index')}}" class="btn btn-danger">
									<i class="fa fa-trash"></i>
									Cancel
								</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<div class="col-md-8">	
			<div class="box">
				<div class="col-md-12">
					<h3 class="box-title" style="width: 100%">Agents Area List</h3>
					<hr>
				</div>

				<div class="box-body">
					@if(count($areas) >0)
						<div class="col-md-12 customAlign table-responsive">

							<table id="dataTable" class="table table-bordered table-striped dataTable">
								<thead>
									<tr style="background-color: #F9E79F">
										<th>SL</th>
										<th>Area</th>
										<th>District Name</th>
										<th>Description</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@if(!empty($areas) && (count($areas)>0) )
									@foreach($areas as $key => $area)
									<tr>
										<td>{{ $key + 1 }}</td>
										<td>{{ $area->name }}</td>
										<td>{{ $area->district->name }}</td>
										<td>{{ $area->description }}</td>
										<td>
											<a href="{{ route('reporter_area_edit', $area->id) }}" class="btn btn-success btn-xs"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i></a>
											<a href="{{ route('reporter_area_delete', $area->id) }}" class="btn btn-danger btn-xs confirm_box"><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></a>
										</td>
									</tr>
									@endforeach
									@endif
								</tbody>
							</table>
						</div>
					</div>
					@else
					<div class="box-body">
						<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
							<p style="text-align: center;font-size: 18px">No Data Found.<br>Please Insert Customer Information.</p>
						</div>
					</div>
					@endif
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@push('scripts')
<script type="text/javascript">
	$(".confirm_box").click(function()
	{
		if (!confirm("Do you want to delete"))
		{
			return false;
		}
	});
</script>
@endpush