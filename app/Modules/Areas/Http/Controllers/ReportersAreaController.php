<?php

namespace App\Modules\Areas\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Models\Areas;
use App\Models\Districts;

class ReportersAreaController extends Controller
{
    public function index()
    {	
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('reporter_area_index');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

    	$areas     = Areas::where('status', '=', 1)->orderBy('district_id', 'DESC')->get();
        $districts = Districts::where('status', '=', 1)->orderBy('name', 'asc')->get();

    	return view('areas::index', compact('areas', 'districts'));
    }

    public function create()
    {	
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('reporter_area_create');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

    	$districts = Districts::where('status', '=', 1)->orderBy('name', 'asc')->get();
    	
    	return view('areas::create', compact('districts'));
    }

    public function store(Request $request)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('reporter_area_store');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $this->validate($request,[
         'district_id'  => 'required',
         'name'         => 'required',
        ]);

        $areas               = new Areas;
        $areas->district_id  = $request->district_id;
        $areas->name         = $request->name;
        $areas->description  = $request->description;
        $areas->status       = 1;
        $areas->created_by   = Auth::user()->id;

        if ($areas->save())
        {
            return redirect()->route('reporter_area_index')->with('message', 'Successfully added !!');
        }else{
            return back()->with('message', 'Something went wrong !! Please try again.');
        }
    }

    public function edit($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('reporter_area_edit');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $area       = Areas::find($id);
        $districts  = Districts::where('status', '=', 1)->get()->sortBy('name');

    	return view('areas::edit', compact('area', 'districts'));
    }

    public function update(Request $request, $id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('reporter_area_update');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $this->validate($request,[
         'district_id'  => 'required',
         'name'         => 'required',
        ]);

        $areas               = Areas::find($id);
        $areas->district_id  = $request->district_id;
        $areas->name         = $request->name;
        $areas->description  = $request->description;
        $areas->updated_by   = Auth::user()->id;

        if ($areas->save())
        {
            return redirect()->route('reporter_area_index')->with('message', 'Successfully updated !!');
        }else{
            return back()->with('message', 'Something went wrong !! Please try again.');
        }
    }

    public function destroy($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('reporter_area_delete');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $areas               = Areas::find($id);
        $areas->status       = 0;
        $areas->updated_by   = Auth::user()->id;

        if ($areas->save())
        {
            return redirect()->route('reporter_area_index')->with('message', 'Successfully deleted !!');
        }else{
            return back()->with('message', 'Something went wrong !! Please try again.');
        }

    	return view('areas::index');
    }
}
