<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'areas', 'middleware' => 'auth'], function () {
    Route::get('/index', 'ReportersAreaController@index')->name('reporter_area_index');
    Route::get('/create', 'ReportersAreaController@create')->name('reporter_area_create');
    Route::post('/store', 'ReportersAreaController@store')->name('reporter_area_store');
    Route::get('/edit/{id}', 'ReportersAreaController@edit')->name('reporter_area_edit');
    Route::post('/update/{id}', 'ReportersAreaController@update')->name('reporter_area_update');
    Route::get('/delete/{id}', 'ReportersAreaController@destroy')->name('reporter_area_delete');
});
