<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'items', 'middleware' => 'auth'], function () {
    Route::get('/index', 'ItemsController@index')->name('items_index');
    Route::get('/create', 'ItemsController@create')->name('items_create');
    Route::post('/store', 'ItemsController@store')->name('items_store');
    Route::get('/edit/{id}', 'ItemsController@edit')->name('items_edit');
    Route::post('/update/{id}', 'ItemsController@update')->name('items_update');
    Route::get('/delete/{id}', 'ItemsController@destroy')->name('items_delete');
});

Route::group(['prefix' => 'category', 'middleware' => 'auth'], function () {
    Route::get('/index', 'CategoryController@index')->name('category_index');
    Route::get('/create', 'CategoryController@create')->name('category_create');
    Route::post('/store', 'CategoryController@store')->name('category_store');
    Route::get('/edit/{id}', 'CategoryController@edit')->name('category_edit');
    Route::post('/update/{id}', 'CategoryController@update')->name('category_update');
    Route::get('/delete/{id}', 'CategoryController@destroy')->name('category_delete');
});
