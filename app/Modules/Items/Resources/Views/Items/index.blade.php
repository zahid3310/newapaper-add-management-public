@extends('layouts.app')

@section('title', 'Advertisement List')

@section('content')
<style type="text/css">
	.customAlign table thead tr th{
		vertical-align: middle;
	}
	.table-tr-style tr:nth-child(even) {
		background-color: #dddddd;
	}
	.table-tr-style tr:nth-child(odd) {
		background-color: #F2F3F4;
	}
	.total th{
		font-size: 20px
	}
	.select2-container .select2-selection--single{
		height: auto
	}
	.select2-container--default .select2-selection--single .select2-selection__rendered{
		line-height: 20px
	}
	.select2-container .select2-selection--single .select2-selection__rendered{
		margin-top: 0px
	}
</style>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				@if(Session::has('message'))
				<div class="col-md-12">
					<div class="alert alert-success alert-dismissable text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{Session::get('message')}}
					</div>
				</div>
				@endif
				@if(Session::has('errors'))
				<div class="col-md-12">
					<div class="alert alert-success alert-dismissable text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{Session::get('errors')}}
					</div>
				</div>
				@endif
			</div>
		</div>

		<div class="col-md-4">	
			<div class="box">
				<div class="col-md-12">
					<h3 class="box-title" style="width: 100%">Add New Advertisement</h3>
					<hr>
				</div>

				<div class="box-body">
					<form class="form-horizontal" method="POST" action="{{ route('items_store') }}" enctype="multipart/form-data">
						{{ csrf_field() }}

						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<div class="col-md-12">
								<label for="name" class="control-label">Name *</label>
								<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

								@if ($errors->has('name'))
								<span class="help-block">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
							<div class="col-md-12">
								<label for="code" class="control-label">Code</label>
								<input id="code" type="text" class="form-control" name="code" value="{{ old('code') }}">

								@if ($errors->has('code'))
								<span class="help-block">
									<strong>{{ $errors->first('code') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
							<div class="col-md-12">
								<label for="price" class="control-label">Price (inch X column)</label>
								<input id="price" type="text" class="form-control" name="price" value="{{ old('price') }}">

								@if ($errors->has('price'))
								<span class="help-block">
									<strong>{{ $errors->first('price') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('commission') ? ' has-error' : '' }}">
							<div class="col-md-9 col-xs-7">
								<label for="commission" class="control-label">Comission</label>
								<input id="commission" type="text" class="form-control" name="commission" value="{{ old('commission') }}">

								@if ($errors->has('commission'))
								<span class="help-block">
									<strong>{{ $errors->first('commission') }}</strong>
								</span>
								@endif
							</div>

							<div style="padding-left: 0px" class="col-md-3 col-xs-5">
								<select style="padding: 6px;border-radius: 4px;margin-top: 28px" name="commission_type">
									<option style="padding: 10px" value="1" selected>BDT</option>
									<option style="padding: 10px" value="0">%</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-plus-circle"></i>
									Save
								</button>
								<a href="{{ route('items_index') }}" class="btn btn-danger">
									<i class="fa fa-trash"></i>
									Cancel
								</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<div class="col-md-8">	
			<div class="box">
				<div class="col-md-12">
					<h3 class="box-title" style="width: 100%">Advertisement List</h3>
					<hr>
				</div>

				<div class="box-body">
					@if(count($items) >0)
					<div class="col-md-12 customAlign table-responsive">

						<table id="dataTable" class="table table-bordered table-striped dataTable">
							<thead>
								<tr style="background-color: #F9E79F">
									<th>SL</th>
									<th>Code</th>
									<th>Name</th>
									<th>Price (inch X column)</th>
									<th>Comission</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@if(!empty($items) && (count($items)>0) )
								@foreach($items as $key => $item)
								<tr>
									<td>{{ $key + 1 }}</td>
									<td>{{ $item->code }}</td>
									<td>{{ $item->name }}</td>
									<td>{{ number_format($item->price,2,'.',',') }}</td>
									<td>
										@if($item->commission && $item->commission_type == 1)
										{{ number_format($item->commission,2,'.',',') }} (BDT)
										@elseif($item->commission && $item->commission_type == 0)
										{{ $item->commission }} (%)
										@endif
									</td>
									<td>
										<a href="{{ route('items_edit', $item->id) }}" class="btn btn-success btn-xs"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i></a>
										<a href="{{ route('items_delete', $item->id) }}" class="btn btn-danger btn-xs confirm_box"><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></a>
									</td>
								</tr>
								@endforeach
								@endif
							</tbody>
						</table>
					</div>
					@else
					<div class="box-body">
						<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
							<p style="text-align: center;font-size: 18px">No Data Found.<br>Please Insert Item Information.</p>
						</div>
					</div>
					@endif
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@push('scripts')
<script type="text/javascript">
	$(".confirm_box").click(function()
	{
		if (!confirm("Do you want to delete"))
		{
			return false;
		}
	});
</script>
@endpush