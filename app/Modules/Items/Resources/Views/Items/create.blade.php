@extends('layouts.app')

@section('title', 'Create Advertisement')

@section('content')

<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<div class="col-md-12">
				<h3 class="box-title" style="width: 100%">Create Advertisement
			</h3>
		</div>
	</div>

	<div class="box-body">
		@if(Session::has('message'))
		<div style="padding: 0px" class="col-md-12">
			<div class="alert alert-success alert-dismissable text-center">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				{{Session::get('message')}}
			</div>
		</div>
		@endif
		@if(Session::has('errors'))
		<div style="padding: 0px" class="col-md-12">
			<div class="alert alert-success alert-dismissable text-center">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				{{Session::get('errors')}}
			</div>
		</div>
		@endif
		<form class="form-horizontal" method="POST" action="{{ route('items_store') }}" enctype="multipart/form-data">
			{{ csrf_field() }}

			<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
				<label for="name" class="col-md-4 control-label">
					Name *
				</label>

				<div class="col-md-6">
					<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

					@if ($errors->has('name'))
					<span class="help-block">
						<strong>{{ $errors->first('name') }}</strong>
					</span>
					@endif
				</div>
			</div>

			<div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
				<label for="code" class="col-md-4 control-label">
					Code
				</label>

				<div class="col-md-6">
					<input id="code" type="text" class="form-control" name="code" value="{{ old('code') }}">

					@if ($errors->has('code'))
					<span class="help-block">
						<strong>{{ $errors->first('code') }}</strong>
					</span>
					@endif
				</div>
			</div>

			<div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
				<label for="price" class="col-md-4 control-label">
					Price (inch X column)
				</label>

				<div class="col-md-6">
					<input id="price" type="text" class="form-control" name="price" value="{{ old('price') }}">

					@if ($errors->has('price'))
					<span class="help-block">
						<strong>{{ $errors->first('price') }}</strong>
					</span>
					@endif
				</div>
			</div>

			<div class="form-group{{ $errors->has('commission') ? ' has-error' : '' }}">
				<label for="commission" class="col-md-4 col-sm-4 col-xs-12 col-lg-4 control-label">
					Agent Comission
				</label>

				<div class="col-md-5 col-sm-5 col-xs-5 col-lg-5">
					<input id="commission" type="text" class="form-control" name="commission" value="{{ old('commission') }}">

					@if ($errors->has('commission'))
					<span class="help-block">
						<strong>{{ $errors->first('commission') }}</strong>
					</span>
					@endif
				</div>

				<div class="col-md-3 col-sm-3 col-xs-12 col-lg-3">
					<select style="padding: 6px;border-radius: 4px" name="commission_type">
						<option style="padding: 10px" value="1" selected>BDT</option>
						<option style="padding: 10px" value="0">%</option>
					</select>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-6 col-md-offset-4">
					<button type="submit" class="btn btn-primary">
						<i class="fa fa-plus-circle"></i>
						Save
					</button>
					<a href="{{ route('items_index') }}" class="btn btn-danger">
						<i class="fa fa-trash"></i>
						Cancel
					</a>
				</div>
			</div>
		</form>
	</div>
</div>
</section>
@endsection