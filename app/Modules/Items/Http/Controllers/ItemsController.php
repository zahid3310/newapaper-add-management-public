<?php

namespace App\Modules\Items\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Models\Areas;
use App\Models\Districts;
use App\Models\Customers;
use App\Models\Items;
use App\Models\Categories;

class ItemsController extends Controller
{
    public function index()
    {	
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('items_index');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

    	$items = Items::where('status', '=', 1)->get()->sortBy('name');

    	return view('items::Items.index', compact('items'));
    }

    public function create()
    {	
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('items_create');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $categories = Categories::where('status', '=', 1)->get();

    	return view('items::Items.create', compact('categories'));
    }

    public function store(Request $request)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('items_store');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $this->validate($request,[
         'name'         => 'required',
        ]);

        $items                  = new Items;
        $items->name  		    = $request->name;
        $items->code            = $request->code;
        $items->price       	= $request->price;
        $items->commission_type = $request->commission_type;
        $items->commission      = $request->commission;
        $items->sell  		    = 0;
        $items->status      	= 1;
        $items->created_by  	= Auth::user()->id;

        if ($items->save())
        {
            return redirect()->route('items_index')->with('message', 'Successfully added !!');
        }else{
            return back()->with('message', 'Something went wrong !! Please try again.');
        }
    }

    public function edit($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('items_edit');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $item       = Items::find($id);
        $categories = Categories::where('status', '=', 1)->get();

    	return view('items::Items.edit', compact('item', 'categories'));
    }

    public function update(Request $request, $id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('items_update');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $this->validate($request,[
         'name'         => 'required',
        ]);

        $items                  = Items::find($id);
        $items->name            = $request->name;
        $items->code            = $request->code;
        $items->price           = $request->price;
        $items->commission_type = $request->commission_type;
        $items->commission      = $request->commission;
        $items->updated_by      = Auth::user()->id;

        if ($items->save())
        {
            return redirect()->route('items_index')->with('message', 'Successfully updated !!');
        }else{
            return back()->with('message', 'Something went wrong !! Please try again.');
        }
    }

    public function destroy($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('items_delete');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $items               = Items::find($id);
        $items->status       = 0;
        $items->updated_by   = Auth::user()->id;

        if ($items->save())
        {
            return redirect()->route('items_index')->with('message', 'Successfully deleted !!');
        }else{
            return back()->with('message', 'Something went wrong !! Please try again.');
        }
    }

}
