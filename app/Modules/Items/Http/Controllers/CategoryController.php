<?php

namespace App\Modules\Items\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Models\Areas;
use App\Models\Districts;
use App\Models\Customers;
use App\Models\Categories;

class CategoryController extends Controller
{
    public function index()
    {	
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('category_index');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

    	$categories = Categories::where('status', '=', 1)->get()->sortBy('name');

    	return view('items::Categories.index', compact('categories'));
    }

    public function create()
    {	
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('category_create');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

    	return view('items::Categories.create');
    }

    public function store(Request $request)
    {  
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('category_store');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $this->validate($request,[
         'name'  => 'required',
        ]);

        $categories             	= new Categories;
        $categories->name  			= $request->name;
        $categories->description  	= $request->description;
        $categories->status      	= 1;
        $categories->created_by  	= Auth::user()->id;

        if ($categories->save())
        {
            return redirect()->route('category_index')->with('message', 'Successfully added !!');
        }else{
            return back()->with('message', 'Something went wrong !! Please try again.');
        }
    }

    public function edit($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('category_edit');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $category = Categories::find($id);

    	return view('items::Categories.edit', compact('category'));
    }

    public function update(Request $request, $id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('category_update');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $this->validate($request,[
         'name'  => 'required',
        ]);

        $categories             	= Categories::find($id);
        $categories->name  			= $request->name;
        $categories->description  	= $request->description;
        $categories->updated_by  	= Auth::user()->id;

        if ($categories->save())
        {
            return redirect()->route('category_index')->with('message', 'Successfully updated!!');
        }else{
            return back()->with('message', 'Something went wrong !! Please try again.');
        }
    }

    public function destroy($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('category_delete');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $categories               = Categories::find($id);
        $categories->status       = 0;
        $categories->updated_by   = Auth::user()->id;

        if ($categories->save())
        {
            return redirect()->route('category_index')->with('message', 'Successfully deleted !!');
        }else{
            return back()->with('message', 'Something went wrong !! Please try again.');
        }

    	return view('areas::index');
    }

}
