<?php

namespace App\Modules\SalesComission\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

//Models
use App\Models\Areas;
use App\Models\Districts;
use App\Models\Customers;
use App\Models\Agents;
use App\Models\Items;
use App\Models\Invoices;
use App\Models\InvoiceDetails;
use App\Models\Transactions;
use Response;
use DB;

class SalesComissionController extends Controller
{
    public function index()
    {
    }

    public function create()
    {	
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('sales_comission_create');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

    	$agent_id 		= isset($_GET['agent_id']) ? $_GET['agent_id'] : 0;
    	$agents 		= Agents::where('status', '=', 1)->get();
    	$find_agent 	= Agents::where('id', $agent_id)->where('status', '=', 1)->first();

    	$invoices       = Invoices::join('transactions', 'transactions.invoice_id', 'invoices.id')
    	                                ->where('invoices.agent_id', $agent_id)
        								->where('invoices.status', 1)
        								->where('transactions.status', 1)
        								->whereNotNull('invoices.agent_id')
        								->where('invoices.commission_amount', '>', 0)
        								->where('transactions.type', 'invoice')
        								->where('transactions.trans_paid', '>', 0)
                                        ->selectRaw('invoices.*')
                                        ->orderBy('created_at', 'ASC')
        								->get();

        $transactions   = Transactions::leftjoin('invoices', 'invoices.id', 'transactions.invoice_id')
        								->where('transactions.agent_id', $agent_id)
                                        ->where('transactions.type', 'sales_comission')
                                        ->where('transactions.status', 1)
                                        ->where('invoices.status', 1)
                                        ->selectRaw('transactions.*, invoices.invoice_number as invoice_number')
                                        ->orderBy('created_at', 'DESC')
                                        ->get();

        foreach ($invoices as $key => $invoice)
        {   
            $invoice_details            = InvoiceDetails::join('transactions', 'transactions.invoice_id', 'invoices_details.invoice_id')
                                                        ->where('invoices_details.invoice_id', $invoice->id)
                                                        ->where('transactions.type', 'invoice')
                                                        ->where('transactions.status', 1)
                                                        ->where('transactions.trans_paid', '>', 0)
                                                        ->selectRaw('(CASE WHEN invoices_details.commission_type = 1 THEN invoices_details.amount ELSE ((transactions.trans_paid*invoices_details.commission_amount)/100) END) AS com_amount')
                                                        ->get();
                                                        
            $invoice_val                = $invoices->where('id', $invoice->id);
            $comission                  = $invoice_details->sum('com_amount');
            // $other_expense_deduction = ($comission*$invoice->other_expense)/$invoice->total_amount;
            $other_expense_deduction    = 0;
            $payable                    = $comission - $other_expense_deduction; 
            $paid                       = $transactions->where('invoice_id', $invoice->id)->where('type', 'sales_comission')->sum('trans_paid');

            $invoice_wise_payable[]     = round($payable, 2);
            $invoice_wise_paid[]        = round($paid, 2);
            $invoice_wise_dues[]        = round($payable - $paid, 2);
        }

        if (isset($invoice_wise_payable))
        {
            $payable_sum = array_sum($invoice_wise_payable);
        }
        else
        {
            $payable_sum = 0;
        }

        if (isset($invoice_wise_paid))
        {
            $paid_sum = array_sum($invoice_wise_paid);
        }
        else
        {
            $paid_sum = 0;
        }

    	return view('salesComission::create', compact('agent_id', 'agents', 'find_agent', 'invoices', 'transactions', 'invoice_wise_payable', 'invoice_wise_paid', 'invoice_wise_dues', 'payable', 'payable_sum', 'paid_sum'));
    }

    public function store(Request $request)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('sales_comission_store');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $this->validate($request,[
         	'date'  	          => 'required',
         	'payment_methode'     => 'required',
         	'amount'     		  => 'required',
         	'invoice_id'     	  => 'required',
        ]);
  
        try
        {   
            DB::beginTransaction();

            foreach ($request->invoice_id as $key => $value) 
            {
                $invoices        = Invoices::find($value);
                $transactions    = Transactions::where('transactions.agent_id', $request->agent_id_send)
                                                ->where('transactions.type', 'sales_comission')
                                                ->where('transactions.invoice_id', $value)
                                                ->where('transactions.status', 1)
                                                ->sum('transactions.trans_paid');

                if($request->amount > ($request->payable - $transactions))
                {
                    return back()->with('message', 'Payment amount is grater than due amount !!');
                }

                $transactions                       = new Transactions;
                $transactions->date                 = $request->date;
                $transactions->invoice_id           = $value;
                $transactions->agent_id             = $request->agent_id_send;
                $transactions->type                 = 'sales_comission';
                $transactions->trans_receivable     = 0;
                $transactions->trans_paid           = $request->paid[$key];
                $transactions->payment_methode      = $request->payment_methode;
                $transactions->payment_through      = $request->payment_through;
                $transactions->status               = 1;
                $transactions->created_by           = Auth::user()->id;
                $transactions->save();
            }
              
                DB::commit();

                return back()->with('message', 'Payment Successfully added !!');

        }
        catch(\Exception $e)
        {dd($e);
            DB::rollback();
            $mesg = $e->getMessage();
            return back()->with('message', 'Something went wrong try again !!');
        }     
    }

    public function edit($id)
    {  
    }

    public function update(Request $request, $id)
    {    
    }

    public function destroy($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('sales_comission_delete');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        try
        {  
           $transactions            = Transactions::find($id);
           $transactions->status    = 0;

           if ($transactions->save())
            {   
                return back()->with('message', 'Successfully deleted !!');
            }else{
                return back()->with('message', 'Something went wrong !! Please try again.');
            }
        }
        catch(\Exception $e)
        {
            DB::rollback();
            $mesg = $e->getMessage();
            return back()->with('message', 'Something went wrong try again !!');
        }
    }

}
