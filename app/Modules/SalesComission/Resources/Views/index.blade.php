@extends('layouts.app')

@section('title', 'Sales Comission List')

@section('content')
	<style type="text/css">
		.customAlign table thead tr th{
			vertical-align: middle;
		}
		.table-tr-style tr:nth-child(even) {
			background-color: #dddddd;
		}
		.table-tr-style tr:nth-child(odd) {
			background-color: #F2F3F4;
		}
		.total th{
			font-size: 20px
		}
		.select2-container .select2-selection--single{
			height: auto
		}
		.select2-container--default .select2-selection--single .select2-selection__rendered{
			line-height: 20px
		}
		.select2-container .select2-selection--single .select2-selection__rendered{
			margin-top: 0px
		}
	</style>

	<section class="content-header">
		<h1>
			Manage Payment List
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i>
			 	Home
			 </a></li>
			<li><a href="#">
				Payments
			</a></li>
			<li class="active">
				Payment List
			</li>
		</ol>
	</section>

	<section class="content">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
						Payment List
				</h3><br><br>
				<div class="col-xs-12 col-sm-2 pull-right">
					<a style="color: white;border-radius: 0px" href="{{ route('payment_receive_create') }}" class="btn btn-success btn-block">
					   Add New
					</a>
				</div>	
			</div>

			@if(count($payments) >0)
			<div class="box-body">
				<div class="row">
					@if(Session::has('message'))
					<div class="col-md-12">
						<div class="alert alert-success alert-dismissable text-center">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							{{Session::get('message')}}
						</div>
					</div>
					@endif
					@if(Session::has('errors'))
					<div class="col-md-12">
						<div class="alert alert-success alert-dismissable text-center">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							{{Session::get('errors')}}
						</div>
					</div>
					@endif
				</div>

				<div class="col-md-12 customAlign table-responsive">

					<table id="dataTable" class="table table-bordered table-striped dataTable">
						<thead>
							<tr style="background-color: #F9E79F">
								<th>SL</th>
								<th>Payment Date</th>
								<th>Invoice Number</th>
								<th>Customer Name</th>
								<th>Amount Paid</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						@if(!empty($payments) && (count($payments)>0) )
						@foreach($payments as $key => $payment)
							<tr>
								<td>{{ $key + 1 }}</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td>
									<a href="{{ route('payments_edit', $payment->id) }}" class="btn btn-success btn-xs"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i></a>
									<a href="{{ route('payments_delete', $payment->id) }}" class="btn btn-danger btn-xs confirm_box"><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></a>
								</td>
							</tr>
						@endforeach
						@endif
						</tbody>
					</table>
				</div>
		    </div>
		    @else
		    <div class="box-body">
		    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
		    	<p style="text-align: center;font-size: 18px">No Data Found.</p>
		    </div>
		    </div>
		    @endif
		</div>
	</section>
@endsection

@push('scripts')
	<script type="text/javascript">
		$(".confirm_box").click(function()
		{
			if (!confirm("Do you want to delete"))
			{
				return false;
			}
		});
	</script>
@endpush