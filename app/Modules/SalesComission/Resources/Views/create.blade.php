@extends('layouts.app')

@section('title', 'Create Sales Comission')

@section('content')

	<style type="text/css">
		.customAlign table thead tr th{
			vertical-align: middle;
		}
		.table-tr-style tr:nth-child(even) {
			background-color: #dddddd;
		}
		.table-tr-style tr:nth-child(odd) {
			background-color: #F2F3F4;
		}
		.total th{
			font-size: 20px;
		}
		.select2-container .select2-selection--single{
			height: auto;
		}
		.select2-container--default .select2-selection--single .select2-selection__rendered{
			line-height: 20px;
		}
		.select2-container .select2-selection--single .select2-selection__rendered{
			margin-top: 0px;
		}

		.loader {
		  border: 16px solid #f3f3f3;
		  border-radius: 50%;
		  border-top: 16px solid #3498db;
		  width: 120px;
		  height: 120px;
		  -webkit-animation: spin 2s linear infinite; /* Safari */
		  animation: spin 2s linear infinite;
		}

		/* Safari */
		@-webkit-keyframes spin {
		  0% { -webkit-transform: rotate(0deg); }
		  100% { -webkit-transform: rotate(360deg); }
		}

		@keyframes spin {
		  0% { transform: rotate(0deg); }
		  100% { transform: rotate(360deg); }
		}
	</style>

	<section class="content" style="min-height: auto">
		<div class="box" style="margin-bottom: 0px">
			<div class="box-body">
				@if(Session::has('message'))
				<div style="padding: 0px" class="col-md-12">
					<div class="alert alert-success alert-dismissable text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{Session::get('message')}}
					</div>
				</div>
				<hr>
				<hr>
				@endif
				@if(Session::has('errors'))
				<div style="padding: 0px" class="col-md-12">
					<div class="alert alert-danger alert-dismissable text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<b>Warning !</b> {{Session::get('errors')}}
					</div>
				</div>
				<hr>
				<hr>
				@endif
				<!-- SELECT2 EXAMPLE -->
				<!-- <div class="box box-default"> -->
				<div class="box-body">
					<div class="callout callout-info" style="margin-bottom: 0px">
						<div class="row">

							<form class="form-horizontal" method="GET" action="{{ route('sales_comission_create') }}" enctype="multipart/form-data">
								<!-- {{ csrf_field() }} -->

								<label for="name" class="col-md-2 col-xs-12 control-label text-right" style="padding-top: 6px">Search Agent :</label>
								<div class="col-md-8">
									<select id="agent_id" name="agent_id" class="form-control select2" style="width: 100%" onchange="selectEvent()">
										<option value="">--Select Agent--</option>
										@if(!empty($agents))
											@foreach($agents as $key => $agent)
											<option @if(isset($find_agent)) {{ $find_agent['id'] == $agent['id'] ? 'selected' : '' }} @endif value="{{ $agent->id }}">{{ $agent->name }}</option>
											@endforeach
										@endif
									</select>
								</div>

								<div style="display: none" class="col-md-2">
									<button id="searbButton" type="submit" class="btn btn-success btn-block">Search 
									</button>
								</div>
							</form>

						</div>
					</div>	
				</div>
			</div>
		</div>
	</section>

	<section class="content" style="padding-top: 8px">

		<div class="row">

			<div class="col-md-4">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title" style="width:100%">Comission Summary</h3>
					</div>
					<div class="box-body">
						<div class="table-responsive">
							<table class="table table-bordered text-center " style="background-color: #ecebeb;margin-bottom: 0px">
								<thead>
									<tr>
										<th style="text-align: left;width: 30%">Agent Name</th>
										<th style="text-align: left;width: 40%">{{ $find_agent['name'] }}</th>
									</tr>

									<tr>
										<th style="text-align: left;width: 30%">Total Payable</th>
										<th style="text-align: left;width: 40%">{{ number_format($payable_sum,2,'.',',') }}</th>
									</tr>

									<tr>
										<th style="text-align: left;width: 30%">Total Paid</th>
										<th style="text-align: left;width: 40%">{{ number_format($paid_sum,2,'.',',') }}</th>
									</tr>

									<tr>
										<th style="text-align: left;width: 30%">Total Dues</th>
										<th style="text-align: left;width: 40%">{{ number_format($payable_sum - $paid_sum,2,'.',',') }}</th>
									</tr>

								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-8">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title" style="width:100%">Pay Comission</h3>
					</div>
					<div class="box-body">
						<form method="POST" action="{{ route('sales_comission_store') }}" enctype="multipart/form-data">
							{{ csrf_field() }}

							<input type="hidden" class="form-control" name="agent_id_send" value="{{ isset($_GET['agent_id']) ? $_GET['agent_id'] : 0 }}">

							<input type="hidden" name="payable" value="{{$payable_sum}}">
							
							<div class="form-group">
								<div class="col-md-6">
									<label>Amount*</label>
									<input id="amount" class="form-control" type="number" name="amount" required="required" oninput="calculate()">
								</div>

								<div class="col-md-6">
									<label>Date*</label>
									<input type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control datepicker" name="date" required="required">
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-12 table-responsive">
									<br>
									<table class="table table-bordered table-responsive text-center table-tr-style">
										<thead>
											<tr>
												<th style="text-align: left">Invoice#</th>
												<th style="text-align: left">Amount</th>
												<th style="text-align: left">Due</th>
												<th style="text-align: left;width: 100%">Paid</th>
											</tr>
										</thead>

										<tbody>
											@if(!empty($invoices) && (count($invoices) > 0))
											@foreach($invoices as $key1 => $invoice_value)
												@if(isset($invoice_wise_dues[$key1]) && ($invoice_wise_dues[$key1] > 0))
													<tr>
														<input class="form-control" type="hidden" name="invoice_id[]" value="{{ $invoice_value->id }}">
														<td style="text-align: left">
															{{ $invoice_value->invoice_number }}
														</td>
														<td style="text-align: left">
															{{ isset($invoice_wise_payable[$key1]) ? $invoice_wise_payable[$key1] : 0 }}
														</td>
														<td style="text-align: left">
															{{ isset($invoice_wise_dues[$key1]) ? $invoice_wise_dues[$key1] : 0 }}</td>
														<td style="text-align: left">
															<input style="width: 300px" id="paid_{{$key1}}" class="form-control" type="text" name="paid[]">
														</td>
													</tr>
												@endif
											@endforeach
											@endif
										</tbody>
									</table>
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-6">
									<br>
									<label>Payment Methode*</label>
									<select name="payment_methode" class="form-control" required="required">
										<option value="">--Select Payment Methode--</option>
										<option value="Cash">Cash</option>
										<option value="Bank">Bank</option>
										<option value="Mobile Banking/Others">Mobile Banking/Others</option>
									</select>
								</div>

								<div class="col-md-6">
									<br>
									<label>Paid Through</label>
									<input class="form-control" type="text" name="payment_through">
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-12">
									<br>
									<button type="submit" class="btn btn-success btn-block" style="border-radius: 0px">Save Payment</button>
									<br>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="box-body">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<a style="color: white;border-radius: 0px" class="col-md-5 col-sm-3">
				</a>
				<a id="historyShow" style="color: white;border-radius: 0px" class="btn btn-info col-md-2 col-sm-6 col-xs-12">
				    Show Payment History
				</a>
			</div>

			<br>
			<br>
		</div>

		<div class="box HistoryIndex">
			<div class="box-body">
				<div class="col-md-12 customAlign">
					<hr>
					<div class="table-responsive">
						<table id="dataTable" class="table table-bordered text-center table-tr-style">
							<thead>
								<tr style="background-color: #F9E79F">
									<th style="text-align: left">SL</th>
									<th style="text-align: left">Payment Date</th>
									<th style="text-align: left">Invoice Number</th>
									<th style="text-align: left">Payment Methode</th>
									<th style="text-align: left">Paid Through</th>
									<th style="text-align: right">Trans. Paid</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $total_paid = 0; ?>
								@foreach($transactions as $key => $transaction)
									
									<?php $total_paid = $total_paid + $transaction->trans_paid; ?>
									@if($transaction->trans_paid != 0)
										<tr>
											<td style="text-align: left">{{ $key + 1 }}</td>
											<td style="text-align: left">{{ date('d-m-Y', strtotime($transaction->date)) }}</td>
											<td style="text-align: left">{{ $transaction->invoice_number }}</td>
											<td style="text-align: left">{{ $transaction->payment_methode }}</td>
											<td style="text-align: left">{{ $transaction->payment_through }}</td>
											<td style="text-align: right">{{ number_format($transaction->trans_paid,2,'.',',') }}</td>
											<td>
												<a href="{{ route('sales_comission_delete', $transaction->id) }}" class="btn btn-danger btn-xs confirm_box"><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></a>
											</td>
										</tr>
									@endif
								@endforeach
								
							</tbody>
							<tr class="total" style="background-color: #F9E79F">
								<th style="text-align: right"></th>
								<th style="text-align: right"></th>
								<th style="text-align: right"></th>
								<th style="text-align: right"></th>
								<th style="text-align: right">Total Transaction</th>
								<th style="text-align: right">{{ number_format($total_paid,2,'.',',') }}</th>
								<th>
									<!-- <a class="btn btn-success btn-xs" href=""><i class="fa fa-download"></i> Download</a> -->
								</th>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>

	</section>

	<div class="modal fade reloadModal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="text-center">
	      <div style="text-align: -webkit-center" class="modal-body">
	        <div class="loader">
	        	
	        </div>
	      </div>
	    </div>
	  </div>
	</div>

@endsection

@push('scripts')
	<script type="text/javascript">
		$(".confirm_box").click(function(){
			if (!confirm("Do you want to delete")){
				return false;
			}
		});
	</script>

	<script type="text/javascript">
		function calculate()
		{	
			var invoice_wise_dues       = <?php echo collect(isset($invoice_wise_dues) ? $invoice_wise_dues : 0); ?>;
			var amount 					= $('#amount').val();
			var amount_left     		= parseFloat(amount);
			var total_due       		= 0;

			$.each(invoice_wise_dues, function(i, data )
			{	
				if(data > 0)
				{
					//Calculation for setup amount start
					var due_amount  = data;
					total_due      += parseFloat(data);

					if (amount_left > due_amount)
					{
						$('#paid_'+i).val(due_amount.toFixed(2));

						amount_left     -= parseFloat(due_amount);
					}
					else
					{
						if (amount_left > 0)
						{
							$('#paid_'+i).val(amount_left.toFixed(2));
							amount_left     -= parseFloat(due_amount);
						}
						else
						{
							$('#paid_'+i).val(0);
						}
					}
				}
				//Calculation for setup amount end
			});

			if (amount > total_due)
			{
				$('#amount').val(total_due.toFixed(2));
			}
		}
	</script>

	<script type="text/javascript">
		$(document).ready(function()
		{
		   	$(".HistoryIndex").hide();
		});
	</script>

	<script type="text/javascript">
		$("#historyShow").click(function(){
		  $(".HistoryIndex").toggle();
		});
	</script>

	<script type="text/javascript">
		function selectEvent()
		{	
			$('.reloadModal').modal('show');
			$('#searbButton').click();
		}
	</script>
@endpush