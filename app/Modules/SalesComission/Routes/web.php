<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'salesComission', 'middleware' => 'auth'], function () {
    Route::get('/index', 'SalesComissionController@index')->name('sales_comission_index');
    Route::get('/create', 'SalesComissionController@create')->name('sales_comission_create');
    Route::post('/store', 'SalesComissionController@store')->name('sales_comission_store');
    Route::get('/edit/{id}', 'SalesComissionController@edit')->name('sales_comission_edit');
    Route::post('/update/{id}', 'SalesComissionController@update')->name('sales_comission_update');
    Route::get('/delete/{id}', 'SalesComissionController@destroy')->name('sales_comission_delete');
});
