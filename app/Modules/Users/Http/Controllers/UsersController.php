<?php

namespace App\Modules\Users\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use DB;
use Auth;
use Validator;
use App\Models\Users;
use App\Models\Agents;
use Hash;

class UsersController extends Controller
{	
	public function index()
	{
		//Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('users_index');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

		$users = Users::orderBy('name', 'asc')->get();

		return view('users::index', compact('users'));
	}

    public function create()
    {
    	//Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('users_create');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End
        $agents  = Agents::where('status', 1)->orderBy('name', 'ASC')->get();

		return view('users::create', compact('agents'));
	}

	public function store(Request $request)
	{
		//Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('users_store');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

		$rules = array(
			'email' 	=> ['required', 'string', 'email', 'max:255', 'unique:users'],
			'password'  => 'required|string|min:6|confirmed',
			);

		$validation = Validator::make(\Request::all(),$rules);
		if ($validation->fails()) {
			return redirect()->back()->withInput()->withErrors($validation);
		}

		DB::beginTransaction();

		try{

			if (!empty($request->agent_id) || $request->agent_id != null)
			{
				$agent          = Agents::find($request->agent_id);
				$agent_name     = $agent->name;
			}
			else
			{
				$agent_name     = $request->name;
			}

			$users   			= new Users;
			$users->name   		= $agent_name;
			$users->agent_id    = $request->agent_id;
			$users->address   	= $request->address;
			$users->designation = $request->designation;
			$users->mobile   	= $request->mobile;
			$users->email   	= $request->email;
			$users->role   		= $request->role;
			$users->password	= Hash::make($request->password);
			$users->status   	= $request->status;
			$users->created_by  = Auth::user()->id;

			if($request->hasFile('photo'))
		    {
		      	$image 				= $request->file('photo');
				$image_name 		= time().'.'.$image->getClientOriginalExtension();
				$file_path 			= 'assets/images/Users/';
				$rowImage 			= $image;
				$savingPath 		= $file_path;
				$imageName 			= $image_name;
				$resizeImage 		= $this->compressAndResize($rowImage, $savingPath, $imageName, 50, 250,250);
				$users->photo  		= $image_name;
		    }

		    if($users->save())
		    {
		    	DB::commit();
				return redirect()->route('users_index')->with('message','Successfully added !');
		    }
			

		}catch(\Exception $e){
			dd($e);
			DB::rollback();
			return redirect()->route('users_index')->with('message','Something went wrong.Try again !');
		}
	}

	public function edit($id)
    {
    	//Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('users_edit');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

    	$user 		= Users::find($id);
    	$agents  	= Agents::where('status', 1)->orderBy('name', 'ASC')->get();

		return view('users::edit', compact('user', 'agents'));
	}

	public function update(Request $request, $id)
	{
		//Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('users_update');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

		$rules = array(
			);

		$validation = Validator::make(\Request::all(),$rules);
		if ($validation->fails()) {
			return redirect()->back()->withInput()->withErrors($validation);
		}

		DB::beginTransaction();

		try{

			if (!empty($request->agent_id) || $request->agent_id != null)
			{
				$agent          = Agents::find($request->agent_id);
				$agent_name     = $agent->name;
			}
			else
			{
				$agent_name     = $request->name;
			}
			
			$users   			= Users::find($id);
			$users->name   		= $agent_name;
			$users->agent_id    = $request->role == 3 ? $request->agent_id : null;
			$users->address   	= $request->address;
			$users->designation = $request->designation;
			$users->mobile   	= $request->mobile;
			$users->role   		= $request->role;
			$users->status   	= $request->status;
			$users->created_by  = Auth::user()->id;

			if($request->hasFile('photo'))
		    {	
		    	if($users->photo != null)
		    	{
		    		unlink('assets/images/Users/'.$users->photo);
		    	}

		      	$image 				= $request->file('photo');
				$image_name 		= time().'.'.$image->getClientOriginalExtension();
				$file_path 			= 'assets/images/Users/';
				$rowImage 			= $image;
				$savingPath 		= $file_path;
				$imageName 			= $image_name;
				$resizeImage 		= $this->compressAndResize($rowImage, $savingPath, $imageName, 50, 250,250);
				$users->photo  		= $image_name;
		    }

		    if($users->save())
		    {
		    	DB::commit();
				return redirect()->route('users_index')->with('message','Successfully updated !');
		    }
			

		}catch(\Exception $e){
			dd($e);
			DB::rollback();
			return redirect()->route('users_index')->with('message','Something went wrong.Try again !');
		}
	}

	public function compressAndResize($source_url, $destination_url, $imageName , $quality, $desired_width, $desired_height)
    {
		$info = getimagesize($source_url);
		if ($info['mime'] == 'image/jpeg')
		{
			list($width, $height) = getimagesize($source_url);
			$src = imagecreatefromjpeg($source_url);
			$dst = imagecreatetruecolor($desired_width, $desired_height);
			imagecopyresampled($dst, $src, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
			imagejpeg($dst, $destination_url.$imageName, $quality);
		}
		elseif ($info['mime'] == 'image/png'){
			list($width, $height) = getimagesize($source_url);
			$src = imagecreatefrompng($source_url);
			$dst = imagecreatetruecolor($desired_width, $desired_height);
			imagecopyresampled($dst, $src, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
			imagejpeg($dst, $destination_url.$imageName, $quality);
		}
		elseif ($info['mime'] == 'image/gif'){
			list($width, $height) = getimagesize($source_url);
			$src = imagecreatefromgif($source_url);
			$dst = imagecreatetruecolor($desired_width, $desired_height);
			imagecopyresampled($dst, $src, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
			imagejpeg($dst, $destination_url.$imageName, $quality);
		}
		return $destination_url.$imageName;
	}
}
