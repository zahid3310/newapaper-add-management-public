@extends('layouts.app')

@section('title', 'Users List')

@section('content')
	<style type="text/css">
		.customAlign table thead tr th{
			vertical-align: middle;
		}
		.table-tr-style tr:nth-child(even) {
			background-color: #dddddd;
		}
		.table-tr-style tr:nth-child(odd) {
			background-color: #F2F3F4;
		}
		.total th{
			font-size: 20px
		}
		.select2-container .select2-selection--single{
			height: auto
		}
		.select2-container--default .select2-selection--single .select2-selection__rendered{
			line-height: 20px
		}
		.select2-container .select2-selection--single .select2-selection__rendered{
			margin-top: 0px
		}
	</style>

	<section class="content">
		<div class="box">
			
			<div class="box-header with-border">
			    <div class="col-md-12">
				<h3 class="box-title" style="width:100%">
						Users List <a style="color: white;border-radius: 0px" href="{{ route('users_create') }}" class="btn btn-success btn-xs pull-right">
					   <i class="fa fa-plus"></i> Add New
					</a>
				</h3>
				</div>
			</div>

			@if(count($users) >0)
			<div class="box-body">
				<div class="row">
					@if(Session::has('message'))
					<div class="col-md-12">
						<div class="alert alert-success alert-dismissable text-center">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							{{Session::get('message')}}
						</div>
					</div>
					@endif
					@if(Session::has('errors'))
					<div class="col-md-12">
						<div class="alert alert-success alert-dismissable text-center">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							{{Session::get('errors')}}
						</div>
					</div>
					@endif
				</div>

				<div class="col-md-12 customAlign table-responsive">

					<table id="dataTable" class="table table-bordered table-striped dataTable">
						<thead>
							<tr style="background-color: #F9E79F">
								<th>SL</th>
								<th>Photo</th>
								<th>Name</th>
								<th>Role</th>
								<th>Designation</th>
								<th>Address</th>
								<th>Email</th>
								<th>Mobile</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						@if(!empty($users) && (count($users)>0) )
						@foreach($users as $key => $user)
							<tr>
								<td>{{ $key + 1 }}</td>
								<td>
									@if($user->photo != null)
										<img src="{{ asset('assets/images/Users/'.$user->photo) }}" style="height: 30px; width: 40px">
									@else
										<img src="{{ asset('assets/images/Users/default-user-photo.png') }}" style="height: 30px; width: 40px">
									@endif
								</td>
								<td>{{ $user->name }}</td>
								<td>
									@if($user->role == 1)
										Admin
									@elseif($user->role == 2)
										Employee
									@elseif($user->role == 3)
										Agent
									@endif
								</td>
								<td>{{ $user->designation }}</td>
								<td>{{ $user->address }}</td>
								<td>{{ $user->email }}</td>
								<td>{{ $user->mobile }}</td>
								<td>{{ $user->status == 1 ? 'Active' : 'Inactive' }}</td>
								<td>
									<a href="{{ route('users_edit', $user->id) }}" class="btn btn-success btn-xs"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i></a>
								</td>
							</tr>
						@endforeach
						@endif
						</tbody>
					</table>
				</div>
		    </div>
		    @else
		    <div class="box-body">
		    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
		    	<p style="text-align: center;font-size: 18px">No Data Found.<br>Please Insert User Information.</p>
		    </div>
		    </div>
		    @endif
		</div>
	</section>
@endsection

@push('scripts')
	<script type="text/javascript">
		$(".confirm_box").click(function()
		{
			if (!confirm("Do you want to delete"))
			{
				return false;
			}
		});
	</script>
@endpush