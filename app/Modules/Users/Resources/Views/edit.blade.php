@extends('layouts.app')

@section('title', 'Edit User')

@section('content')
	<section class="content">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
				Edit User
				</h3>
			</div>
			<div class="box-body">
				@if(Session::has('message'))
				<div style="padding: 0px" class="col-md-12">
					<div class="alert alert-success alert-dismissable text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{Session::get('message')}}
					</div>
				</div>
				@endif
				@if(Session::has('errors'))
				<div style="padding: 0px" class="col-md-12">
					<div class="alert alert-success alert-dismissable text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{Session::get('errors')}}
					</div>
				</div>
				@endif
				<form class="form-horizontal" method="POST" action="{{ route('users_update', $user->id) }}" enctype="multipart/form-data">
					{{ csrf_field() }}

					<div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
						<label for="name" class="col-md-4 control-label">
						Role 
						</label>

						<div class="col-md-3">
							<select id="role" class="form-control" name="role" onchange="getVal()">
									<option {{ $user->role == 1 ? 'selected' : '' }} value="1">Admin</option>
									<option {{ $user->role == 2 ? 'selected' : '' }} value="2">Employee</option>
									<option {{ $user->role == 3 ? 'selected' : '' }} value="3">Agent</option>
							</select>
							@if ($errors->has('role'))
							<span class="help-block">
								<strong>{{ $errors->first('role') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div style="display: none" class="form-group{{ $errors->has('agent_id') ? ' has-error' : '' }} agents">
						<label for="name" class="col-md-4 control-label">
						Agents  
						</label>

						<div class="col-md-6">
							<select class="form-control select2 select2-hidden-accessible" style="width: 100%;padding: 15px" tabindex="-1" aria-hidden="true" name="agent_id">
									<option style="padding: 15px" value="" selected>Select Agent</option>
								@foreach($agents as $agent)
									<option {{ $agent->id == $user->agent_id ? 'selected' : '' }} style="padding: 15px" value="{{ $agent->id }}">{{ $agent->name }}</option>
								@endforeach
							</select>

							@if ($errors->has('agent_id'))
							<span class="help-block">
								<strong>{{ $errors->first('agent_id') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div style="display: none" class="form-group{{ $errors->has('name') ? ' has-error' : '' }} name">
						<label for="name" class="col-md-4 control-label">
						Name *
						</label>

						<div class="col-md-6">
							<input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}" >

							@if ($errors->has('name'))
							<span class="help-block">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('designation') ? ' has-error' : '' }}">
						<label for="designation" class="col-md-4 control-label">
						Designation
						</label>

						<div class="col-md-6">
							<input id="designation" type="text" class="form-control" name="designation" value="{{ $user->designation }}">

							@if ($errors->has('designation'))
							<span class="help-block">
								<strong>{{ $errors->first('designation') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
						<label for="address" class="col-md-4 control-label">
						Address
						</label>

						<div class="col-md-6">
							<input id="address" type="text" class="form-control" name="address" value="{{ $user->address }}">

							@if ($errors->has('address'))
							<span class="help-block">
								<strong>{{ $errors->first('address') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
						<label for="mobile" class="col-md-4 control-label">
						Mobile
						</label>

						<div class="col-md-6">
							<input id="mobile" type="text" class="form-control" name="mobile" value="{{ $user->mobile }}">

							@if ($errors->has('mobile'))
							<span class="help-block">
								<strong>{{ $errors->first('mobile') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
						<label for="name" class="col-md-4 control-label">
						Status 
						</label>

						<div class="col-md-3">
							<select class="form-control" name="status">
									<option  {{ $user->status == 1 ? 'selected' : '' }} value="1">Active</option>
									<option  {{ $user->status == 0 ? 'selected' : '' }} value="0">Inactive</option>
							</select>
							@if ($errors->has('status'))
							<span class="help-block">
								<strong>{{ $errors->first('status') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
						<label for="photo" class="col-md-4 control-label">
						Old Photo
						</label>

						<div class="col-md-6">
							@if($user->photo != null)
								<img src="{{ asset('assets/images/users/'.$user->photo) }}" style="height: 250px; width: 250px">
							@else
								<img src="{{ asset('assets/images/users/default-customer-photo.png') }}" style="height: 250px; width: 250px">
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
						<label for="photo" class="col-md-4 control-label">
						Photo
						</label>

						<div class="col-md-6">
							<input id="photo" type="file" class="form-control" name="photo">

							@if ($errors->has('photo'))
							<span class="help-block">
								<strong>{{ $errors->first('photo') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<hr>

					<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
						<label for="email" class="col-md-4 control-label">
						Email *
						</label>

						<div class="col-md-6">
							<input id="email" type="text" class="form-control" name="email" value="{{ $user->email }}" readonly>

							@if ($errors->has('email'))
							<span class="help-block">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
			          <label for="password" class="col-md-4 control-label">Password *</label>

			          <div class="col-md-6">
			            <input id="password" type="password" class="form-control" name="password" value="******" readonly>

			            @if ($errors->has('password'))
			            <span class="help-block">
			              <strong>{{ $errors->first('password') }}</strong>
			            </span>
			            @endif
			          </div>
			        </div>

			        <div class="form-group">
			          <label for="password-confirm" class="col-md-4 control-label">Confirm Password *</label>

			          <div class="col-md-6">
			            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" value="******" readonly>
			            @if ($errors->has('password_confirmation'))
			            <span class="help-block">
			              <strong>{{ $errors->first('password_confirmation') }}</strong>
			            </span>
			            @endif
			          </div>
			        </div>

					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
							<button type="submit" class="btn btn-primary">
								<i class="fa fa-plus-circle"></i>
							Register
							</button>
							<a href="{{ route('users_index') }}" class="btn btn-danger">
								<i class="fa fa-trash"></i>
							Cancel
							</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
@endsection

@push('scripts')
	<script type="text/javascript">
		$( document ).ready(function()
		{
		    var role_id = $('#role').val();

		    if(role_id == 3)
		    {
		    	$(".agents").show();
		    }
		    else
		  	{
		  		$(".agents").hide();
		  	}

		    if(role_id == 1 || role_id == 2)
		    {
		    	$(".name").show();
		    }
		    else
		  	{
		  		$(".name").hide();
		  	}
		});
	</script>

	<script type="text/javascript">
		$('#role').on('change', function()
		{
		  	if(this.value == 3)
		  	{	
		  		$(".agents").show();
		  	}
		  	else
		  	{
		  		$(".agents").hide();
		  	}

		  	if(this.value == 1 || this.value == 2)
		  	{	
		  		$(".name").show();
		  	}
		  	else
		  	{
		  		$(".name").hide();
		  	}
		});
	</script>
@endpush