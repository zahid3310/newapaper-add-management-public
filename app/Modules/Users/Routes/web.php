<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'users', 'middleware' => 'auth'], function () {
	Route::get('/customer-list', 'UsersController@index')->name('users_index');
    Route::get('/customer-create', 'UsersController@create')->name('users_create');
	Route::post('/customer-store', 'UsersController@store')->name('users_store');
	Route::get('/customer-edit/{id}', 'UsersController@edit')->name('users_edit');
	Route::post('/customer-update/{id}', 'UsersController@update')->name('users_update');
	Route::get('/customer-delete/{id}', 'UsersController@delete')->name('users_delete');

});
