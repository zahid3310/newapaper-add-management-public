@extends('layouts.app')

@section('title', 'Agents List')

@section('content')
<style type="text/css">
	.customAlign table thead tr th{
		vertical-align: middle;
	}
	.table-tr-style tr:nth-child(even) {
		background-color: #dddddd;
	}
	.table-tr-style tr:nth-child(odd) {
		background-color: #F2F3F4;
	}
	.total th{
		font-size: 20px
	}
	.select2-container .select2-selection--single{
		height: auto
	}
	.select2-container--default .select2-selection--single .select2-selection__rendered{
		line-height: 20px
	}
	.select2-container .select2-selection--single .select2-selection__rendered{
		margin-top: 0px
	}
</style>

<section class="content">

	<div class="row">
		<div class="col-md-12">
			<div class="row">
				@if(Session::has('message'))
				<div class="col-md-12">
					<div class="alert alert-success alert-dismissable text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{Session::get('message')}}
					</div>
				</div>
				@endif
				@if(Session::has('errors'))
				<div class="col-md-12">
					<div class="alert alert-success alert-dismissable text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{Session::get('errors')}}
					</div>
				</div>
				@endif
			</div>
		</div>

		<div class="col-md-4">	
			<div class="box">
				<div class="col-md-12">
					<h3 class="box-title" style="width: 100%">Add New Agent</h3>
					<hr>
				</div>

				<div class="box-body">
					<form class="form-horizontal" method="POST" action="{{ route('agents_store') }}" enctype="multipart/form-data">
						{{ csrf_field() }}

						<div class="form-group{{ $errors->has('area') ? ' has-error' : '' }}">
							<div class="col-md-12">
								<label for="name" class="control-label">
									Agents Area  
								</label>
								<select class="form-control select2 select2-hidden-accessible" style="width: 100%;padding: 15px" tabindex="-1" aria-hidden="true" name="area">
									<option style="padding: 15px" value="" selected>Select Area</option>
									@foreach($areas as $area)
									<option style="padding: 15px" value="{{ $area->id }}">{{ $area->name }}</option>
									@endforeach
								</select>

								@if ($errors->has('area'))
								<span class="help-block">
									<strong>{{ $errors->first('area') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<div class="col-md-12">
								<label for="name" class="control-label">
									Name *
								</label>
								<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

								@if ($errors->has('name'))
								<span class="help-block">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('intro') ? ' has-error' : '' }}">
							<div class="col-md-12">
								<label for="intro" class="control-label">
									Introduction
								</label>
								<input id="intro" type="text" class="form-control" name="intro" value="{{ old('intro') }}">

								@if ($errors->has('intro'))
								<span class="help-block">
									<strong>{{ $errors->first('intro') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
							<div class="col-md-12">
								<label for="code" class="control-label">
									Code
								</label>
								<input id="code" type="text" class="form-control" name="code" value="{{ old('code') }}">

								@if ($errors->has('code'))
								<span class="help-block">
									<strong>{{ $errors->first('code') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
							<div class="col-md-12">
								<label for="address" class="control-label">
									Address
								</label>
								<input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}">

								@if ($errors->has('address'))
								<span class="help-block">
									<strong>{{ $errors->first('address') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<div class="col-md-12">
								<label for="email" class="control-label">
									Email
								</label>
								<input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}">

								@if ($errors->has('email'))
								<span class="help-block">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
							<div class="col-md-12">
								<label for="mobile" class="control-label">
									Mobile
								</label>
								<input id="mobile" type="text" class="form-control" name="mobile" value="{{ old('mobile') }}">

								@if ($errors->has('mobile'))
								<span class="help-block">
									<strong>{{ $errors->first('mobile') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
							<div class="col-md-12">
								<label for="photo" class="control-label">
									Photo
								</label>
								<input id="photo" type="file" class="form-control" name="photo">

								@if ($errors->has('photo'))
								<span class="help-block">
									<strong>{{ $errors->first('photo') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<hr>

						<div class="form-group{{ $errors->has('billing_info') ? ' has-error' : '' }}">
							<div class="col-md-12">
								<label for="billing_info" class="control-label">
									Billing Information
								</label>
								<textarea id="billing_info" type="text" class="form-control" name="billing_info">{{ old('billing_info') }}</textarea> 

								@if ($errors->has('billing_info'))
								<span class="help-block">
									<strong>{{ $errors->first('billing_info') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-plus-circle"></i>
									Save
								</button>
								<a href="{{ route('agents_index') }}" class="btn btn-danger">
									<i class="fa fa-trash"></i>
									Cancel
								</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<div class="col-md-8">	
			<div class="box">
				<div class="col-md-12">
					<h3 class="box-title" style="width: 100%">Agent List</h3>
					<hr>
				</div>

				<div class="box-body">
					@if(count($agents) >0)
					<div class="box-body">
						<div class="col-md-12 customAlign table-responsive">

							<table id="dataTable" class="table table-bordered table-striped dataTable">
								<thead>
									<tr style="background-color: #F9E79F">
										<th>SL</th>
										<th>Photo</th>
										<th>Name</th>
										<th>Code</th>
										<th>District</th>
										<th>Area</th>
										<!-- <th>Introduction</th>
										<th>Address</th> -->
										<th>Email</th>
										<th>Mobile</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@if(!empty($agents) && (count($agents)>0) )
									@foreach($agents as $key => $agent)
									<tr>
										<td>{{ $key + 1 }}</td>
										<td>
											@if($agent->photo != null)
											<img src="{{ asset('assets/images/agents/'.$agent->photo) }}" style="height: 30px; width: 30px;border-radius: 50%">
											@else
											<img src="{{ asset('assets/images/agents/default-agent-photo.png') }}" style="height: 30px; width: 30px;border-radius: 50%">
											@endif
										</td>
										<td>{{ $agent->name }}</td>
										<td>{{ $agent->code }}</td>
										<td>{{ isset($agent->districtName->name) ? $agent->districtName->name : '' }}</td>
										<td>{{ isset($agent->areaName->name) ? $agent->areaName->name : '' }}</td>
										<!-- <td>{{ $agent->intro }}</td>
										<td>{{ $agent->address }}</td> -->
										<td>{{ $agent->email }}</td>
										<td>{{ $agent->mobile }}</td>
										<td>
											<a href="{{ route('agents_edit', $agent->id) }}" class="btn btn-success btn-xs"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i></a>
											<a href="{{ route('agents_delete', $agent->id) }}" class="btn btn-danger btn-xs confirm_box"><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></a>
										</td>
									</tr>
									@endforeach
									@endif
								</tbody>
							</table>
						</div>
					</div>
					@else
					<div class="box-body">
						<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
							<p style="text-align: center;font-size: 18px">No Data Found.<br>Please Insert Agent Information.</p>
						</div>
					</div>
					@endif
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@push('scripts')
<script type="text/javascript">
	$(".confirm_box").click(function()
	{
		if (!confirm("Do you want to delete"))
		{
			return false;
		}
	});
</script>
@endpush