<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'agents', 'middleware' => 'auth'], function () {
    Route::get('/index', 'AgentsController@index')->name('agents_index');
    Route::get('/create', 'AgentsController@create')->name('agents_create');
    Route::post('/store', 'AgentsController@store')->name('agents_store');
    Route::get('/edit/{id}', 'AgentsController@edit')->name('agents_edit');
    Route::post('/update/{id}', 'AgentsController@update')->name('agents_update');
    Route::get('/delete/{id}', 'AgentsController@destroy')->name('agents_delete');
});
