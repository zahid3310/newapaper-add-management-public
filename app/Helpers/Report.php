<?php
namespace App\Helpers;
use Auth;
use App\Models\Invoices;
use App\Models\Transactions;
use App\Models\Payments;

class Report
{
  	public function PaymentMethode($invoice_id)
  	{	 
      $transactions   = Transactions::where('invoice_id', $invoice_id)
                                    ->where('status', 1)
                                    ->where('type', 'sales_comission')
                                    ->selectRaw('GROUP_CONCAT(payment_methode SEPARATOR "<br>") as payment_methode, GROUP_CONCAT(payment_through SEPARATOR "<br>") as payment_through')
                                    ->groupBy('invoice_id')
                                    ->first();

  		return $transactions;
  	}

}