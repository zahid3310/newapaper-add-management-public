<?php
namespace App\Helpers;
use Auth;
class AccessLevel
{
  	public function test($route)
  	{	
  		//All Routes start

	  		//Agents Module Start
	  			$agent_index 	        = 'agents_index';
	  			$agent_create         = 'agents_create';
	  			$agent_store 	        = 'agents_store';
	  			$agent_edit 	        = 'agents_edit';
	  			$agent_update         = 'agents_update';
          $agent_delete         = 'agents_delete';
	  			$agent_invoice_show   = 'agent-invoice-show';
  			//Agents Module End

	  		//Areas Module Start
	  			$reporter_area_index 	= 'reporter_area_index';
	  			$reporter_area_create = 'reporter_area_create';
	  			$reporter_area_store 	= 'reporter_area_store';
	  			$reporter_area_edit 	= 'reporter_area_edit';
	  			$reporter_area_update = 'reporter_area_update';
	  			$reporter_area_delete = 'reporter_area_delete';
  			//Areas Module End

	  		//Customers Module Start
	  			$customer_index 	= 'customers_index';
	  			$customer_create 	= 'customers_create';
	  			$customer_store 	= 'customers_store';
	  			$customer_edit 		= 'customers_edit';
	  			$customer_update 	= 'customers_update';
	  			$customer_delete 	= 'customers_delete';
  			//Customers Module End

	  		//Invoice Module Start
	  			$invoice_index 		= 'invoices_index';
	  			$invoice_create 	= 'invoices_create';
	  			$invoice_store 		= 'invoices_store';
	  			$invoice_edit 		= 'invoices_edit';
	  			$invoice_update 	= 'invoices_update';
	  			$invoice_delete 	= 'invoices_delete';
	  			$invoice_show   	= 'invoices_show';
	  			$invoice_download = 'invoices_download';
  			//Invoice Module End

	  		//Item Module Start
	  			$item_index 	= 'items_index';
	  			$item_create 	= 'items_create';
	  			$item_store 	= 'items_store';
	  			$item_edit 		= 'items_edit';
	  			$item_update 	= 'items_update';
	  			$item_delete 	= 'items_delete';

	  			$category_index 	= 'category_index';
	  			$category_create 	= 'category_create';
	  			$category_store 	= 'category_store';
	  			$category_edit 		= 'category_edit';
	  			$category_update 	= 'category_update';
	  			$category_delete 	= 'category_delete';
  			//Item Module End

	  		//Payment Receive Module Start
	  			$payment_receive_index 			    = 'payment_receive_index';
	  			$payment_receive_create 		    = 'payment_receive_create';
	  			$payment_receive_store 			    = 'payment_receive_store';
	  			$payment_receive_edit 			    = 'payment_receive_edit';
	  			$payment_receive_update 		    = 'payment_receive_update';
	  			$payment_receive_delete 		    = 'payment_receive_delete';
	  			$payment_receive_all_invoices   = 'payment_receive_all_invoices';
          $payment_receive_get_invoice    = 'payment_receive_get_invoice';
	  			$payment_receive_download_file 	= 'payment_receive_download_uploaded_file';
  			//Payment Receive Module End

	  		//Sales Comission Module Start
	  			$sales_comission_index 			= 'sales_comission_index';
	  			$sales_comission_create 		= 'sales_comission_create';
	  			$sales_comission_store 			= 'sales_comission_store';
	  			$sales_comission_edit 			= 'sales_comission_edit';
	  			$sales_comission_update 		= 'sales_comission_update';
	  			$sales_comission_delete 		= 'sales_comission_delete';
  			//Sales Comission Module End

	  		//Users Module Start
	  			$user_index 		= 'users_index';
	  			$user_create 		= 'users_create';
	  			$user_store 		= 'users_store';
	  			$user_edit 			= 'users_edit';
	  			$user_update 		= 'users_update';
	  			$user_delete 		= 'users_delete';
  			//Users Module End

	  		//Report Module Start
	  			$customer_report_index 							            = 'customer_report_index';
	  			$customer_wise_details_report_index 			      = 'customer_wise_details_report_index';
	  			$customer_report_index_download 				        = 'customer_report_index_download';
	  			$customer_wise_details_report_index_download 	  = 'customer_wise_details_report_index_download';

	  			$sales_comission_report_index 					        = 'sales_comission_report_index';
	  			$sales_comission_details_report_index 			    = 'sales_comission_details_report_index';
	  			$sales_comission_report_index_download 			    = 'sales_comission_report_index_download';
	  			$sales_comission_details_report_index_download 	= 'sales_comission_details_report_index_download';

          $paynent_report_index           = 'paynent_report_index';
          $payment_report_index_download  = 'payment_report_index_download';
  			//Report Module End

        //Personal Account Start
          $personal_account_index     = 'personal_account_index';
          $personal_account_create    = 'personal_account_create';
          $personal_account_store     = 'personal_account_store';
          $personal_account_edit      = 'personal_account_edit';
          $personal_account_update    = 'personal_account_update';
          $personal_account_delete    = 'personal_account_delete';

          $personal_account_category_index     = 'personal_account_category_index';
          $personal_account_category_create    = 'personal_account_category_create';
          $personal_account_category_store     = 'personal_account_category_store';
          $personal_account_category_edit      = 'personal_account_category_edit';
          $personal_account_category_update    = 'personal_account_category_update';
          $personal_account_category_delete    = 'personal_account_category_delete';

          $personal_transaction_report_index                      = 'personal_transaction_report_index';
          $personal_transaction_report_index_download             = 'personal_transaction_report_index_download';
          $personal_transaction_report_report_details             = 'personal_transaction_report_report_details';
          $personal_transaction_report_report_details_download    = 'personal_transaction_report_report_details_download';
        //Personal Account End

        //Reminder Start
          $reminder_index     = 'reminder_index';
          $reminder_create    = 'reminder_create';
          $reminder_store     = 'reminder_store';
          $reminder_edit      = 'reminder_edit';
          $reminder_update    = 'reminder_update';
          $reminder_delete    = 'reminder_delete';
        //Reminder End

  		//All Routes End

  		if (Auth::user()->role == 3)
  		{
  			if ( ($route == $customer_index) || 
  				($route == $customer_create) || 
  				($route == $customer_store) || 
  				($route == $customer_edit) || 
  				($route == $customer_update) || 
  				($route == $customer_delete) || 
  				($route == $agent_index) || 
  				($route == $agent_create) || 
  				($route == $agent_store) || 
  				($route == $agent_edit) || 
  				($route == $agent_update) || 
  				($route == $agent_delete) || 
  				($route == $reporter_area_index) || 
  				($route == $reporter_area_create) || 
  				($route == $reporter_area_store) || 
  				($route == $reporter_area_edit) || 
  				($route == $reporter_area_update) || 
  				($route == $reporter_area_delete) || 
  				($route == $invoice_index) || 
  				($route == $invoice_create) || 
  				($route == $invoice_store) || 
  				($route == $invoice_edit) || 
  				($route == $invoice_update) || 
  				($route == $invoice_delete) ||
          ($route == $invoice_show) ||
  				($route == $item_index) || 
  				($route == $item_create) || 
  				($route == $item_store) || 
  				($route == $item_edit) ||
  				($route == $item_update) || 
  				($route == $item_delete) || 
  				($route == $category_index) || 
  				($route == $category_create) || 
  				($route == $category_store) || 
  				($route == $category_edit) || 
  				($route == $category_update) || 
  				($route == $category_delete) || 
  				($route == $payment_receive_index) || 
  				($route == $payment_receive_create) || 
  				($route == $payment_receive_store) || 
  				($route == $payment_receive_edit) || 
  				($route == $payment_receive_update) || 
  				($route == $payment_receive_delete) || 
  				($route == $sales_comission_index) || 
  				($route == $sales_comission_create) || 
  				($route == $sales_comission_store) || 
  				($route == $sales_comission_edit) || 
  				($route == $sales_comission_update) || 
  				($route == $sales_comission_delete) || 
  				($route == $user_index) || 
  				($route == $user_create) || 
  				($route == $user_store) || 
  				($route == $user_edit) || 
  				($route == $user_update) || 
  				($route == $user_delete) || 
  				($route == $customer_report_index) || 
  				($route == $customer_wise_details_report_index) || 
  				($route == $customer_report_index_download) || 
  				($route == $customer_wise_details_report_index_download) || 
  				($route == $sales_comission_report_index) || 
  				($route == $sales_comission_details_report_index) || 
  				($route == $sales_comission_report_index_download) || 
          ($route == $sales_comission_details_report_index_download) ||
          ($route == $paynent_report_index) ||
  				($route == $payment_report_index_download) ||
          ($route == $personal_account_index) ||
          ($route == $personal_account_create) ||
          ($route == $personal_account_store) ||
          ($route == $personal_account_edit) ||
          ($route == $personal_account_update) ||
          ($route == $personal_account_delete) ||
          ($route == $personal_account_category_index) ||
          ($route == $personal_account_category_create) ||
          ($route == $personal_account_category_store) ||
          ($route == $personal_account_category_edit) ||
          ($route == $personal_account_category_update) ||
          ($route == $personal_account_category_delete ) ||
          ($route == $personal_transaction_report_index ) ||
          ($route == $personal_transaction_report_index_download ) ||
          ($route == $personal_transaction_report_report_details ) ||
          ($route == $personal_transaction_report_report_details_download ) || 
          ($route == $agent_invoice_show )||
          ($route == $reminder_index) || 
          ($route == $reminder_create) || 
          ($route == $reminder_store) || 
          ($route == $reminder_edit) || 
          ($route == $reminder_update) || 
          ($route == $reminder_delete) )
  			{
  				return "No";
  			}
        else{
          return "accepted";
        }
  		}	
  		else
  		{
  				return "accepted";
  		}
  	}

}