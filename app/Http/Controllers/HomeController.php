<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Invoices;
use App\Models\InvoiceDetails;
use App\Models\Transactions;
use App\Models\Items;
use App\Models\Users;
use App\Models\Notice;
use App\Models\Reminder;
use DB;
use Auth;
use Validator;
use Hash;
use Response;
use App\Models\Payments;
use App\Models\Customers;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function home()
    {   
        if (Auth::user()->role == 1)
        {   
            $invoices           = Invoices::where('status', 1)->get();

            $total_receivable   = $invoices->sum('total_amount');
            $total_dues         = $invoices->sum('due_amount');
            $total_expense      = $invoices->sum('other_expense');
            $total_received     = $total_receivable - $total_dues - $total_expense;
            $total_invoices     = $invoices->count();
            
            $inv_sale_com       =  Invoices::join('transactions', 'transactions.invoice_id', 'invoices.id')
            								->where('invoices.status', 1)
            								->where('transactions.status', 1)
            								->whereNotNull('invoices.agent_id')
            								->where('invoices.commission_amount', '>', 0)
            								->where('transactions.type', 'invoice')
            								->where('transactions.trans_paid', '>', 0)
                                            ->selectRaw('invoices.*')
                                            ->orderBy('created_at', 'ASC')
            								->get();    
            								
            
            $total_sales_comission_payable      = 0;
            foreach($inv_sale_com as $key1 => $value)
            {   
                $invoice_details                = InvoiceDetails::join('transactions', 'transactions.invoice_id', 'invoices_details.invoice_id')
                                                                ->where('invoices_details.invoice_id', $value->id)
                                                                ->where('transactions.type', 'invoice')
                                                                ->selectRaw('(CASE WHEN invoices_details.commission_type = 1 THEN invoices_details.amount ELSE ((transactions.trans_paid*invoices_details.commission_amount)/100) END) AS com_amount')
                                                                ->get();
                                                        
                $comission_payable              = $invoice_details->sum('com_amount');
                $other_expense_payable          = 0;
                // $comission_payable              = $comission_payables - (($comission_payables*$other_expense_payable)/($value->total_amount));
                $total_sales_comission_payable  = $total_sales_comission_payable + $comission_payable;
            }

            $total_sales_comission_paid         = Transactions::where('status', 1)->where('type', 'sales_comission')->sum('trans_paid');
            $total_sales_comission_dues         = $total_sales_comission_payable - $total_sales_comission_paid;

            $total_sell                         = Items::where('status', 1)->sum('sell');
            $notices                            = Notice::where('status', 1)->orderBy('created_at','DESC')->simplePaginate(5);

            $reminder_1                         = Reminder::where('status', 1)->orderBy('created_at','DESC')->where('date', date('Y-m-d'))->where('reminder_status', '=', 1)->orWhere('reminder_status', '=', 0)->get()->toArray();
            $reminder_2                         = Reminder::where('status', 1)->orderBy('created_at','DESC')->where('reminder_status', '=', 2)->get()->toArray();
            $reminders                          = array_merge($reminder_1, $reminder_2);
            
            return view('home', compact('total_receivable', 'total_received', 'total_dues', 'total_invoices', 'total_sales_comission_payable', 'total_sales_comission_paid', 'total_sales_comission_dues', 'total_sell', 'total_expense', 'notices', 'reminders'));
        }
        else{
            $invoices           = Invoices::where('status', 1)->get();

            $total_receivable   = $invoices->sum('total_amount');
            $total_dues         = $invoices->sum('due_amount');
            $total_received     = $total_receivable - $total_dues;
            $total_invoices     = $invoices->count();

            $total_sales_comission_payable      = $invoices->sum('commission_amount');
            $total_sales_comission_paid         = Transactions::where('status', 1)->where('type', 'sales_comission')->sum('trans_paid');
            $total_sales_comission_dues         = $total_sales_comission_payable - $total_sales_comission_paid;

            $total_sell                         = Items::where('status', 1)->sum('sell');
            $notices                            = Notice::where('status', 1)->orderBy('created_at','DESC')->simplePaginate(5);

            $reminders                          = Reminder::where('status', 1)->orderBy('created_at','DESC')->where('date', date('Y-m-d'))->simplePaginate(10);
            
            return view('agent_home', compact('total_receivable', 'total_received', 'total_dues', 'total_invoices', 'total_sales_comission_payable', 'total_sales_comission_paid', 'total_sales_comission_dues', 'total_sell', 'notices', 'reminders'));
        }      
    }

    public function editProfile($id)
    {   
        //Users Access Level Start
        if($id != Auth::user()->id)
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $user  = Users::find($id);

        return view('edit-profile', compact('user'));
    }

    public function updateProfile(Request $request, $id)
    {   
       //Users Access Level Start
        if($id != Auth::user()->id)
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End 

        $rules = array(
            'name'  => 'required',
            );

        $validation = Validator::make(\Request::all(),$rules);
        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        DB::beginTransaction();

        try{

            $users               = Users::find($id);
            $users->name         = $request->name;

            if($request->password != null)
            {
                $users->password = Hash::make($request->password);
            }

            if($request->hasFile('photo'))
            {   
                if($users->photo != null)
                {
                    unlink('assets/images/Users/'.$users->photo);
                }

                $image              = $request->file('photo');
                $image_name         = time().'.'.$image->getClientOriginalExtension();
                $file_path          = 'assets/images/Users/';
                $rowImage           = $image;
                $savingPath         = $file_path;
                $imageName          = $image_name;
                $resizeImage        = $this->compressAndResize($rowImage, $savingPath, $imageName, 50, 250,250);
                $users->photo       = $image_name;
            }

            if($users->save())
            {
                DB::commit();
                return back()->with('message','Successfully updated !');
            }
            

        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return back()->with('message','Something went wrong.Try again !');
        }
    }

    public function editAgentProfile($id)
    {   
        //Users Access Level Start
        if($id != Auth::user()->id)
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $user  = Users::find($id);

        return view('edit-agent-profile', compact('user'));
    }

    public function updateAgentProfile(Request $request, $id)
    {   
       //Users Access Level Start
        if($id != Auth::user()->id)
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End 
        
        $rules = array(
            'name'  => 'required',
            );

        $validation = Validator::make(\Request::all(),$rules);
        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        DB::beginTransaction();

        try{

            $users               = Users::find($id);
            $users->name         = $request->name;

            if($request->password != null)
            {
                $users->password = Hash::make($request->password);
            }

            if($request->hasFile('photo'))
            {   
                if($users->photo != null)
                {
                    unlink('assets/images/Users/'.$users->photo);
                }

                $image              = $request->file('photo');
                $image_name         = time().'.'.$image->getClientOriginalExtension();
                $file_path          = 'assets/images/Users/';
                $rowImage           = $image;
                $savingPath         = $file_path;
                $imageName          = $image_name;
                $resizeImage        = $this->compressAndResize($rowImage, $savingPath, $imageName, 50, 250,250);
                $users->photo       = $image_name;
            }

            if($users->save())
            {
                DB::commit();
                return back()->with('message','Successfully updated !');
            }
            

        }catch(\Exception $e){
            DB::rollback();
            return back()->with('message','Something went wrong.Try again !');
        }
    }

    //All Functions For Notice Board Crude
    public function storeNoticeBoard(Request $request)             
    {  
        $this->validate($request,[
            'title'   => 'required',
            'file'    => 'required',
        ]);

        $notice                   = new Notice;
        $notice->title            = $request->title;
        $notice->status           = 1;
        $notice->created_by       = Auth::user()->id;

        if($request->hasFile('file'))
        {
            $image              = $request->file('file');
            $image_name         = time().'.'.$image->getClientOriginalExtension();
            $file_path          = 'assets/images/notice/';
            $image->move($file_path, $image_name);
            $notice->file_url   = $image_name;
        }

        if($notice->save())
        {
            return back()->with('message', 'Notice added successfully !!');
        }
        else
        {
           return back()->with('message', 'Something went wrong try again !!');
        }
    }

    public function deleteNoticeBoard($id)             
    {  
        $notice              = notice::find($id);
        $notice->status      = 0;
        $notice->updated_by  = Auth::user()->id;

        if($notice->save())
        {   
            return back()->with('message', 'Notice deleted successfully !!');
        }
        else
        {
           return back()->with('message', 'Something went wrong try again !!');
        }
    }

    public function downloadNoticeBoard($id)             
    {  
        $notice     = Notice::find($id);
        $file_path  = ('assets/images/notice/'.$notice->file_url);
        $name       = 'NOTICE - '.$notice->id;

        return response()->download($file_path, $name);
    }

    public function compressAndResize($source_url, $destination_url, $imageName , $quality, $desired_width, $desired_height)
    {
        $info = getimagesize($source_url);
        if ($info['mime'] == 'image/jpeg')
        {
            list($width, $height) = getimagesize($source_url);
            $src = imagecreatefromjpeg($source_url);
            $dst = imagecreatetruecolor($desired_width, $desired_height);
            imagecopyresampled($dst, $src, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
            imagejpeg($dst, $destination_url.$imageName, $quality);
        }
        elseif ($info['mime'] == 'image/png'){
            list($width, $height) = getimagesize($source_url);
            $src = imagecreatefrompng($source_url);
            $dst = imagecreatetruecolor($desired_width, $desired_height);
            imagecopyresampled($dst, $src, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
            imagejpeg($dst, $destination_url.$imageName, $quality);
        }
        elseif ($info['mime'] == 'image/gif'){
            list($width, $height) = getimagesize($source_url);
            $src = imagecreatefromgif($source_url);
            $dst = imagecreatetruecolor($desired_width, $desired_height);
            imagecopyresampled($dst, $src, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
            imagejpeg($dst, $destination_url.$imageName, $quality);
        }
        return $destination_url.$imageName;
    }
    
    public static function GetBangla($text)
    {

        $search_array= array("Sat", "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", ":", ",");

        $replace_array= array("শনিবার", "রোববার", "সোমবার", "মঙ্গলবার", "বুধবার", "বৃহস্পতিবার", "শুক্রবার", "১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০", "জানুয়ারী", "ফেব্রুয়ারী", "মার্চ", "এপ্রিল", "মে", "জুন", "জুলাই", "আগস্ট", "সেপ্টেম্বর", "অক্টোবর", "নভেম্বর", "ডিসেম্বর", ":", ",");

        $text = str_replace($search_array, $replace_array,$text);

        return $text;
    }
    
    public function done($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('home');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End
        
        $reminder            	    = Reminder::find($id);
        $reminder->reminder_status  = 1;
        $reminder->updated_by       = Auth::user()->id;

        if ($reminder->save())
        {
            return redirect()->route('home')->with('message', 'Successfully done !!');
        }else{
            return back()->with('message', 'Something went wrong !! Please try again.');
        }
    }
    
    public function later($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('home');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End
        
        $reminder            	    = Reminder::find($id);
        $reminder->reminder_status  = 2;
        $reminder->updated_by       = Auth::user()->id;

        if ($reminder->save())
        {
            return redirect()->route('home')->with('message', 'Successfully send to pending !!');
        }else{
            return back()->with('message', 'Something went wrong !! Please try again.');
        }
    }
    
    public function reminderDelete($id)
    {   
        //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('home_reminder_delete');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $reminder               = Reminder::find($id);
        $reminder->status       = 0;
        $reminder->updated_by   = Auth::user()->id;

        if ($reminder->save())
        {
            return redirect()->route('home')->with('message', 'Successfully deleted !!');
        }else{
            return back()->with('message', 'Something went wrong !! Please try again.');
        }
    }
    
    public function moneyReceipt()
    {
         //Users Access Level Start
        $helper = new \App\Helpers\AccessLevel;
        $access = $helper->test('payment_receive_create');
        if($access != 'accepted')
        {
            return back()->with('message', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End
        
        $customers      = Customers::where('status', '=', 1)->get();
        $invoices       = Invoices::where('status', '=', 1)->orderBy('created_at', 'ASC')->get();

        $payments       = Payments::join('transactions', 'transactions.payment_id', 'payments.id')
                                    ->join('invoices', 'invoices.id', 'transactions.invoice_id')
                                    ->where('payments.status', '=', 1)
                                    ->where('invoices.status', '=', 1)
                                    ->selectRaw('GROUP_CONCAT(payments.id) as id,
                                                 GROUP_CONCAT(DISTINCT payments.date) as date,
                                                 GROUP_CONCAT(invoices.invoice_number) as invoice_number,
                                                 GROUP_CONCAT(DISTINCT transactions.payment_through) as payment_through,
                                                 GROUP_CONCAT(DISTINCT payments.total_amount) as total_amount,
                                                 GROUP_CONCAT(DISTINCT payments.total_other_expense) as total_other_expense')
                                    ->groupBy('payments.id')
                                    ->orderBy('payments.date', 'DESC')
                                    ->get();

        $transactions   = Transactions::where('type', 'invoice')
                                        ->where('status', 1)
                                        ->orderBy('created_at', 'DESC')
                                        ->get();

                                        // dd($payments->toArray());

        $recevable      = round($invoices->sum('total_amount'), 2); 
        $other_expense  = round($invoices->sum('other_expense'), 2);
        $dues           = round($invoices->sum('due_amount'), 2);
        $paid           = round($transactions->sum('trans_paid'), 2);
        $invoice_count  = $invoices->count(); 

        // dd($other_expense); 

        return view('money_receipt', compact('customers', 'transactions', 'recevable', 'dues', 'paid', 'invoice_count', 'invoices', 'other_expense', 'payments'));
    }

    public function moneyReceiptShow($id)
    {   
        $payments = Payments::join('transactions', 'transactions.payment_id', 'payments.id')
                            ->join('invoices', 'invoices.id', 'transactions.invoice_id')
                            ->where('payments.id', $id)
                            ->selectRaw('GROUP_CONCAT(DISTINCT payments.id) as id,
                                         GROUP_CONCAT(DISTINCT payments.customer_id) as customer_id,
                                         GROUP_CONCAT(DISTINCT transactions.payment_through) as payment_through,
                                         GROUP_CONCAT(DISTINCT invoices.invoice_number) as invoice_number,
                                         GROUP_CONCAT(DISTINCT payments.date) as date,
                                         GROUP_CONCAT(DISTINCT payments.total_amount) as total_amount,
                                         GROUP_CONCAT(DISTINCT payments.amount_bangla) as amount_bangla')
                            ->groupBy('payments.id')
                            ->first();

        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML(view('money_receipt_show', compact('payments')));
        $mpdf->Output('Receipt', "I");
        
        //return view('paymentReceive::show');
    }
}
